# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.http import HttpResponse , HttpResponseRedirect, HttpResponseForbidden

import os,reportlab
from settings import PROJECT_DIR
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import TypeFace
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib import pdfencrypt
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import A4

from reportlab.lib import colors
from reportlab.platypus import Paragraph, Frame,Table,Image
from reportlab.lib.styles import ParagraphStyle
from reportlab.graphics.shapes import  Drawing

from regis.models import Student, Company, Assignment, WorkDetail, Department, DepartmentNameInRequestLetter, Receipt, ConfigLetter, CompanyStatusMarker
from std.models import CoOpSemester
from committee.models import CompanySelection, CoOpCommitteeContact
from commons.models import Configuration, Log
from commons.utils import to_thai_date, to_eng_date
from commons.academia import get_next_worktraining_year


MONTH_STR = {1: u'มกราคม', 2: u'กุมภาพันธ์', 3: u'มีนาคม', 4: u'เมษายน',
             5: u'พฤษภาคม', 6: u'มิถุนายน', 7: u'กรกฎาคม', 8: u'สิงหาคม',
             9: u'กันยายน', 10: u'ตุลาคม', 11: u'พฤศจิกายน', 12: u'ธันวาคม'}

BASE_FONT_FILENAME = 'THSarabun.ttf'
BASE_BOLD_FONT_FILENAME = 'THSarabunBold.ttf'
BASE_FONT_NAME = 'THSarabun'
BASE_BOLD_FONT_NAME = 'THSarabunBd'
BASE_FONT_FAMILY = 'THSarabun'
BASE_FONT_TYPEFACE = 'THSarabun'

def thai_date_string(date):
    try:
        y,m,d = [int(s) for s in  date.split('-')]
    except:
        y,m,d = date.year, date.month, date.day

    if y < 2500:
        y += 543

    return str(d) + ' ' + MONTH_STR[m] + ' ' + str(y)


def get_evaluation_qr_code_filename(student,
                                    evaluation_secret):

    from django.core.urlresolvers import reverse
    
    filename = '/tmp/evqr-'+student.student_id+'.png'
    url = ('https://wt.eng.ku.ac.th' +
           reverse("short_evaluations_edit_by_company",
                   args=[
                       student.student_id[5:],
                       evaluation_secret.secret,
                   ]))

    from os import system

    system('qr "' + url + '" > ' + filename)
    return filename


def build_sending_letter_pdf_response_one_page(pdf,
                                               student,
                                               year_no,
                                               letter_number,
                                               letter_date,
                                               signer_name,
                                               letter_beginning_date,
                                               letter_end_date,
                                               eval_end_date,
                                               is_student_copy=False,
                                               show_signature=False,
                                               evaluation_secret=None,
                                               company_name=None):
    students = [student]
    evaluation_secrets = [evaluation_secret]
    
    th_startdate = thai_date_string(letter_beginning_date)
    th_enddate = thai_date_string(letter_end_date)
    th_letterdate = thai_date_string(letter_date)
    th_eval_enddate = thai_date_string(eval_end_date)
    signature_name = Configuration.objects.get(name="default.letter_signature").value
    letter_number = u'ที่ ' + letter_number
    
    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,BASE_FONT_FILENAME)
    THNiramitBd_font = os.path.join(font_path,BASE_BOLD_FONT_FILENAME)

    pdfmetrics.registerFont(TTFont(BASE_FONT_NAME, THNiramit_font))
    pdfmetrics.registerFont(TTFont(BASE_BOLD_FONT_NAME, THNiramitBd_font))
    pdfmetrics.registerFontFamily(BASE_FONT_FAMILY,normal=BASE_FONT_NAME,bold=BASE_BOLD_FONT_NAME)
    pdfmetrics.registerTypeFace(TypeFace(BASE_FONT_TYPEFACE))

    styleN = ParagraphStyle(name='normal', fontSize=15,fontName=BASE_FONT_NAME,leading=18)
    styleNS = ParagraphStyle(name='normal', fontSize=14,fontName=BASE_FONT_NAME,leading=18)
    styleNSsmall = ParagraphStyle(name='normal', fontSize=12,fontName=BASE_FONT_NAME,leading=18,spaceBefore=0)
    styleP = ParagraphStyle(name='normal', fontSize=15,fontName=BASE_FONT_NAME,leading=18,firstLineIndent=40,spaceBefore=10)
    stylePS = ParagraphStyle(name='normal', fontSize=15,fontName=BASE_FONT_NAME,leading=18,firstLineIndent=40,spaceBefore=0)
    stylePH = ParagraphStyle(name='normal', fontSize=15,fontName=BASE_FONT_NAME,leading=18,spaceBefore=10)
    styleSIGN = ParagraphStyle(name='normal',fontSize=15,fontName=BASE_FONT_NAME,leading=8,leftIndent=40,alignment=1,spaceBefore=0)
    styleTable = ParagraphStyle(name='normal', fontSize=15,fontName=BASE_FONT_NAME,alignment=1)
    
    tra_krut =  os.path.join(PROJECT_DIR, 'private_media/image/krut.jpg')
    sign = os.path.join(PROJECT_DIR,'private_media/image/signature.jpg')
    signature_img = Image(sign)
    signature_img.drawHeight = 0.4*signature_img.drawHeight
    signature_img.drawWidth = 0.4*signature_img.drawWidth
    
    content = []

    pdf.setFont(BASE_FONT_NAME,15)
    pdf.drawImage(tra_krut,3.5*inch,10.25*inch,1.1*inch,1.1*inch)
    pdf.drawString(1*inch,10.35*inch,letter_number)
    if is_student_copy:
        pdf.drawString(6.5*inch,11*inch,u'(สำเนาสำหรับนิสิต)')
    address = Frame(5*inch, 9.45*inch, 2.8*inch, 1.2*inch,showBoundary=0)
    tmp = u"คณะวิศวกรรมศาสตร์<br/>มหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตบางเขน<br/>ตู้ ป.ณ.1032 ปทฝ. เกษตรศาสตร์ กรุงเทพมหานคร 10903"
    address.addFromList([Paragraph(tmp,styleN)],pdf)
    pdf.drawString(4*inch,9.25*inch,th_letterdate)
    content.append(Paragraph(u"เรื่อง ส่งรายชื่อนิสิตเข้าฝึกงานและประเมินผลออนไลน์",stylePH))
    if company_name:
        content.append(Paragraph(u"เรียน "+signer_name+" &nbsp;&nbsp;"+company_name,stylePH))
    else:
        content.append(Paragraph(u"เรียน "+signer_name,stylePH))
    # content.append(Paragraph(u'สิ่งที่ส่งมาด้วย  แบบตอบรับยืนยันการรับนิสิตเข้าฝึกงาน',stylePH))
    # old message-------------
    content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ ขอขอบคุณที่ท่านให้ความอนุเคราะห์ในการรับนิสิตเข้าฝึกงาน โดยนิสิตต้องเข้ารับการฝึกงานระหว่างวันที่ <u>"+th_startdate+u" ถึง "+th_enddate+u"</u> และรวมเวลาฝึกงานไม่น้อยกว่า 240 ชั่วโมง และจำนวนวันไม่น้อยกว่า 30 วันทำการ จำนวน "+str(len(students))+u" คน มีรายชื่อดังต่อไปนี้<br/>",styleP))
    
    #content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ ขอขอบคุณที่ท่านให้ความร่วมมือในการรับนิสิต เข้าฝึกงาน ซึ่งคณะฯ กำหนดให้นิสิตเข้าฝึกงานภาคฤดูร้อน พ.ศ. 2555 ดังนี้<br/>", styleP))

    #content.append(Paragraph(u"-<u>บริษัทเอกชน/หน่วยงานที่ฝึกงานวันละ 8 ชั่วโมง</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>ฝึกงานระหว่างวันที่ 18 เมษายน – 8  มิถุนายน 2555</u><br/>", stylePS))
    #content.append(Paragraph(u"-<u>หน่วยงานราชการ/หน่วยงานที่ฝึกงานวันละ 7 ชั่วโมง</u> <u>ฝึกงานระหว่างวันที่ 18 เมษายน– 15 มิถุนายน 2555</u><br/>", stylePS))

    #content.append(Paragraph(u"ทั้งนี้นิสิตต้องฝึกงานไม่ต่ำกว่า 30 วันทำการและไม่น้อยกว่า 240 ชั่วโมง (ไม่นับเวลาพักกลางวัน และหลังเวลาเลิกงาน)  โดยมีรายชื่อนิสิตเข้าฝึกงาน จำนวน "+str(len(students))+u" คน<br/>",stylePH))
    
    colWidths = (10, 100, None, 40, None,None)
    ts = [('FONT', (0,0), (-1,-1), BASE_FONT_NAME),
          ('FONTSIZE', (0,0), (-1,-1), 15),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ]
    
    max_student_per_page = 21
    counter = 0
    number = 1
    page = 1

    major_header = u'สาขาวิชาวิศวกรรม'
    if students[0].department.id in [17]:
        major_header = u'สาขาวิชา'
    
    tmp = [['',u'เลขประจำตัวนิสิต',u'ชื่อ-นามสกุล',u'ชั้นปีที่', major_header]]
    for student in students:
        if counter == 0:
            tmp = [['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',styleTable),
                    Paragraph(u'<u>ชื่อ-นามสกุล</u>',styleTable),
                    Paragraph(u'<u>ชั้นปีที่</u>',styleTable),
                    Paragraph(u'<u>' + major_header + u'</u>',styleTable),
                    Paragraph(u'<u>เบอร์โทรศัพท์</u>',styleTable),
                    ]]
        if student.prefix == u"นางสาว":
            prefix = u"น.ส."
        else:
            prefix = student.prefix
        
        
        if student.department.short_name_for_letters:
            department_name = student.department.short_name_for_letters
        else:
            department_name = student.department.name

        if department_name.startswith(u'วิศวกรรม'):
            department_name = department_name[len(u'วิศวกรรม'):]
            
        tmp.append([number,student.student_id,prefix+student.full_name(),student.get_year(),department_name,student.tel_no])
        counter += 1
        if counter == max_student_per_page:
            table = Table(tmp, style=ts,colWidths=colWidths)
            content.append(table)
            if page == 1:
                f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 8.65*inch, showBoundary=0)
            else:
                f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 9.8*inch, showBoundary=0)
            f.addFromList(content,pdf)
            pdf.showPage()
            content = []
            tmp = []
            page += 1
            pdf.setFont(BASE_FONT_NAME,16)
            pdf.drawString(4*inch,10.51*inch,'-'+str(page)+'-')
            counter = 0
            max_student_per_page = 32
        number += 1
        
    if len(tmp) != 0:
        table = Table(tmp, style=ts,colWidths=colWidths)
        content.append(table)
        content.append(Paragraph(u' ',styleP))

    content.append(Paragraph(u"ทั้งนี้ เมื่อนิสิตฝึกงานเรียบร้อยแล้ว ขอความอนุเคราะห์หัวหน้างาน หรือผู้ควบคุมการฝึกงาน ดำเนินการประเมินผล ฝึกงานโดยสแกน QR Code หรือ Link เข้าไปที่แบบประเมินผลจากด้านล่างหนังสือฉบับนี้ ซึ่งคณะใส่ซองปิดผนึก (ลับ) เพื่อให้ผู้ควบคุมการฝึกงาน ดำเนินการประเมินผลการฝึกงาน ให้แก่นิสิตทางออนไลน์ <u>ภายในวันที่ "+th_eval_enddate+u"</u>",stylePS))
    content.append(Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณาดำเนินการภายในกำหนดด้วย จะขอบคุณยิ่ง มิฉะนั้นนิสิตจะไม่ผ่านการฝึกงาน",styleP))

    content.append(Paragraph(u"<br/>ขอแสดงความนับถือ<br/><br/><br/><br/>",styleSIGN))

    if show_signature:
        content.append(Paragraph('<img src="'+sign+'" width='+str(signature_img.drawWidth)+' height='+str(signature_img.drawHeight)+' />',styleSIGN))
    else:
        content.append(Paragraph(u"<br/>",styleSIGN))
    
    content.append(Paragraph(u"<br/><br/>("+signature_name+u")<br/><br/>รองคณบดีฝ่ายกิจการนิสิตและกิจการพิเศษ",styleSIGN))

    tel = Frame(0.9*inch, 0.25*inch, 8.25*inch, 1.2*inch, showBoundary=0)
    tmp = u"คณะวิศวกรรมศาสตร์ มก.<br/>โทรศัพท์ 0 2579 5897, 0 2797 0969<br/>โทรสาร 0 2579 5897 (คุณนบชุลีและคุณโบรณี ผู้ประสานงาน)<br/><u>หมายเหตุ</u> เพื่อความปลอดภัยการประเมินผลทำได้ครั้งเดียว หากท่านต้องการแก้ไข กรุณาติดต่อคุณนบชุลีหรือคุณโบรณี (E-mail: sa.eng@ku.th)"
    tel.addFromList([Paragraph(tmp,styleNSsmall)],pdf)
    if page == 1:
        f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 8.65*inch, showBoundary=0)
    else:
        f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 9.8*inch, showBoundary=0)
    f.addFromList(content,pdf)

    if evaluation_secrets != None:
        for student, evaluation_secret in zip(students, evaluation_secrets):
            if evaluation_secret == None:
                continue
            row_bottom = 1.35*inch
            code_label_frame = Frame(2.1*inch, row_bottom, 8.25*inch, 1.3*inch, showBoundary=0)

            code_content = []
            code_content.append(Paragraph(u'<u>ข้อมูลสำหรับส่งแบบประเมินผล</u> (เปิดระบบก่อนนิสิตฝึกงานเสร็จเรียบร้อย 2 สัปดาห์)<br/>' +
                                          u'Website URL: https://wt.eng.ku.ac.th/e/<br/>' +
                                          u'Student code: ' + student.student_id[5:] + '<br/>' +
                                          u'Password: ' + evaluation_secret.easy_secret,
                                          stylePH))
        
            code_label_frame.addFromList(code_content, pdf)

            qr_filename = get_evaluation_qr_code_filename(student, 
                                                          evaluation_secret)
            #qr_filenames.append(qr_filename)
            pdf.drawImage(qr_filename,
                          0.9*inch, row_bottom,
                          1.25*inch, 1.25*inch)
        

def build_sending_letter_pdf_response(students,
                                      year_no,
                                      letter_number,
                                      letter_date,
                                      signer_name,
                                      letter_beginning_date,
                                      letter_end_date,
                                      eval_end_date,
                                      is_student_copy=False,
                                      show_signature=False,
                                      evaluation_secrets=None,
                                      company_name=None):

    if evaluation_secrets == None:
        evaluation_secrets = [None] * len(students)
        
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
    enc.setAllPermissions(0)
    enc.canPrint = 1

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=internship_letter.pdf'
    
    pdf = Canvas(response,encrypt=enc)
    for student, evaluation_secret in zip(students, evaluation_secrets):
        build_sending_letter_pdf_response_one_page(pdf,
                                                   student,
                                                   year_no,
                                                   letter_number,
                                                   letter_date,
                                                   signer_name,
                                                   letter_beginning_date,
                                                   letter_end_date,
                                                   eval_end_date,
                                                   is_student_copy,
                                                   show_signature,
                                                   evaluation_secret,
                                                   company_name)
        pdf.showPage()

    pdf.save()
    return response


def print_letter_pdf(company_id, 
                     year_no,
                     letter_number,
                     letter_date,
                     signer_name,
                     letter_beginning_date,
                     letter_end_date,
                     eval_end_date,
                     is_student_copy=False,
                     show_signature=False,
                     show_secret=False):

    company = get_object_or_404(Company,pk=company_id)
    assignments = company.assignments.filter(year=year_no).select_related('student')
    students = [ a.student for a in assignments ]

    if show_secret:
        from evaluations.models import EvaluationSecret
        evaluation_secrets = [EvaluationSecret.get_or_create_for_student(student,
                                                                         year_no)
                              for student in students]
    else:
        evaluation_secrets = None
        
    return build_sending_letter_pdf_response(students,
                                             year_no,
                                             letter_number,
                                             letter_date,
                                             signer_name,
                                             letter_beginning_date,
                                             letter_end_date,
                                             eval_end_date,
                                             is_student_copy,
                                             show_signature,
                                             evaluation_secrets=evaluation_secrets)


def build_request_letter_pdf_response(companies, all_department_names, send_to_all):
    month = {'01':u'มกราคม','02':u'กุมภาพันธ์','03':u'มีนาคม','04':u'เมษายน','05':u'พฤษภาคม','06':u'มิถุนายน','07':u'กรกฎาคม','08':u'สิงหาคม','09':u'กันยายน','10':u'ตุลาคม','11':u'พฤศจิกายน','12':u'ธันวาคม'}
    
    sy,sm,sd = Configuration.objects.get(name="default.letter_beginning_date").value.split('-')
    th_startdate = str(int(sd)) + ' ' + month[sm]
    ey,em,ed = Configuration.objects.get(name="default.letter_end_date").value.split('-')
    th_enddate = str(int(ed)) + ' ' + month[em] + ' ' + str(int(ey) + 543)
    ly,lm,ld = Configuration.objects.get(name="default.letter_date").value.split('-')
    th_letterdate = str(int(ld)) + ' ' + month[lm] + ' ' + str(int(ly) + 543)
    signature_name = Configuration.objects.get(name="default.letter_signature").value
    letter_number = u'ที่ ' + Configuration.objects.get(name="default.letter_number").value
    ry,rm,rd = Configuration.objects.get(name="default.letter_receipt_deadline").value.split('-')
    receipt_deadline = str(int(rd)) + ' ' + month[rm] + ' ' + str(int(ry) + 543)

    #create pdf letter
    #TODO change password for encrypt
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
    enc.setAllPermissions(0)
    enc.canPrint = 1

    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,'THNiramit.ttf')
    THNiramitBd_font = os.path.join(font_path,'THNiramitBold.ttf')

    pdfmetrics.registerFont(TTFont('THNiramit', THNiramit_font))
    pdfmetrics.registerFont(TTFont('THNiramitBd', THNiramitBd_font))
    pdfmetrics.registerFontFamily('THNiramit',normal='THNiramit',bold='THNiramitBd')
    pdfmetrics.registerTypeFace(TypeFace('THNiramit'))

    styleN = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15)
    styleP = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,firstLineIndent=40,spaceBefore=10)
    stylePN = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,firstLineIndent=40)
    stylePH = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,spaceBefore=10)
    stylePHN = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,spaceBefore=0)
    styleSIGN = ParagraphStyle(name='normal',fontSize=14,fontName='THNiramit',leading=8,leftIndent=30,alignment=1,spaceBefore=0)
    tra_krut =  os.path.join(PROJECT_DIR, 'private_media/image/krut.jpg')
    sign = os.path.join(PROJECT_DIR,'private_media/image/signature.jpg')
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=internship_all_aid_letter.pdf'

    signature_img = Image(sign)
    signature_img.drawHeight = 0.4*signature_img.drawHeight
    signature_img.drawWidth = 0.4*signature_img.drawWidth

    year = get_next_worktraining_year()

    content = []
    
    company_number = 1

    pdf = Canvas(response,encrypt=enc)
    for company in companies:
        pdf.setFont('THNiramit',14)
        pdf.drawImage(tra_krut,3.5*inch,10.25*inch,1.1*inch,1.1*inch)
        pdf.drawString(0.75*inch,10.25*inch,letter_number)
        address = Frame(5*inch, 9.3*inch, 2.8*inch, 1.2*inch,showBoundary=0)
        tmp = u"คณะวิศวกรรมศาสตร์<br/>มหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตบางเขน<br/>ตู้ ป.ณ.1032 ปทฝ. เกษตรศาสตร์ กรุงเทพมหานคร 10903"
        address.addFromList([Paragraph(tmp,styleN)],pdf)
        pdf.drawString(4*inch,9.35*inch,th_letterdate)
        content.append(Paragraph(u"เรื่อง ขอความร่วมมือในการรับนิสิตเข้าฝึกงาน ประจำปี พ.ศ. " + str(year),stylePH))
        
        if company.signer_name and company.signer_name != '':
            signer_name = company.signer_name
        else:
            signer_name = u'ผู้จัดการแผนกทรัพยากรมนุษย์'        
        
        content.append(Paragraph(u"เรียน " + signer_name + " " + company.name,stylePH))
        content.append(Paragraph(u'สิ่งที่ส่งมาด้วย 1. ความสามารถของนิสิตฝึกงาน   2. หนังสือตอบรับ  3. แบบฟอร์มรายละเอียดในการฝึกงาน',stylePH))
        
        f = Frame(0.6*inch, 0.5*inch, 7.2*inch, 8.8*inch, showBoundary=0)
    
        content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ เป็นสถาบันการศึกษาที่ผลิตวิศวกรสาขาต่าง ๆ เพื่อสนองความ<br/>ต้องการของประเทศ ด้วยสิ่งที่มหาวิทยาลัยฯ สามารถประสิทธิ์ประสาทความรู้ให้นิสิตได้คือความรู้ทางภาคทฤษฎี ส่วนความ<br/>ชำนาญงานและประสบการณ์ มหาวิทยาลัยฯ ไม่อาจให้กับนิสิตได้อย่างเต็มที่ และเนื่องจากความชำนาญและประสบการณ์มี<br/>ความสำคัญยิ่งเช่นกัน  ดังนั้นหลักสูตรวิศวกรรมศาสตรบัณฑิตของคณะวิศวกรรมศาสตร์ กำหนดให้นิสิตทุกคนต้องผ่านการ<br/>ฝึกงานด้วย",styleP))
        content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ ได้พิจารณาแล้วเห็นว่าหน่วยงานของท่านเป็นหน่วยงานที่สามารถจะช่วยมหาวิทยาลัยฯ <br/>ผลิตวิศวกรที่มีคุณภาพไปรับใช้สังคมได้ โดยในปีการศึกษา 2564 แบ่งการฝึกงานเป็น 2 ช่วง ดังนี้",styleP))
        content.append(Paragraph(u"<b>1. นิสิตภาคปกติและภาคพิเศษ ระหว่างวันที่ "+th_startdate+u" - "+th_enddate + u"</b>", stylePN))
        content.append(Paragraph(u"<b>2. นิสิตโครงการเปิดสอนหลักสูตรนานาชาติ ระหว่างวันที่ 1 มิถุนายน - 29 กรกฎาคม 2565</b>", stylePN))
        content.append(Paragraph(u"จึงขอความอนุเคราะห์จากท่านเพื่อพิจารณารับนิสิตเข้าฝึกงานตามหลักสูตรในสาขาวิชาดังต่อไปนี้",stylePHN))
        
        
        #Get all department that selected this company
        if not send_to_all:
            dept_selections = CompanySelection.objects.filter(company=company,year=year).all() 
            department_names = DepartmentNameInRequestLetter.clean_dupplicate_names(
                [sel.department.name for sel in dept_selections])
        else:
            department_names = all_department_names

        #for selection in selections:
            #content.append(Paragraph(selection.department.name,styleP))
        txtleft=[]
        txtright=[]
        row =[]
        for i in range(0,len(department_names)):
            if i%2==0:               
                txtleft.append(str(i+1)+". " + department_names[i])
            else:
                txtright.append(str(i+1)+". " + department_names[i])
        for i in range(0,len(txtleft)):
            if len(txtleft) == len(txtright):
                row.append((txtleft[i],txtright[i]))
            else:
                if i <= len(txtright)-1:
                    row.append((txtleft[i],txtright[i]))
                else:
                    row.append((txtleft[i],''))
                            
        ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
                ('FONTSIZE', (0,0), (-1,-1), 14)]    
        t = Table(row , style=ts)
        content.append(t)    
                
        content.append(Paragraph(u"โดยระบุสถานที่ฝึกงาน ลักษณะงาน  และเจ้าหน้าที่ที่สามารถติดต่อได้โดยตรงลงในใบตอบรับที่แนบมาพร้อมนี้  และส่งแฟ็กซ์<br/>ตอบรับมายังหน่วยกิจการนิสิต งานบริการการศึกษา คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ <b><u>ภายในวันที่<br/>" + receipt_deadline + u"</u></b> ด้วย จักขอบคุณยิ่ง",stylePH))
        content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ขอขอบคุณในความร่วมมือมา  ณ โอกาสนี้ อนึ่งสำหรับ<br/>ค่าตอบแทนในการฝึกงานของนิสิตนั้น เนื่องจากคณะฯ ได้ตระหนักถึงสภาวะเศรษฐกิจในปัจจุบัน จึงใคร่ขอให้อยู่ใน<br/> ดุลยพินิจของบริษัท/หน่วยงาน ตามแต่จะเห็นสมควร", styleP))
        content.append(Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณา", styleP))
        
        tmp = u"("+signature_name+")"
        content.append(Paragraph(u"<br/>ขอแสดงความนับถือ<br/><br/><br/><br/><br/>",styleSIGN))

        #content.append(Paragraph(u"<br/><br/>",styleSIGN))
        content.append(Paragraph('<img src="'+sign+'" width='+str(signature_img.drawWidth)+' height='+str(signature_img.drawHeight)+' /><br/><br/>',styleSIGN))
        
        content.append(Paragraph(tmp+u"<br/><br/>รองคณบดีฝ่ายกิจการนิสิตและกิจการพิเศษ",styleSIGN))
    
        tel = Frame(0.7*inch, 0.2*inch, 4.25*inch, 0.9*inch, showBoundary=0)
        tmp = u"คณะวิศวกรรมศาสตร์<br/>โทรศัพท์ 0 2797 0969 &nbsp;&nbsp; โทรสาร 0 2579 5897<br/>Email: sa.eng@ku.th"
        tel.addFromList([Paragraph(tmp,styleN)],pdf)
        

        company_number_frame = Frame(7.5*inch,0.2*inch,0.5*inch,0.5*inch, showBoundary=0)
        company_number_frame.addFromList([Paragraph(str(company_number),styleN)],pdf)
            
        f.addFromList(content,pdf)
        pdf.showPage()
        company_number += 1
    pdf.save()
    return response  


def build_label_pdf_response(companies, file_name, print_number=False,
                             xoffset=None, yoffset=None, rowwidth=None):
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="passwordlabelaidletter")
    enc.setAllPermissions(0)
    enc.canPrint = 1
    
    # Set font
    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,'THNiramit.ttf')
    pdfmetrics.registerFont(TTFont('THNiramit', THNiramit_font))
    pdfmetrics.registerTypeFace(TypeFace('THNiramit'))
    
    # Set style for Paragraph
    stylePH = ParagraphStyle(name='normal', fontSize=16,fontName='THNiramit',leading=15,leftIndent=10,spaceBefore=0)
    styleAd = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,leftIndent=10,spaceBefore=5)
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=' + file_name

    pdf = Canvas(response, encrypt=enc)
    pdf.setFont('THNiramit',16)
    
    # initial counter for printing label
    loop_counter = 0
    
    # Set frame position
    frame_X = (0,4.13)
    frame_Y = (10.14,8.72,7.30,5.88,4.45,3.03-.09,1.61-.18,0.18-.27)

    if not xoffset:
        xoffset = 0
    xoffset *= cm
    if not yoffset:
        yoffset = 0
    yoffset *= cm
    
    company_number = 1
    for company in companies:

        row = loop_counter/2
        col = loop_counter%2

        if not rowwidth:
            label = Frame(frame_X[col]*inch + xoffset, 
                          frame_Y[row]*inch - yoffset, 
                          4.0*inch, 
                          1.42*inch,
                          showBoundary=0)
        else:
            base_y = frame_Y[0]*inch - row * rowwidth * cm
            label = Frame(frame_X[col]*inch + xoffset, 
                          base_y - yoffset, 
                          4.0*inch, 
                          1.42*inch,
                          showBoundary=0)

        if print_number:
            company_number_frame = Frame((frame_X[col]+3.35)*inch,frame_Y[row]*inch,0.5*inch,0.5*inch,showBoundary=0)
            company_number_frame.addFromList([Paragraph(str(company_number),styleAd)],pdf)

        if company:
            if company.signer_name and company.signer_name != '':
                signer_name = company.signer_name
            else:
                signer_name = u'ผู้จัดการแผนกทรัพยากรมนุษย์'        
        
            content = []
            content.append(Paragraph(u'เรียน ' + signer_name + '<br/>'+company.name,stylePH))
            content.append(Paragraph(company.address,styleAd))
        else:
            content = []

        label.addFromList(content, pdf)
        
        loop_counter += 1
        
        if loop_counter == 16:
            pdf.showPage()
            pdf.setFont('THNiramit',16)
            loop_counter = 0
            
        company_number += 1
    pdf.save()
    return response


def build_student_request_letter_pdf_response(company_request, students, 
                                              company_name=None,
                                              year=None,
                                              signer_name=None,
                                              only_request_letter=False):
    department = students[0].department
    
    if company_request==None or company_request.is_default_schedule:
        th_startdate = to_thai_date(Configuration.get('default.letter_beginning_date'))
        th_enddate = to_thai_date(Configuration.get('default.letter_end_date'))
    else:
        th_startdate = to_thai_date(company_request.beginning_date)
        th_enddate = to_thai_date(company_request.end_date)

    th_letterdate = to_thai_date(Configuration.get(name="default.letter_date"))

    signature_name = Configuration.get("default.letter_signature")
    letter_number = u'ที่ ' + Configuration.get("default.letter_number")

    from commons.models import convert_config_str_to_date
    receipt_deadline = to_thai_date(convert_config_str_to_date(Configuration.get('default.letter_receipt_deadline')))

    #create pdf letter
    #TODO change password for encrypt
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
    enc.setAllPermissions(0)
    enc.canPrint = 1

    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,'THNiramit.ttf')
    THNiramitBd_font = os.path.join(font_path,'THNiramitBold.ttf')

    pdfmetrics.registerFont(TTFont('THNiramit', THNiramit_font))
    pdfmetrics.registerFont(TTFont('THNiramitBd', THNiramitBd_font))
    pdfmetrics.registerFontFamily('THNiramit',normal='THNiramit',bold='THNiramitBd')
    pdfmetrics.registerTypeFace(TypeFace('THNiramit'))

    styleN = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15)
    styleNWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,wordWrap='CJK')
    styleP = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,firstLineIndent=40,spaceBefore=10)
    stylePWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,firstLineIndent=40,spaceBefore=10,wordWrap='CJK')
    stylePL = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=25,firstLineIndent=40,spaceBefore=10)
    stylePH = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,spaceBefore=10)
    stylePHWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,spaceBefore=10,wordWrap='CJK')
    stylePHL = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=25,spaceBefore=10)
    stylePHLWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=25,spaceBefore=10,wordWrap='CJK')
    styleMid = ParagraphStyle(name='normal',fontSize=14,fontName='THNiramit',leading=15,leftIndent=0,alignment=1,spaceBefore=0)
    styleSIGN = ParagraphStyle(name='normal',fontSize=14,fontName='THNiramit',leading=8,leftIndent=150,alignment=1,spaceBefore=0)
    styleTable = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',alignment=1)
    styleTableL = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit')
    tra_krut =  os.path.join(PROJECT_DIR, 'private_media/image/krut.jpg')
    sign = os.path.join(PROJECT_DIR,'private_media/image/signature.jpg')
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=company_letter.pdf'

    signature_img = Image(sign)
    signature_img.drawHeight = 0.4*signature_img.drawHeight
    signature_img.drawWidth = 0.4*signature_img.drawWidth

    content = []
    
    company_number = 1

    if not year:
        if company_request:
            year = company_request.year
        else:
            year = ''

    pdf = Canvas(response,encrypt=enc,pagesize=A4)
    pdf.setFont('THNiramit',14)
    pdf.drawImage(tra_krut,3.5*inch,10.25*inch,1.1*inch,1.1*inch)
    pdf.drawString(0.75*inch,10.25*inch,letter_number)
    address = Frame(5*inch, 9.3*inch, 2.8*inch, 1.2*inch,showBoundary=0)
    tmp = u"คณะวิศวกรรมศาสตร์<br/>มหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตบางเขน<br/>ตู้ ป.ณ.1032 ปทฝ. เกษตรศาสตร์ กรุงเทพมหานคร 10903"
    address.addFromList([Paragraph(tmp,styleN)],pdf)
    pdf.drawString(4*inch,9.25*inch,th_letterdate)
    content.append(Paragraph(u"เรื่อง ขอความอนุเคราะห์ในการรับนิสิตเข้าฝึกงาน ประจำปี พ.ศ." + str(year),stylePH))

    if signer_name==None:
        signer_name = company_request.signer_name
    if company_name==None:
        company_name = company_request.company_name
        
    content.append(Paragraph(u"เรียน " + signer_name + " " + company_name,stylePH))

    if not only_request_letter:
        content.append(Paragraph(u'สิ่งที่ส่งมาด้วย 1. หนังสือตอบรับ  2. แบบฟอร์มรายละเอียดในการฝึกงาน',stylePH))
        
    f = Frame(0.6*inch, 0.5*inch, 7.2*inch, 8.8*inch, showBoundary=0)
    
    #content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ เป็นสถาบันการศึกษาที่ผลิตวิศวกรสาขาต่าง ๆ เพื่อสนองความ<br/>ต้องการของประเทศ ด้วยสิ่งที่มหาวิทยาลัยฯ สามารถประสิทธิ์ประสาทความรู้ให้นิสิตได้คือความรู้ทางภาคทฤษฎี ส่วนความ<br/>ชำนาญงานและประสบการณ์ มหาวิทยาลัยฯ ไม่อาจให้กับนิสิตได้อย่างเต็มที่ และเนื่องจากความชำนาญและประสบการณ์มีความ<br/>สำคัญยิ่งเช่นกัน  ดังนั้นหลักสูตรวิศวกรรมศาสตรบัณฑิตของคณะวิศวกรรมศาสตร์ กำหนดให้นิสิตทุกคนต้องผ่านการฝึกงานด้วย",stylePWrap))
    content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ มีความมุ่งมั่นที่จะผลิตบัณฑิตที่มีคุณธรรม จริยธรรม และตอบสนอง ความต้องการของสังคม คณะให้ความสำคัญกับการเรียนรู้งานผ่านการปฏิบัติจริงและประสบการณ์ในการทำงาน จึงได้กำหนดให้ นิสิตในหลักสูตรวิศวกรรมศาสตรบัณฑิตทุกหลักสูตรของคณะต้องผ่านการฝึกงาน",stylePWrap))

    # TODO: fix this hack
    department_name = department.name
    if department.id == 12:
        department_name = u'วิศวกรรมสำรวจและ สารสนเทศภูมิศาสตร์'
    content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ ได้พิจารณาแล้วเห็นว่าหน่วยงานของท่านเป็นหน่วยงานที่สามารถจะช่วยมหาวิทยาลัยฯ <br/>ผลิตวิศวกรที่มีคุณภาพไปรับใช้สังคมได้  จึงขอความอนุเคราะห์จากท่านเพื่อพิจารณารับนิสิตสาขาวิชา<u>" + department_name + u" จำนวน %d คน</u> ได้แก่" % len(students),stylePWrap))

    colWidths = (10, 120, 220, 40)
    ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
          ('FONTSIZE', (0,0), (-1,-1), 14),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ]

    max_student_per_page = 20
    counter = 0
    number = 1
    page = 1
    for student in students:
        if counter == 0:
            tmp = [['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',styleTable),
                    Paragraph(u'<u>ชื่อ-นามสกุล</u>',styleTableL),
                    Paragraph(u'<u>ชั้นปีที่</u>',styleTable),
                    ]]
        if student.prefix == u"นางสาว":
            prefix = u"น.ส."
        else:
            prefix = student.prefix
        
        tmp.append([number,student.student_id,prefix+student.full_name(),student.get_year()])
        counter += 1
        if counter == max_student_per_page:
            table = Table(tmp, style=ts,colWidths=colWidths)
            content.append(table)
            f.addFromList(content,pdf)
            pdf.showPage()
            f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 9.8*inch, showBoundary=0)
            content = []
            tmp = []
            page += 1
            pdf.setFont('THNiramit',16)
            pdf.drawString(4*inch,10.51*inch,'-'+str(page)+'-')
            counter = 0
            max_student_per_page = 32
        number += 1

    if ((page == 1 and counter > 9) or
        (counter > 24)):
            table = Table(tmp, style=ts,colWidths=colWidths)
            content.append(table)
            f.addFromList(content,pdf)
            pdf.showPage()        
            f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 9.8*inch, showBoundary=0)
            content = []
            tmp = []
            page += 1
            pdf.setFont('THNiramit',16)
            pdf.drawString(4*inch,10.51*inch,'-'+str(page)+'-')

    if len(tmp) != 0:
        table = Table(tmp, style=ts,colWidths=colWidths)
        content.append(table)
        content.append(Paragraph(u' ',styleP))

    if company_request!=None:
        content.append(Paragraph(u"เพื่อเข้าฝึกงานในระหว่างปิดภาคการศึกษา <u>("+th_startdate+u" - "+th_enddate+u")</u>   โดยระบุสถานที่ฝึกงาน ลักษณะงาน  และเจ้าหน้าที่ที่สามารถติดต่อได้โดยตรงลงในใบตอบรับที่แนบมาพร้อมนี้  <b>และส่งอีเมลตอบรับมายังหน่วยกิจการนิสิต งานบริการการศึกษา คณะวิศวกรรมศาสตร์  มหาวิทยาลัยเกษตรศาสตร์</b>",stylePH))
    else:
        content.append(Paragraph(u"เพื่อเข้าฝึกงานในระหว่างปิดภาคการศึกษา ระหว่างวันที่ <u>"+th_startdate+u" ถึง "+th_enddate+u"</u> หรือตามแต่หน่วยงานกำหนด (อย่างไรก็ตามช่วงเวลาการฝึกงานจะต้องเป็นช่วงเวลา 2 เดือนเต็ม โดยขอให้ระบุก่อนวันที่นิสิตไปฝึกงาน มิฉะนั้นจะถือว่านิสิต ต้องฝึกงานตามที่คณะฯ กำหนด ซึ่งในช่วง 2 เดือนนั้น ต้องมีระยะเวลาการฝึกงานไม่น้อยกว่า  240  ชั่วโมง และไม่น้อยกว่า 30 วันทำการ ตามระเบียบเกี่ยวกับการฝึกงานของคณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ หากนิสิตมีจำนวนวัน และเวลาฝึกงาน ไม่ครบทั้ง 3 ส่วน จะไม่ผ่านการฝึกงาน)",stylePH))

    content.append(Paragraph(u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ขอขอบคุณในความร่วมมือมา  ณ โอกาสนี้ อนึ่งสำหรับ<br/>ค่าตอบแทนในการฝึกงานของนิสิตนั้น ขอให้อยู่ในดุลยพินิจของบริษัท/หน่วยงาน ตามแต่จะเห็นสมควร",styleP))
    content.append(Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณา", styleP))
        
    tmp = u"("+signature_name+")"
    content.append(Paragraph(u"<br/>ขอแสดงความนับถือ<br/><br/><br/><br/><br/>",styleSIGN))

    content.append(Paragraph('<img src="'+sign+'" width='+str(signature_img.drawWidth)+' height='+str(signature_img.drawHeight)+' /><br/><br/>',styleSIGN))
        
    content.append(Paragraph(tmp+u"<br/><br/>รองคณบดีฝ่ายกิจการนิสิตและกิจการพิเศษ",styleSIGN))
    
    tel = Frame(0.7*inch, 0.2*inch, 4.25*inch, 0.9*inch, showBoundary=0)
    tmp = u"คณะวิศวกรรมศาสตร์<br/>โทรศัพท์ 0 2797 0921 โทรสาร 0 2579 5897<br/>Email: sa.eng@ku.th"
    tel.addFromList([Paragraph(tmp,styleN)],pdf)
        

    company_number_frame = Frame(7.5*inch,0.2*inch,0.5*inch,0.5*inch, showBoundary=0)
    company_number_frame.addFromList([Paragraph(str(company_number),styleN)],pdf)
            
    f.addFromList(content,pdf)
    pdf.showPage()

    if only_request_letter:
        pdf.save()
        return response  
        

    # ---------- acceptance form

    title_frame = Frame(1.5*inch, 10.5*inch, 5.27*inch, 0.5*inch,showBoundary=0)
    title_frame.addFromList([Paragraph(u'<u>แบบตอบรับการฝึกงาน</u>',styleMid)],pdf)
    pdf.setFont('THNiramit',14)
    pdf.drawString(0.75*inch,10.3*inch,u'ที่..................................')
    address = Frame(5*inch, 9*inch, 2.8*inch, 1.5*inch,showBoundary=0)
    tmp = (company_request.company_name + u'<br/>' +
           u'<br/>'.join(company_request.letter_address.split('\n')))
    address.addFromList([Paragraph(tmp,styleN)],pdf)
    address.addFromList([Paragraph(u' <br/>วันที่........ เดือน................... พ.ศ...........', styleN)], pdf)

    f = Frame(0.6*inch, 0.5*inch, 7.2*inch, 8.4*inch, showBoundary=0)
    content = []
    content.append(Paragraph(u"เรื่อง รับนิสิตเข้าฝึกงานตามหลักสูตร<br/>"+
                             u"เรียน รองคณบดีฝ่ายกิจการนิสิตและกิจการพิเศษ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์<br/>"+
                             u"อ้างถึง หนังสือที่ " + Configuration.get("default.letter_number") + 
                             u" ลงวันที่ " + th_letterdate + "<br/>"+
                             u"สิ่งที่ส่งมาด้วย  รายละเอียดในการฝึกงาน",stylePH))

    content.append(Paragraph(u"ตามหนังสือที่อ้างถึง หน่วยงานของข้าพเจ้ายินดีรับนิสิตสาขา%s เข้าฝึกงาน ดังรายชื่อต่อไปนี้" % students[0].department.name,styleP))

                   
    colWidths = (10, 120, 220, 40)
    ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
          ('FONTSIZE', (0,0), (-1,-1), 14),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ]

    max_student_per_page = 25
    counter = 0
    number = 1
    page = 1
    tmp = [['',u'เลขประจำตัวนิสิต',u'ชื่อ-นามสกุล',u'ชั้นปีที่',u'สาขาวิชาวิศวกรรม']]
    for student in students:
        if counter == 0:
            tmp = [['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',styleTable),
                    Paragraph(u'<u>ชื่อ-นามสกุล</u>',styleTable),
                    Paragraph(u'<u>ชั้นปีที่</u>',styleTable),
                    ]]
        if student.prefix == u"นางสาว":
            prefix = u"น.ส."
        else:
            prefix = student.prefix
        
        tmp.append([number,student.student_id,prefix+student.full_name(),student.get_year()])
        counter += 1
        if counter == max_student_per_page:
            table = Table(tmp, style=ts,colWidths=colWidths)
            content.append(table)
            f.addFromList(content,pdf)
            pdf.showPage()

            f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 9.8*inch, showBoundary=0)
            content = []
            tmp = []
            page += 1
            pdf.setFont('THNiramit',16)
            pdf.drawString(4*inch,10.51*inch,'-'+str(page)+'-')
            counter = 0
            max_student_per_page = 32
        number += 1

    if ((page == 1 and counter > 12) or
        (counter > 22)):
            table = Table(tmp, style=ts,colWidths=colWidths)
            content.append(table)
            f.addFromList(content,pdf)
            pdf.showPage()        
            f = Frame(0.9*inch, 0.5*inch, 6.9*inch, 9.8*inch, showBoundary=0)
            content = []
            tmp = []
            page += 1
            pdf.setFont('THNiramit',16)
            pdf.drawString(4*inch,10.51*inch,'-'+str(page)+'-')

    if len(tmp) != 0:
        table = Table(tmp, style=ts,colWidths=colWidths)
        content.append(table)
        content.append(Paragraph(u' ',styleP))

    content.append(Paragraph(u"โดยเริ่มฝึกงานตั้งแต่วันที่ <u>"+th_startdate+u" - "+th_enddate+u"</u> ในการฝึกงานนี้ นิสิตต้องฝึกงานตามระเบียบเกี่ยวกับ การฝึกงานของคณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ ได้แก่ 1) ฝึกครบตามช่วงวันที่กำหนด 2) รวมเวลาฝึกงาน ไม่น้อยกว่า 240 ชั่วโมง และ 3) จำนวนวันไม่น้อยกว่า 30 วันทำการ <u>หากนิสิตมีวัน-เวลาฝึกงานไม่ครบทั้ง 3 ข้อ จะไม่ผ่านการฝึกงาน</u>",stylePH))
    content.append(Paragraph(u"จึงเรียนมาเพื่อโปรดทราบ",styleP))

    content.append(Paragraph(u"<br/>ขอแสดงความนับถือ<br/><br/><br/><br/><br/><br/>",styleSIGN))
        
    content.append(Paragraph(u"(....................................)<br/><br/><br/>ตำแหน่ง................................",styleSIGN))
    
    content.append(Paragraph(u"<br/>ชื่อเจ้าหน้าที่ประสานงานติดต่อของบริษัท/หน่วยงาน<br/>"+
                             company_request.contact_person + u' E-mail: ' + company_request.contact_email + '<br/>'+
                             u'โทรศัพท์ ' + company_request.contact_tel_no + '<br/>'+
                             u'โทรสาร ' + company_request.work_fax_no,stylePH))

    f.addFromList(content,pdf)
    pdf.showPage()

    # --------------------------------------------

    pdf.setFont('THNiramit',14)
    title_frame = Frame(1.5*inch, 10.8*inch, 5.27*inch, 0.5*inch,showBoundary=0)
    title_frame.addFromList([Paragraph(u'<u>รายละเอียดในการฝึกงาน</u>',styleMid)],pdf)

    f = Frame(0.6*inch, 0.5*inch, 7.07*inch, 10.5*inch, showBoundary=0)
    content = []
    content.append(Paragraph(u"บริษัท/หน่วยงาน " + company_request.company_name,stylePH))
    content.append(Paragraph(u"ประเภทธุรกิจ" + u"."*100,stylePH))
    content.append(Paragraph(u"",stylePH))

    description_ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
                      ('FONTSIZE', (0,0), (-1,-1), 14),
                      ('LEADING', (0,0), (-1,-1), 25),
                      ('VALIGN', (0,0), (-1,-1), 'TOP'),
                      ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                      ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black), 
                      ('BOTTOMPADDING', (1,0), (-1,-1), 10), ]
    work_address_paragraph = Paragraph('\n'.join([s.strip() for s in company_request.working_address.split("\n")]) + " " + "."*250,stylePHLWrap)
    work_description_paragraph = Paragraph(company_request.description+" "+
                                           "."*250, stylePHLWrap)
    department_name_paragraph = Paragraph(department_name,stylePHLWrap)

    table = Table([[u"สาขาวิชา",u"สถานที่ฝึกงาน",u"ลักษณะของงาน"],
                   [department_name_paragraph,
                    work_address_paragraph,
                    work_description_paragraph,]],
                  colWidths=(100,200,200),
                  style=description_ts)
    content.append(table)

    content.append(Paragraph(u"หมายเหตุ:  1. โปรดระบุการแต่งกายของนิสิตที่จะไปฝึกงาน<br/>" +
                             u"."*180 + u"<br/>"+
                             u"."*180 + u"<br/>"+
                             u"."*180 + u"<br/>",stylePHL))

    content.append(Paragraph(u"2. รายละเอียดอื่น ๆ ที่บริษัท/หน่วยงานต้องการ (โปรดระบุ)<br/>" +
                             u"."*180 + u"<br/>"+
                             u"."*180 + u"<br/>"+
                             u"."*180 + u"<br/>",stylePL))

    content.append(Paragraph(u"3. กรณีบริษัท/หน่วยงานอยู่นอกเขตกรุงเทพมหานคร (โปรดระบุ)<br/>",styleP))

    option_ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
                 ('FONTSIZE', (0,0), (-1,-1), 14)]
    table = Table([[u"❑ มีที่พักให้นิสิต",u"❑ นิสิตต้องจัดหาที่พักเอง"],
                   [u"❑ มีค่าตอบแทนวันละ............. บาท",u"❑ ไม่มีค่าตอบแทน"],
                   [u"❑ อื่น ๆ (โปรดระบุ).........................",'']],style=option_ts)
    content.append(table)

    f.addFromList(content,pdf)
    pdf.showPage()

    pdf.save()

    return response  


def build_assigned_student_list_pdf_response(assigned_students, department):
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
    enc.setAllPermissions(0)
    enc.canPrint = 1

    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,'THNiramit.ttf')
    THNiramitBd_font = os.path.join(font_path,'THNiramitBold.ttf')

    pdfmetrics.registerFont(TTFont('THNiramit', THNiramit_font))
    pdfmetrics.registerFont(TTFont('THNiramitBd', THNiramitBd_font))
    pdfmetrics.registerFontFamily('THNiramit',normal='THNiramit',bold='THNiramitBd')
    pdfmetrics.registerTypeFace(TypeFace('THNiramit'))

    styleN = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15)
    styleNWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,wordWrap='CJK')
    styleP = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,firstLineIndent=40,spaceBefore=10)
    stylePWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,firstLineIndent=40,spaceBefore=10,wordWrap='CJK')
    stylePL = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=25,firstLineIndent=40,spaceBefore=10)
    stylePH = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,spaceBefore=10)
    stylePHWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=15,spaceBefore=10,wordWrap='CJK')
    stylePHL = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=25,spaceBefore=10)
    stylePHLWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',leading=25,spaceBefore=10,wordWrap='CJK')
    styleMid = ParagraphStyle(name='normal',fontSize=14,fontName='THNiramit',leading=15,leftIndent=0,alignment=1,spaceBefore=0)
    styleSIGN = ParagraphStyle(name='normal',fontSize=14,fontName='THNiramit',leading=8,leftIndent=150,alignment=1,spaceBefore=0)
    styleTable = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit',alignment=1)
    styleTableL = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit')
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=student-list-' + department.code + '.pdf'

    pdf = Canvas(response,encrypt=enc,pagesize=A4)
    pdf.setFont('THNiramit',14)

    pdf.drawString(0.5*inch,10.75*inch,u'ใบลงนามนิสิตสาขาวิชา' + department.name)

    f = Frame(0.8*inch, 0.5*inch, 7.2*inch, 10.2*inch, showBoundary=0)
    
    colWidths = (10, 100, 80, 180, 200)
    ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
          ('FONTSIZE', (0,0), (-1,-1), 14),
          ('LEADING', (0,0), (-1,-1), 18),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ('ALIGNMENT',(2,0),(4,-1),'LEFT'),
          ]

    content = []
    max_student_per_page = 27
    counter = 0
    number = 1
    page = 1
    tmp = []
    for student in assigned_students:
        if counter == 0:
            tmp = [['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',styleTable),
                    Paragraph(u'<u>ลงนาม</u>',styleTable),
                    Paragraph(u'<u>ชื่อ-นามสกุล</u>',styleTableL),
                    Paragraph(u'<u>หน่วยงาน</u>',styleTable),
                    ]]
        if student.prefix == u"นางสาว":
            prefix = u"น.ส."
        else:
            prefix = student.prefix

        company_name = ''
        if student.assignment:
            if student.assignment.is_self_application:
                company_name = student.assignment.get_company_name()
            else:
                company_name = student.assignment.company.name
        else:
            company_name = student.assigned_company

        tmp.append([number,
                    student.student_id,
                    '______________',
                    prefix+student.full_name(),
                    company_name])
        counter += 1
        if counter == max_student_per_page:
            table = Table(tmp, style=ts,colWidths=colWidths)
            content.append(table)
            f.addFromList(content,pdf)
            pdf.showPage()
            f = Frame(0.8*inch, 0.5*inch, 7.2*inch, 10.2*inch, showBoundary=0)
            content = []
            tmp = []
            page += 1
            pdf.setFont('THNiramit',16)
            pdf.drawString(4*inch,10.91*inch,'-'+str(page)+'-')
            counter = 0
            max_student_per_page = 27
        number += 1

    if len(tmp) != 0:
        table = Table(tmp, style=ts,colWidths=colWidths)
        content.append(table)
        content.append(Paragraph(u' ',styleP))

    f.addFromList(content,pdf)
    pdf.showPage()

    pdf.save()
    return response  


def build_evaluation_label_pdf_response(students,
                                        department,
                                        year,
                                        file_name,
                                        xoffset=None,
                                        yoffset=None,
                                        user=None):
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="passwordlabelaidletter")
    enc.setAllPermissions(0)
    enc.canPrint = 1
    
    # Set font
    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,'THNiramit.ttf')
    pdfmetrics.registerFont(TTFont('THNiramit', THNiramit_font))
    pdfmetrics.registerTypeFace(TypeFace('THNiramit'))
    
    # Set style for Paragraph
    stylePH = ParagraphStyle(name='normal',
                             fontSize=16,
                             fontName='THNiramit',
                             leading=15,
                             leftIndent=0,
                             spaceBefore=0)
    styleAd = ParagraphStyle(name='normal',
                             fontSize=14,
                             fontName='THNiramit',
                             leading=15,
                             leftIndent=0,
                             spaceBefore=5)
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=' + file_name

    pdf = Canvas(response, encrypt=enc)
    pdf.setFont('THNiramit',16)
    
    # initial counter for printing label
    item_counter = 0
    row_counter = 0
    counter = 0
    
    # Set frame position
    a4_height = 11.69*inch
    top_margin = 0.125*inch
    slot_width = 1.42*inch
    
    if not xoffset:
        xoffset = 0
    xoffset *= inch
    if not yoffset:
        yoffset = 0
    yoffset *= inch

    qr_filenames = []
    for student in students:
        counter += 1

        # assignment = student.assignment
        # if assignment:
        #    if assignment.is_self_application:
        #        company_name = student.assignment.get_company_name()
        #    else:
        #        company_name = student.assignment.company.name
        # else:
        #    company_name = student.assigned_company
        company_name = ''
        
        row_bottom = a4_height - top_margin - (row_counter+1) * slot_width

        if item_counter % 2 == 0:
            env_label_frame = Frame(0.2*inch, row_bottom,
                                    4 * inch, (slot_width-0.05*inch),
                                    showBoundary=0)
            env_num_frame = Frame(3.5 * inch, row_bottom,
                                  1 * inch, 0.5 * inch,
                                  showBoundary=0)
            env_num_frame.addFromList([Paragraph('('+str(counter)+')', styleAd)], pdf)
        else:
            env_label_frame = Frame(4.235 * inch, row_bottom,
                                    4 * inch, (slot_width-0.05*inch),
                                    showBoundary=0)
            env_num_frame = Frame(4.3 * inch, row_bottom,
                                  1 * inch, 0.5 * inch,
                                  showBoundary=0)
            env_num_frame.addFromList([Paragraph('('+str(counter)+')', styleAd)], pdf)
        
            
        # code_label_frame = Frame(4.235 * inch, row_bottom,
        #                          4 * inch, (slot_width-0.05*inch),
        #                          showBoundary=0)
        # env_num_frame = Frame(4.3 * inch, row_bottom,
        #                       1 * inch, 0.5 * inch,
        #                       showBoundary=0)
        # env_num_frame.addFromList([Paragraph('('+str(counter)+')', styleAd)], pdf)
        
        env_content = []
        #env_content.append(Paragraph(company_name,
        #                             stylePH))
        env_content.append(Paragraph(student.student_id + ' ' + student.full_name(),
                                     stylePH))
        if student.eng_first_name + student.eng_last_name != '':
            env_content.append(Paragraph(student.eng_full_name(),
                                         stylePH))
        env_content.append(Paragraph(department.name, stylePH))
        if department.eng_name.strip() != '':
            env_content.append(Paragraph(department.eng_name, stylePH))

        env_label_frame.addFromList(env_content, pdf)


        # from evaluations.models import EvaluationSecret
        # evaluation_secret = EvaluationSecret.get_or_create_for_student(student,
        #                                                                year,
        #                                                                user)
        # code_content = []
        # code_content.append(Paragraph(u'Website URL: https://wt.eng.ku.ac.th/e/',
        #                               stylePH))
        # code_content.append(Paragraph(u'Student code: ' + student.student_id[5:],
        #                               stylePH))
        # code_content.append(Paragraph(u'Password: ' + evaluation_secret.easy_secret,
        #                               stylePH))
        
        # code_label_frame.addFromList(code_content, pdf)

        # qr_filename = get_evaluation_qr_code_filename(student, 
        #                                               evaluation_secret)
        # qr_filenames.append(qr_filename)
        # pdf.drawImage(qr_filename,
        #               6.8*inch, row_bottom + 0.05 * inch,
        #               1.25*inch, 1.25*inch)
        
        item_counter += 1
        if item_counter % 2 == 0:
            row_counter += 1
        
        if item_counter == 16:
            pdf.showPage()
            pdf.setFont('THNiramit',16)
            item_counter = 0
            row_counter = 0
            
    pdf.save()

    from os import remove
    for f in qr_filenames:
        remove(f)
    
    return response

def load_font_and_pdf_style():
    font_path = os.path.join(PROJECT_DIR, 'commons/static/font/')
    THNiramit_font = os.path.join(font_path,'THNiramit.ttf')
    THNiramitBd_font = os.path.join(font_path,'THNiramitBold.ttf')
    pdfmetrics.registerFont(TTFont('THNiramit', THNiramit_font))
    pdfmetrics.registerFont(TTFont('THNiramitBd', THNiramitBd_font))
    pdfmetrics.registerFontFamily('THNiramit',normal='THNiramit',bold='THNiramitBd')
    pdfmetrics.registerTypeFace(TypeFace('THNiramit'))

    class Style():
        pass

    style = Style()
    
    style.styleN       = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15)
    style.styleNWrap   = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,wordWrap='CJK')
    style.styleNI      = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,firstLineIndent=40)
    style.styleP       = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,firstLineIndent=40,spaceBefore=10)
    style.stylePWrap   = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,firstLineIndent=40,spaceBefore=10,wordWrap='CJK')
    style.stylePL      = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=25,firstLineIndent=40,spaceBefore=10)
    style.stylePH      = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,spaceBefore=10)
    style.stylePHWrap  = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,spaceBefore=10,wordWrap='CJK')
    style.stylePHL     = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=25,spaceBefore=10)
    style.stylePHLWrap = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=25,spaceBefore=10,wordWrap='CJK')
    style.stylePS      = ParagraphStyle(name='normal', fontSize=15,fontName='THNiramit', leading=18,firstLineIndent=40,spaceBefore=0)
    style.styleMid     = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=15,leftIndent=0,alignment=1,spaceBefore=0)
    style.styleSIGN    = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', leading=8, leftIndent=150,alignment=1,spaceBefore=0)
    style.styleTable   = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit', alignment=1)
    style.styleTableL  = ParagraphStyle(name='normal', fontSize=14,fontName='THNiramit')
    style.styleNSsmall = ParagraphStyle(name='normal', fontSize=12,fontName='THNiramit', leading=18, spaceBefore=0)
    style.styleNSxsmall = ParagraphStyle(name='normal', fontSize=10,fontName='THNiramit', leading=10, spaceBefore=0)

    return style


def get_co_op_batch_number(year, semester):
    return 42 + (year - 2566)*2 + semester

def get_eng_letter_number(letter_number):
    return ''.join([c for c in letter_number.replace(u'อว.',u'').replace(u'ว.',u'') if c in '01234567890./'])

def build_co_op_request_letter_main_letter(pdf,
                                           co_op_request,
                                           student, department,
                                           year, semester,
                                           lang,
                                           style):

    co_op_company = co_op_request.co_op_company
    co_op_semester = CoOpSemester.get_for(year, semester)
    co_op_batch_str = str(get_co_op_batch_number(year, semester))

    if lang == 'th':
        th_startdate = to_thai_date(co_op_request.beginning_date)
        th_enddate = to_thai_date(co_op_request.end_date)
    else:
        th_startdate = to_eng_date(co_op_request.beginning_date)
        th_enddate = to_eng_date(co_op_request.end_date)

    week_count_str = str(((co_op_request.end_date - co_op_request.beginning_date).days + 6) / 7)

    if lang == 'th':
        th_letterdate = to_thai_date(Configuration.get(name="default.letter_date"))
    else:
        th_letterdate = to_eng_date(Configuration.get(name="default.letter_date"))

    signature_name = Configuration.get("default.letter_signature")
    signature_name_english = Configuration.get("default.letter_signature_english")
    if lang == 'th':
        letter_number = u'ที่ ' + Configuration.get("default.letter_number")
    else:
        letter_number = u'Ref.No. ' + get_eng_letter_number(Configuration.get("default.letter_number"))

    if co_op_semester != None:
        if lang == 'th':
            response_date_str = to_thai_date(co_op_semester.request_deadline)
        else:
            response_date_str = to_eng_date(co_op_semester.request_deadline)
    else:
        response_date_str = 'FIXTHIS'
    
    signer_name = co_op_request.signer_name
    company_name = co_op_company.name

    krut_image_filename =  os.path.join(PROJECT_DIR, 'private_media/image/krut.jpg')

    sign_filename = os.path.join(PROJECT_DIR,'private_media/image/signature.jpg')
    signature_img = Image(sign_filename)
    signature_img.drawHeight = 0.4*signature_img.drawHeight
    signature_img.drawWidth = 0.4*signature_img.drawWidth

    pdf.drawImage(krut_image_filename, 3.5*inch, 10.25*inch, 1.1*inch, 1.1*inch)
    pdf.drawString(0.75*inch, 10.25*inch, letter_number)

    address = Frame(5*inch, 9.3*inch, 2.8*inch, 1.2*inch,showBoundary=0)

    if lang == 'th':
        tmp = u"คณะวิศวกรรมศาสตร์<br/>มหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตบางเขน<br/>ตู้ ป.ณ.1032 ปทฝ. เกษตรศาสตร์ กรุงเทพมหานคร 10903"
    else:
        tmp = u"Faculty of Engineering<br/>Kasetsart University<br/>P.O. Box 1032 Kasetsart Post Office,<br/>Bangkok 10903 Thailand"
    address.addFromList([Paragraph(tmp, style.styleN)],pdf)
    pdf.drawString(4*inch, 9.25*inch, th_letterdate)

    content = []

    if lang == 'th':
        content += [
            Paragraph(u"เรื่อง ขอความอนุเคราะห์ให้นิสิตเข้าร่วมปฏิบัติงานในสถานประกอบการตามโครงการสหกิจศึกษา",style.stylePH),
            Paragraph(u"เรียน " + signer_name + " " + company_name, style.stylePH),
            Paragraph(u'สิ่งที่ส่งมาด้วย<br/>'
                      u'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. แบบตอบรับนิสิตโครงการสหกิจศึกษาเข้าร่วมปฏิบัติงาน จำนวน 1 ฉบับ<br/>' +
                      u'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. แบบฟอร์มรายละเอียดการเข้าร่วมปฏิบัติงานในสถานประกอบการ จำนวน 1 ฉบับ<br/>' +
	              u'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. รายละเอียดโครงการสหกิจศึกษาในสาขาวิชาวิศวกรรมศาสตร์ รุ่นที่ ' +
                      co_op_batch_str,style.stylePH),
            Paragraph(u"ตามที่คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ " +
                      u"ได้มีนโยบายในการพัฒนาการศึกษาระบบสหกิจศึกษาใน<br/>"
                      u"สถาบันอุดมศึกษาเพื่อพัฒนาศักยภาพของนิสิตในด้านวิชาการควบคู่ไปกับประสบการณ์ด้านอาชีพ " +
                      u"และการพัฒนาตนเอง เพื่อความพร้อมในการประกอบอาชีพในอนาคตต่อไป " +
                      u"ซึ่งคณะวิศวกรรมศาสตร์ได้เข้าร่วมโครงการสหกิจศึกษา ของสำนักงานคณะกรรมการการอุดมศึกษา เป็นรุ่นที่ " +
                      co_op_batch_str +
                      u" โดยมีรายละเอียดโครงการ<br/>ตาม QR Code ด้านล่าง",style.stylePWrap),
        ]
        content += [
            Paragraph(u"คณะวิศวกรรมศาสตร์ จึงเรียนมาเพื่อขอเชิญหน่วยงานของท่านเข้าร่วมโครงการสหกิจศึกษา" +
                      u"และโปรดพิจารณา อนุเคราะห์ให้นิสิตเข้าร่วมปฏิบัติงานในสถานประกอบการของท่าน" +
                      u"<u>ระหว่างวันที่ " + th_startdate +
                      u" ถึงวันที่ " + th_enddate +
                      u" เป็นเวลา " + week_count_str + u" สัปดาห์</u> จำนวน 1 คน ได้แก่", style.stylePWrap),
        ]
    else:
        content += [
            Paragraph(u"Attn: Approvals of students practicing in workplaces following Cooperative Education Program.",style.stylePH),
            Paragraph(u"Dear " + signer_name + " " + company_name, style.stylePH),
            Paragraph(u'Enclosed with 1. Approval Form for accepting students ' +
                      'joining the cooperative education Program<br/>',style.stylePH),
            Paragraph(u"The Faculty of Engineering at Kasetsart University has a policy in supporting "+
                      u"the development of cooperative systems for higher educational institutions." +
                      u"This is expected improve student capabilities in combining academic knowledge " +
                      u"with real world professional experience in order to prepare readiness of " +
                      u"future career development.",style.stylePWrap)
        ]
        
        content += [
            Paragraph("Our faculty would like to invite your company/organization to " +
                      "join this cooperation project and please consider the following students " +
                      "to participate in the program from <u>" +
                      th_startdate + " to " + th_enddate + " for " + week_count_str + " weeks</u>.",style.stylePWrap),
        ]

    colWidths = (10, 120, 220, 40)
    ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
          ('FONTSIZE', (0,0), (-1,-1), 14),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ]

    if lang == 'th':
        table_data = [['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',style.styleTable),
                       Paragraph(u'<u>ชื่อ-นามสกุล</u>',style.styleTableL),
                       Paragraph(u'<u>ชั้นปีที่</u>',style.styleTable)],
                      ['', student.student_id,
                       student.short_prefix() + student.full_name(),
                       student.get_year()]]
    else:
        table_data = [['',Paragraph(u'<u>Student No.</u>',style.styleTable),
                       Paragraph(u'<u>Full name</u>',style.styleTableL),
                       Paragraph(u'<u>Year</u>',style.styleTable)],
                      ['', student.student_id,
                       student.eng_full_name(),
                       student.get_year()]]
    table = Table(table_data, style=ts, colWidths=colWidths)
    content.append(table)
    content.append(Paragraph(u' ',style.styleP))

    if lang=='th':
        content += [
            Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณา และขอได้โปรดแจ้งผลตอบรับการพิจารณาให้คณะวิศวกรรมศาสตร์ " +
                      u'มหาวิทยาลัยเกษตรศาสตร์ทราบ <u>ภายในวันที่ ' + response_date_str +
                      u' ด้วย</u> จักขอบคุณยิ่ง', style.styleP),
            Paragraph(u"<br/>ขอแสดงความนับถือ<br/><br/><br/><br/><br/>",style.styleSIGN),
            Paragraph('<img src="'+sign_filename+'" width='+str(signature_img.drawWidth)+' height='+str(signature_img.drawHeight)+' />' +
                      '<br/><br/>',style.styleSIGN),
            Paragraph(u"("+signature_name+u")<br/><br/>รองคณบดีฝ่ายกิจการนิสิตและกิจการพิเศษ",style.styleSIGN),
        ]
    else:
        content += [
            Paragraph(u'Please kindly provide your final decision to us before <u>' + response_date_str +
                      u'</u>. We look forward to hearing from you.', style.styleP),
            Paragraph(u"<br/>Yours faithfully,<br/><br/><br/><br/><br/>",style.styleSIGN),
            Paragraph('<img src="'+sign_filename+'" width='+str(signature_img.drawWidth)+' height='+str(signature_img.drawHeight)+' />' +
                      '<br/><br/>',style.styleSIGN),
            Paragraph(u"("+signature_name_english+u")<br/><br/>Vice Dean for Student Affairs and Special Activities",style.styleSIGN),
        ]
        
    tel = Frame(0.7*inch, 0.2*inch, 4.25*inch, 0.9*inch, showBoundary=0)
    if lang=='th':
        tmp = (u"คณะวิศวกรรมศาสตร์<br/>" +
               u"โทรศัพท์ 0 2797 0921 โทรสาร 0 2579 5897<br/>" +
               u"ผู้ประสานงาน นบชุลี/โบรณี หน่วยกิจการนิสิต Email: sa.eng@ku.th")
    else:
        tmp = (u"Faculty of Engineering<br/>" +
               u"Tel (662) 797 0921 Fax (662) 579 5897<br/>" +
               u"Contact: Nopchulee/Boranee Email: sa.eng@ku.th")
    tel.addFromList([Paragraph(tmp, style.styleN)],pdf)

    coop_qr_image_filename = os.path.join(PROJECT_DIR, 'private_media/image/coop-qr.jpg')
    pdf.drawImage(coop_qr_image_filename, 6.7*inch, 0.7*inch, 0.8*inch, 0.8*inch)
    qr_test_frame = Frame(6*inch, 0.3*inch, 10*inch, 0.5*inch, showBoundary=0)
    if lang == 'th':
        qr_test_frame.addFromList([Paragraph(u'(สแกนเพื่อเข้าเว็บไซต์โครงการสหกิจศึกษา มก.)',style.styleNSxsmall),
                                   Paragraph(u'&nbsp;'*12 + u'https://registrar.ku.ac.th/coed', style.styleNSxsmall)],pdf)
    else:
        qr_test_frame.addFromList([Paragraph(u'&nbsp;'*5 + '(Scan for info for KU Co-Op Education)',style.styleNSxsmall),
                                   Paragraph(u'&nbsp;'*11 + u'https://registrar.ku.ac.th/coed', style.styleNSxsmall)],pdf)
    
    frame = Frame(0.6*inch, 0.5*inch, 7.2*inch, 8.8*inch, showBoundary=0)
    frame.addFromList(content,pdf)
    pdf.showPage()


def build_co_op_request_letter_acceptance_letter(pdf,
                                                 co_op_request,
                                                 student, department,
                                                 lang, style):
    co_op_company = co_op_request.co_op_company
    co_op_semester = CoOpSemester.get_for(co_op_request.internship_year,
                                          co_op_request.internship_semester)

    if lang=='th':
        th_startdate = to_thai_date(co_op_request.beginning_date)
        th_enddate = to_thai_date(co_op_request.end_date)
    else:
        th_startdate = to_eng_date(co_op_request.beginning_date)
        th_enddate = to_eng_date(co_op_request.end_date)

    committee_contact = CoOpCommitteeContact.get_for(department)
    if committee_contact:
        if lang == 'th':
            committee_name = committee_contact.full_name
            committee_tel_number = committee_contact.tel_number
        else:
            committee_name = committee_contact.full_name_english
            committee_tel_number = committee_contact.tel_number_english
            committee_email = committee_contact.email
    else:
        committee_name = u'-'
        committee_tel_number = u'-'
        committee_email = u'-'

    if co_op_semester != None:
        if lang == 'th':
            response_date_str = to_thai_date(co_op_semester.request_deadline)
        else:
            response_date_str = to_eng_date(co_op_semester.request_deadline)
    else:
        response_date_str = 'FIXTHIS'

    department_name = department.name
    if department.id == 12:
        department_name = u'วิศวกรรมสำรวจและ สารสนเทศภูมิศาสตร์'

    title_frame = Frame(1.5*inch, 10.5*inch, 5.27*inch, 0.5*inch,showBoundary=0)
    if lang=='th':
        title_frame.addFromList([Paragraph(u'<u>แบบตอบรับนิสิตโครงการสหกิจศึกษา</u>',style.styleMid)],pdf)
    else:
        title_frame.addFromList([Paragraph(u'<u>Approval Form for accepting students joining the cooperative education program</u>',style.styleMid)],pdf)
    
    pdf.setFont('THNiramit',14)

    content = []

    if lang=='th':
        content += [
            Paragraph(' ', style.stylePH),
            Paragraph(u'เข้าร่วมปฏิบัติงาน ณ บริษัท/หน่วยงาน' +
                      '.' * 50, style.styleN),
            Paragraph(u'ประเภทธุรกิจ' +
                      '.' * 60, style.stylePH),
            Paragraph(u'ชื่อ-นามสกุล (นิสิตปฏิบัติงานสหกิจศึกษา)', style.styleN),
        ]
    else:
        content += [
            Paragraph(' ', style.stylePH),
            Paragraph(u'Participate at:' +
                      '.' * 50, style.styleN),
            Paragraph(u'Type of business/work:' +
                      '.' * 60, style.stylePH),
            Paragraph(u'Name of student:', style.styleN),
        ]

    colWidths = (10, 120, 220, 40)
    ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
          ('FONTSIZE', (0,0), (-1,-1), 14),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ('ALIGNMENT',(2,0),(2,-1),'LEFT'),
          ]

    if lang=='th':
        table_data = [['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',style.styleTable),
                       Paragraph(u'<u>ชื่อ-นามสกุล</u>',style.styleTableL),
                       Paragraph(u'<u>ชั้นปีที่</u>',style.styleTable)],
                      ['', student.student_id,
                       student.short_prefix() + student.full_name(),
                       student.get_year()]]
    else:
        table_data = [['',Paragraph(u'<u>Student No.</u>',style.styleTable),
                       Paragraph(u'<u>Full name</u>',style.styleTableL),
                       Paragraph(u'<u>Year</u>',style.styleTable)],
                      ['', student.student_id,
                       student.eng_full_name(),
                       student.get_year()]]
    table = Table(table_data, style=ts, colWidths=colWidths)
    content.append(table)

    if lang == 'en':
        content.append(Paragraph(u'<br/>Coordinator: ' + committee_name + '<br/>' +
                                 u'Tel: ' + committee_tel_number + '<br/>' + 
                                 u'E-mail: ' + committee_email + '<br/><br/>',
                                 style.stylePH))

    
    if lang == 'th':
        content.append(Paragraph(u'<u>กรุณาระบุผลการตอบรับ</u><br/>' +
                                 u'❑ สะดวกรับเข้าร่วมปฏิบัติงาน ระหว่างวันที่ ' + th_startdate +
                                 u' ถึงวันที่ ' + th_enddate + '<br/>' +
                                 u'❑ ไม่สะดวก เนื่องจาก ' + 30*'.', style.stylePH))
    else:
        content.append(Paragraph(u'<u>Please kindly select the following choices</u><br/>' +
                                 u'❑ Yes, we can accept the above student name proposed to practice from ' + th_startdate +
                                 u' to ' + th_enddate + '.<br/>' +
                                 u'❑ No, we cannot because ' + 30*'.', style.stylePH))
    content.append(Paragraph(u' ',style.styleP))


    if lang=='th':
        description_ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
                          ('FONTSIZE', (0,0), (-1,-1), 14),
                          ('LEADING', (0,0), (-1,-1), 25),
                          ('VALIGN', (0,0), (-1,-1), 'TOP'),
                          ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                          ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black), 
                          ('BOTTOMPADDING', (1,0), (-1,-1), 10), ]
        work_address_paragraph = Paragraph('\n'.join([s.strip() for s in co_op_company.address.split("\n")]) + " " + "."*40,style.stylePHLWrap)
        work_description_paragraph = Paragraph("."*200, style.stylePHLWrap)
        department_name_paragraph = Paragraph(department_name,style.stylePHLWrap)

        table = Table([[u"สาขาวิชา",u"สถานที่ฝึกงาน",u"ลักษณะของงาน"],
                       [department_name_paragraph,
                        work_address_paragraph,
                        work_description_paragraph,]],
                      colWidths=(100,200,200),
                      style=description_ts)
        content.append(table)
        
        content.append(Paragraph(u"หมายเหตุ:  1. โปรดระบุการแต่งกายของนิสิตที่จะไปฝึกงาน<br/>" +
                                 u"."*180 + u"<br/>",style.stylePHL))
        
        content.append(Paragraph(u"2. รายละเอียดอื่น ๆ ที่บริษัท/หน่วยงานต้องการ (โปรดระบุ)<br/>" +
                                 u"."*180 + u"<br/>",style.stylePL))
        
        content.append(Paragraph(u"3. กรณีบริษัท/หน่วยงานอยู่นอกเขตกรุงเทพมหานคร (โปรดระบุ)<br/>",style.styleNI))
        
        option_ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
                     ('FONTSIZE', (0,0), (-1,-1), 14)]
        table = Table([[u"❑ มีที่พักให้นิสิต",u"❑ นิสิตต้องจัดหาที่พักเอง"],
                       [u"❑ มีค่าตอบแทนวันละ............. บาท",u"❑ ไม่มีค่าตอบแทน"],
                       [u"❑ อื่น ๆ (โปรดระบุ).........................",'']],style=option_ts)
        content.append(table)
        
        content.append(Paragraph(u"จึงเรียนมาเพื่อโปรดทราบ",
                                 style.styleP))

    if lang=='th':
        content.append(Paragraph(u"<br/><br/>(....................................)" +
                                 u"<br/><br/>ตำแหน่ง................................" + 
                                 u"<br/><br/>วันที่ ................................",
                                 style.styleSIGN))
    else:
        content.append(Paragraph(u"Sign  ................................................" +
                                 u"<br/><br/>(....................................)" +
                                 u"<br/><br/>Position ................................" + 
                                 u"<br/><br/>Date ................................",
                                 style.styleSIGN))

    if lang == 'en':
        content.append(Paragraph('<br/><br/>Note: Please return the reply form to Admin, ' +
                                 'Faculty of Engineering Kasetsart University on or before ' +
                                 response_date_str + '.<br/>' +
                                 'Tel. (662) 797-0999, (662) 797-0921 | Fax : (662) 579-5897 | E-mail : sa.eng@ku.th',
                                 style.stylePH))
        
    f = Frame(0.6*inch, 1.0*inch, 7.07*inch, 9.5*inch, showBoundary=0)
    f.addFromList(content,pdf)

    if lang == 'th':
        f = Frame(0.6*inch, 0.75*inch, 4.5*inch, 0.7*inch, showBoundary=0)
        f.addFromList([Paragraph(u'ชื่ออาจารย์ผู้ประสานงาน: ' + committee_name + '<br/>' +
                                 u'โทร. ' + committee_tel_number,
                                 style.stylePH)],
                      pdf)

    pdf.showPage()


    
def build_co_op_request_letter_pdf_response(co_op_request, lang):
    student = co_op_request.student
    co_op_company = co_op_request.co_op_company
    department = student.department
    
    year = co_op_request.internship_year
    semester = co_op_request.internship_semester

    #create pdf letter
    #TODO change password for encrypt
    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
    enc.setAllPermissions(0)
    enc.canPrint = 1

    style = load_font_and_pdf_style()
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=company_letter.pdf'

    pdf = Canvas(response,encrypt=enc,pagesize=A4)
    pdf.setFont('THNiramit',14)

    build_co_op_request_letter_main_letter(pdf, co_op_request,
                                           student, department,
                                           year, semester,
                                           lang,
                                           style)
    build_co_op_request_letter_acceptance_letter(pdf,
                                                 co_op_request,
                                                 student, department,
                                                 lang, style)

    pdf.save()

    return response  


def build_co_op_sending_letter_pdf_response(co_op_request,
                                            lang,
                                            show_signature):
    student = co_op_request.student
    co_op_company = co_op_request.co_op_company
    department = student.department

    year = co_op_request.internship_year
    semester = co_op_request.internship_semester
    co_op_semester = CoOpSemester.get_for(year, semester)

    co_op_batch_str = str(get_co_op_batch_number(year, semester))
    if lang == 'th':
        if semester == 1:
            co_op_semester_str = u'ภาคต้น ปีการศึกษา ' + str(year)
        else:
            co_op_semester_str = u'ภาคปลาย ปีการศึกษา ' + str(year)
    else:
        if semester == 1:
            co_op_semester_str = u'first semester of academic year ' + str(year-543)
        else:
            co_op_semester_str = u'second semester of academic year ' + str(year-543)

    if lang == 'th':
        th_startdate = to_thai_date(co_op_request.beginning_date)
        th_enddate = to_thai_date(co_op_request.end_date)
        th_letterdate = to_thai_date(co_op_semester.sending_letter_date)
    else:
        th_startdate = to_eng_date(co_op_request.beginning_date)
        th_enddate = to_eng_date(co_op_request.end_date)
        th_letterdate = to_eng_date(co_op_semester.sending_letter_date)
        
    week_count_str = str(((co_op_request.end_date - co_op_request.beginning_date).days + 6) / 7)
    letter_number = co_op_semester.sending_letter_number

    signer_name = co_op_request.signer_name
    company_name = co_op_company.name

    year = co_op_request.internship_year
    semester = co_op_request.internship_semester

    enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
    enc.setAllPermissions(0)
    enc.canPrint = 1

    style = load_font_and_pdf_style()
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=company_letter.pdf'

    pdf = Canvas(response,encrypt=enc,pagesize=A4)
    pdf.setFont('THNiramit',14)

    style = load_font_and_pdf_style()

    if lang == 'th':
        signature_name = Configuration.get(name="default.letter_signature")
        letter_number = u'ที่ ' + letter_number
    else:
        signature_name = Configuration.get(name="default.letter_signature_english")
        letter_number = u'Ref.No. ' + get_eng_letter_number(letter_number)

    krut_image_filename =  os.path.join(PROJECT_DIR, 'private_media/image/krut.jpg')

    if show_signature:
        sign_filename = os.path.join(PROJECT_DIR,'private_media/image/signature.jpg')
        signature_img = Image(sign_filename)
        signature_img.drawHeight = 0.4*signature_img.drawHeight
        signature_img.drawWidth = 0.4*signature_img.drawWidth

    pdf.drawImage(krut_image_filename,3.5*inch,10.25*inch,1.1*inch,1.1*inch)
    pdf.drawString(1*inch,10.35*inch,letter_number)

    #if is_student_copy:
    #    pdf.drawString(6.5*inch,11*inch,u'(สำเนาสำหรับนิสิต)')

    address = Frame(5*inch, 9.45*inch, 2.9*inch, 1.2*inch,showBoundary=0)
    if lang == 'th':
        tmp = u"คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์<br/>50 ถนนงามวงศ์วาน แขวงลาดยาว เขตจุตจักร<br/>กรุงเทพ 10900"
    else:
        tmp = u"Faculty of Engineering<br/>Kasetsart University<br/>P.O. Box 1032 Kasetsart Post Office,<br/>Bangkok 10903 Thailand"
    address.addFromList([Paragraph(tmp,style.styleN)], pdf)

    pdf.drawString(4*inch,9.25*inch,th_letterdate)

    content = []

    if lang == 'th':
        content += [
            Paragraph(u"เรื่อง ส่งรายชื่อนิสิตเข้าร่วมฝึกปฏิบัติงานในสถานประกอบการตามโครงการสหกิจศึกษา",
                      style.stylePH),
            Paragraph(u"เรียน "+signer_name+" &nbsp;&nbsp;"+company_name, style.stylePH),
            Paragraph(u'สิ่งที่ส่งมาด้วย  1. เอกสารประกอบการเข้าร่วมฝึกปฏิบัติงาน จำนวน 1 ชุด', style.stylePH),
            
            Paragraph(u"ตามที่ท่านให้ความอนุเคราะห์ในการรับนิสิตเข้าร่วมฝึกปฏิบัติงานในสถานประกอบการ ตามโครงการสหกิจศึกษา คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ " +
                      u"ประจำ" + co_op_semester_str + u" รุ่นที่ " + co_op_batch_str + u" นั้น คณะวิศวกรรมศาสตร์ ขอขอบคุณที่ท่านให้ความอนุเคราะห์ในการรับนิสิตเข้าร่วมฝึกปฏิบัติงาน " +
                      u"จึงขอส่งนิสิตเข้าร่วมฝึกปฏิบัติงาน ในสถานประกอบการของท่าน ตั้งแต่วันที่ <u>" + th_startdate +
                      u"ถึงวันที่ " + th_enddate + u" เป็นเวลา " +
                      unicode(week_count_str) + u" สัปดาห์</u> จำนวน 1 ราย ดังนี้", style.styleP)
        ]
    else:
        content += [
            #Paragraph(u"เรื่อง ส่งรายชื่อนิสิตเข้าร่วมฝึกปฏิบัติงานในสถานประกอบการตามโครงการสหกิจศึกษา",
            #          style.stylePH),
            Paragraph(u"Dear "+signer_name+" &nbsp;&nbsp;"+company_name, style.stylePH),
            Paragraph(u'Enclosed with  1. Supplementary sheet of Cooperative Education Program (1 set)', style.stylePH),
            
            Paragraph(u"As you allowed our students to take the Cooperative Education Program " +
                      u"in your organization at the " + co_op_semester_str + u".   " +
                      u"We would like to take this opportunity sending the name of students " +
                      u"who will participate in this program from <u>" + 
                      th_startdate +
                      u" to " + th_enddate + u" (" +
                      unicode(week_count_str) + u" weeks)</u> as follows:", style.styleP)
        ]

    ################################################################
    
    if student.prefix == u"นางสาว":
        prefix = u"น.ส."
    else:
        prefix = student.prefix
        
    if student.department.short_name_for_letters:
        department_name = student.department.short_name_for_letters
    else:
        department_name = student.department.name

    if department_name.startswith(u'วิศวกรรม'):
        department_name = department_name[len(u'วิศวกรรม'):]
    
    colWidths = (10, 100, None, 40, None)
    ts = [('FONT', (0,0), (-1,-1), 'THNiramit'),
          ('FONTSIZE', (0,0), (-1,-1), 15),
          
          ('ALIGNMENT',(0,0),(-1,-1),'CENTER'),
          ]

    if lang == 'th':
        table_data = [
            ['',Paragraph(u'<u>เลขประจำตัวนิสิต</u>',style.styleTable),
             Paragraph(u'<u>ชื่อ-นามสกุล</u>',style.styleTable),
             Paragraph(u'<u>ชั้นปีที่</u>',style.styleTable),
             Paragraph(u'<u>สาขาวิศวกรรม</u>',style.styleTable),],
            ['1',
             student.student_id,
             student.short_prefix() + student.full_name(),
             student.get_year(),
             department_name],
        ]
    else:
        table_data = [
            ['',Paragraph(u'<u>Student No.</u>',style.styleTable),
             Paragraph(u'<u>Full name</u>',style.styleTable),
             Paragraph(u'<u>Year</u>',style.styleTable),
             Paragraph(u'<u></u>',style.styleTable),],
            ['1',
             student.student_id,
             student.eng_full_name(),
             student.get_year(),
             ''],
        ]
        
    table = Table(table_data, style=ts, colWidths=colWidths)
    content.append(table)

    if lang == 'th':
        content += [
            Paragraph(u' ',style.styleP),
            
            Paragraph(u"ทั้งนี้ เมื่อนิสิตฝึกปฏิบัติโครงการสหกิจศึกษาเรียบร้อยแล้ว ขอความอนุเคราะห์หัวหน้างานหรือผู้ควบคุมการฝึกงาน ดำเนินการประเมินผลตามแบบประเมินผลนิสิตสหกิจศึกษา (หมายเลขเอกสาร 08)", style.styleP),
            Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณาดำเนินการด้วย จักขอบคุณยิ่ง",style.styleP),
            
            Paragraph(u"<br/>ขอแสดงความนับถือ<br/><br/><br/><br/>",style.styleSIGN),
            Paragraph(u' ',style.styleP),
        ]
    else:
        content += [
            Paragraph(u' ',style.styleP),

            Paragraph(u"Thank you very much for your cooperation and we look forward " +
                      u"that your organization will participate in this program in future.", style.styleP),
            #Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณาดำเนินการด้วย จักขอบคุณยิ่ง",style.styleP),

            Paragraph(u"<br/>Yours Sincerely,<br/><br/><br/><br/>",style.styleSIGN),
            Paragraph(u' ',style.styleP),
    ]

    if show_signature:
        content += [
            Paragraph('<img src="'+sign+'" width='+str(signature_img.drawWidth)+' height='+str(signature_img.drawHeight)+' />',style.styleSIGN),
        ]
    else:
        content += [
            Paragraph('<br/><br/><br/>',style.styleSIGN),
        ]

    if lang == 'th':
        content += [
            Paragraph(u"<br/><br/>("+signature_name+u")<br/><br/>รองคณบดีฝ่ายกิจการนิสิตและกิจการพิเศษ",style.styleSIGN),
        ]
    else:
        content += [
            Paragraph(u"<br/><br/>("+signature_name+u")<br/><br/>Vice Dean for Student Affairs and Special Activities",style.styleSIGN),
        ]

    tel = Frame(0.9*inch, 0.25*inch, 8.25*inch, 1.2*inch, showBoundary=0)
    if lang == 'th':
        tmp = (u"คณะวิศวกรรมศาสตร์ มก.<br/>" +
               u"โทรศัพท์ 0 2797 0967, 0 2797 0921<br/>" +
               u"โทรสาร 0 2579 5897<br/>" +
               u"ผู้ประสานงาน คุณนบชุลี/คุณโบรณี ผู้ประสานงาน E-mail: sa.eng@ku.th")
    else:
        tmp = (u"Faculty of Engineering<br/>" +
               u"Tel (662) 797 0921. Fax (662) 579 5897<br/>" +
               u"Contact: Nopchulee/Boranee Email: sa.eng@ku.th")

    tel.addFromList([Paragraph(tmp,style.styleNSsmall)],pdf)
    
    frame = Frame(0.9*inch, 0.5*inch, 6.9*inch, 8.65*inch, showBoundary=0)
    frame.addFromList(content,pdf)

    pdf.save()

    return response  



