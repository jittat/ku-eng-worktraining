# -*- coding: utf-8 -*-
# Django settings for worktraining project.
import os.path
PROJECT_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Jittat Fakcharoenphol', 'jittat@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'dev.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Bangkok'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'th-th'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/site_media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/site_media/admin/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '**c#ycpqy1r$+(eg0scj0tf+j-q9347%r^u5_6&cdkt$te&72d'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

ROOT_URLCONF = 'worktraining.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_DIR, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #'south',
    'debug_toolbar',
    'mailer',
    'django_extensions',
    'commons',
    'regis',
    'std',
    'evaluations',
    'committee',
    'direct_appl',
    'peakhours',
)

STATIC_DOC_ROOT = os.path.join(PROJECT_DIR, 'media')

STATIC_URL = '/site_media/'
STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# KU Eng Worktraining settings
# ----------------------------

# Department list in request letter (when sending letters to all companies)
DEPARTMENT_NAMES = [u'วิศวกรรมเคมี',
                    u'วิศวกรรมโยธา',
                    u'วิศวกรรมคอมพิวเตอร์',
                    u'วิศวกรรมอุตสาหการ',
                    u'วิศวกรรมเครื่องกล',
                    u'วิศวกรรมโยธา-ทรัพยากรน้ำ',
                    u'วิศวกรรมสิ่งแวดล้อม',
                    u'วิศวกรรมการบินและอวกาศ',
                    u'วิศวกรรมวัสดุ',
                    u'วิศวกรรมไฟฟ้า ( สื่อสาร/ อิเล็กทรอนิกส์/ ควบคุม/ กำลัง )',
                    u'วิศวกรรมไฟฟ้าเครื่องกลการผลิต',
                    u'วิศวกรรมสำรวจและสารสนเทศภูมิศาสตร์',
                    u'สาขาวิชาเทคโนโลยีการจัดการการบิน']


# Custom user profile
AUTH_PROFILE_MODULE = 'commons.UserProfile'

# Added ImapAuthentication (taken from http://code.google.com/p/webpymail/)
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'commons.backends.tqfauthen.WebBackend',
    #'worktraining.commons.backends.imapauthen.ImapBackend',
)

DEPARTMENT_ADMIN_INDEX = 'committee_letters_index_nodept'

# Set IMAP server and port for IMAP authentication.  Set IMAP_SERVER
# to '' or None to disable IMAP authen.
IMAP_SERVER = 'nontri.ku.ac.th'
IMAP_PORT = 993

# Set this to True for debugging (only works when DEBUG==True)
FAKE_STUDENT_AUTHEN = False
FAKE_SENDING_MAIL = True

LOGOUT_URL = '/'

# This is for demo
DEMO_STUDENT_PREFIX = '5199'
DEMO_STUDENT_PASSWORD = 'nopassword'

HTTPS_LOGIN = False

TOTAL_CREDITS_VALIDATION = True
APPLICATION_CONFIRMATION_SALT = 'km65osdnwl3'

TOKEN_LOGIN = False
TOKEN_SECRET = 'secret'
IUP_TOKEN_SECRET = 'secret'

LOGIN_HOOKS = []

LOGINS_WITH_KUSSO = False
KUSSO_LOGIN_URL = {
    'std': '',
    'regis': '',
    'committee': '',
}
KUSSO_LOGOUT_URL = ''

LETTER_DOWNLOADABLE = False

QUIZ_REQUIRED_BEFORE_LETTER_DOWNLOAD = False
QUIZ_RESULT_URL = ''
QUIZ_URL = 'https://www.google.com/'

SHOWS_QR_CODE_IN_STD_LETTER = False

try:
    from settings_local import *
except ImportError:
    pass 
