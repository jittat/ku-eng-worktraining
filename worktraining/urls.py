from django.conf.urls import url, include, patterns
from django.conf import settings

import django.contrib.staticfiles.views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import commons.views
import regis.announcement.views
import evaluations.views
import commons.auth_views

urlpatterns = patterns(
    '',
    #(r'^$',
    # 'django.views.generic.simple.direct_to_template',
    # {'template': 'front.html'}),
    url(r'^$', commons.views.index, name='front_page'),

    url(r'^announcement/(?P<announcement_id>\d+)/$',
        regis.announcement.views.announcement_detail,
        name='front_announcement_detail'),
    url(r'^announcement/edit/(?P<announcement_id>\d+)/$',
        regis.announcement.views.announcement_edit,
        name='announcement_edit'),
    url(r'^announcement/index/$',
        regis.announcement.views.list,
        name='announcement_index'),
    url(r'^announcement/create/$',
        regis.announcement.views.announcement_create,
        name='announcement_create'),
    url(r'^announcement/delete/(?P<announcement_id>\d+)/$',
        regis.announcement.views.announcement_delete,
        name='announcement_delete'),
    
    (r'^regis/', include('regis.urls')),
    (r'^std/', include('std.urls')),
    (r'^eval/', include('evaluations.urls')),
    (r'^committee/', include('committee.urls')),
    (r'^commons/', include('commons.urls')),
    (r'^direct/', include('direct_appl.urls')),

    url(r'^ev/(\d+)/(.+)/$',
        evaluations.views.edit_by_company,
        name='short_evaluations_edit_by_company'),


    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),

    # authentication
    url(r'^accounts/login/$', 
        commons.auth_views.login,
        name='admin_login'),
    url(r'^accounts/logout/$', 
        'django.contrib.auth.views.logout',
        { 'next_page': settings.LOGOUT_URL },
        name='admin_logout'),
    url(r'^accounts/login/token/$',
        commons.auth_views.login_user_with_token,
        name='admin_login_token'),
)


if settings.DEBUG:
    urlpatterns += patterns(
        '',
        # Static media (for development)
        (r'^site_media/(?P<path>.*)$',
         django.contrib.staticfiles.views.serve),
    )

