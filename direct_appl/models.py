# -*- coding: utf-8 -*-

from django.db import models

from regis.models import Company, Student

DIRECT_APPL_ONLY_CHOICES = (
    (True,u'คัดเลือกเองอย่างเดียว'),
    (False,u'ให้คณะคัดเลือกให้บางส่วนด้วย'))

class DirectApplicationPosition(models.Model):
    company = models.ForeignKey(Company, 
                                verbose_name=u'สถานประกอบการ',
                                related_name='direct_appl_info_set')
    year = models.IntegerField(
        verbose_name=u'ปีที่เข้ารับฝึกงาน')

    is_direct_appl_only = models.BooleanField(
        default=True,
        verbose_name=u'คัดเลือกเองเท่านั้น?',
        help_text=u'สถานประกอบการรับเฉพาะที่คัดเลือกเองเท่านั้นหรือไม่',
        choices=DIRECT_APPL_ONLY_CHOICES)

    appl_requirements = models.TextField(
        blank=True,
        verbose_name=u'เงื่อนไขในการสมัคร')

    application_deadline = models.DateField(null=True,
                                            default=None,
                                            blank=True,
                                            verbose_name=u'ส่งใบสมัครภายในวันที่')

    application_form_file = models.FileField( 
        upload_to='applications/%y/',
        null=True,
        default=None,
        blank=True,
        verbose_name=u'ใบสมัคร')

    is_hidden = models.BooleanField(default=False,
                                    verbose_name=u'ซ่อน')

    class Meta:
        verbose_name = u'ตำแหน่งงานที่สถานประกอบการคัดเลือกเอง'
        verbose_name_plural = verbose_name

    @staticmethod
    def get_all_for_year(year):
        return (DirectApplicationPosition
                .objects
                .select_related('company')
                .filter(year=year))

APPLICATION_RESULT_CHOICES = (
    (None,u'รอผล'),
    (True,u'สถานประกอบการตอบรับ'),
    (False,u'สถานประกอบการตอบปฏิเสธ'))

class StudentApplication(models.Model):
    student = models.ForeignKey(Student,
                                verbose_name=u'นิสิต')
    position = models.ForeignKey(DirectApplicationPosition,
                                 verbose_name=u'ตำแหน่งงาน')
    is_accepted = models.NullBooleanField(default=None,
                                          choices=APPLICATION_RESULT_CHOICES,
                                          verbose_name=u'ผลการสมัคร')

    apply_at = models.DateTimeField(auto_now_add=True,
                                    verbose_name=u'วันที่ได้รับใบสมัคร')
    result_date = models.DateField(blank=True,
                                   null=True,
                                   default=None,
                                   verbose_name=u'วันที่ทราบผลการสมัคร')

    class Meta:
        ordering = ['apply_at']


    def is_rejected(self):
        return self.is_accepted == False

    def __unicode__(self):
        return "%s applied for %s in %d" % (self.student.full_name(),
                                            self.position.company.name,
                                            self.position.year)

    @staticmethod
    def all_for_position(position):
        return (StudentApplication
                .objects
                .filter(position=position)
                .all())

    @staticmethod
    def all_for_student(student, year=None,related=True):
        if related:
            results = (StudentApplication
                       .objects
                       .filter(student=student)
                       .select_related('student','position')
                       .all())
        else:
            results = (StudentApplication
                       .objects
                       .filter(student=student)
                       .all())
        if year:
            results = results.filter(position__year__exact=year)
        return results

    @staticmethod
    def count_all_for_student(student, year=None):
        results = (StudentApplication
                   .objects
                   .filter(student=student)
                   .all())
        if year:
            results = results.filter(position__year__exact=year)
        return results.count()

    @staticmethod
    def all_for_department(department, year=None):
        results = (StudentApplication
                   .objects
                   .filter(student__department=department)
                   .select_related('student','position')
                   .all())
        if year:
            results = results.filter(position__year__exact=year)
        return results


