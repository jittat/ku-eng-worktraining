# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'DirectApplicationPosition'
        db.create_table('direct_appl_directapplicationposition', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(related_name='direct_appl_info_set', to=orm['regis.Company'])),
            ('year', self.gf('django.db.models.fields.IntegerField')()),
            ('is_direct_appl_only', self.gf('django.db.models.fields.BooleanField')(default=True, blank=True)),
            ('appl_requirements', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('application_form_file', self.gf('django.db.models.fields.files.FileField')(default=None, max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('direct_appl', ['DirectApplicationPosition'])


    def backwards(self, orm):
        
        # Deleting model 'DirectApplicationPosition'
        db.delete_table('direct_appl_directapplicationposition')


    models = {
        'direct_appl.directapplicationposition': {
            'Meta': {'object_name': 'DirectApplicationPosition'},
            'appl_requirements': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'application_form_file': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'direct_appl_info_set'", 'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_direct_appl_only': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        }
    }

    complete_apps = ['direct_appl']
