# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DirectApplicationPosition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35\u0e17\u0e35\u0e48\u0e40\u0e02\u0e49\u0e32\u0e23\u0e31\u0e1a\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
                ('is_direct_appl_only', models.BooleanField(default=True, help_text='\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23\u0e23\u0e31\u0e1a\u0e40\u0e09\u0e1e\u0e32\u0e30\u0e17\u0e35\u0e48\u0e04\u0e31\u0e14\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e40\u0e2d\u0e07\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19\u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e21\u0e48', verbose_name='\u0e04\u0e31\u0e14\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e40\u0e2d\u0e07\u0e40\u0e17\u0e48\u0e32\u0e19\u0e31\u0e49\u0e19?', choices=[(True, '\u0e04\u0e31\u0e14\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e40\u0e2d\u0e07\u0e2d\u0e22\u0e48\u0e32\u0e07\u0e40\u0e14\u0e35\u0e22\u0e27'), (False, '\u0e43\u0e2b\u0e49\u0e04\u0e13\u0e30\u0e04\u0e31\u0e14\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e43\u0e2b\u0e49\u0e1a\u0e32\u0e07\u0e2a\u0e48\u0e27\u0e19\u0e14\u0e49\u0e27\u0e22')])),
                ('appl_requirements', models.TextField(verbose_name='\u0e40\u0e07\u0e37\u0e48\u0e2d\u0e19\u0e44\u0e02\u0e43\u0e19\u0e01\u0e32\u0e23\u0e2a\u0e21\u0e31\u0e04\u0e23', blank=True)),
                ('application_deadline', models.DateField(default=None, null=True, verbose_name='\u0e2a\u0e48\u0e07\u0e43\u0e1a\u0e2a\u0e21\u0e31\u0e04\u0e23\u0e20\u0e32\u0e22\u0e43\u0e19\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48', blank=True)),
                ('application_form_file', models.FileField(default=None, upload_to=b'applications/%y/', null=True, verbose_name='\u0e43\u0e1a\u0e2a\u0e21\u0e31\u0e04\u0e23', blank=True)),
                ('is_hidden', models.BooleanField(default=False, verbose_name='\u0e0b\u0e48\u0e2d\u0e19')),
            ],
            options={
                'verbose_name': '\u0e15\u0e33\u0e41\u0e2b\u0e19\u0e48\u0e07\u0e07\u0e32\u0e19\u0e17\u0e35\u0e48\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23\u0e04\u0e31\u0e14\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e40\u0e2d\u0e07',
                'verbose_name_plural': '\u0e15\u0e33\u0e41\u0e2b\u0e19\u0e48\u0e07\u0e07\u0e32\u0e19\u0e17\u0e35\u0e48\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23\u0e04\u0e31\u0e14\u0e40\u0e25\u0e37\u0e2d\u0e01\u0e40\u0e2d\u0e07',
            },
        ),
        migrations.CreateModel(
            name='StudentApplication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_accepted', models.NullBooleanField(default=None, verbose_name='\u0e1c\u0e25\u0e01\u0e32\u0e23\u0e2a\u0e21\u0e31\u0e04\u0e23', choices=[(None, '\u0e23\u0e2d\u0e1c\u0e25'), (True, '\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23\u0e15\u0e2d\u0e1a\u0e23\u0e31\u0e1a'), (False, '\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23\u0e15\u0e2d\u0e1a\u0e1b\u0e0f\u0e34\u0e40\u0e2a\u0e18')])),
                ('apply_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a\u0e43\u0e1a\u0e2a\u0e21\u0e31\u0e04\u0e23')),
                ('result_date', models.DateField(default=None, null=True, verbose_name='\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e17\u0e23\u0e32\u0e1a\u0e1c\u0e25\u0e01\u0e32\u0e23\u0e2a\u0e21\u0e31\u0e04\u0e23', blank=True)),
                ('position', models.ForeignKey(verbose_name='\u0e15\u0e33\u0e41\u0e2b\u0e19\u0e48\u0e07\u0e07\u0e32\u0e19', to='direct_appl.DirectApplicationPosition')),
            ],
            options={
                'ordering': ['apply_at'],
            },
        ),
    ]
