# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('direct_appl', '0001_initial'),
        ('regis', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentapplication',
            name='student',
            field=models.ForeignKey(verbose_name='\u0e19\u0e34\u0e2a\u0e34\u0e15', to='regis.Student'),
        ),
        migrations.AddField(
            model_name='directapplicationposition',
            name='company',
            field=models.ForeignKey(related_name='direct_appl_info_set', verbose_name='\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23', to='regis.Company'),
        ),
    ]
