# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext

from direct_appl.models import DirectApplicationPosition, StudentApplication
from regis.models import Student
from std.decorators import std_login_required
from commons.academia import get_next_worktraining_year

@std_login_required
def list(request):
    return redirect('std_index')
    year = get_next_worktraining_year()

    if not request.student.has_registered(year):
        return render_to_response("commons/forbidden.html")

    direct_positions = DirectApplicationPosition.get_all_for_year(year).filter(is_hidden=False)

    return render_to_response("direct_appl/std/list.html",
                              { 'year': year,
                                'direct_positions': direct_positions,
                                'currentpage': 'company' },
                              context_instance=RequestContext(request))

@std_login_required
def show(request, direct_position_id):
    year = get_next_worktraining_year()

    if not request.student.has_registered(year):
        return render_to_response("commons/forbidden.html")

    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=direct_position_id)

    company = direct_position.company
    requirements = direct_position.appl_requirements
    return render_to_response("direct_appl/std/show.html",
                              { 'year': year,
                                'company': company,
                                'requirements': requirements,
                                'direct_position': direct_position,
                                'currentpage': 'company' },
                              context_instance=RequestContext(request))


@std_login_required
def show_applications(request):
    student = request.student
    year = get_next_worktraining_year()

    if not request.student.has_registered(year):
        return render_to_response("commons/forbidden.html")

    applications = StudentApplication.all_for_student(student,year)

    return render_to_response("direct_appl/std/show_applications.html",
                              { 'student': student,
                                'year': year,
                                'applications': applications },
                              context_instance=RequestContext(request))
