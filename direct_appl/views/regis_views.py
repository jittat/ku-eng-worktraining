# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.forms import ModelForm
from django import forms
from django.http import HttpResponse

from direct_appl.models import DirectApplicationPosition, StudentApplication
from regis.models import Company, Student, Department, Assignment
from commons.academia import get_next_worktraining_year
from commons.decorators import sa_admin_required
from commons.utils import extract_notice
from commons.email import send_direct_approval_confirmation
from commons.models import Log

@sa_admin_required
def company_list(request):
    year = get_next_worktraining_year()
    positions = DirectApplicationPosition.get_all_for_year(year).all()
    notice = extract_notice(request.session)

    return render_to_response("direct_appl/regis/company_list.html",
                              { 'company_positions': positions,
                                'year': year,
                                'notice': notice,
                                'currentpage': 'company' },
                              context_instance=RequestContext(request))

class DirectApplicationPositionForm(ModelForm):
    company_name = forms.CharField(label=u'ชื่อสถานประกอบการ')

    class Meta:
        model = DirectApplicationPosition
        exclude = ['year', 'company']
        fields = [ 'company_name', 
                   'is_direct_appl_only',
                   'appl_requirements', 
                   'application_deadline',
                   'application_form_file' ]

    def get_companies(self):
        company_name = self.cleaned_data['company_name']
        companies = Company.all_with_name(company_name)
        return companies

    def clean_company_name(self):
        company_name = self.cleaned_data['company_name']
        companies = self.get_companies()
        company_count = len(companies)
        if company_count == 0:
            raise forms.ValidationError(u"ไม่พบสถานประกอบการชื่อดังกล่าว")
        #
        #elif company_count > 1:
        #    raise forms.ValidationError(u"มีสถานประกอบการชื่อดังกล่าวมากกว่า 1 ที่")
        self.cleaned_company = companies[0]
        return company_name


    def get_direct_application_position(self):
        appl_position = self.save(commit=False)
        appl_position.company = self.cleaned_company
        appl_position.appl_requirements = (
            appl_position.appl_requirements.strip())
        if appl_position.appl_requirements=='':
            appl_position.appl_requirements = u'ไม่มี'
        return appl_position
        

@sa_admin_required
def company_add(request):
    year = get_next_worktraining_year()
    
    if request.method=='POST':
        if 'cancel' not in request.POST:
            form = DirectApplicationPositionForm(request.POST, 
                                                 request.FILES)
            if form.is_valid():
                appl_position = form.get_direct_application_position()
                appl_position.year = year
                appl_position.save()

                request.session['notice'] = u'เพิ่มรายชื่อสถานประกอบการแล้ว'
                return redirect('direct_appl_regis_company_list')
        else:
            request.session['notice'] = u'ยกเลิกการเพิ่มรายชื่อ'
            return redirect('direct_appl_regis_company_list')

    else:
        form = DirectApplicationPositionForm()

    return render_to_response("direct_appl/regis/add.html",
                              { 'year': year,
                                'form': form },
                              context_instance=RequestContext(request))

@sa_admin_required
def company_edit(request, position_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    year = direct_position.year

    if request.method=='POST':
        if 'delete' in request.POST:
            direct_position.delete()
            request.session['notice'] = u'ลบสถานประกอบการออกจากรายการแล้ว'
            return redirect('direct_appl_regis_company_list')

        if 'cancel' not in request.POST:
            form = DirectApplicationPositionForm(request.POST,
                                                 request.FILES,
                                                 instance=direct_position)
            if form.is_valid():
                appl_position = form.get_direct_application_position()
                appl_position.year = year
                appl_position.save()

                request.session['notice'] = u'จัดเก็บการแก้ไขเรียบร้อย'
                return redirect('direct_appl_regis_company_list')
        else:
            request.session['notice'] = u'ยกเลิกการแก้ไข'
            return redirect('direct_appl_regis_company_list')

    else:
        form = (DirectApplicationPositionForm(
                instance=direct_position,
                initial={ 'company_name':
                              direct_position.company.name }))

    return render_to_response("direct_appl/regis/edit.html",
                              { 'year': year,
                                'form': form },
                              context_instance=RequestContext(request))


class StudentSearchForm(forms.Form):
    student_id = forms.CharField(label=u'รหัสนิสิต',required=False)
    student_name = forms.CharField(label=u'ชื่อ',required=False)
    departments = Department.objects.all()
    DEPARTMENT_CHOICE = [[0,u'ทุกภาควิชา']]
    for department in departments:
        DEPARTMENT_CHOICE.append([department.id,department.name])

    student_department = forms.ChoiceField(choices=DEPARTMENT_CHOICE,label=u'ภาควิชา',required=False)

@sa_admin_required
def company_student_search(request, position_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    year = direct_position.year
    company = direct_position.company

    students = None
    is_too_many_results = False

    if request.method == 'POST':
        form = StudentSearchForm(request.POST)
        if form.is_valid():
            students = Student.all_by_id_name_department(
                student_id=form.cleaned_data['student_id'],
                name=form.cleaned_data['student_name']
                )
            if students.count() > 30:
                is_too_many_results = True
                students = None
    else:
        form = StudentSearchForm()

    return render_to_response("direct_appl/regis/company_student_search.html",
                              { 'year': year,
                                'company': company,
                                'direct_position': direct_position,
                                'form': form,
                                'students': students,
                                'too_many_results': is_too_many_results,
                                'currentpage': 'company' },
                              context_instance=RequestContext(request))


@sa_admin_required
def company_apply(request, position_id, student_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    student = get_object_or_404(Student,
                                student_id=student_id)

    if not student.is_eligible_to_apply_more_direct_application():
        request.session['notice'] = u'นิสิตไม่สามารถสมัครเข้ารับการคัดเลือกได้เพิ่ม'
    else:
        # make sure there is no dupplicate
        old_app_count = (StudentApplication
                         .objects.filter(student=student,
                                         position=direct_position)
                         .count())

        if old_app_count == 0:
            application = StudentApplication(student=student,
                                             position=direct_position)
            application.save()
        request.session['notice'] = u'เก็บข้อมูลการสมัครเรียบร้อย'

    return redirect('direct_appl_regis_company_show',position_id)

@sa_admin_required
def company_cancel(request, position_id, application_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    application = get_object_or_404(StudentApplication,
                                    pk=application_id)
    if application.is_accepted:
        request.session['notice'] = (u'ไม่สามารถยกเลิกการสมัครของ %s %s ได้เนื่องจากบริษัทได้ตอบรับแล้ว ถ้าต้องการยกเลิกให้ดำเนินการที่หน้าข้อมูลสถานประกอบการ' %
                                     (application.student.student_id,
                                      application.student.full_name()))
    else:
        application.delete()
        request.session['notice'] = (u'ยกเลิกการสมัครของ %s %s แล้ว' %
                                     (application.student.student_id,
                                      application.student.full_name()))
    return redirect('direct_appl_regis_company_show',position_id)


def update_assignment_status(app, year):
    assignment = app.student.assignment_for_year(year)
    if assignment!=None:
        if assignment.company != app.position.company:
            app.student.assigned_company = assignment.company
        else:
            app.student.assigned_company = None
    else:
        app.student.assigned_company = None


@sa_admin_required
def company_show(request, position_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    year = direct_position.year

    company = direct_position.company
    requirements = direct_position.appl_requirements

    applications = StudentApplication.all_for_position(direct_position).select_related('student')

    for a in applications:
        update_assignment_status(a,year)

    notice = extract_notice(request.session)

    return render_to_response("direct_appl/regis/show.html",
                              { 'year': year,
                                'company': company,
                                'requirements': requirements,
                                'direct_position': direct_position,
                                'applications': applications,
                                'notice': notice,
                                'currentpage': 'company' },
                              context_instance=RequestContext(request))


def update_other_applications(app, year):
    student = app.student

    all_applications = student.studentapplication_set.all()
    outstanding_apps = [a for a in all_applications
                        if (a != app) and (a.is_accepted == None) and (a.position.year==year)]

    app.outstanding_apps = outstanding_apps


def validate_application(app, position, result):
    current_assignment = app.student.assignment_for_year(position.year)

    if (result != None) and (current_assignment):
        if current_assignment.company != position.company:
            return [u'%s %s ได้รับคัดเลือกให้ฝึกงานที่ %s แล้ว' % (app.student.full_name(),
                                                         app.student.student_id,
                                                         current_assignment.company.name)]
    return []

def process_application(app, position, result):
    current_assignment = app.student.assignment_for_year(position.year)

    if result:
        if not current_assignment:
            assignment = Assignment(student=app.student,
                                    company=position.company,
                                    year=position.year)
            assignment.save()
        app.is_accepted = True
        app.save()

        send_direct_approval_confirmation(app)

    elif result == False:
        if current_assignment:
            current_assignment.delete()
        
        app.is_accepted = False
        app.save()

        send_direct_approval_confirmation(app)

    else:
        app.is_accepted = None
        app.save()

        if current_assignment:
            if current_assignment.company == position.company:
                current_assignment.delete()
                send_direct_approval_confirmation(app)


@sa_admin_required
def company_approve(request, position_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    year = direct_position.year

    company = direct_position.company
    requirements = direct_position.appl_requirements

    applications = StudentApplication.all_for_position(direct_position).select_related('student')

    for a in applications:
        update_assignment_status(a,year)
        update_other_applications(a,year)

    notice = extract_notice(request.session)
    error_msg = ""

    if request.method=='POST':
        if "ok" in request.POST:
            errors = []
            for a in applications:
                
                if ("yes_app_%d" % a.id) in request.POST:
                    errors += validate_application(a, direct_position, True)
                elif ("no_app_%d" % a.id) in request.POST:
                    errors += validate_application(a, direct_position, False)
                else:
                    errors += validate_application(a, direct_position, None)

            if errors == []:
                for a in applications:
                    if ("yes_app_%d" % a.id) in request.POST:
                        process_application(a, direct_position, True)
                    elif ("no_app_%d" % a.id) in request.POST:
                        process_application(a, direct_position, False)
                    else:
                        process_application(a, direct_position, None)

                    Log.create("approve direct application - from: %s, student: %s, app: %d" %
                               (request.META['REMOTE_ADDR'],
                                a.student.student_id,
                                a.id),
                               user=request.user,
                               student=a.student)

                request.session['notice'] = u'จัดเก็บข้อมูลผลการสมัครเรียบร้อย'
                return redirect("direct_appl_regis_company_show",position_id)
            else:
                error_msg = '\n'.join(errors)

        else:
            request.session['notice'] = u'ยกเลิกการเก็บข้อมูลผลการสมัคร'
            return redirect("direct_appl_regis_company_show",position_id)

    return render_to_response("direct_appl/regis/approve.html",
                              { 'year': year,
                                'company': company,
                                'requirements': requirements,
                                'direct_position': direct_position,
                                'applications': applications,
                                'notice': notice,
                                'error_msg': error_msg,
                                'currentpage': 'company' },
                              context_instance=RequestContext(request))


@sa_admin_required
def company_toggle(request, position_id):
    direct_position = get_object_or_404(DirectApplicationPosition,
                                        pk=position_id)
    direct_position.is_hidden = not direct_position.is_hidden
    direct_position.save()

    if direct_position.is_hidden:
        result = "true"
    else:
        result = "false"

    return HttpResponse(result);



