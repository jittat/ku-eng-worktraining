from django.contrib import admin

from models import DirectApplicationPosition, StudentApplication

admin.site.register(DirectApplicationPosition)
admin.site.register(StudentApplication)

