from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'direct_appl.views',

    # regis views
    url(r'^company/list/', 'regis_views.company_list',
        name='direct_appl_regis_company_list'),
    url(r'^company/add/', 'regis_views.company_add',
        name='direct_appl_regis_company_add'),
    url(r'^company/(\d+)/edit/$', 'regis_views.company_edit',
        name='direct_appl_regis_company_edit'),
    url(r'^company/(\d+)/$', 'regis_views.company_show',
        name='direct_appl_regis_company_show'),
    url(r'^company/(\d+)/search/$', 'regis_views.company_student_search',
        name='direct_appl_regis_company_student_search'),
    url(r'^company/apply/(\d+)/(\d+)/', 'regis_views.company_apply',
        name='direct_appl_regis_company_apply'),
    url(r'^company/cancel/(\d+)/(\d+)/', 'regis_views.company_cancel',
        name='direct_appl_regis_company_cancel'),
    url(r'^company/(\d+)/approve/$', 'regis_views.company_approve',
        name='direct_appl_regis_company_approve'),
    url(r'^company/(\d+)/toggle/$', 'regis_views.company_toggle',
        name='direct_appl_regis_company_toggle'),

    # student views
    url(r'^std/companies/', 'std_views.list',
        name='direct_appl_std_list'),
    url(r'^std/company/(\d+)/', 'std_views.show',
        name='direct_appl_std_show'),
    url(r'^std/applications/', 'std_views.show_applications',
        name='direct_appl_std_show_applications'),
)
