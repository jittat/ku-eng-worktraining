# -*- coding: utf-8 -*-
from django.db import models, IntegrityError
from regis.models import Student, Company, Assignment, Receipt, ConfigLetterBase, Department
from commons.models import Configuration
from commons.academia import get_next_worktraining_year
import hashlib
import random
from datetime import datetime
from django.conf import settings

class StudentExtraPassword(models.Model):
    student_id = models.CharField(max_length=15,
                                  verbose_name=u'รหัสนิสิต',
                                  unique=True)
    hashed_password = models.CharField(max_length=128)
    salt = models.CharField(max_length=10)

    @staticmethod
    def set_password_for(student_id, password):
        try:
            passwd = StudentExtraPassword.objects.get(student_id=student_id)
        except StudentExtraPassword.DoesNotExist:
            passwd = StudentExtraPassword(student_id=student_id)

        passwd.salt = str(random.random())[-5:]
        passwd.hashed_password = hashlib.sha1(passwd.salt + password).hexdigest()
        passwd.save()

    @staticmethod
    def check_password_for(student_id, password):
        try:
            passwd = StudentExtraPassword.objects.get(student_id=student_id)
        except StudentExtraPassword.DoesNotExist:
            return False

        try:
            return (passwd.hashed_password == 
                    hashlib.sha1(passwd.salt + password).hexdigest())
        except:
            return False

class StudentTokenNonce(models.Model):
    student_id = models.CharField(max_length=15)
    nonce = models.CharField(max_length=128)

    class Meta:
        unique_together = [['student_id','nonce']]

    @staticmethod
    def check_and_save(student_id, nonce):
        if len(nonce) < 8:
            return False
        
        count = StudentTokenNonce.objects.filter(student_id=student_id,
                                                 nonce=nonce).count()
        if count != 0:
            return False

        n = StudentTokenNonce(student_id=student_id,
                              nonce=nonce)
        try:
            n.save()
            return True
        except:
            return False

    @staticmethod
    def check_token_hash(student_id, nonce, token_hash):
        key = nonce + student_id + settings.TOKEN_SECRET
        key_hash = hashlib.sha1(key).hexdigest()[:20]
        return token_hash == key_hash

    @staticmethod
    def generate_token(student_id, nonce='', secret=''):
        if nonce == '':
            nonce = ''.join([random.choice('0123456789abcdefghijklmnopqrstuvwxyz') for i in range(10)])
        if secret == '':
            secret = settings.TOKEN_SECRET
        key = nonce + student_id + secret
        key_hash = hashlib.sha1(key).hexdigest()[:20]
        return student_id + '_' + nonce + '_' + key_hash


ALP = '0123456789'

class EmailConfirmation(models.Model):
    student = models.OneToOneField(Student, 
                                   related_name='email_confirmation')
    is_confirmed = models.BooleanField()
    requested_at = models.DateTimeField()
    confirmed_at = models.DateTimeField(null=True)
    code = models.CharField(max_length=100,
                            unique=True)

    @staticmethod
    def random_confirmation_code():
        return ''.join([random.choice(ALP) for i in range(20)])

    @staticmethod
    def create_or_update_for(student):
        try:
            confirmation = student.email_confirmation
        except EmailConfirmation.DoesNotExist:
            confirmation = EmailConfirmation()

        confirmation.student = student
        confirmation.is_confirmed = False
        confirmation.requested_at = datetime.now()
        confirmation.confirmed_at = None

        for i in range(10):
            confirmation.code = EmailConfirmation.random_confirmation_code()
            try:
                confirmation.save()
            except IntegrityError:
                continue
            break

        return confirmation

    @staticmethod
    def confirm_using(code):
        try:
            c = EmailConfirmation.objects.get(code=code)
        except EmailConfirmation.DoesNotExist:
            c = None
        if c:
            if not c.is_confirmed:
                c.is_confirmed = True
                c.confirmed_at = datetime.now()
                c.save()
                return (c.student, False)
            else:
                return (c.student, True)
        else:
            return (None, False)

    def get_confirmation_url(self):
        #return 'http://localhost:8000/std/confirm_email/%s' % self.code
        return 'http://wt.eng.ku.ac.th/wt/std/confirm_email/%s/' % self.code


class SelectionRound(models.Model):
    number = models.IntegerField(verbose_name=u"รอบ")
    beginning_time = models.DateTimeField(verbose_name=u'วันและเวลาที่เปิดให้นิสิตเลือก')
    end_time = models.DateTimeField(verbose_name=u'วันและเวลาสิ้นสุดการเลือก')

    year = models.IntegerField(verbose_name=u'ปีการศึกษา')

    departments = models.ManyToManyField(Department)
    
    class Meta:
        ordering = ['-year','number']
    
    def __unicode__(self):
        return "Round %d of %d (%s - %s)" % (self.number, self.year, self.beginning_time, self.end_time)

    @staticmethod
    def get_round(number, year=None):
        if year == None:
            year = get_next_worktraining_year()
        try:
            r = SelectionRound.objects.get(number=number,year=year)
        except SelectionRound.DoesNotExist:
            return None
        return r

    @staticmethod
    def get_all_rounds(year=None):
        if year == None:
            year = get_next_worktraining_year()
        return SelectionRound.objects.filter(year=year)
    

class DepartmentAdditionalSelectionRound(models.Model):
    year = models.IntegerField(verbose_name=u'ปีการศึกษา')
    number = models.IntegerField(verbose_name=u'รอบที่เห็น')
    beginning_time = models.DateTimeField(verbose_name=u'วันและเวลาที่เปิดให้นิสิตเลือก')
    end_time = models.DateTimeField(verbose_name=u'วันและเวลาสิ้นสุดการเลือก')
    department = models.ForeignKey(Department)
    
    def __unicode__(self):
        return u"Round %d of %d (%s - %s) (%s)" % (self.number, self.year, self.beginning_time, self.end_time, self.department)

    @staticmethod
    def get_for_department(year, department, now=None):
        rounds = (DepartmentAdditionalSelectionRound.
                  objects.
                  filter(year=year,
                         department=department).
                  all())

        if now:
            t = now
        else:
            t = datetime.now()
        for r in rounds:
            if (r.beginning_time <= t) and (t <= r.end_time):
                return r
        return None
            
    @staticmethod
    def get_for_department_as_selection_round(year, department):
        r = DepartmentAdditionalSelectionRound.get_for_department(year, 
                                                                  department)
        if r:
            fake_round = SelectionRound()
            fake_round.number = r.number
            fake_round.beginning_time = r.beginning_time
            fake_round.end_time = r.end_time
            fake_round.year = r.year
            return fake_round
        else:
            return None


class StudentSelection(models.Model):
    student = models.ForeignKey(Student,
                                related_name='selections')
    company = models.ForeignKey(Company,
                                related_name='student_selections')
    receipt = models.ForeignKey(Receipt,
                                related_name='student_selections',
                                null=True,
                                blank=True,
                                default=None)
    round_number = models.IntegerField(verbose_name=u'หมายเลขรอบ')
    year = models.IntegerField(verbose_name=u'ปีการศึกษา')
    selection_at = models.DateTimeField(verbose_name=u'เวลาที่นิสิตเลือก')
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=u'เวลาที่นิสิตเลือกเป็นครั้งแรก')
    modified_at = models.DateTimeField(auto_now=True,
                                       verbose_name=u'เวลาแก้ไขล่าสุด')
    result = models.NullBooleanField(null=True,
                                     verbose_name=u'ผลการคัดเลือก')
    verification_token = models.CharField(max_length=20,
                                          blank=True,
                                          default='')

    extra_ranking = models.IntegerField(default=0,
                                        verbose_name=u'สำหรับจัดเรียงกรณีที่เวลาตรงกัน')

    is_imported = models.BooleanField(default=False,
                                      verbose_name=u'เป็นการเลือกที่ถูกนำเข้าจากเฟสบุ๊ค')

    class Meta:
        ordering = ['selection_at', 'extra_ranking']

    def __unicode__(self):
        return self.student.full_name()
    
    def get_token(self):
        if self.verification_token=='':
            return ''

        from django.utils.hashcompat import sha_constructor
        hash = sha_constructor("%d-%d-%d-%d-%s" %
                               (self.year,
                                self.round_number,
                                self.student.id,
                                self.company.id,
                                self.verification_token)).hexdigest()[:20]
        return hash


    def save(self, *args, **kwargs):
        self.verification_token = ''.join([str(random.random())[-4:] for i in range(4)])
        super(StudentSelection, self).save(*args, **kwargs) 


    def save_to_assignment(self):
        if not self.result:
            return

        student = self.student
        current_assignment = student.assignment_for_year(self.year)
        if not current_assignment:
            current_assignment = Assignment(year=self.year,
                                            student=student)
        current_assignment.company = self.company
        current_assignment.receipt = self.receipt
        current_assignment.save()


    @staticmethod
    def get_for_student(student, round_number, year):
        selections = StudentSelection.objects.filter(student=student,
                                                     round_number=round_number,
                                                     year=year)
        if len(selections)==0:
            return None
        else:
            return selections[0]


    @staticmethod
    def all_for_receipt(receipt, year, round_number=None, department=None):
        company = receipt.company
        selections = (StudentSelection
                      .objects
                      .filter(receipt=receipt,
                              year=year))
        if round_number!=None:
            selections = selections.filter(round_number=round_number)
        if department!=None:
            selections = selections.filter(student__department__exact=department)
        return selections.select_related('student')
    
    @staticmethod
    def all_for_department(year, round_number=None, department=None):
        selections = (StudentSelection
                      .objects
                      .filter(year=year))
        if round_number!=None:
            selections = selections.filter(round_number=round_number)
        if department!=None:
            selections = selections.filter(student__department__exact=department)
        return selections.select_related('student')


                      
class SelectionRoundStatus(models.Model):
    is_open = models.BooleanField(verbose_name=u'เปิดให้เลือกสถานประกอบการ')
    current_round_number = models.IntegerField(verbose_name=u'รอบปัจจุบัน',
                                               null=True)
    next_round_number = models.IntegerField(verbose_name=u'รอบถัดไป',
                                            null=True)
    year = models.IntegerField()

    __current_round = None

    @staticmethod
    def get_status(year=None, department=None):
        if year==None:
            year = get_next_worktraining_year()

        try:
            status = SelectionRoundStatus.objects.get(year=year)
            status.is_fake_round = False

            if status.is_open and department!=None:
                current_round = status.get_current_round(status=status, department=department)
                departments = current_round.departments.all()
                if len(departments) != 0:
                    if department.id not in [d.id for d in departments]:
                        status.is_open = False
            
            if not status.is_open and department:
                additional_round = (DepartmentAdditionalSelectionRound.
                                    get_for_department_as_selection_round(status.year,
                                                                          department))
                if additional_round:
                    status.is_open = True
                    status.current_round_number = additional_round.number
                    status.next_round_number = additional_round.number + 1
                    status.is_fake_round = True
                    status.fake_round = additional_round
                    return status
            return status
        except:
            return None

    @staticmethod
    def update(now, fake=False):
        status = SelectionRoundStatus.get_status()
        if not status:
            year = get_next_worktraining_year()
            status = SelectionRoundStatus(year=year)

        status.is_open = False
        status.next_round_number = None

        year = get_next_worktraining_year()
        for selround in SelectionRound.objects.filter(year=year).all():
            if now >= selround.beginning_time and now < selround.end_time:
                status.is_open = True
                status.current_round_number = selround.number
            if (now < selround.beginning_time and 
                status.next_round_number == None):
                status.next_round_number = selround.number

        if not fake:
            status.save()

        return status

    @staticmethod
    def get_current_round(status=None, department=None):
        if not status:
            status = SelectionRoundStatus.get_status(department=department)
        if not status or (not status.is_open):
            return None

        try:
            if status.is_fake_round:
                return status.additional_round
        except:
            pass

        if (not SelectionRoundStatus.__current_round or 
            (SelectionRoundStatus.__current_round.number != 
             status.current_round_number)):

            SelectionRoundStatus.__current_round = (
                SelectionRound.get_round(status.current_round_number))

        return SelectionRoundStatus.__current_round


    @staticmethod
    def get_next_round(status=None):
        if not status:
            status = SelectionRoundStatus.get_status()
        if not status or (not status.next_round_number):
            return None
        else:
            return SelectionRound.get_round(status.next_round_number)

    @staticmethod
    def get_last_round():
        status = SelectionRoundStatus.get_status()
        if not status.next_round_number:
            rounds = list(SelectionRound.get_all_rounds().all())
            if len(rounds)==0:
                return None
            else:
                return rounds[-1]
        else:
            if status.next_round_number!=1:
                return SelectionRound.get_round(status.next_round_number - 1)
            else:
                return None

    @staticmethod
    def is_approval_period_for_round(sel_round):
        status = SelectionRoundStatus.get_status()
        last_round = SelectionRoundStatus.get_last_round()
        return (not status.is_open and
                last_round and 
                last_round.id == sel_round.id)


def assign_selections_to_receipts(receipts, round_number=None, department=None):
    if len(receipts)==0:
        return

    for r in receipts:
        r.selections = []

    #companies = [r.company for r in receipts]
    year = receipts[0].year

    student_selections = (StudentSelection
                          .objects
                          .filter(receipt__in=receipts,
                                  year=year)
                          .select_related('student'))

    if round_number:
        student_selections = student_selections.filter(round_number=round_number)

    if department:
        student_selections = student_selections.filter(student__department__exact=department)

    rmaps = dict([(r.id,r) for r in receipts])
    for sel in student_selections:
        if sel.receipt_id in rmaps:
            r = rmaps[sel.receipt_id]
            r.selections.append(sel)


def assign_company_address_to_receipts(receipts):
    for r in receipts:
        if not r.work_address:
            r.work_address = r.company.address


class SelectionRoundForReceipt(models.Model):
    receipt = models.ForeignKey(Receipt, related_name='selection_round_set')
    round_number = models.IntegerField()
    year = models.IntegerField()

    @staticmethod
    def create_one_for_receipt_and_round(receipt, round_number, year):
        try:
            s = (SelectionRoundForReceipt.
                 objects.
                 get(receipt=receipt,
                     year=year,
                     round_number=round_number))
        except SelectionRoundForReceipt.DoesNotExist:
            s = SelectionRoundForReceipt(receipt=receipt,
                                         year=year,
                                         round_number=round_number)
            s.save()        

    @staticmethod
    def update_receipts_round_shown(selection_round):
        year = get_next_worktraining_year()
        receipts = Receipt.all_for_round(selection_round)
        for r in receipts:
            if r.is_shown_to_students():
                (SelectionRoundForReceipt.
                 create_one_for_receipt_and_round(r, 
                                                  selection_round.number, 
                                                  year))

        for r in Receipt.objects.filter(year=year):
            rounds_shown = r.selection_round_set.count()
            if r.num_rounds_shown != rounds_shown:
                r.num_rounds_shown = rounds_shown
                r.save()
            

class VerifiedDirectApplicationCompany(models.Model):
    year = models.IntegerField(verbose_name=u'ปีการศึกษา')
    company = models.ForeignKey('regis.Company')

    @staticmethod
    def check_for_year(company, year):
        try:
            c = VerifiedDirectApplicationCompany.objects.get(company=company,
                                                             year=year)
            return True
        except VerifiedDirectApplicationCompany.DoesNotExist:
            return False

    @staticmethod
    def get_dict_for_years(year):
        companies = {}
        for v in (VerifiedDirectApplicationCompany
                 .objects
                 .filter(year=year)
                 .select_related('company')
                 .all()):
            companies[v.company.id] = v.company
        return companies


class CompanyDirectApplication(models.Model):
    year = models.IntegerField(verbose_name=u'ปีการศึกษา')
    student = models.ForeignKey('regis.Student',
                                related_name='company_direct_applications')
    company_name = models.CharField(max_length=200,
                                    verbose_name=u'ชื่อหน่วยงาน')
    company = models.ForeignKey('regis.Company', 
                                default=None,
                                null=True)

    created_at = models.DateTimeField()

    is_accepted = models.NullBooleanField()
    accepted_at = models.DateTimeField(null=True, default=None, blank=True)

    is_chosen = models.NullBooleanField()
    chosen_at = models.DateTimeField(null=True, default=None, blank=True)

    is_confirmed_by_committee = models.NullBooleanField()
    confirmed_at = models.DateTimeField(null=True, default=None, blank=True)
    
    is_letter_downloaded = models.BooleanField(default=False)
    letter_downloaded_at = models.DateTimeField(null=True, blank=True)

    is_sending_letter_printed = models.BooleanField(default=False,
                                                    verbose_name=u'พิมพ์จดหมายส่งตัวแล้ว')
    sending_letter_printed_at = models.DateTimeField(null=True)

    class Meta:
        ordering = ['created_at']

    def is_waiting(self):
        return self.is_accepted == None

    def is_active(self):
        if (self.is_accepted == False) and (self.is_confirmed_by_committee):
            return False
        elif self.is_accepted and (self.is_chosen == False) and (self.is_confirmed_by_committee):
            return False
        else:
            return True

    def get_status_message_for_committee(self):
        if self.is_accepted == False:
            return u"หน่วยงานไม่รับ: %s" % self.company_name
        if self.is_accepted:
            if self.is_chosen == False:
                return u"ปฏิเสธหน่วยงาน: %s" % self.company_name
            if self.is_chosen and (not self.is_confirmed_by_committee):
                return u"รอการอนุมัติ: %s" % self.company_name
        else:
            return u"รอผล: %s" % self.company_name

    @staticmethod
    def get_all_for(student,year):
        apps = student.company_direct_applications.filter(year=year).all()
        return apps

    @staticmethod
    def get_active_for(student,year):
        apps = CompanyDirectApplication.get_all_for(student, year)
        active_apps = [a for a in apps if a.is_active()]
        return active_apps

    @staticmethod
    def all_for_department(department, year):
        results = (CompanyDirectApplication
                   .objects
                   .filter(student__department=department)
                   .filter(year=year)
                   .select_related('student')
                   .all())
        return results

    def can_be_cancelled(self):
        if self.is_accepted != None:
            return False

        from datetime import timedelta
        now = datetime.now()
        return (not self.is_letter_downloaded) and (now <= self.created_at + timedelta(days=2))

    def is_verified(self):
        if self.company!=None:
            company = app.company
        else:
            companies = Company.objects.filter(name=self.company_name).all()
            if len(companies)!=0:
                company = companies[0]
            else:
                company = None
        return ((company) and 
                (VerifiedDirectApplicationCompany.check_for_year(company,self.year)))

    def is_accepted_and_confirmed(self):
        return (self.is_accepted and 
                self.is_chosen and 
                self.is_confirmed_by_committee)

    def default_letter_config(self, cls=None):
        try:
            detail = self.detail
        except:
            detail = None

        if cls:
            config = cls()
        else:
            config = ConfigLetterBase()

        config.year = self.year
        config.letter_number = Configuration.get('default.sending_letter_number')
        config.letter_date = Configuration.get('default.sending_letter_date')
        if detail:
            config.signer_name = detail.signer_name
            if detail.is_default_schedule:
                config.beginning_date = Configuration.get('default.letter_beginning_date')
                config.end_date = Configuration.get('default.letter_end_date')
            else:
                config.beginning_date = detail.beginning_date
                config.end_date = detail.end_date
        else:
            config.signer_name = u'ผู้จัดการฝ่ายทรัพยากรบุคคล'
            config.beginning_date = Configuration.get('default.letter_beginning_date')
            config.end_date = Configuration.get('default.letter_end_date')
        config.eval_date = Configuration.get('default.eval_end_date')
        return config

    def delete_letter_config(self):
        try:
            config = self.letter_config
            config.delete()
        except:
            pass

    def get_letter_config_or_default(self, cls=None, save=False):
        try:
            return self.letter_config
        except:
            letter_config = self.default_letter_config(cls)
            if save:
                letter_config.application = self;
                letter_config.save()
            return letter_config

    def get_temp_company(self):
        try:
            config = self.letter_config
        except:
            config = self.default_letter_config()

        try:
            detail = self.detail
        except:
            detail = None

        company = Company()
        company.name = self.company_name
        company.signer_name = config.signer_name
        if detail:
            company.address = detail.letter_address
        else:
            company.address = ''
        return company


class CompanyRequest(models.Model):
    year = models.IntegerField(verbose_name=u'ปีการศึกษา')
    requested_at = models.DateTimeField()

    student = models.ForeignKey('regis.Student')
    company_name = models.CharField(max_length=200,
                                    verbose_name=u'ชื่อหน่วยงาน')
    company = models.ForeignKey('regis.Company',null=True)
    division_name = models.CharField(max_length=100,
                                       blank=True,
                                       verbose_name=u'หน่วยงานย่อย')
    letter_address = models.TextField(verbose_name=u'ที่อยู่สำหรับส่งหนังสือ')
    signer_name = models.CharField(max_length=100,
                                   verbose_name=u'ชื่อหรือตำแหน่งที่ทำจดหมายถึง')
    
    contact_person = models.CharField(max_length=100,
                                      verbose_name=u'ผู้ประสานงาน')
    contact_tel_no = models.CharField(max_length=50,
                                      verbose_name=u'เบอร์โทรศัพท์ผู้ประสานงาน')
    contact_email = models.EmailField(max_length=100,
                                      verbose_name=u'อีเมล์ผู้ประสานงาน')
    
    working_address = models.TextField(verbose_name=u'สถานที่ตั้ง')
    work_tel_no = models.CharField(max_length=50,
                                   verbose_name=u'เบอร์โทรศัพท์')    
    work_fax_no = models.CharField(max_length=50,
                                   verbose_name=u'เบอร์โทรสาร')
 
    description = models.TextField(verbose_name=u'ลักษณะงาน')

    is_default_schedule = models.BooleanField(verbose_name=u'ฝึกงานตามกำหนดที่คณะกำหนด')
    beginning_date = models.DateField(blank=True,
                                      null=True,
                                      verbose_name=u'วันที่เริ่มต้นฝึกงาน')
    end_date = models.DateField(blank=True,
                                null=True,
                                verbose_name=u'วันที่สิ้นสุดการฝึกงาน')

    has_all_confirmed = models.BooleanField(verbose_name=u'ยืนยันครบแล้ว')

    is_approved_by_committee = models.NullBooleanField(verbose_name=u'ได้รับการอนุมัติ')
    approved_at = models.DateTimeField(null=True)
    reject_comments = models.TextField(null=True,
                                       verbose_name=u'สาเหตุที่กรรมการไม่อนุมัติ')

    is_accepted = models.NullBooleanField(verbose_name=u'หน่วยงานตอบรับ')
    accepted_at = models.DateTimeField(null=True)

    is_sending_letter_printed = models.BooleanField(default=False,
                                                    verbose_name=u'พิมพ์จดหมายส่งตัวแล้ว')
    sending_letter_printed_at = models.DateTimeField(null=True)

    class Meta:
        ordering = ['requested_at']

    @staticmethod
    def can_request(student, year, regtype_confirmed_required):
        status = student.get_status(year)
        if not status:
            return False

        if not status.is_registered_for_self_application():
            return False

        if (regtype_confirmed_required and 
            (not status.is_self_appl_confirmed)):
            return False

        requests = student.request_set.filter(year=year).all()
    
        for r in requests:
            if r.is_approved:
                if r.is_approved_by_committee == None:
                    return False

                if r.is_approved_by_committee and r.is_accepted != False:
                    return False

        direct_apps = student.company_direct_applications.filter(year=year).all()
        for a in direct_apps:
            if a.is_accepted != False:
                return False

        return True

    @staticmethod
    def all_for_department(department, year, has_confirmed=None):
        results = (CompanyRequest
                   .objects
                   .filter(student__department=department)
                   .filter(year=year)
                   .select_related('student')
                   .all())
        if has_confirmed:
            results = results.filter(has_all_confirmed=True)
        return results


    def update_confirm_status(self):
        all_confirmed = True
        for item in self.students.all():
            if item.is_approved == None:
                all_confirmed = False
        if all_confirmed:
            self.has_all_confirmed = True
            self.save()

    def get_approved_students(self):
        return [rs.student for rs in self.students.all()
                if rs.is_approved]

    def get_accepted_students(self):
        return [rs.student for rs in self.students.all()
                if rs.is_approved and rs.is_approved_by_committee and rs.is_accepted ]

    def save_committee_approval_to_students(self, result):
        for rs in self.students.all():
            if rs.is_approved:
                rs.is_approved_by_committee = result
                rs.save()

    def default_letter_config(self, cls=None):
        if cls:
            config = cls()
        else:
            config = ConfigLetterBase()

        config.year = self.year
        config.letter_number = Configuration.get('default.sending_letter_number')
        config.letter_date = Configuration.get('default.sending_letter_date')
        config.signer_name = self.signer_name
        if self.is_default_schedule:
            config.beginning_date = Configuration.get('default.letter_beginning_date')
            config.end_date = Configuration.get('default.letter_end_date')
        else:
            config.beginning_date = self.beginning_date
            config.end_date = self.end_date
        config.eval_date = Configuration.get('default.eval_end_date')
        return config

    def delete_letter_config(self):
        try:
            config = self.letter_config
            config.delete()
        except:
            pass

    def get_letter_config_or_default(self, cls=None, save=False):
        try:
            return self.letter_config
        except:
            letter_config = self.default_letter_config(cls)
            if save:
                letter_config.company_request = self;
                letter_config.save()
            return letter_config

    def get_temp_company(self):
        try:
            config = self.letter_config
        except:
            config = self.default_letter_config()

        company = Company()
        company.name = self.company_name
        company.signer_name = config.signer_name
        company.address = self.letter_address
        return company


class RequestStudent(models.Model):
    year = models.IntegerField()
    company_request = models.ForeignKey('CompanyRequest', related_name='students')
    student = models.ForeignKey('regis.Student', related_name='request_set')
    is_owner = models.BooleanField()

    is_approved = models.NullBooleanField()
    approved_at = models.DateTimeField(null=True)

    is_approved_by_committee = models.NullBooleanField()

    is_accepted = models.NullBooleanField()
    accepted_at = models.DateTimeField(null=True)

    def is_active(self):
        if self.is_approved == None:
            return True
        if self.is_approved:
            if self.is_approved_by_committee == False:
                return False
            if self.is_accepted != False:
                return True
        return False

    @staticmethod
    def get_active_for(student, year):
        items = []
        for request_item in student.request_set.filter(year=year).all(): 
            if request_item.is_active():
                items.append(request_item)
        return items


class CompanyDirectApplicationDetail(models.Model):
    application = models.OneToOneField('std.CompanyDirectApplication',
                                       related_name='detail')
    student = models.ForeignKey('regis.Student', null=True)
    company = models.ForeignKey('regis.Company', 
                                default=None,
                                null=True)

    letter_address = models.TextField(verbose_name=u'ที่อยู่สำหรับส่งหนังสือ')
    signer_name = models.CharField(max_length=100,
                                   verbose_name=u'ชื่อหรือตำแหน่งที่ทำจดหมายถึง')
    
    contact_person = models.CharField(max_length=100,
                                      verbose_name=u'ผู้ประสานงาน')
    contact_tel_no = models.CharField(max_length=50,
                                      verbose_name=u'เบอร์โทรศัพท์ผู้ประสานงาน',
                                      blank=True)
    contact_email = models.EmailField(max_length=100,
                                      verbose_name=u'อีเมล์ผู้ประสานงาน',
                                      blank=True)
    
    working_address = models.TextField(verbose_name=u'สถานที่ตั้ง',
                                       blank=True,
                                       null=True,
                                       default=None)
    work_tel_no = models.CharField(max_length=50,
                                   verbose_name=u'เบอร์โทรศัพท์',
                                   blank=True,
                                   null=True,
                                   default=None)    
    work_fax_no = models.CharField(max_length=50,
                                   verbose_name=u'เบอร์โทรสาร',
                                   blank=True,
                                   null=True,
                                   default=None)
 
    is_default_schedule = models.BooleanField(verbose_name=u'ฝึกงานตามกำหนดที่คณะกำหนด')
    beginning_date = models.DateField(blank=True,
                                      null=True,
                                      verbose_name=u'วันที่เริ่มต้นฝึกงาน')
    end_date = models.DateField(blank=True,
                                null=True,
                                verbose_name=u'วันที่สิ้นสุดการฝึกงาน')

    created_at = models.DateTimeField(auto_now_add=True)

    
class CoOpStudentDetail(models.Model):
    student = models.ForeignKey('regis.Student')
    year = models.IntegerField()

    address = models.TextField(verbose_name=u'ที่อยู่')

    class Meta:
        unique_together = (('year','student'))

    @staticmethod
    def get_for_student_year(student, year):
        details = CoOpStudentDetail.objects.filter(student=student, year=year).all()
        if len(details)!=0:
            return details[0]
        else:
            return None

class CoOpCompany(models.Model):
    department = models.ForeignKey('regis.Department')
    year = models.IntegerField()

    name = models.CharField(max_length=200,
                            verbose_name=u'ชื่อหน่วยงาน')
    address = models.TextField(verbose_name=u'ที่อยู่')
    tel_number = models.CharField(max_length=50,
                                  verbose_name=u'เบอร์โทรศัพท์',
                                  blank=True,
                                  null=True,
                                  default=None)    
    fax_number = models.CharField(max_length=50,
                                  verbose_name=u'เบอร์โทรสาร',
                                  blank=True,
                                  null=True,
                                  default=None)

    is_public = models.BooleanField(verbose_name=u'แสดงให้นิสิตคนอื่นในภายวิชาเห็น',
                                    default=True)
    
    created_by = models.ForeignKey('regis.Student')
    created_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def get_for_department_year(department, year):
        return CoOpCompany.objects.filter(department=department, year=year)
    
    @staticmethod
    def get_visible_for_department_year(department, year, student):
        return [company for company in
                CoOpCompany.objects.filter(department=department, year=year)
                if company.is_public or company.created_by_id == student.id]

    def is_created_by(self, student):
        return self.created_by_id == student.id
    
    def is_info_editable(self):
        return self.cooprequest_set.count() == 0
    
class CoOpRequest(models.Model):
    student = models.ForeignKey('regis.Student')
    department = models.ForeignKey('regis.Department')
    year = models.IntegerField()

    internship_year = models.IntegerField()
    internship_semester = models.IntegerField()
    
    co_op_company = models.ForeignKey(CoOpCompany)

    description = models.TextField(default='',
                                   verbose_name=u'ลักษณะงาน')
    
    is_approved_by_committee = models.NullBooleanField(verbose_name=u'ได้รับการอนุมัติ')
    approved_at = models.DateTimeField(null=True)
    reject_comments = models.TextField(null=True,
                                       verbose_name=u'สาเหตุที่กรรมการไม่อนุมัติ')
    
    is_accepted = models.NullBooleanField(verbose_name=u'หน่วยงานตอบรับ')
    accepted_at = models.DateTimeField(null=True)

    is_sending_letter_printed = models.BooleanField(default=False,
                                                    verbose_name=u'พิมพ์จดหมายส่งตัวแล้ว')
    sending_letter_printed_at = models.DateTimeField(null=True)
  
    signer_name = models.CharField(max_length=100,
                                   verbose_name=u'ชื่อหรือตำแหน่งที่ทำจดหมายถึง')
    
    contact_person = models.CharField(max_length=100,
                                      verbose_name=u'ผู้ประสานงาน')
    contact_tel_number = models.CharField(max_length=50,
                                          verbose_name=u'เบอร์โทรศัพท์ผู้ประสานงาน',
                                          blank=True)
    contact_email = models.EmailField(max_length=100,
                                      verbose_name=u'อีเมล์ผู้ประสานงาน',
                                      blank=True)
    
    beginning_date = models.DateField(blank=True,
                                      null=True,
                                      verbose_name=u'วันที่เริ่มต้นฝึกงาน')
    end_date = models.DateField(blank=True,
                                null=True,
                                verbose_name=u'วันที่สิ้นสุดการฝึกงาน')

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['internship_year','internship_semester']
    
    @staticmethod
    def get_for_student_year(student, year):
        return CoOpRequest.objects.filter(student=student, year=year).all()

    @staticmethod
    def get_for_department_year(department, year):
        return CoOpRequest.objects.filter(department=department, year=year)

    def is_active(self):
        if self.is_approved_by_committee == False:
            return False
        if self.is_approved_by_committee and (self.is_accepted == False):
            return False
        return True
    
    def internship_semester_display(self):
        if self.internship_semester == 2:
            return u'ภาคปลาย'
        else:
            return u'ภาคต้น'

    def internship_semester_year_display(self):
        return self.internship_semester_display() + u' ปีการศึกษา ' + str(self.internship_year)

    def get_status_code(self):
        if self.is_approved_by_committee:
            if self.is_accepted:
                return 'company-accepted'
            elif self.is_accepted == False:
                return 'company-rejected'
            else:
                return 'committee-accepted'
        elif self.is_approved_by_committee == False:
            return 'committee-rejected'
        else:
            return 'committee-wait'

    
class CoOpSemester(models.Model):
    year = models.IntegerField()
    semester = models.IntegerField()
    request_deadline = models.DateField(verbose_name=u'วันตอบจดหมายขอความอนุเคราะห์')

    sending_letter_number = models.CharField(verbose_name=u'เลขที่จดหมายส่งตัว',
                                             max_length=100,
                                             default='',
                                             blank=True)
    sending_letter_date = models.DateField(verbose_name=u'วันที่จดหมายส่งตัว',
                                           default=None,
                                           blank=True,
                                           null=True)

    def __unicode__(self):
        return "{}-{}".format(self.year, self.semester)

    @staticmethod
    def get_for(year, semester):
        semesters = CoOpSemester.objects.filter(year=year, semester=semester).all()
        if len(semesters) > 0:
            return semesters[0]
        else:
            return None
    
    class Meta:
        ordering= ['-year','-semester']
        unique_together = (('year','semester'),)

    def is_info_ready_for_sending_letter(self):
        return ((self.sending_letter_number != '') and
                (self.sending_letter_date != None))
