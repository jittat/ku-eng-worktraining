# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('std', '0002_auto_20231201_1514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coopsemester',
            name='sending_letter_number',
            field=models.CharField(default=b'', max_length=100, verbose_name='\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e2a\u0e48\u0e07\u0e15\u0e31\u0e27', blank=True),
        ),
    ]
