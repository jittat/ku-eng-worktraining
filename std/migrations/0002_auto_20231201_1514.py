# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('std', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='coopsemester',
            name='sending_letter_date',
            field=models.DateField(default=None, null=True, verbose_name='\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e2a\u0e48\u0e07\u0e15\u0e31\u0e27', blank=True),
        ),
        migrations.AddField(
            model_name='coopsemester',
            name='sending_letter_number',
            field=models.TextField(default=b'', verbose_name='\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e2a\u0e48\u0e07\u0e15\u0e31\u0e27', blank=True),
        ),
    ]
