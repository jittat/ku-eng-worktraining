from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from regis.models import Student
from django.core.urlresolvers import reverse
from django.utils.http import urlquote

def std_login_required(view_function):

    def decorate(request, *args, **kwargs):
        if 'std_id' in request.session:
            # already authenticated
            student = get_object_or_404(Student, student_id=request.session['std_id'])
            request.student = student
            return view_function(request, *args, **kwargs)
        else:
            path = urlquote(request.get_full_path())
            return HttpResponseRedirect('%s?next=%s' %
                                        (reverse('std_login'),
                                         path))

    return decorate
