from django.contrib import admin

from models import SelectionRound
from models import DepartmentAdditionalSelectionRound
from models import CoOpSemester

class CoOpSemesterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'request_deadline','sending_letter_date', 'sending_letter_number')

admin.site.register(SelectionRound)
admin.site.register(DepartmentAdditionalSelectionRound)
admin.site.register(CoOpSemester, CoOpSemesterAdmin)
