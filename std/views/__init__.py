# -*- coding: utf-8 -*-
import time ,datetime
from datetime import date

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django import forms
from django.template import RequestContext
from django.shortcuts import redirect
from django.conf import settings

from regis.models import Student, Conditions, Status, Department, DepartmentNameInRequestLetter, Receipt, ConfigLetter, WorkDetail
from std.decorators import std_login_required
from evaluations.models import Evaluation
from direct_appl.models import DirectApplicationPosition, StudentApplication
from std.models import SelectionRoundStatus
from regis.company.views import default_student_letter_data, print_letter_pdf

from commons.academia import get_current_academic_year, get_next_worktraining_year
from commons import academia
from commons.models import Log
from commons.email import send_application_confirmation_email
from commons.utils import now_within_config_deadline, check_if_can_print_letter

currentpage = 'std'

def get_worktraining_data_for_year(student, year):
    result = { 'status': None,
               'sent_req_form': False,
               'attended_orientation': False,
               'registered': False,

               'receipt': None,
               'work_detail': None,

               'evaluation': None,
               'has_evaluation_form': False,
               'has_timesheet': False, }
    
    statuses = Status.objects.filter(student=student, year=year)
    if(statuses):
        result['status'] = statuses[0]
        result['sent_req_form'] = result['status'].sent_request_form
        result['attended_orientation'] = result['status'].attended_orientation
        result['registered'] = result['status'].is_registered

    result['direct_appls_count'] = StudentApplication.count_all_for_student(student, year)

    assignment = student.assignment_for_year(year)
    result['assignment'] = assignment
    if assignment:
        result['receipt'] = assignment.get_receipt()
        if assignment.company:
            result['work_detail'] = assignment.company.workdetail_for_year(year)

    
    evaluations = Evaluation.objects.filter(student=student, assignment=assignment)
    if(evaluations):
        result['evaluation'] = evaluations[0]
        result['has_evaluation_form'] = result['evaluation'].evaluation_form_received
        result['has_timesheet'] = result['evaluation'].timesheet_received

    return result

def check_apply_to_co_op_permission_after_deadline(student, academic_year, current_data=None):
    if not current_data:
        current_data = get_worktraining_data_for_year(student, academic_year)
    return current_data['has_evaluation_form'] and current_data['evaluation'].is_info_completed()

@std_login_required
def index(request):
    student = request.student
    department = student.department

    current_academic_year = get_current_academic_year()
    current_data = get_worktraining_data_for_year(student, current_academic_year)

    next_academic_year = get_next_worktraining_year()
    next_data = get_worktraining_data_for_year(student, next_academic_year)

    can_select_companies = student.is_eligible_to_apply_in_selection_round(next_academic_year)

    selection_round_status = SelectionRoundStatus.get_status(department=department)
    current_round = SelectionRoundStatus.get_current_round(selection_round_status)
    next_round = SelectionRoundStatus.get_next_round(selection_round_status)

    direct_position_count = DirectApplicationPosition.get_all_for_year(next_academic_year).count()

    application_deadline_passed = not now_within_config_deadline('deadline.applying')

    can_print_std_letter = check_if_can_print_letter(student, next_data['status'])
    letter_downloadable = settings.LETTER_DOWNLOADABLE

    can_apply_to_co_op_after_deadeline = check_apply_to_co_op_permission_after_deadline(student,
                                                                                        current_academic_year,
                                                                                        current_data)
    
    if (next_data['registered'] and
        next_data['status'].is_registered_for_self_application()
        and ((not next_data['assignment']) or (next_data['assignment'].is_self_application())) and
        (application_deadline_passed or next_data['status'].is_registration_type_confirmed)):
        return redirect('std_self_appl_index')

    if (next_data['registered'] and
        next_data['status'].is_registered_for_cooperative_education() and
        (application_deadline_passed or next_data['status'].is_registration_type_confirmed)):
        return redirect('std_coop_index')

    if 'std_index_notice' in request.session:
        notice = request.session.pop('std_index_notice')
    else:
        notice = ''

    return render_to_response("std/index.html",
                              { 'student': student,
                                'department': department,

                                'notice': notice,

                                'current_academic_year': current_academic_year,
                                'current_data': current_data,
                                
                                'next_academic_year': next_academic_year,
                                'next_data': next_data,

                                'application_deadline_passed': 
                                application_deadline_passed,

                                'direct_position_count':
                                    direct_position_count,

                                'can_select_companies':
                                    can_select_companies,
                                'selection_round_status':
                                    selection_round_status,
                                'current_round': current_round,
                                'next_round': next_round,

                                'can_print_std_letter':
                                can_print_std_letter,
                                'letter_downloadable':
                                letter_downloadable,

                                'can_apply_to_co_op_after_deadeline':
                                can_apply_to_co_op_after_deadeline,

                                'currentpage':currentpage },
                              context_instance=RequestContext(request))


@std_login_required
def choose_reg_type(request,reg_type):
    if request.method!='POST':
        return HttpResponseRedirect(reverse('std_index'))

    rtype = int(reg_type)
    if rtype not in [1,2,3]:
        return HttpResponseForbidden()

    next_academic_year = get_next_worktraining_year()
    status = request.student.get_status(next_academic_year)

    #if status.is_registration_type_confirmed:
    #    return HttpResponseForbidden()

    status.registration_type = rtype
    status.save()

    Log.create("Student reg type selected (%d) - from: %s, student: %s" %
               (rtype,
                request.META['REMOTE_ADDR'],
                request.student.student_id),
               student=request.student)

    return HttpResponse('ok')

@std_login_required
def confirm_reg_type(request,reg_type):
    if request.method!='POST':
        return HttpResponseRedirect(reverse('std_index'))

    rtype = int(reg_type)
    if rtype not in [0,1,2,3]:
        return HttpResponseRedirect(reverse('std_index'))

    next_academic_year = get_next_worktraining_year()
    status = request.student.get_status(next_academic_year)
    if rtype in [1,3]:
        status.registration_type = rtype
        status.is_registration_type_confirmed = True
    else:
        status.is_registration_type_confirmed = False
        
    status.save()

    Log.create("Student reg type confirmed (%d) - from: %s, student: %s" %
               (rtype,
                request.META['REMOTE_ADDR'],
                request.student.student_id),
               student=request.student)

    return redirect('std_self_appl_index')

@std_login_required
def company_list(request):
    student = request.student
    department = student.department

    current_academic_year = get_current_academic_year()
    next_academic_year = get_next_worktraining_year()

    current_round = SelectionRoundStatus.get_current_round()
    next_round = SelectionRoundStatus.get_next_round()

    if department:
        actual_department = DepartmentNameInRequestLetter.to_requested_department(department)
        available_receipts = [r for r in
                              Receipt.objects.filter(year=next_academic_year,
                                                     department=actual_department)
                              if r.is_shown_to_students()]
    else:
        available_receipts = []

    return render_to_response("std/company_list.html",
                              { 'student': student,
                                'department': department,
                                'current_academic_year':
                                    current_academic_year,
                                'next_academic_year':
                                    next_academic_year,

                                'current_round': current_round,
                                'available_receipts': available_receipts,

                                'currentpage':currentpage },
                              context_instance=RequestContext(request))


@std_login_required
def check_internship_status(request):
    student = request.student
    current_academic_year = get_current_academic_year()
    
    if (not student.is_personal_information_complete()) or (not student.has_confirmed_email()):
        return HttpResponseRedirect(reverse('std_index'))

    if not now_within_config_deadline('deadline.applying'):
        if ((not request.method=='POST') or
            ('accept_type3' not in request.POST) or
            (not check_apply_to_co_op_permission_after_deadline(student, current_academic_year))):
            return render_to_response("std/appl_deadline.html",
                                      {'student': student},
                                      context_instance=RequestContext(request))

    credit_validation = settings.TOTAL_CREDITS_VALIDATION

    if credit_validation:
        eligible = student.is_eligible_to_apply()
    else:
        eligible = True

    if not eligible:
        return render_to_response("std/appl_condition_failed.html",
                                  {'student': student,
                                   'required_credits':
                                       academia.REQUIRED_CREDITS_FOR_APPLICATION,
                                   'current_credits':
                                       student.current_credits() },
                                  context_instance=RequestContext(request))
    if request.method=='POST':
        if (('accept_type0' in request.POST) or
            ('accept_type1' in request.POST) or
            ('accept_type2' in request.POST) or
            ('accept_type3' in request.POST)):
            #student internship next academic year
            if 'accept_type0' in request.POST:
                reg_type = Status.NO_PREF_TYPE
                reg_type_message = u'(ยังไม่ได้เลือกว่าฝึกงานหรือสหกิจศึกษา)'
            elif 'accept_type1' in request.POST:
                reg_type = Status.OWN_SELECTION_TYPE
                reg_type_message = u'1 (ฝึกงานโดยนิสิตจัดหาหน่วยงานเอง)'
            elif 'accept_type2' in request.POST:
                reg_type = Status.FACULTY_SELECTION_TYPE
                reg_type_message = u'2 (ฝึกงานให้คณะจัดหน่วยงานให้)'
            else:
                reg_type = Status.CO_OP_SELECTION_TYPE
                reg_type_message = u'3 (สหกิจศึกษา)'
                
            status = student.save_registered_status(get_current_academic_year() + 1,reg_type)
            if status.registration_type == Status.OWN_SELECTION_TYPE:
                status.is_registration_type_confirmed = True
                status.save()
            send_application_confirmation_email(student,reg_type_message)
            Log.create("Student internship applied (type %d) - from: %s, student: %s" %
                       (reg_type,
                        request.META['REMOTE_ADDR'],
                        student.student_id),
                       student=student)
            return HttpResponseRedirect(reverse('std_index'))

        elif 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('std_index'))
    else:
        if not student.has_registered_for_next_year():
            conditions = Conditions.get_combined_conditions()
            return render_to_response("std/agreement.html",
                                      { 'student': student,
                                        'conditions':conditions,
                                        'credit_validation': credit_validation },
                                      context_instance=RequestContext(request))
        else:
            return HttpResponseRedirect(reverse('std_index'))
    

    


@std_login_required
def PrintRequestForm(request):
    student = request.student
    if student.has_registered(get_current_academic_year()+1):
        import md5, random
        student_id = student.student_id
        student_id += str(random.random())
        std_id = student.id
        h =  md5.new(student_id)
        check_code = h.hexdigest()
        statuses = Status.objects.filter(student=student)
        if(statuses):
            status = statuses[0]
            status.request_form_checkcode = check_code
            status.save()

        date = datetime.date.today()
        month = {'01':u'มกราคม','02':u'กุมภาพันธ์','03':u'มีนาคม','04':u'เมษายน','05':u'พฤษภาคม','06':u'มิถุนายน','07':u'กรกฎาคม','08':u'สิงหาคม','09':u'กันยายน','10':u'ตุลาคม','11':u'พฤศจิกายน','12':u'ธีนวาคม'}
        tmptime = date.strftime('%d') + ' ' + month[date.strftime('%m')] + ' ' + str(date.year + 543)

        import os,reportlab
        from settings import PROJECT_DIR
        from reportlab.pdfbase import pdfmetrics
        from reportlab.pdfbase.ttfonts import TTFont
        from reportlab.pdfgen.canvas import Canvas
        from reportlab.lib import pdfencrypt
        from reportlab.lib.units import inch
        from reportlab.platypus import Paragraph, Frame,Table
        from reportlab.lib.styles import ParagraphStyle
        from reportlab.graphics.shapes import Image, Drawing

        #TODO change password for encrypt
        enc = pdfencrypt.StandardEncryption(userPassword="",ownerPassword="password")
        enc.setAllPermissions(0)
        enc.canPrint = 1
        font_path = os.path.join(PROJECT_DIR, 'media/font/')
        THSarabun_font = os.path.join(font_path,'THSarabun.ttf')

        pdfmetrics.registerFont(TTFont('THSarabun', THSarabun_font))
        styleN = ParagraphStyle(name='normal', fontSize=17,fontName='THSarabun',leading=16)
        styleP = ParagraphStyle(name='normal', fontSize=17,fontName='THSarabun',leading=16,firstLineIndent=24,spaceBefore=10)
        stylePH = ParagraphStyle(name='normal', fontSize=17,fontName='THSarabun',leading=16,spaceBefore=10)
        styleSIGN = ParagraphStyle(name='normal',fontSize=17,fontName='THSarabun',leading=16,leftIndent=100,alignment=1,spaceBefore=20)
        request_form_logo =  os.path.join(PROJECT_DIR, 'media/image/requestformlogo.png')

        content = []

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'filename=request_form.pdf'

        pdf = Canvas(response,encrypt=enc)
        pdf.setFont('THSarabun',16)
        pdf.drawImage(request_form_logo,3.5*inch,10.25*inch,1.12*inch,1.1*inch)
        pdf.drawString(3.2*inch,9.8*inch,u"ใบคำร้องขอฝึกงานภาคฤดูร้อน")

        pdf.drawString(6*inch,9.5*inch,tmptime)

        if student.department == None:
            department_name = '-'
        else:
            department_name = student.department.name

        content.append(Paragraph(u"ข้าพเจ้า " + student.prefix
                                 + student.first_name + " " +student.last_name
                                 + u" เลขประจำตัว " + student.student_id
                                 + u" ภาควิชา " + department_name
                                 + u" คณะวิศวกรรมศาสตร์ "
                                   ,styleP))
        content.append(Paragraph(u"มีความประสงค์ขอฝึกงานภาคฤดูร้อน ประจำปี พ.ศ. "+str(get_current_academic_year()+1)+u" ทั้งนี้ข้าพเจ้ามีหน่วยกิตสะสม "+str(student.education.total_credit)+u" หน่วยกิต ผ่านตามระเบียบฯ และผลการเรียนอยู่ในข่ายที่สามารถฝึกงานได้ (หน่วยกิตสะสม หมายถึง หน่วยกิต วิชาที่ลงทะเบียนแล้วมีผลการเรียนเป็น P ตั้งแต่ A – D โดยไม่นับหน่วยกิตรายวิชาที่มีผลเป็น F, W และ P)",styleP))
        content.append(Paragraph(u"ทั้งนี้ข้าพเจ้าได้อ่านและยอมรับเงื่อนไขและกฎข้อบังคับในการเข้ารับการฝึกงานที่กำหนด ซึ่งเงื่อนไขดังกล่าวอาจมีการเปลี่ยนแปลงได้โดยที่ไม่ต้องแจ้งให้ข้าพเจ้าทราบล่วงหน้า โดยพิมพ์แบบขอคำร้องฝึกงานนี้จากทาง website ของทางมหาวิทยาลัยฯ  พร้อมลงชื่อ",styleP))
        content.append(Paragraph(u"จึงเรียนมาเพื่อโปรดพิจารณา",styleP))
        content.append(Paragraph(u"(ลงนาม).........................................................................(นิสิต)",styleSIGN))
        content.append(Paragraph(u"<br/><br/>รหัสตรวจสอบ: "+check_code,stylePH))
        f = Frame(0.6*inch, 0.5*inch, 7.2*inch, 8.5*inch, showBoundary=0)
        f.addFromList(content,pdf)
        pdf.showPage()
        pdf.save()
        return response
    else:
        return HttpResponseRedirect(reverse('std_index'))


@std_login_required
def print_letter(request, year):
    if not settings.LETTER_DOWNLOADABLE:
        return HttpResponseForbidden()
    
    student = request.student
    status = student.get_status(year)
    if not status:
        return HttpResponseForbidden()

    if not check_if_can_print_letter(student, status):
        return HttpResponseForbidden()
    
    assignment = student.assignment_for_year(year)
    
    if not assignment:
        return HttpResponseForbidden()

    company = assignment.company
    config_letter = ConfigLetter.objects.filter(company=company,
                                                year=year).all()
    if config_letter:
        config_letter = config_letter[0]

    try:
        work_detail = WorkDetail.objects.get(company=company.id, year=year)
    except:
        work_detail = None

    (letter_number, 
     letter_date, 
     signer_name, 
     letter_beginning_date, 
     letter_end_date, 
     eval_end_date) = default_student_letter_data(company, 
                                                  config_letter, 
                                                  work_detail)

    letter_date = str(letter_date)
    letter_beginning_date = str(letter_beginning_date)
    letter_end_date = str(letter_end_date)

    show_secret = settings.SHOWS_QR_CODE_IN_STD_LETTER
    is_student_copy = not show_secret

    if show_secret:
        Log.create("assignment print letter (secret) - from: %s, student: %s" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id),
                   student=student)
    else:
        Log.create("assignment print letter (no secret) - from: %s, student: %s" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id),
                   student=student)

    return print_letter_pdf(company.id,
                            year,
                            letter_number,
                            letter_date,
                            signer_name,
                            letter_beginning_date,
                            letter_end_date,
                            eval_end_date,
                            is_student_copy=is_student_copy,
                            show_signature=True,
                            show_secret=show_secret)
                            
    
@std_login_required
def underconst(request):
    return render_to_response("std/underconst.html",
                              {'student': request.student},
                              context_instance=RequestContext(request))


