# -*- coding: utf-8 -*-
import json

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseForbidden

from django import forms
from django.template import loader

from std.decorators import std_login_required
from commons.academia import get_next_worktraining_year

from std.models import CoOpCompany, CoOpStudentDetail, CoOpRequest, CoOpSemester
from committee.models import CoOpCommitteeContact
from reports import build_co_op_request_letter_pdf_response

currentpage = 'std'

class CoOpCompanyForm(forms.Form):
    name = forms.CharField(max_length=100,
                           label=u'ชื่อหน่วยงาน')
    address = forms.CharField(label=u'สถานที่ตั้ง (ที่นิสิตต้องไปฝึกงาน)',
                              widget=forms.Textarea(attrs={'rows':'3'}))
    tel_number = forms.CharField(max_length=30,
                                 label=u'เบอร์โทรศัพท์')
    fax_number = forms.CharField(max_length=30,
                                 label=u'เบอร์โทรสาร')
    is_public = forms.BooleanField(label=u'แสดงให้นิสิตคนอื่นเห็น',
                                   required=False)


class CoOpRequestForm(forms.Form):
    internship_semester = forms.ChoiceField(choices=[('1','ภาคต้นปีการศึกษาหน้า'),
                                                     ('2','ภาคปลายปีการศึกษานี้')],
                                           label=u'ภาคการศึกษาที่ต้องการฝึกสหกิจ')
    description = forms.CharField(max_length=200,
                                  label=u'ลักษณะงาน')
    signer_name = forms.CharField(max_length=100,
                                  label=u'ชื่อผู้ที่จะทำจดหมายไปถึง',
                                  required=True)

    contact_person = forms.CharField(max_length=100,
                                     label=u'ผู้ประสานงานการฝึกงาน',
                                     help_text=u'โปรดระบุชื่อผู้ประสานงานการฝึกงานของหน่วยงาน')
    contact_tel_number = forms.CharField(max_length=30,
                                     label=u'เบอร์โทรศัพท์ติดต่อผู้ประสานงาน')
    contact_email = forms.EmailField(label=u'อีเมล์ของผู้ประสานงาน')

    beginning_date = forms.DateField(label=u'วันที่เริ่มฝึกงาน') 
    end_date = forms.DateField(label=u'วันที่สิ้นสุดการฝึกงาน')

    def clean(self):
        cleaned_data = super(CoOpRequestForm, self).clean()
        beginning_date = cleaned_data.get('beginning_date')
        end_date = cleaned_data.get('end_date')

        if beginning_date and end_date:
            if end_date <= beginning_date:
                self._errors['end_date'] = self.error_class([u'ต้องระบุวันสิ้นสุดการฝึกงานภายหลังหลังวันเริ่มต้นฝึก'])
        return cleaned_data
    

@std_login_required
def index(request):
    student = request.student
    department = student.department
    next_academic_year = get_next_worktraining_year()
    status = student.get_status(next_academic_year)
    if not status:
        return HttpResponseForbidden()
    
    student_detail = CoOpStudentDetail.get_for_student_year(student, next_academic_year)
    co_op_companies = CoOpCompany.get_visible_for_department_year(department,
                                                                  next_academic_year,
                                                                  student)
    co_op_requests = CoOpRequest.get_for_student_year(student, next_academic_year)

    co_op_committee_contact = CoOpCommitteeContact.get_for(department)
    co_op_semesters = {(y,s): CoOpSemester.get_for(y,s) for y,s in
                       [(next_academic_year-1, 2), (next_academic_year,1)]}

    for r in co_op_requests:
        r.is_request_letter_printable = ((co_op_committee_contact != None) and
                                         co_op_semesters[(r.internship_year, r.internship_semester)] != None)
    
    has_active_co_op_reqests = len([c for c in co_op_requests if c.is_active()]) != 0
    
    for c in co_op_companies:
        c.is_editable = c.is_created_by(student)            
    
    forms = {
        'co_op_company_form': CoOpCompanyForm()
    }

    if 'std_coop_notice' in request.session:
        notice = request.session.pop('std_coop_notice')
    else:
        notice = ''
    
    return render_to_response("std/coop/index.html",
                              { 'student': student,
                                'department': department,
                                'next_academic_year':
                                next_academic_year,

                                'student_detail': student_detail,

                                'co_op_companies': co_op_companies,
                                'co_op_requests': co_op_requests,

                                'has_active_co_op_reqests': has_active_co_op_reqests,
                                
                                'forms': forms,

                                'notice': notice,
                                'currentpage': currentpage },
                              context_instance=RequestContext(request))


@std_login_required
def update_address(request):
    if request.method != 'POST':
        return HttpResponseForbidden()
    
    student = request.student
    next_academic_year = get_next_worktraining_year()

    student_detail = CoOpStudentDetail.get_for_student_year(student, next_academic_year)

    if not student_detail:
        student_detail = CoOpStudentDetail(student=student,
                                           year=next_academic_year)

    student_detail.address = request.POST['address']
    student_detail.save()
    request.session['std_coop_notice'] = u'คุณได้แก้ไขที่อยู่แล้ว'
    return redirect('std_coop_index')


@std_login_required
def update_company(request, co_op_company_id=None):
    student = request.student

    if request.method == 'POST':
        form = CoOpCompanyForm(request.POST)

        if co_op_company_id:
            company = get_object_or_404(CoOpCompany, pk=co_op_company_id)
            is_info_editable = company.is_info_editable()
        else:
            is_info_editable = True
            
        if form.is_valid():
            department = student.department
            next_academic_year = get_next_worktraining_year()

            if not co_op_company_id:
                company = CoOpCompany(department=department,
                                      created_by=student,
                                      year=next_academic_year)
                is_info_editable = True

            if is_info_editable:
                company.name=form.cleaned_data['name']
                
            company.address=form.cleaned_data['address']
            company.tel_number=form.cleaned_data['tel_number']
            company.fax_number=form.cleaned_data['fax_number']
            company.is_public=form.cleaned_data['is_public']
            company.save()
                                  
            return HttpResponse(json.dumps({'result': 'ok'}),
                                content_type='application/json')

        return HttpResponse(json.dumps({
            'result': 'error',
            'html': loader.render_to_string('std/coop/include/coop_company_form.html',
                                            { 'form': form,
                                              'is_info_editable': is_info_editable,
                                              'co_op_company_id': co_op_company_id },
                                            context_instance=RequestContext(request))
        }), content_type='application/json')

    else:
        company = get_object_or_404(CoOpCompany, pk=co_op_company_id)
        is_info_editable = company.is_info_editable()

        if not company.is_created_by(student):
            return HttpResponseForbidden()

        form = CoOpCompanyForm(initial={'name': company.name,
                                        'address': company.address,
                                        'tel_number': company.tel_number,
                                        'fax_number': company.fax_number,
                                        'is_public': company.is_public})
        return HttpResponse(json.dumps({
            'html': loader.render_to_string('std/coop/include/coop_company_form.html',
                                            { 'form': form,
                                              'is_info_editable': is_info_editable,
                                              'co_op_company_id': co_op_company_id },
                                            context_instance=RequestContext(request))
        }), content_type='application/json')
    

def can_student_apply_to_coop_company(student, company):
    company_student = company.created_by
    
    if company_student.id == student.id:
        return True
    if company.department_id == student.department_id:
        return company.is_public
    return False


@std_login_required
def create_request(request, co_op_company_id):
    student = request.student
    department = student.department
    next_academic_year = get_next_worktraining_year()
    status = student.get_status(next_academic_year)
    if not status:
        return HttpResponseForbidden()
    
    company = get_object_or_404(CoOpCompany, pk=co_op_company_id)

    if not can_student_apply_to_coop_company(request.student, company):
        return redirect('std_coop_index')

    if request.method == 'POST':
        if 'cancel' in request.POST:
            return redirect('std_coop_index')
        form = CoOpRequestForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['internship_semester'] == '1':
                internship_semester = 1
                internship_year = next_academic_year
            else:
                internship_semester = 2
                internship_year = next_academic_year-1
                
            co_op_request = CoOpRequest(student=student,
                                        department=department,
                                        year=next_academic_year,
                                        internship_semester=internship_semester,
                                        internship_year=internship_year,
                                        co_op_company=company,
                                        description=form.cleaned_data['description'],
                                        signer_name=form.cleaned_data['signer_name'],
                                        contact_person=form.cleaned_data['contact_person'],
                                        contact_tel_number=form.cleaned_data['contact_tel_number'],
                                        contact_email=form.cleaned_data['contact_email'],
                                        beginning_date=form.cleaned_data['beginning_date'],
                                        end_date=form.cleaned_data['end_date'])
            co_op_request.save()

            request.session['std_coop_notice'] = u'คุณได้เพิ่มข้อมูลฝึกงานสหกิจเพื่อขออนุมัติแล้ว'
            return redirect('std_coop_index')
    else:
        form = CoOpRequestForm()

    return render_to_response("std/coop/request_form.html",
                              { 'student': student,
                                'department': department,
                                'next_year':
                                next_academic_year,

                                'company': company,
                                'form': form,

                                'currentpage': currentpage },
                              context_instance=RequestContext(request))

@std_login_required
def print_request(request, coop_request_id, lang):
    student = request.student
    department = student.department
    coop_request = get_object_or_404(CoOpRequest, pk=coop_request_id)

    if coop_request.student_id != student.id:
        return HttpResponseForbidden()
    
    if not coop_request.is_approved_by_committee:
        return HttpResponseForbidden()

    if lang not in ['th','en']:
        lang = 'th'

    return build_co_op_request_letter_pdf_response(coop_request, lang)

