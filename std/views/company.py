# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseForbidden
from django import forms

from datetime import datetime

from commons.models import Configuration
from regis.models import Company, Student, Status
from std.decorators import std_login_required
from std.models import CompanyRequest, RequestStudent, CompanyDirectApplication, VerifiedDirectApplicationCompany, CompanyDirectApplicationDetail, Department
from commons.utils import to_thai_date
from commons.academia import get_current_academic_year, get_next_worktraining_year, can_convert_student_reg_type
from commons.utils import now_within_config_deadline, check_if_can_print_letter
from commons.models import Log
from commons.email import send_regtype_conversion_decision

from reports import build_student_request_letter_pdf_response,  build_sending_letter_pdf_response
from committee.models import CompanyDirectApplicationConfigLetter, CompanyRequestConfigLetter

from evaluations.models import EvaluationSecret

from django.conf import settings

currentpage = 'std'

def search(request):
    companies = []
    if (request.method == 'POST') and ('company_name' in request.POST):
        company_name = request.POST['company_name']
        is_show_instruction = False
    else:
        if 'company_name' in request.GET:
            company_name = request.GET['company_name']
        else:
            company_name = ''
            companies = []
            is_show_instruction = True

    if company_name != '':
        companies = (Company
                     .objects
                     .filter(name__icontains=company_name)
                     .all()[:50])

        if request.method == 'GET':   # ajax
            return render_to_response('std/company/include/company_list.html',
                                      { 'companies': companies })

    return render_to_response('std/company/search.html', 
                              { 'companies': companies,
                                'is_show_instruction': is_show_instruction },
                              context_instance=RequestContext(request))   


def list_by_char(request,ch):
    from commons.academia import STARTING_CHARACTERS

    companies = Company.objects.filter(starting_char=ch).order_by('name').all()
    return render_to_response('std/company/search_by_char.html', 
                              { 'starting_char': ch,
                                'characters': STARTING_CHARACTERS,
                                'companies': companies },
                              context_instance=RequestContext(request))   

class CompanyApplicationForm(forms.Form):
    company_name = forms.CharField(max_length=100,
                                   label=u'ชื่อหน่วยงาน')

class CompanyDirectApplicationDetailForm(forms.ModelForm):
    letter_address = forms.CharField(label=u'ที่อยู่สำหรับส่งหนังสือ',
                                     required=True,
                                     widget=forms.Textarea(attrs={'rows':'3'}))
    period_choice = forms.ChoiceField(choices = [('standard',u'ตามที่คณะกำหนด'),
                                                 ('custom',u'ระบุระยะเวลาเอง')])
    beginning_date = forms.DateField(label=u'วันที่เริ่มฝึกงาน', 
                                     required=False)
    end_date = forms.DateField(label=u'วันที่สิ้นสุดการฝึกงาน',
                               required=False,
                               widget=forms.HiddenInput)

    class Meta:
        model = CompanyDirectApplicationDetail
        exclude = ('application','student','company','created_at',
                   'working_address', 'work_tel_no', 'work_fax_no',
                   'is_default_schedule')

    def clean(self):
        cleaned_data = super(CompanyDirectApplicationDetailForm, self).clean()

        contact_tel_no = cleaned_data.get('contact_tel_no')
        contact_email = cleaned_data.get('contact_email')

        if (not contact_tel_no) and (not contact_email):
            self._errors['contact_tel_no'] = self.error_class([u'ต้องระบุอย่างน้อยเบอร์ติดต่อหรืออีเมล์'])
            del cleaned_data['contact_tel_no']
            if 'contact_email' in cleaned_data:
                self._errors['contact_email'] = self.error_class([u'ต้องระบุอย่างน้อยเบอร์ติดต่อหรืออีเมล์'])
                del cleaned_data['contact_email']

        pchoice = cleaned_data.get('period_choice')
        beginning_date = cleaned_data.get('beginning_date')
        end_date =cleaned_data.get('end_date')

        if pchoice:
            if pchoice == 'custom':
                if not beginning_date or not end_date:
                    self._errors['period_choice'] = self.error_class([u'ระบุวันที่เริ่มและวันที่สิ้นสุดการฝึกงานไม่ถูกต้อง'])
                    del cleaned_data['period_choice']

        return cleaned_data


class CompanyRequestForm(forms.Form):
    company_name = forms.CharField(max_length=100,
                                   label=u'ชื่อหน่วยงาน')
    division_name = forms.CharField(max_length=100,
                                    required=False,
                                    label=u'หน่วยงานย่อย/แผนก/ฝ่าย',
                                    help_text=u'ถ้าไม่มีหน่วยงานย่อย ไม่จำเป็นต้องระบุ')
    letter_address = forms.CharField(label=u'ที่อยู่สำหรับส่งหนังสือ',
                                     required=True,
                                     widget=forms.Textarea(attrs={'rows':'2'}))

    signer_name = forms.CharField(max_length=100,
                                  label=u'ชื่อผู้ที่จะทำจดหมายไปถึง',
                                  required=True)

    contact_person = forms.CharField(max_length=100,
                                     label=u'ผู้ประสานงานการฝึกงาน',
                                     help_text=u'โปรดระบุชื่อผู้ประสานงานการฝึกงานของหน่วยงาน')
    contact_number = forms.CharField(max_length=30,
                                     label=u'เบอร์โทรศัพท์ติดต่อผู้ประสานงาน')
    contact_email = forms.EmailField(label=u'อีเมล์ของผู้ประสานงาน',
                                     required=False)

    working_address = forms.CharField(label=u'สถานที่ตั้ง (ที่นิสิตต้องไปฝึกงาน)',
                                      widget=forms.Textarea(attrs={'rows':'3'}))
    working_number = forms.CharField(max_length=30,
                                     label=u'เบอร์โทรศัพท์')
    working_fax_number = forms.CharField(max_length=30,
                                         label=u'เบอร์โทรสาร')
    description = forms.CharField(widget=forms.Textarea(attrs={'rows':'3'}),
                                  label=u'ลักษณะงานที่มอบหมายให้นิสิตทำ')
    period_choice = forms.ChoiceField(choices = [('standard',u'ตามที่คณะกำหนด'),
                                                 ('custom',u'ระบุระยะเวลาเอง')])

    beginning_date = forms.DateField(label=u'วันที่เริ่มฝึกงาน', 
                                     required=False)
    end_date = forms.DateField(label=u'วันที่สิ้นสุดการฝึกงาน',
                               required=False)

    def clean(self):
        cleaned_data = super(CompanyRequestForm, self).clean()

        pchoice = cleaned_data.get('period_choice')
        beginning_date = cleaned_data.get('beginning_date')
        end_date =cleaned_data.get('end_date')

        if pchoice:
            if pchoice == 'custom':
                if not beginning_date or not end_date:
                    self._errors['period_choice'] = self.error_class([u'ระบุวันที่เริ่มและวันที่สิ้นสุดการฝึกงานไม่ถูกต้อง'])
                    del cleaned_data['period_choice']

        return cleaned_data

    model_fields = ['company_name',
                    'division_name',
                    'letter_address',
                    'signer_name',
                    'contact_person',
                    ('contact_number', 'contact_tel_no'),
                    'contact_email',
                    'working_address',
                    ('working_number','work_tel_no'),
                    ('working_fax_number','work_fax_no'),
                    'description',]

    def save_to_company_request(self, cr):
        for fname in CompanyRequestForm.model_fields:
            if type(fname) != tuple:
                setattr(cr, fname, self.cleaned_data[fname])
            else:
                form_field, obj_field = fname
                setattr(cr, obj_field, self.cleaned_data[form_field])

        if self.cleaned_data['period_choice']=='standard':
            cr.is_default_schedule = True
            cr.beginning_date = None
            cr.end_date = None
        else:
            cr.is_default_schedule = False
            cr.beginning_date = self.cleaned_data['beginning_date']
            cr.end_date = self.cleaned_data['end_date']
        return cr
    
    def new_company_request(self):
        cr = CompanyRequest()
        self.save_to_company_request(cr)

        return cr

    @staticmethod
    def create_from_company_request(company_request):
        initial = {}

        for fname in CompanyRequestForm.model_fields:
            if type(fname) != tuple:
                initial[fname] = getattr(company_request, fname)
            else:
                form_field, obj_field = fname
                initial[form_field] = getattr(company_request, obj_field)

        if company_request.is_default_schedule:
            initial['period_choice'] = 'standard'
        else:
            initial['period_choice'] = 'custom'
            initial['beginning_date'] = company_request.beginning_date
            initial['end_date'] = company_request.end_date
                
        return CompanyRequestForm(initial=initial)
        
    
def confirmed_student_required():
    return not now_within_config_deadline('deadline.free_self_appl_date')

@std_login_required
def student_search(request):
    import re

    DEPARTMENT_MERGE = {6:11, 11:6}
    
    student = request.student
    department = student.department
    items = request.POST['q'].strip().split()

    std_id = None
    left = []
    for i in items:
        if re.match(r'\d+', i):
            std_id = i
        else:
            left.append(i)

    left = left[:2]

    next_academic_year = get_next_worktraining_year()
    if department.id in DEPARTMENT_MERGE:
        from django.db.models import Q
        dpair = Department.objects.get(pk=DEPARTMENT_MERGE[department.id])
        students = Student.objects.filter(Q(department=department) | Q(department=dpair))
    else:
        students = Student.objects.filter(department=department)
    regtype_confirmed_required = (confirmed_student_required() and
                                  can_convert_student_reg_type(student, department))

    if std_id:
        students = students.filter(student_id__startswith=std_id)
    if len(left)==2:
        students = (students.
                    filter(first_name__contains=left[0]).
                    filter(last_name__contains=left[1]))
    elif len(left)==1:
        students = students.filter(first_name__contains=left[0])

    students = students.select_related('department').all()[:5]
    
    for s in students:
        s.can_request = CompanyRequest.can_request(s, 
                                                   next_academic_year, 
                                                   regtype_confirmed_required)

    return render_to_response("std/self_appl/include/stdresults.html",
                              { 'students': students },
                              context_instance=RequestContext(request))


@std_login_required
def search_by_name_ajax(request):
    import json

    company_name = request.GET['term']
    companies = Company.all_containing_name(company_name)

    companies = companies[:20]  # take only the first 20

    return HttpResponse(json.dumps([c.name for c in companies]))

def get_all_request_students(student_ids):
    students = []
    for i in student_ids:
        try:
            s = Student.objects.get(student_id=i)
        except Student.DoesNotExist:
            s = None
        students.append(s)
    return students

@std_login_required
def new_request(request):
    student = request.student
    department = student.department
    next_year = get_next_worktraining_year()
    status = student.get_status(next_year)
    if not status:
        return HttpResponseForbidden()

    regtype_confirmed_required = (confirmed_student_required() and
                                  can_convert_student_reg_type(student,department))

    if status.registration_type != Status.OWN_SELECTION_TYPE:
        return HttpResponseForbidden()

    if (regtype_confirmed_required and 
        not status.is_self_appl_confirmed):
        return HttpResponseForbidden()

    default_beginning_date = Configuration.get('default.beginning_date')
    default_end_date = Configuration.get('default.end_date')

    if request.method == 'POST':
        if 'cancel' in request.POST:
            request.session['std_index_notice'] = u'คุณได้ยกเลิกการกรอกข้อมูลแล้ว'
            return redirect('std_index')

        form = CompanyRequestForm(request.POST)

        request_students = get_all_request_students(request.POST.getlist('request_students'))

        students_ok = CompanyRequest.can_request(student, next_year, regtype_confirmed_required)
        for s in request_students:
            if not CompanyRequest.can_request(s, next_year, regtype_confirmed_required):
                students_ok = False

        if form.is_valid() and students_ok:
            cr = form.new_company_request()
            cr.year = get_next_worktraining_year()
            cr.student = student
            cr.requested_at = datetime.now()
            cr.has_all_confirmed = (len(request_students)==0)
            cr.save()

            for std in request_students:
                rstd = RequestStudent(year=next_year,
                                      company_request=cr,
                                      student=std,
                                      is_owner=False)
                rstd.save()
            rstd = RequestStudent(year=next_year,
                                  company_request=cr,
                                  student=student,
                                  is_owner=True,
                                  is_approved=True,
                                  approved_at=datetime.now())
            rstd.save()

            Log.create("created company request - from: %s, student: %s, cr: %d" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id,
                        cr.id),
                       student=student)

            request.session['std_index_notice'] = u'คุณได้ส่งข้อมูลเพื่อขอหนังสือขอความอนุเคราะห์แล้ว'
            return redirect('std_index')

    else:
        request_students = []
        form = CompanyRequestForm(initial={'beginning_date': default_beginning_date,
                                           'end_date': default_end_date})

    return render_to_response("std/self_appl/new.html",
                              { 'student': student,
                                'next_year': next_year,
                                'currentpage':currentpage,
                                'form': form,
                                'request_students': request_students,
                                'default_beginning_date': default_beginning_date,
                                'default_end_date': default_end_date },
                              context_instance=RequestContext(request))


@std_login_required
def request_confirm(request, request_student_id):
    request_student = get_object_or_404(RequestStudent, pk=request_student_id)
    student = request.student
    if request_student.student_id != student.id:
        return HttpResponseForbidden()

    active_requests = RequestStudent.get_active_for(student, request_student.year)

    request_student.approved_at = datetime.now()
    decline_all_other = False

    if 'ok' in request.POST: 
        pending_approved = False
        for r in active_requests:
            if r.is_approved and r.is_accepted != False:
                pending_approved = True
        if not pending_approved:
            request.session['std_index_notice'] = u'คุณได้ยืนยันการขอหนังสือขอความอนุเคราะห์แล้ว'
            request_student.is_approved = True
            decline_all_other = True
            request_student.save()

            Log.create("approved company request - from: %s, student: %s, rs: %d" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id,
                        request_student.id),
                       student=student)
        else:
            request.session['std_index_notice'] = u'เกิดข้อผิดพลาด คุณไม่สามารถยืนยันได้มากกว่าหนึ่งหน่วยงาน'
    else:
        request.session['std_index_notice'] = u'คุณได้ถอนชื่อจากหนังสือขอความอนุเคราะห์แล้ว'
        request_student.is_approved = False
        request_student.save()

        Log.create("rejected company request - from: %s, student: %s, rs: %d" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id,
                    request_student.id),
                   student=student)

    if decline_all_other:
        for item in active_requests:
            if item.id == request_student.id:
                continue
            if item.is_approved == None:
                item.is_approved = False
                item.approved_at = datetime.now()
                item.save()
                item.company_request.update_confirm_status()

    request_student.company_request.update_confirm_status()
    return redirect('std_self_appl_index')

def build_inactive_stats(company_request_items):
    rejected_count = 0
    committee_rejected_count = 0
    company_rejected_count = 0

    for i in company_request_items:
        if i.is_approved == False:
            rejected_count += 1
        elif i.is_approved_by_committee == False:
            committee_rejected_count += 1
        elif i.is_accepted == False:
            company_rejected_count += 1

    return {'rejected_count': rejected_count,
            'committee_rejected_count': committee_rejected_count,
            'company_rejected_count': company_rejected_count }

@std_login_required
def self_appl_index(request):
    student = request.student
    department = student.department
    next_academic_year = get_next_worktraining_year()

    status = student.get_status(next_academic_year)
    if not status:
        return HttpResponseForbidden()

    registered = status.is_registered
    application_deadline_passed = not now_within_config_deadline('deadline.applying')

    if (not application_deadline_passed) and (not status.is_registration_type_confirmed):
        return redirect('std_index')
    
    regtype_confirmed_required = (confirmed_student_required() and
                                  can_convert_student_reg_type(student,department))

    if not status.is_registered_for_self_application():
        return redirect('std_index')

    if 'std_index_notice' in request.session:
        notice = request.session.pop('std_index_notice')
    else:
        notice = ''

    all_company_request_items = student.request_set.filter(year=next_academic_year).all()
    company_request_items = [r for r in all_company_request_items 
                             if r.is_active()]
    inactive_company_request_items = [r for r in all_company_request_items
                                      if not r.is_active()]
    inactive_stats = build_inactive_stats(inactive_company_request_items)

    has_approved_company_request = False
    for r in company_request_items:
        if r.is_approved:
            has_approved_company_request = True
    has_active_company_requests = len(company_request_items)!=0

    company_direct_applications = CompanyDirectApplication.get_all_for(student, next_academic_year)
    has_active_company_applications = len([a for a in company_direct_applications if a.is_active()])!=0

    has_chosen_company = len([a for a in company_direct_applications if a.is_chosen]) != 0
    if not has_chosen_company:
        chosen_company_application = None
    else:
        chosen_company_application = [a for a in company_direct_applications if a.is_chosen][0]

    inactive_company_applications = [a for a in company_direct_applications if not a.is_active()]

    if (regtype_confirmed_required and
        (not has_active_company_requests) and 
        (not has_active_company_applications) and
        (not status.is_self_appl_confirmed)):
        if not status.confirmed_at:
            return redirect('std_self_appl_confirm')

    can_create_new_requests = ((not regtype_confirmed_required) or
                               (status.is_self_appl_confirmed))
    can_create_new_applications = can_create_new_requests

    new_direct_app_form = CompanyApplicationForm()

    letter_downloadable = settings.LETTER_DOWNLOADABLE
    
    can_print_std_letter = check_if_can_print_letter(student, status)

    return render_to_response("std/self_appl/index.html",
                              { 'student': student,
                                'department': department,
                                
                                'notice': notice,
                                
                                'next_academic_year':
                                next_academic_year,
                                
                                'status': status,
                                'registered': registered,
                                'application_deadline_passed': application_deadline_passed,
                                
                                'has_active_company_applications': 
                                has_active_company_applications,
                                'company_direct_applications':
                                company_direct_applications,
                                'inactive_company_applications':
                                inactive_company_applications,
                                'has_chosen_company':
                                has_chosen_company,
                                'chosen_company_application':
                                chosen_company_application,

                                'new_direct_app_form':
                                new_direct_app_form,

                                'has_active_company_requests':
                                has_active_company_requests,
                                'has_approved_company_request':
                                has_approved_company_request,
                                'company_request_items':
                                company_request_items,
                                'inactive_company_request_items':
                                inactive_company_request_items,
                                'inactive_stats':
                                inactive_stats,

                                'can_create_new_requests':
                                can_create_new_requests,
                                'can_create_new_applications':
                                can_create_new_applications,
                                'can_print_std_letter':
                                can_print_std_letter,

                                'letter_downloadable':
                                letter_downloadable,

                                'application_deadline_passed':
                                application_deadline_passed,

                                'currentpage':currentpage },
                              context_instance=RequestContext(request))


@std_login_required
def self_appl_confirm(request):
    student = request.student
    department = student.department
    next_academic_year = get_next_worktraining_year()

    if not can_convert_student_reg_type(student, department):
        return HttpResponseForbidden()

    status = student.get_status(next_academic_year)
    if not status:
        return HttpResponseForbidden()

    if status.registration_type != Status.OWN_SELECTION_TYPE:
        return HttpResponseForbidden()

    application_deadline_passed = not now_within_config_deadline('deadline.applying')
    free_self_appl_passed = not now_within_config_deadline('deadline.free_self_appl_date')

    if (application_deadline_passed) or (free_self_appl_passed):
        return HttpResponseForbidden()

    all_company_request_items = student.request_set.filter(year=next_academic_year).all()
    company_request_items = [r for r in all_company_request_items 
                             if r.is_active()]
    has_active_company_requests = len(company_request_items)!=0

    company_direct_applications = CompanyDirectApplication.get_active_for(student, next_academic_year)
    has_active_company_applications = len(company_direct_applications)!=0

    if has_active_company_applications or has_active_company_requests:
        return HttpResponseForbidden()

    if request.method == 'POST':
        if request.POST['convertchoice'] == 'convert':
            status.registration_type = Status.FACULTY_SELECTION_TYPE
            status.is_converted_from_self_appl = True
            status.converted_at = datetime.now()
            status.is_self_appl_confirmed = False
            status.confirmed_at = datetime.now()
            status.save()
 
            Log.create("changed regtype to faculty-selection-type - from: %s, student: %s" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id),
                       student=student)

            send_regtype_conversion_decision(student, True)

            request.session['std_index_notice'] = u'คุณได้เปลี่ยนประเภทการจัดหาหน่วยงานสำหรับฝึกงานเป็นประเภทให้คณะจัดหาให้แล้ว'
            return redirect('std_index')
             
        elif request.POST['convertchoice'] == 'confirm':
            status.is_self_appl_confirmed = True
            status.confirmed_at = datetime.now()
            status.save()
 
            Log.create("confirmed regtype to own-selection-type - from: %s, student: %s" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id),
                       student=student)

            send_regtype_conversion_decision(student, False)

            request.session['std_index_notice'] = u'คุณได้ยืนยันที่จะรับผิดชอบจัดหาหน่วยงานเอง คุณจะต้องจัดหาหน่วยงานให้ได้ภายในกำหนด ไม่เช่นนั้นจะต้องเข้าฝึกงานในปีการศึกษาถัดไป'
            return redirect('std_index')

    Log.create("viewed regtype confirm page - from: %s, student: %s" %
               (request.META['REMOTE_ADDR'],
                student.student_id),
               student=student)

    return render_to_response("std/self_appl/confirm.html",
                              { 'student': student,
                                'department': department,
                                
                                'next_academic_year':
                                next_academic_year,
                                
                                'currentpage':currentpage },
                              context_instance=RequestContext(request))


@std_login_required
def inactive_requests(request):
    student = request.student
    department = student.department
    next_academic_year = get_next_worktraining_year()

    status = student.get_status(next_academic_year)
    if not status:
        return HttpResponseForbidden()

    registered = status.is_registered
    application_deadline_passed = not now_within_config_deadline('deadline.applying')

    if not status.is_registered_for_self_application():
        return redirect('std_index')

    all_company_request_items = student.request_set.filter(year=next_academic_year).all()
    company_request_items = [r for r in all_company_request_items 
                             if r.is_active()]
    inactive_company_request_items = [r for r in all_company_request_items
                                      if not r.is_active()]
    inactive_stats = build_inactive_stats(inactive_company_request_items)

    return render_to_response("std/self_appl/inactive_requests.html",
                              { 'student': student,
                                'department': department,
                                
                                'next_academic_year':
                                next_academic_year,
                                
                                'company_request_items':
                                company_request_items,
                                'inactive_company_request_items':
                                inactive_company_request_items,
                                'inactive_stats':
                                inactive_stats,

                                'currentpage':currentpage },
                              context_instance=RequestContext(request))


@std_login_required
def print_request(request, company_request_id):
    student = request.student
    department = student.department
    company_request = get_object_or_404(CompanyRequest, pk=company_request_id)

    if not company_request.is_approved_by_committee:
        return HttpResponseForbidden()

    students = company_request.get_approved_students()

    found = False
    for s in students:
        if s.id == student.id:
            found = True
            break
    if not found:
        return HttpResponseForbidden()

    return build_student_request_letter_pdf_response(company_request,
                                                     students)

@std_login_required
def print_announcement(request):
    import os
    from settings import PROJECT_DIR

    pdf_file = os.path.join(PROJECT_DIR,'private_media/files/process-change-wt-27.pdf')
    data = open(pdf_file,'rb').read()

    response = HttpResponse(data,content_type='application/pdf')
    response['Content-Disposition'] = 'filename=process-change-57.pdf'

    return response


@std_login_required
def cancel_direct_application(request, application_id):
    student = request.student
    app = get_object_or_404(CompanyDirectApplication,
                            pk=application_id)
    if app.student_id != student.id:
        return HttpResponseForbidden()

    if app.can_be_cancelled():
        app.delete()
    return redirect('std_index')


@std_login_required
def create_direct_application(request):
    student = request.student
    department = student.department
    next_academic_year = get_next_worktraining_year()

    return redirect('std_index')

    status = student.get_status(next_academic_year)
    if not status:
        return HttpResponseForbidden()

    regtype_confirmed_required = (confirmed_student_required() and
                                  can_convert_student_reg_type(student, department))

    if status.registration_type != Status.OWN_SELECTION_TYPE:
        return HttpResponseForbidden()

    if (regtype_confirmed_required and 
        not status.is_self_appl_confirmed):
        return HttpResponseForbidden()

    company_direct_applications = CompanyDirectApplication.get_all_for(student, next_academic_year)
    if len([a for a in company_direct_applications if a.is_active()]) != 0:
        return HttpResponseForbidden()

    if request.method == 'POST':
        if 'cancel' in request.POST:
            return redirect('std_index')

        form = CompanyApplicationForm(request.POST)
        if form.is_valid():
            app = CompanyDirectApplication(year=next_academic_year,
                                           student=student,
                                           company_name=form.cleaned_data['company_name'],
                                           created_at=datetime.now())
            app.save()

            Log.create("created company direct application - from: %s, student: %s, app: %d" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id,
                        app.id),
                       student=student)

            request.session['std_index_notice'] = u'คุณได้เพิ่มรายชื่อหน่วยงานที่คุณสมัครแล้ว'
            return redirect('std_self_appl_index')
    else:
        form = CompanyApplicationForm()

    return render_to_response('std/self_appl/application_create.html',
                              { 'student': student,
                                'department': department,
                                'next_year': next_academic_year,
                                'form': form },
                              context_instance=RequestContext(request))


@std_login_required
def update_direct_application(request, application_id):
    student = request.student
    app = get_object_or_404(CompanyDirectApplication,
                            pk=application_id)
    if app.student_id != student.id:
        return HttpResponseForbidden()

    if not app.is_waiting():
        return redirect('std_index')

    department = student.department
    next_academic_year = get_next_worktraining_year()

    is_accepted = "none"
    is_chosen = None

    all_apps = student.company_direct_applications.filter(year=next_academic_year).all()
    has_chosen = len([a for a in all_apps if a.is_chosen]) != 0

    if request.method == 'POST':
        if 'cancel' in request.POST:
            return redirect('std_index')

        form = CompanyDirectApplicationDetailForm(request.POST)

        if request.POST['appresult']!='none':
            app.is_accepted = request.POST['appresult'] == 'accepted'
            app.accepted_at = datetime.now()

            if app.is_accepted:
                app.is_chosen =  (('company_select' in request.POST) and 
                                  (request.POST['company_select'] == 'selected'))
                app.chosen_at = datetime.now()

            if ((not app.is_accepted) or (not app.is_chosen) or 
                ((not has_chosen) and form.is_valid())):
                app.save()

                if app.is_accepted and app.is_chosen:
                    appdetail = form.save(commit=False)
                    appdetail.is_default_schedule = (form.cleaned_data['period_choice'] == 'standard')
                    appdetail.application = app
                    appdetail.student = student
                    appdetail.save()

                Log.create("updated company direct application (%s,%s)- from: %s, student: %s, app: %d" %
                           (app.is_accepted, 
                            app.is_chosen,
                            request.META['REMOTE_ADDR'],
                            student.student_id,
                            app.id),
                           student=student)

                request.session['std_index_notice'] = u'คุณแจ้งผลการสมัครแล้ว'
                return redirect('std_self_appl_index')

            is_chosen = app.is_chosen
            is_accepted = app.is_accepted
    else:
        form = CompanyDirectApplicationDetailForm()

    return render_to_response('std/self_appl/application_update.html',
                              { 'student': student,
                                'application': app,
                                'department': department,
                                'next_year': next_academic_year,
                                'form': form,
                                'is_accepted': is_accepted,
                                'is_chosen': is_chosen,
                                'has_chosen_company': has_chosen},
                              context_instance=RequestContext(request))

@std_login_required
def print_direct_application_letter(request, application_id):
    student = request.student
    app = get_object_or_404(CompanyDirectApplication,
                            pk=application_id)
    if app.student_id != student.id:
        return HttpResponseForbidden()
    year = get_next_worktraining_year()
    
    if not app.is_verified():
        return HttpResponseForbidden()

    students = [student]

    if not app.is_letter_downloaded:
        app.is_letter_downloaded = True
        app.letter_downloaded_at = datetime.now()
        app.save()
        
    return build_student_request_letter_pdf_response(None, students,
                                                     company_name=app.company_name,
                                                     year=year,
                                                     signer_name=u'ผู้จัดการแผนกทรัพยากรมนุษย์',
                                                     only_request_letter=True)

@std_login_required
def list_verified_direct_application_companies(request):
    student = request.student
    department = student.department
    year = get_next_worktraining_year()

    vcompanies = (VerifiedDirectApplicationCompany
                  .objects
                  .filter(year=year)
                  .select_related('company')
                  .all())

    companies = [c.company for c in vcompanies]

    return render_to_response('std/self_appl/list_verified_companies.html',
                              { 'student': student,
                                'department': department,
                                'year': year,
                                'companies': companies },
                              context_instance=RequestContext(request))

@std_login_required
def print_direct_application_sending_letter_pdf(request, application_id):
    student = request.student
    app = get_object_or_404(CompanyDirectApplication,
                            pk=application_id)
    if app.student_id != student.id:
        return HttpResponseForbidden()
    year = get_next_worktraining_year()
    status = student.get_status(year)

    if not check_if_can_print_letter(student, status):
        return HttpResponseForbidden()

    if not app.is_accepted_and_confirmed():
        return HttpResponseForbidden()

    letter_config = app.get_letter_config_or_default(CompanyDirectApplicationConfigLetter,
                                                     save=True)

    print_students = [student]

    show_secret = settings.SHOWS_QR_CODE_IN_STD_LETTER
    is_student_copy = not show_secret
        
    if show_secret:
        evaluation_secrets = [
            EvaluationSecret.get_or_create_for_student(student,
                                                       year)
        ]
        Log.create("direct application print letter (secret) - from: %s, student: %s, secret: %s/%s" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id,
                    evaluation_secrets[0].easy_secret,
                    evaluation_secrets[0].secret),
                   student=student)
    else:
        Log.create("direct application print letter (no secret) - from: %s, student: %s, no secret" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id),
                   student=student)
        evaluation_secrets = None
    
    return build_sending_letter_pdf_response(print_students,
                                             letter_config.year,
                                             letter_config.letter_number,
                                             letter_config.letter_date,
                                             letter_config.signer_name,
                                             letter_config.beginning_date,
                                             letter_config.end_date,
                                             letter_config.eval_date,
                                             is_student_copy=is_student_copy,
                                             show_signature=True,
                                             evaluation_secrets=evaluation_secrets)

@std_login_required
def print_request_sending_letter_pdf(request, company_request_id):
    student = request.student
    department = student.department
    company_request = get_object_or_404(CompanyRequest, pk=company_request_id)

    if not company_request.is_approved_by_committee:
        return HttpResponseForbidden()
    if not company_request.is_accepted:
        return HttpResponseForbidden()

    year = get_next_worktraining_year()
    status = student.get_status(year)

    if not check_if_can_print_letter(student, status):
        return HttpResponseForbidden()

    students = company_request.get_accepted_students()

    found = ([s for s in students if s.id == student.id] != [])
    if not found:
        return HttpResponseForbidden()

    letter_config = company_request.get_letter_config_or_default(CompanyRequestConfigLetter,
                                                                 save=True)

    print_students = [student]
    
    show_secret = settings.SHOWS_QR_CODE_IN_STD_LETTER
    is_student_copy = not show_secret
        
    if show_secret:
        evaluation_secrets = [
            EvaluationSecret.get_or_create_for_student(student,
                                                       year)
        ]
        Log.create("company request print letter (secret) - from: %s, student: %s, secret: %s/%s" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id,
                    evaluation_secrets[0].easy_secret,
                    evaluation_secrets[0].secret),
                   student=student)
    else:
        Log.create("company request print letter (no secret) - from: %s, student: %s, no secret" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id),
                   student=student)
        evaluation_secrets = None
    
    return build_sending_letter_pdf_response(print_students,
                                             letter_config.year,
                                             letter_config.letter_number,
                                             letter_config.letter_date,
                                             letter_config.signer_name,
                                             letter_config.beginning_date,
                                             letter_config.end_date,
                                             letter_config.eval_date,
                                             is_student_copy=is_student_copy,
                                             evaluation_secrets=evaluation_secrets,
                                             show_signature=True)
