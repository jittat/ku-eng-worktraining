# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django import forms
import time, datetime

from django.template import RequestContext
from datetime import date
from regis.models import Student, Receipt, Department
from regis.models import DepartmentNameInRequestLetter
from commons.academia import get_current_academic_year
from commons.models import Log
from commons.email import send_selection_confirmation, send_deselection_confirmation
from std.decorators import std_login_required
from std.models import SelectionRoundStatus, StudentSelection
from std.models import assign_selections_to_receipts, assign_company_address_to_receipts

currentpage = 'std'

@std_login_required
def list(request):
    student = request.student
    current_round = SelectionRoundStatus.get_current_round(department=student.department)
    if not current_round:
        return HttpResponseForbidden()

    if not student.is_eligible_to_apply_in_selection_round():
        return HttpResponseForbidden()

    department = student.department
    actual_department = DepartmentNameInRequestLetter.to_requested_department(department)

    all_receipts = Receipt.all_for_round(current_round, 
                                         department=actual_department,
                                         cache=True)                            
    
    receipts = [r for r in all_receipts 
                if r.is_shown_to_students()]

    assign_selections_to_receipts(receipts, current_round.number, department)
    assign_company_address_to_receipts(receipts)

    current_selection = StudentSelection.get_for_student(student,
                                                         current_round.number,
                                                         current_round.year)
    if current_selection:
        selected_company = current_selection.company
        selected_receipt = current_selection.receipt
        if (selected_receipt and 
            (len([r for r in receipts if r.id == selected_receipt.id])==0)):
            selected_receipt = None
    else:
        selected_company = None
        selected_receipt = None

    return render_to_response("std/selection/list.html",
                              { 'current_round': current_round,
                                'selected_company': selected_company,
                                'selected_receipt': selected_receipt,
                                'current_selection': current_selection,
                                'student': student,
                                'currentpage':currentpage,
                                'receipts':receipts },
                              context_instance=RequestContext(request))

@std_login_required
def select(request, receipt_id):
    student = request.student
    current_round = SelectionRoundStatus.get_current_round(department=student.department)
    if not current_round:
        return HttpResponseForbidden()

    if not student.is_eligible_to_apply_in_selection_round():
        return HttpResponseForbidden()

    department = DepartmentNameInRequestLetter.to_requested_department(student.department)

    receipt = get_object_or_404(Receipt, pk=receipt_id)
    if receipt.department != department:
        return HttpResponseForbidden()
        
    if not receipt.available:
        return HttpResponseForbidden()

    selection = StudentSelection.get_for_student(student,
                                                 current_round.number,
                                                 current_round.year)

    if not selection:
        selection = StudentSelection(student=student,
                                     round_number=current_round.number,
                                     year=current_round.year)

    if selection.result:
        return redirect('std_selection_list')        

    selection.selection_at = datetime.datetime.now()
    selection.company = receipt.company
    selection.receipt = receipt
    selection.extra_ranking = 0
    selection.is_imported = False
    selection.save()

    Log.create("selection - from: %s, student: %s, rcpt: %d, company: %d" %
               (request.META['REMOTE_ADDR'],
                student.student_id,
                receipt.id,
                receipt.company.id),
               student=student)

    send_selection_confirmation(student, selection, current_round.number)

    return redirect('std_selection_list')


@std_login_required
def deselect(request, receipt_id):
    student = request.student
    current_round = SelectionRoundStatus.get_current_round(department=student.department)
    if not current_round:
        return HttpResponseForbidden()

    if not student.is_eligible_to_apply_in_selection_round():
        return HttpResponseForbidden()

    department = DepartmentNameInRequestLetter.to_requested_department(student.department)

    receipt = get_object_or_404(Receipt, pk=receipt_id)
    if receipt.department != department:
        return HttpResponseForbidden()
        
    selection = StudentSelection.get_for_student(student,
                                                 current_round.number,
                                                 current_round.year)

    if (not selection) or (selection.company != receipt.company):
        return HttpResponseForbidden()

    if selection.result:
        return redirect('std_selection_list')        

    selection.delete()

    Log.create("deselection - from: %s, student: %s, rcpt: %d, company: %d" %
               (request.META['REMOTE_ADDR'],
                student.student_id,
                receipt.id,
                receipt.company.id),
               student=student)

    send_deselection_confirmation(student, current_round.number)

    return redirect('std_selection_list')


