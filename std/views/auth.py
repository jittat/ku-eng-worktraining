# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django import forms
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
from django.template import RequestContext
from importlib import import_module

from regis.models import Student
from std.models import StudentExtraPassword, StudentTokenNonce
from commons.backends.imapauthen import ImapBackend
from commons.backends.tqfauthen import WebBackend
from commons.models import Log

class StudentLoginForm(forms.Form):
    student_id = forms.CharField(max_length=15, min_length=3)
    password = forms.CharField(widget=forms.PasswordInput)

def std_id_to_nontri_username(student_id):
    try:
        if int(student_id[:2])>=53:
            return 'b' + student_id
        else:
            return 'b' + student_id[:-1]
    except:
        return None

def login(request):
    if settings.HTTPS_LOGIN and (not request.is_secure()):
        new_url = "https://%s%s" % (request.get_host(), request.get_full_path())
        return HttpResponseRedirect(new_url)

    error_message = ''
    
    if request.method == 'GET':
        form = StudentLoginForm()
        if 'next' in request.GET:
            next = request.GET['next']
        else:
            next = reverse('std_index')
    else:
        form = StudentLoginForm(request.POST)

        if form.is_valid():
            student_id = form.cleaned_data['student_id']
            password = form.cleaned_data['password']

            #backend = ImapBackend()
            backend = WebBackend()

            fake_login = settings.FAKE_STUDENT_AUTHEN and settings.DEBUG
            demo_account = (student_id.startswith(settings.DEMO_STUDENT_PREFIX)
                            and password == settings.DEMO_STUDENT_PASSWORD)

            nontri_username = std_id_to_nontri_username(student_id)

            if (nontri_username and
                ((fake_login) or 
                 (demo_account) or
                 #(backend.check_imap_password(
                 #           nontri_username,
                 #           password)) or
                 (backend.check_password(
                            nontri_username,
                            password)) or
                 (StudentExtraPassword.check_password_for(student_id, 
                                                          password)))):
                    
                # authentication successful
                try:
                    student = Student.objects.get(student_id=student_id)

                    for hook in settings.LOGIN_HOOKS:
                        hook_module = import_module(hook[0])
                        f = getattr(hook_module, hook[1])
                        resp = f(student)
                        if resp:
                            return resp
                    
                    Log.create("Student Login - from: %s, student: %s" %
                               (request.META['REMOTE_ADDR'],
                                student.student_id),
                               student=student)
                    request.session['std_id'] = student_id

                    response = HttpResponseRedirect(request.POST['next'])
                    response['Location'] = 'http://%s%s' % (request.get_host(), response['Location'])
                    return response

                except Student.DoesNotExist:
                    error_message = 'ไม่พบข้อมูลของคุณในระบบ กรุณาติดต่อเจ้าหน้าที่'
            
            else:
                error_message = 'ไม่สามารถเข้าใช้งานได้เนื่องจาก รหัสผ่านผิดพลาด'
        
        if 'next' in request.GET:
            next = request.GET['next']
        else:
            next = reverse('std_index')

    logins_with_kusso = settings.LOGINS_WITH_KUSSO
    kusso_login_url = settings.KUSSO_LOGIN_URL['std']
    
    return render_to_response("std/auth/login.html",
                              { 'form': form,
                                'next': next,
                                'error_message': error_message,

                                'logins_with_kusso': logins_with_kusso,
                                'kusso_login_url': kusso_login_url },
                              context_instance=RequestContext(request))

def login_with_token(request):
    token = request.GET.get('token','')
    token_items = token.split('_')
    if len(token_items) != 3:
        return redirect('std_login')

    student_id, nonce, token_hash = token_items
    try:
        student = Student.objects.get(student_id=student_id)
    except:
        student = None

    if student and StudentTokenNonce.check_and_save(student_id, nonce):
        if StudentTokenNonce.check_token_hash(student_id,
                                              nonce,
                                              token_hash):
            Log.create("Student Token Login - from: %s, student: %s" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id),
                       student=student)
            request.session['std_id'] = student_id
            return redirect('std_index')

    return redirect('std_login')


def logout(request):
    try:
        del request.session['std_id']
    except KeyError:
        pass

    if settings.LOGINS_WITH_KUSSO:
        kusso_logout_url = settings.KUSSO_LOGOUT_URL
        return HttpResponseRedirect(kusso_logout_url)

    return HttpResponseRedirect(reverse("front_page"))


def iup_student_redirect_hook(student):
    if (student.email) and (student.email.endswith("(**iup**)")):
        token = StudentTokenNonce.generate_token(student.student_id, secret=IUP_TOKEN_SECRET)
        url = 'https://wt.eng.ku.ac.th/iupwt/std/login/token/?token=' + token
        print(url)
        return redirect(url)
    else:
        return None
