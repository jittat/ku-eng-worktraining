# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django import forms
import time, datetime

from django.template import RequestContext
from datetime import date
from regis.models import Student,Conditions,Status,Department
from std.models import EmailConfirmation
from std.decorators import std_login_required
from evaluations.models import Evaluation
from commons import academia
from commons.academia import get_current_academic_year
from commons.models import Log
from commons.email import send_email_confirmation_email
from commons.utils import extract_notice

currentpage = 'profile'

class StdEditForm(forms.Form):
    tel_no = forms.CharField(label=u'Tel. :')
    email = forms.EmailField(label=u'Email :')
    department = forms.ModelChoiceField(queryset=Department.objects.filter(is_hidden=False),
                                        empty_label=u"ไม่มีข้อมูล")

def remove_zero_space(st):
    if u'\u200b' in st:
        return st.replace(u'\u200b','')
    else:
        return st
    
@std_login_required
def edit(request):
    student = request.student
    if request.method=='POST':
        form = StdEditForm(request.POST)
        if form.is_valid():
            old_email = student.email
            student.email = remove_zero_space(form.cleaned_data['email'])
            student.tel_no = form.cleaned_data['tel_no']
            student.department = form.cleaned_data['department']
            student.save()

            Log.create("edit profile - from: %s, student: %s" %
                       (request.META['REMOTE_ADDR'],
                        student.student_id),
                       student=student)

            if old_email!=student.email or (not student.has_confirmed_email()):
                confirmation = EmailConfirmation.create_or_update_for(student)
                send_email_confirmation_email(student,confirmation)

                request.session['notice'] = u'เราได้ส่งอีเมล์ไปยังอีเมล์ที่กรอกแล้ว กรุณาตรวจสอบเพื่อยืนยันอีเมล์โดยคลิ๊กลิงก์ในอีเมล์ดังกล่าว'

                Log.create("email confirmation request - from %s, student: %s" %
                           (request.META['REMOTE_ADDR'],
                            student.student_id),
                           student=student)


            return HttpResponseRedirect(reverse('std_profile'))
    else:
        if student.department:
            department_id = student.department.id
        else:
            department_id = None
        form = StdEditForm(initial={'email':student.email,
                                    'tel_no' : student.tel_no,
                                    'department': student.department_id })

    return render_to_response("std/editprofile.html",
                              { 'student': student,
                                'form': form ,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

def confirm_email(request,code):
    student, already_confirmed = EmailConfirmation.confirm_using(code)
    if not student:
        raise Http404

    if already_confirmed:
        return render_to_response("std/email_confirmed.html",
                                  { 'student': student },
                                  context_instance=RequestContext(request))
    else:
        Log.create("Student Login (via email confirmation) - from: %s, student: %s" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id),
                   student=student)
        request.session['std_id'] = student.student_id

        if student.has_registered_for_next_year():
            request.session['std_index_notice'] = 'คุณได้ยืนยันอีเมล์เรียบร้อยแล้ว'
            return redirect('std_index')

        if student.has_not_been_assigned():
            request.session['std_index_notice'] = 'คุณได้ยืนยันอีเมล์เรียบร้อยแล้ว'
            return redirect('std_checkstatus')
        else:
            assignments = student.assignments.all()
            return render_to_response("std/email_confirmed_to_apply.html",
                                      { 'student': student,
                                        'assignments': assignments },
                                      context_instance=RequestContext(request))

@std_login_required
def profile(request):
    notice = extract_notice(request.session)
    return render_to_response("std/profile.html",
                              { 'student': request.student,
                                'notice': notice,
                                'currentpage': currentpage},
                              context_instance=RequestContext(request))
