from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'std.views',
    url(r'^$', 'index', name='std_index'),
    url(r'^underconst/$', 'underconst', name='std_under_construction'),

    url(r'^login/$', 'auth.login', name='std_login'),
    url(r'^login/token/$', 'auth.login_with_token', name='std_login_with_token'),
    url(r'^logout/$', 'auth.logout', name='std_logout'),

    url(r'^edit/', 'profile.edit', name='std_edit'),
    url(r'^profile/','profile.profile',name='std_profile'),
    url(r'^confirm_email/(.+)/','profile.confirm_email'),
    url(r'^regtype/(\d)/$','choose_reg_type',name='std_choose_reg_type'),
    url(r'^regtype/(\d)/confirm/','confirm_reg_type',name='std_confirm_reg_type'),

    url(r'^checkstatus/$','check_internship_status',name='std_checkstatus'),
    url(r'^print_request_form/$','PrintRequestForm',name='std_print_request_form'),

    url(r'^companies/$','company_list',name='std_company_list'),
      
    url(r'^selection/$','company_selection.list',name='std_selection_list'),
    url(r'^selection/select/(\d+)/$','company_selection.select',
        name='std_selection_select'),
    url(r'^selection/deselect/(\d+)/$','company_selection.deselect',
        name='std_selection_deselect'),

    url(r'^company/search/$', 'company.search',
        name='std_company_search'),
    url(r'^company/search/(.+)/$','company.list_by_char',name='std_company_list_by_char'),

    url(r'^self_appl/$', 'company.self_appl_index',
        name='std_self_appl_index'),
    url(r'^self_appl/confirm/$', 'company.self_appl_confirm',
        name='std_self_appl_confirm'),

    url(r'^self_appl/requests/$', 'company.new_request',
        name='std_self_appl_new_company_request'),
    url(r'^self_appl/requests/confirm/(\d+)/', 'company.request_confirm',
        name='std_self_appl_request_confirm'),
    url(r'^self_appl/requests/inactive/$', 'company.inactive_requests',
        name='std_self_appl_inactive_company_requests'),
    url(r'^self_appl/requests/print/(\d+)/$', 'company.print_request',
        name='std_self_appl_request_print'),
    url(r'^self_appl/requests/sending_letter/(\d+)/$', 'company.print_request_sending_letter_pdf',
        name='std_self_appl_print_request_sending_letter'),

    url(r'^self_appl/announcement/print/$', 'company.print_announcement',
        name='std_self_appl_request_print_announcement'),

    url(r'^self_appl/stdsearch/$', 'company.student_search',
        name='std_self_appl_student_search'),
    url(r'^self_appl/name/search/$', 'company.search_by_name_ajax',
        name='std_self_appl_company_search_by_name_ajax'),

    url(r'^self_appl/applications/create/$', 'company.create_direct_application',
        name='std_self_appl_create_application'),
    url(r'^self_appl/applications/update/(\d+)/$', 'company.update_direct_application',
        name='std_self_appl_update_application'),
    url(r'^self_appl/applications/cancel/(\d+)/$', 'company.cancel_direct_application',
        name='std_self_appl_cancel_application'),
    url(r'^self_appl/applications/print/(\d+)/$', 'company.print_direct_application_letter',
        name='std_self_appl_print_application_letter'),
    url(r'^self_appl/applications/verified/$', 'company.list_verified_direct_application_companies',
        name='std_self_appl_list_verified_companies'),
    url(r'^self_appl/applications/sending_letter/(\d+)/$', 'company.print_direct_application_sending_letter_pdf',
        name='std_self_appl_print_application_sending_letter'),
    
    url(r'^print_company_letter/(\d+)/$',
        'print_letter', name='std_print_letter'),

    url(r'^coop/$', 'coop.index',
        name='std_coop_index'),
    url(r'^coop/address/$', 'coop.update_address',
        name='std_coop_address'),

    url(r'^coop/companies/$', 'coop.update_company',
        name='std_coop_create_company'),

    url(r'^coop/companies/(?P<co_op_company_id>\d+)/$', 'coop.update_company',
        name='std_coop_update_company'),
    url(r'^coop/requests/(?P<co_op_company_id>\d+)/$', 'coop.create_request',
        name='std_coop_create_request'),

    url(r'^coop/requests/print/(\d+)/([a-z]+)/$', 'coop.print_request',
        name='std_coop_request_print'),
)
