# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'StudentExtraPassword'
        db.create_table('std_studentextrapassword', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=15)),
            ('hashed_password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('salt', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('std', ['StudentExtraPassword'])


    def backwards(self, orm):
        
        # Deleting model 'StudentExtraPassword'
        db.delete_table('std_studentextrapassword')


    models = {
        'std.studentextrapassword': {
            'Meta': {'object_name': 'StudentExtraPassword'},
            'hashed_password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'salt': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'student_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '15'})
        }
    }

    complete_apps = ['std']
