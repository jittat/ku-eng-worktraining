from regis.models import Student, Assignment, Receipt
from direct_appl.models import StudentApplication
from std.models import StudentSelection, CompanyDirectApplication, RequestStudent
from commons.academia import get_recent_worktraining_year, get_next_worktraining_year, get_self_appl_start_year

def get_registered_students(department, year):
    return Student.all_registered_for_year(year,
                                           department=department)


def get_assigned_students(department, year):
    assignments = (Assignment.objects
                   .filter(student__department=department,
                           year=year)
                   .select_related('student'))
    for a in assignments:
        a.student.assigned_company = a.company
        a.student.assignment = a
    return [a.student for a in assignments]

    
def get_assigned_students_from_selection(department, year):
    selections = (StudentSelection.objects
                  .filter(student__department=department,
                          year=year)
                  .filter(result=True)
                  .select_related('student'))
    for sel in selections:
        sel.student.assigned_company = sel.company
    return [s.student for s in selections]


def get_all_direct_appl_students_as_dict(department, year):
    apps = (StudentApplication.objects
            .filter(student__department=department,
                    position__year__exact=year)
            .select_related('student'))
    smap = {}
    for a in apps:
        s = a.student
        if s.id not in smap:
            smap[s.id] = s
            s.direct_apps = [a]
        else:
            smap[s.id].direct_apps.append(a)

    return smap


def get_all_direct_appl_students(department, year):
    return get_all_direct_appl_students_as_dict(department,year).values()


def get_direct_appl_waitlist_students(direct_app_students):
    waitlist = []
    for s in direct_app_students:
        if not s.has_been_assigned(get_next_worktraining_year()):
            wait = False
            for a in s.direct_apps:
                if a.is_accepted:
                    wait = False
                    break
                if a.is_accepted == None:
                    wait = True
            if wait:
                waitlist.append(s)
    return waitlist


def get_students_with_no_selections(department, year, 
                                    all_students,
                                    assigned_students,
                                    direct_app_students):
    selections = (StudentSelection.objects
                  .filter(student__department=department,
                          year=year)
                  .select_related('student')
                  .all())

    selected_ids = (set([sel.student.id for sel in selections]) |
                    set([std.id for std in direct_app_students]) |
                    set([std.id for std in assigned_students]))
    
    ns_students = [s for s in all_students
                   if s.id not in selected_ids]
    return ns_students


def find_accepted_self_appl_students(year, department=None):
    students = []
    student_ids = set()

    company_direct_applications = (CompanyDirectApplication.
                                   objects.
                                   filter(year=year).
                                   select_related('student').
                                   all())
    if department != None:
        company_direct_applications = company_direct_applications.filter(student__department=department)

    for a in company_direct_applications:
        if a.is_accepted_and_confirmed():
            if (not department) or (department.id == a.student.department_id):
                student = a.student
                if student.id not in student_ids:
                    student.assigned_company = a.get_temp_company()
                    students.append(student)
                    student_ids.add(student.id)
                
    request_students = (RequestStudent.
                        objects.
                        filter(year=year).
                        select_related('student').
                        all())
    if department != None:
        request_students = request_students.filter(student__department=department)

    for r in request_students:
        if r.is_accepted:
            if (not department) or (department.id == r.student.department_id):
                student = r.student
                if student.id not in student_ids:
                    student.assigned_company = r.company_request.get_temp_company()
                    students.append(student)
                    student_ids.add(student.id)
    return students


def get_unassigned_students(department,year):
    all_students = Student.all_registered_for_year(year,department)
    accepted_self_appl_student_ids = set([s.id for s in find_accepted_self_appl_students(year, department)])
    students = []
    for student in all_students:
        if ((not student.has_been_assigned(year)) and
            (student.id not in accepted_self_appl_student_ids)):
            students.append(student)
    return students


def get_students_with_companies(department, year):
    all_assigned_students = get_assigned_students(department, year)
    for s in all_assigned_students:
        s.registration_type = 2
    assigned_ids = set([s.id for s in all_assigned_students])

    accepted_self_appl_students = find_accepted_self_appl_students(year, department)
    for s in accepted_self_appl_students:
        if s.id not in assigned_ids:
            s.registration_type = 1
            all_assigned_students.append(s)
            
    return all_assigned_students


def find_selection_statistics(department, year):
    all_students = get_registered_students(department, year)

    student_count = len(all_students)

    assigned_students = get_assigned_students(department, year)
    assigned_ids = set([s.id for s in assigned_students])

    accepted_self_appl_students = find_accepted_self_appl_students(year, department)
    for s in accepted_self_appl_students:
        if s.id not in assigned_ids:
            assigned_students.append(s)
            assigned_ids.add(s.id)

    assigned_count = len(assigned_students)
    unassigned_count = len(all_students) - assigned_count
    assigned_selection_count = len(
        get_assigned_students_from_selection(department,
                                             year))
    assigned_direct_count = assigned_count - assigned_selection_count

    if year < get_self_appl_start_year():
        direct_app_students = get_all_direct_appl_students(department,
                                                           year)
        waitlist_count = len(get_direct_appl_waitlist_students(direct_app_students))
    else:
        direct_app_students = [s for s in all_students if s.current_status.is_registered_for_self_application()]
        waitlist_count = len([s for s in direct_app_students
                              if s.id not in assigned_ids])
        
    no_activity_count = len(get_students_with_no_selections(department,
                                                            year,
                                                            all_students,
                                                            assigned_students,
                                                            direct_app_students))
    receipts = Receipt.objects.filter(department=department,
                                      year=year).all()
    num_positions = sum([r.total_student for r in receipts])
    num_available_positions = sum([r.num_available_positions() for r in receipts])
    num_hidden_avail_positions = sum([r.num_available_positions() for r in receipts if r.is_hidden])

    return {
        'num_students': student_count,
        'num_faculty_selection': student_count - len(direct_app_students),
        'num_own_selection': len(direct_app_students),
        'num_assigned': assigned_count,
        'num_unassigned' : unassigned_count,
        'num_assigned_selection': assigned_selection_count,
        'num_assigned_direct': assigned_direct_count,
        'num_direct_app_waitlist': waitlist_count,
        'num_no_activity': no_activity_count,
        'num_positions': num_positions,
        'num_available_positions': num_available_positions,
        'num_hidden_avail_positions': num_hidden_avail_positions,
        'num_shown_avail_positions':  num_available_positions - num_hidden_avail_positions,
        }

