# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse

from datetime import datetime

from commons.decorators import committee_required, admin_or_committee_required, department_admin_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from regis.models import Company, Department, Student, DepartmentNameInRequestLetter, Receipt, Grade
from commons.utils import extract_notice, UnicodeWriter
from std.models import CoOpCompany, CoOpStudentDetail, CoOpRequest, CoOpSemester

from std.models import assign_selections_to_receipts
from commons.models import Log
from commons.email import send_approval_confirmation
from commons.email import send_mail_to_student
from commons.email import send_company_request_approval_email
from commons.email import send_company_request_acceptance_confirmation
from commons.email import send_company_request_rejectance_confirmation
from commons.email import send_company_request_result_reset
from commons.email import send_company_request_result_reject
from commons.email import send_company_direct_application_result

from reports import build_student_request_letter_pdf_response

from std.views.coop import CoOpRequestForm, CoOpCompanyForm

currentpage = 'department'
sendmail = False

def load_co_op_requests_and_check_printable(department, year):
    co_op_requests = CoOpRequest.get_for_department_year(department, year)

    co_op_semesters = {(year,1): CoOpSemester.get_for(year,1),
                       (year-1,2): CoOpSemester.get_for(year-1,2)}

    for r in co_op_requests:
        if co_op_semesters[(r.internship_year, r.internship_semester)] == None:
            r.is_sending_letter_printable = False
        else:
            r.is_sending_letter_printable = co_op_semesters[(r.internship_year, r.internship_semester)].is_info_ready_for_sending_letter()

        r.is_shown_in_print_only = r.is_accepted and r.is_sending_letter_printable
            
    return co_op_requests

def load_co_op_company_forms(co_op_companies):
    for company in co_op_companies:
        company.form = CoOpCompanyForm(initial={'name': company.name,
                                                'address': company.address,
                                                'tel_number': company.tel_number,
                                                'fax_number': company.fax_number,
                                                'is_public': company.is_public},
                                       prefix='coop_company_' + str(company.id))

@admin_or_committee_required
def index(request, department_code):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    if request.GET.get('year',None):
        year = int(request.GET.get('year'))

    try:
        current_co_op_company_id = int(request.GET.get('cid','0'))
    except:
        current_co_op_company_id = 0
        
    co_op_companies = CoOpCompany.get_for_department_year(department, year)

    load_co_op_company_forms(co_op_companies)

    co_op_requests = load_co_op_requests_and_check_printable(department, year)
            
    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)
    return render_to_response("committee/coop/index.html",
                              { 'department': department,
                                'year': year,
                                
                                'co_op_companies': co_op_companies,
                                'co_op_requests': co_op_requests,

                                'current_co_op_company_id': current_co_op_company_id,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))

def can_user_update_request(user, department, co_op_request):
    is_committee = user.userprofile.is_committee_member
    committee_department = user.userprofile.department

    return (is_committee and
            (department.id == committee_department.id) and
            (co_op_request.student.department_id == department.id))

def can_user_print_request(user, department, co_op_request):
    is_committee_or_dept_admin = user.userprofile.is_committee_member or user.userprofile.is_department_admin
    committee_department = user.userprofile.department

    return (is_committee_or_dept_admin and
            (department.id == committee_department.id) and
            (co_op_request.student.department_id == department.id))

@committee_required
def update_request(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    co_op_request = get_object_or_404(CoOpRequest, id=request_id) 
    year = get_next_worktraining_year()
    if request.GET.get('year',None):
        year = int(request.GET.get('year'))

    co_op_company = co_op_request.co_op_company
    student = co_op_request.student
    is_committee = request.user.userprofile.is_committee_member

    if not can_user_update_request(request.user, department, co_op_request):
        return HttpResponseForbidden()

    if request.method == 'POST':
        if 'cancel' in request.POST:
            request.session['notice'] = u'คุณได้ยกเลิกการแก้ไขแล้ว'
        else:
            if co_op_request.is_approved_by_committee == None:
                co_op_request.is_approved_by_committee = (request.POST['result'] == 'yes')

                if co_op_request.is_approved_by_committee:
                    co_op_request.reject_comments = None
                    Log.create("committee approve co-op request - from: %s, cr: %d" %
                               (request.META['REMOTE_ADDR'],
                                co_op_request.id),
                               student=student,
                               user=request.user)
                else:
                    co_op_request.reject_comments = request.POST['reject_explanation']
                    Log.create("committee reject co-op request - from: %s, cr: %d" %
                               (request.META['REMOTE_ADDR'],
                                co_op_request.id),
                               student=student,
                               user=request.user)
                co_op_request.approved_at = datetime.now()
                co_op_request.save()

            if co_op_request.is_approved_by_committee:
                request.session['notice'] = (u'คุณได้อนุมัติการขอฝึกงานที่' + co_op_company.name +
                                             u' โดย ' + unicode(student) + u'แล้ว')
            else:
                request.session['notice'] = (u'คุณได้ปฏิเสธการขอฝึกงานที่' + co_op_company.name +
                                             u' โดย ' + unicode(student) + u'แล้ว')

        return redirect('committee_coop_index', department.code)

    return render_to_response("committee/coop/update_request.html",
                              { 'department': department,
                                'year': year,

                                'student': student,
                                'co_op_request': co_op_request,
                                'co_op_company': co_op_company,
                                
                                'currentpage': currentpage,
                                'is_committee': is_committee},
                              context_instance=RequestContext(request))

@committee_required
def edit_request(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    co_op_request = get_object_or_404(CoOpRequest, id=request_id) 
    year = get_next_worktraining_year()
    if request.GET.get('year',None):
        year = int(request.GET.get('year'))
    next_academic_year = get_next_worktraining_year()
    
    co_op_company = co_op_request.co_op_company
    student = co_op_request.student
    is_committee = request.user.userprofile.is_committee_member

    if not can_user_update_request(request.user, department, co_op_request):
        return HttpResponseForbidden()

    if request.method == 'POST':
        if 'cancel' in request.POST:
            request.session['notice'] = u'คุณได้ยกเลิกการแก้ไขแล้ว'
            return redirect('committee_coop_index', department.code)

        form = CoOpRequestForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['internship_semester'] == '1':
                internship_semester = 1
                internship_year = next_academic_year
            else:
                internship_semester = 2
                internship_year = next_academic_year-1

            co_op_request.internship_semester = internship_semester
            co_op_request.internship_year=internship_year
            co_op_request.description=form.cleaned_data['description']
            co_op_request.signer_name=form.cleaned_data['signer_name']
            co_op_request.contact_person=form.cleaned_data['contact_person']
            co_op_request.contact_tel_number=form.cleaned_data['contact_tel_number']
            co_op_request.contact_email=form.cleaned_data['contact_email']
            co_op_request.beginning_date=form.cleaned_data['beginning_date']
            co_op_request.end_date=form.cleaned_data['end_date']

            co_op_request.save()

            Log.create("committee update co-op request - from: %s, cr: %d" %
                       (request.META['REMOTE_ADDR'],
                        co_op_request.id),
                       student=student,
                       user=request.user)
            request.session['notice'] = (u'คุณได้แก้ไขการขอฝึกงานที่' + co_op_company.name +
                                         u' โดย ' + unicode(student) + u'แล้ว')

            return redirect('committee_coop_index', department.code)
    else:
        form = CoOpRequestForm(initial={
            'internship_semester': co_op_request.internship_semester,
            'description': co_op_request.description,
            'signer_name': co_op_request.signer_name,
            'contact_person': co_op_request.contact_person,
            'contact_tel_number': co_op_request.contact_tel_number,
            'contact_email': co_op_request.contact_email,
            'beginning_date': co_op_request.beginning_date,
            'end_date': co_op_request.end_date,
        })

    return render_to_response("committee/coop/edit_request.html",
                              { 'department': department,
                                'year': year,

                                'student': student,
                                'co_op_request': co_op_request,
                                'co_op_company': co_op_company,

                                'form': form,
                                
                                'currentpage': currentpage,
                                'is_committee': is_committee},
                              context_instance=RequestContext(request))


@committee_required
def update_request_result(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    co_op_request = get_object_or_404(CoOpRequest, id=request_id) 
    year = get_next_worktraining_year()
    if request.GET.get('year',None):
        year = int(request.GET.get('year'))

    co_op_company = co_op_request.co_op_company
    student = co_op_request.student
    is_committee = request.user.userprofile.is_committee_member

    if not can_user_update_request(request.user, department, co_op_request):
        return HttpResponseForbidden()

    if request.method == 'POST':
        is_accepted = None
        if 'accepted' in request.POST:
            is_accepted = True
        elif 'rejected' in request.POST:
            is_accepted = False

        if is_accepted != None:
            co_op_request.is_accepted = is_accepted
            co_op_request.accepted_at = datetime.now()
            co_op_request.save()

            if co_op_request.is_accepted:
                Log.create("committee update is-accepted co-op request - from: %s, cr: %d" %
                           (request.META['REMOTE_ADDR'],
                            co_op_request.id),
                           student=student,
                           user=request.user)
                request.session['notice'] = (u'คุณได้บันทึกผลการขอฝึกงานที่' + co_op_company.name +
                                             u' โดย ' + unicode(student) + u'แล้ว (หน่วยงานรับ)')
            else:
                Log.create("committee update is-rejected co-op request - from: %s, cr: %d" %
                           (request.META['REMOTE_ADDR'],
                            co_op_request.id),
                           student=student,
                           user=request.user)
                request.session['notice'] = (u'คุณได้บันทึกผลการขอฝึกงานที่' + co_op_company.name +
                                             u' โดย ' + unicode(student) + u'แล้ว (หน่วยงานปฏิเสธ)')

    return redirect('committee_coop_index', department.code)


@committee_required
def edit_coop_company(request, department_code, co_op_company_id):
    department = get_object_or_404(Department, code=department_code)
    company = get_object_or_404(CoOpCompany, pk=co_op_company_id)

    if request.method == 'POST':
        form = CoOpCompanyForm(request.POST, prefix='coop_company_' + str(company.id))

        if form.is_valid():
            company.name = form.cleaned_data['name']
            company.address=form.cleaned_data['address']
            company.tel_number=form.cleaned_data['tel_number']
            company.fax_number=form.cleaned_data['fax_number']
            company.save()
            
            request.session['notice'] = u'คุณได้แก้ไขรายละเอียดหน่วยงาน ' + company.name + u' แล้ว'
        else:
            request.session['notice'] = u'ERROR: ไม่สามารถแก้ไขรายละเอียดหน่วยงาน ' + company.name + u' ได้'

    return redirect(reverse('committee_coop_index', args=[department.code]) + '?cid=' + str(company.id))

@admin_or_committee_required
def print_request_pdf(request, department_code, co_op_request_id, lang):
    from reports import build_co_op_request_letter_pdf_response

    department = get_object_or_404(Department, code=department_code)
    co_op_request = get_object_or_404(CoOpRequest, id=co_op_request_id) 
    student = co_op_request.student

    if not request.user.userprofile.is_sa_admin:
        if not can_user_print_request(request.user, department, co_op_request):
            return HttpResponseForbidden()
    
        if not co_op_request.is_approved_by_committee:
            return HttpResponseForbidden()

    if lang not in ['th','en']:
        lang = 'th'

    return build_co_op_request_letter_pdf_response(co_op_request, lang)


@department_admin_required
def print_sending_letter_pdf(request, department_code, co_op_request_id, lang):
    from reports import build_co_op_sending_letter_pdf_response

    department = get_object_or_404(Department, code=department_code)
    co_op_request = get_object_or_404(CoOpRequest, id=co_op_request_id) 
    student = co_op_request.student

    if not request.user.userprofile.is_sa_admin:
        if not can_user_print_request(request.user, department, co_op_request):
            return HttpResponseForbidden()
    
        if not co_op_request.is_accepted:
            return HttpResponseForbidden()

    if lang not in ['th','en']:
        lang = 'th'

    return build_co_op_sending_letter_pdf_response(co_op_request,
                                                   lang,
                                                   False)


def build_request_report(co_op_requests, filename, with_department_comment=False):
    header = [
        u'ลำดับ',
        u'เลขประจำตัว',
        u'ชื่อ-นามสกุล',
        u'ที่อยู่นิสิต',
        u'หมายเลขโทรศัพท์',
        u'e-mail address',
        u'ชื่อสถานประกอบการ',
        u'ที่อยู่สถานประกอบการ',
        u'ชื่อผู้ที่จะให้ส่งหนังสือถึง/เบอร์โทรศัพท์/โทรสาร',
        u'ชื่อผู้ประสานงาน/Email'
    ]
    if with_department_comment:
        header += [u'หมายเหตุ']

    rows = []
    counter = 0
    for r in co_op_requests:
        counter += 1
        student = r.student
        company = r.co_op_company
        student_detail = CoOpStudentDetail.get_for_student_year(student, r.year)
        if student_detail:
            student_address = student_detail.address
        else:
            student_address = ''
        items = [
            str(counter),
            student.student_id,
            student.full_name(),
            student_address,
            student.tel_no,
            student.email,
            company.name,
            company.address,
            r.signer_name + u'/' + company.tel_number + u'/' + company.fax_number,
            r.contact_person + u'/' + r.contact_email,
        ]
        if with_department_comment:
            items += [unicode(student.department)]
        rows.append(items)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
    
    writer = UnicodeWriter(response)
    writer.writerow(header)
    for r in rows:
        writer.writerow(r)
    return response
    
@admin_or_committee_required
def report_requests(request, department_code, year, semester):
    if department_code != '0':
        department = get_object_or_404(Department, code=department_code)
    else:
        if not request.user.userprofile.is_sa_admin:
            return HttpResponseForbidden()
        department = None
        
    co_op_requests = CoOpRequest.objects.filter(internship_year=year,
                                                internship_semester=semester,
                                                is_accepted=True)
    if department:
        co_op_requests = co_op_requests.filter(department=department)

    return build_request_report(co_op_requests,
                                'co-op-' + department_code + '-' + year + '-' + semester + '.csv',
                                (department==None))
    
            
