# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext, loader, Context
from django.http import HttpResponseRedirect, HttpResponse
from django import forms
from django.core.urlresolvers import reverse

from commons.decorators import committee_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from regis.models import Company, Department, Receipt, Assignment
from committee.models import CompanySelection, DepartmentSelectionInfo
from commons.utils import extract_notice
from std.models import VerifiedDirectApplicationCompany

MAX_DISPLAY_PER_PAGE = 50

currentpage = 'committee_company'

@committee_required
def menu(request):
    department = request.committee_department
    recent_selection_count = (
        CompanySelection
        .selected_by_department_for_next_year(department)
        .count())
    
    new_company_count = Company.all_new().count()

    notice = extract_notice(request.session)
    year = get_next_worktraining_year()
    
    return render_to_response("committee/company_selection/index.html",
                              { 'recent_selection_count': recent_selection_count,
                                'new_company_count': new_company_count,
                                'year': year,
                                'department': department,
                                'currentpage':currentpage,
                                'notice': notice,
                                'is_committee': True },
                              context_instance=RequestContext(request))

@committee_required
def list(request,page_id):
    department = request.committee_department
    companys = Company.objects.all().order_by('short_name').order_by('is_new')
    total = Company.objects.all().count()
    pages = range(1,((total+MAX_DISPLAY_PER_PAGE-1)/MAX_DISPLAY_PER_PAGE)+1)        
    page_id = int(page_id)
    if page_id==0:
        page_id = pages[-1]
    begin = (page_id - 1) * MAX_DISPLAY_PER_PAGE
    end = begin + MAX_DISPLAY_PER_PAGE

    companys = companys[begin:end]
    companyselections = CompanySelection.objects.filter(department=department,year=get_current_academic_year()+1)
    selected_ids = set([c.company_id for c in companyselections])

    for company in companys:
        company.is_selected = company.id in selected_ids

    bookmarked_page = DepartmentSelectionInfo.get_bookmarked_page_for(department)
    recent_selection_count = (
        CompanySelection
        .selected_by_department_for_next_year(department)
        .count())

    verified_companies = VerifiedDirectApplicationCompany.get_dict_for_years(get_current_academic_year()+1)

    return render_to_response("committee/company_selection/list.html",
                              { 'companys':companys,
                                'verified_companies': verified_companies,
                                'department': department,
                                'pages':pages,
                                'page_current':page_id,
                                'currentpage':currentpage,
                                'bookmarked_page': bookmarked_page,
                                'recent_selection_count':recent_selection_count,
                                'is_committee':True},
                              context_instance=RequestContext(request))


@committee_required
def list_stats(request, reply_years, assign_years):
    if request.method == 'POST':
        if request.POST['qtype']=='assign':
            return redirect('committee_company_list_from_stats',
                            0,request.POST['year'])
        else:
            return redirect('committee_company_list_from_stats',
                            request.POST['year'],0)

    department = request.committee_department
    companyselections = CompanySelection.objects.filter(department=department,year=get_current_academic_year()+1)
    companies = []

    reply_years = int(reply_years)
    assign_years = int(assign_years)

    current_year = get_current_academic_year()
    reply_start_year = current_year - reply_years + 1
    assign_start_year = current_year - assign_years + 1

    msg = u''
    qtype_choice = 'reply'
    year_choice = '1'
    if reply_years > 0:
        year_choice = reply_years
        receipts = (Receipt
                    .objects
                    .filter(department=department,
                            year__gte=reply_start_year)
                    .select_related('company', 'student')
                    .all())
        companies1 = [r.company for r in receipts]
        msg = u'ตอบจดหมายขอความอนุเคราะห์ในรอบ %d ปี' % reply_years
    else:
        companies1 = []

    if assign_years > 0:
        qtype_choice = 'assign'
        year_choice = assign_years
        assignments = (Assignment
                       .objects
                       .filter(year__gte=assign_start_year,
                               student__department=department)
                       .select_related('student', 'company')
                       .all())
        companies2 = [a.company for a in assignments]
        if msg!='':
            msg += u'และ'
        msg += u'รับนิสิตจากสาขาในรอบ %d ปี' % assign_years
    else:
        assignments = []
        companies2 = []

    if reply_start_year < assign_start_year:
        assignments = (Assignment
                       .objects
                       .filter(year__gte=reply_start_year,
                               student__department=department)
                       .select_related('student','company')
                       .all())        

    found_set = set()
    companies = []
    for c in companies2 + companies1:
        if (c) and (c.id not in found_set):
            companies.append(c)
            found_set.add(c.id)

    companies.sort(cmp=lambda x,y: cmp(x.name, y.name))

    selected_ids = set([c.company_id for c in companyselections])
    cdict = dict()
    for c in companies:
        c.is_selected = c.id in selected_ids
        c.assigned_count = 0
        cdict[c.id] = c
    for a in assignments:
        if a.company_id in cdict:
            cdict[a.company_id].assigned_count += 1
    
    recent_selection_count = (
        CompanySelection
        .selected_by_department_for_next_year(department)
        .count())

    verified_companies = VerifiedDirectApplicationCompany.get_dict_for_years(get_current_academic_year()+1)

    return render_to_response("committee/company_selection/list_stats.html",
                              { 'companys':companies,
                                'verified_companies': verified_companies,
                                'page_current': 1,
                                'search_title': msg,
                                'department': department,
                                'qtype_choice': qtype_choice,
                                'year_choice': year_choice,
                                'currentpage':currentpage,
                                'recent_selection_count':recent_selection_count,
                                'is_committee':True},
                              context_instance=RequestContext(request))


@committee_required
def show(request):
    department = request.committee_department
    
    year = get_next_worktraining_year()

    selection = (
        CompanySelection
        .selected_by_department_for_year(department, year))

    companies = [sel.company for sel in selection]

    department_receipts = Receipt.objects.filter(department=department,
                                                 year=year)
    all_receipts = Receipt.objects.filter(year=year)

    accepted_ids = set([r.company_id for r in department_receipts])
    replied_ids = set([r.company_id for r in all_receipts])

    for c in companies:
        c.accepted = (c.id in accepted_ids)
        c.rejected = ((not c.id in accepted_ids) and
                      (c.id in replied_ids))

    verified_companies = VerifiedDirectApplicationCompany.get_dict_for_years(get_current_academic_year()+1)

    return render_to_response("committee/company_selection/show.html",
                              { 'companies':companies,
                                'verified_companies': verified_companies,
                                'department': department,
                                'currentpage':currentpage,
                                'is_committee':True},
                              context_instance=RequestContext(request))

@committee_required   
def show_delete(request, company_id):
    department = request.committee_department
    year = get_next_worktraining_year()
    company = Company.objects.get(id=company_id)
    companyselection = CompanySelection.objects.filter(department=department,company=company,year=year)
    companyselection.delete()
    return HttpResponseRedirect(reverse('committee_company_selection_show'))
    
@committee_required
def select_all(request):
    department = request.committee_department
    year = get_next_worktraining_year()
    company_count = Company.objects.count()
    current_selection_count = CompanySelection.objects.filter(department=department, year=year).count()

    if request.method == 'POST':
        if 'cancel' not in request.POST:
            (CompanySelection
             .selected_by_department_for_year(department, year).delete())
                
            for company in Company.objects.all():
                sel = CompanySelection(department=department,
                                       company=company,
                                       year=year)
                sel.save()
            
            request.session['notice'] = 'เลือกสถานประกอบการทั้งหมดแล้ว'
        else:
            request.session['notice'] = 'ยกเลิกการเลือกสถานประกอบการทั้งหมด รายการที่เลือกไว้ยังคงเป็นเช่นเดิม'

        return redirect("committee_company_selection")

    return render_to_response("committee/company_selection/select_all.html",
                              { 'year': year,
                                'department': department,
                                'company_count': company_count,
                                'selected_company_count': current_selection_count,
                                'currentpage':currentpage,
                                'is_committee':True },
                              context_instance=RequestContext(request))   

    

@committee_required
def select_using_last_year(request):
    department = request.committee_department
    
    last_year = get_recent_worktraining_year()
    year = get_next_worktraining_year()

    assignments = department.assignments_for_year(last_year)
    company_ids = set()
    companies = []
    for assignment in assignments:
        if not assignment.company_id:
            continue
        if assignment.company_id not in company_ids:
            company_ids.add(assignment.company_id)
            companies.append(assignment.company)

    company_count = len(company_ids)

    old_selection_count = (
        CompanySelection
        .selected_by_department_for_next_year(department)
        .count())

    if request.method == 'POST':
        if 'cancel' not in request.POST:
            if 'selection_replace' in request.POST:
                # remove old selection for this year
                (CompanySelection
                 .selected_by_department_for_year(department, year).delete())
                
            recent_selection = (
                CompanySelection
                .selected_by_department_for_year(department, year))

            recent_ids = set([sel.company_id for sel in recent_selection])

            for company in companies:
                if company.id not in recent_ids:
                    sel = CompanySelection(department=department,
                                           company=company,
                                           year=year)
                    sel.save()

            request.session['notice'] = 'เลือกสถานประกอบการจากข้อมูลที่นิสิตไปฝึกงานในปีการศึกษาก่อนแล้ว'
        else:
            request.session['notice'] = 'ยกเลิกการเลือกสถานประกอบการจากรายการที่นิสิตไปฝึกงานในปีการศึกษาก่อนแล้ว รายการที่เลือกไว้ยังคงเป็นเช่นเดิม'

        return redirect("committee_company_selection")

    return render_to_response("committee/company_selection/select_last_year.html",
                              { 'last_year': last_year,
                                'year': year,
                                'companies': companies,
                                'company_count': company_count,
                                'old_selection_count': old_selection_count,
                                'department': department,
                                'currentpage':currentpage,
                                'is_committee':True },
                              context_instance=RequestContext(request))                              

@committee_required
def select_using_copy_other_department(request):
    department = request.committee_department
    year = get_next_worktraining_year()
    departments = Department.objects.all()
    return render_to_response("committee/company_selection/select_department.html",
                              { 'departments': departments,
                                'year': year,
                                'department': department,
                                'currentpage':currentpage,
                                'is_committee':True },
                              context_instance=RequestContext(request))    

@committee_required    
def select_using_copy_other_department_list(request):
    department = request.committee_department
    year = get_next_worktraining_year()
    department_selected_id = (request.POST['department_selected_id'])
    department_selected  = Department.objects.get(id=department_selected_id)
    
    companyselections = CompanySelection.objects.filter(department=department_selected,year=year).select_related('company','department')
    
    old_selection_count = (CompanySelection.selected_by_department_for_next_year(department).count())
    
    return render_to_response("committee/company_selection/select_from_other_department.html",
                              { 'department_selected': department_selected,
                                'year': year,
                                'department': department,
                                'old_selection_count': old_selection_count,
                                'companyselections': companyselections,
                                'currentpage':currentpage,
                                'is_committee':True },
                              context_instance=RequestContext(request))   

@committee_required
def select_using_copy_other_department_submit(request):
    department = request.committee_department
    year = get_next_worktraining_year()
    if request.method == 'POST':
        if 'cancel' not in request.POST:
            if 'selection_replace' in request.POST:
                (CompanySelection
                 .selected_by_department_for_year(department, year).delete())
            
            companyselections_current = CompanySelection.selected_by_department_for_year(department, year)
            department_selected_id = request.POST['department_selected_id']
            department_selected  = Department.objects.get(id=department_selected_id)
            companyselections_selected = CompanySelection.selected_by_department_for_year(department_selected, year)
            
            company_ids_current = set([sel.company_id for sel in companyselections_current])
            
            for companyselection in companyselections_selected:
                if companyselection.company_id not in company_ids_current:
                    company = Company.objects.get(id=companyselection.company_id)
                    sel = CompanySelection(department=department,
                                           company=company,
                                           year=year)
                    sel.save()
        
            request.session['notice'] = 'เลือกสถานประกอบการจากสาขาอื่นแล้ว'
        else:
            request.session['notice'] = 'ยกเลิกการเลือกสถานประกอบการ รายการที่เลือกไว้ยังคงเป็นเช่นเดิม'

        return redirect("committee_company_selection")
            

def add_company_to_selection_for_next_year(department, company):
    companyselections = (CompanySelection.objects
                         .filter(department=department,
                                 company=company,
                                 year=get_next_worktraining_year()))
    if len(companyselections)==0:
        companyselection = CompanySelection(company=company,
                                            department=department,
                                            year=get_next_worktraining_year())
        companyselection.save()
            

@committee_required
def select_from_search(request, company_id):
    """
    select a company using the search result from the generic company
    search.
    """
    department = request.committee_department
    company = get_object_or_404(Company,pk=company_id)

    add_company_to_selection_for_next_year(department,
                                           company)

    request.session['notice'] = (u'เพิ่ม %s เข้าในรายการสถานประกอบการแล้ว' % 
                                 company.name)
    return redirect(reverse('committee_company_selection_search'))
    
@committee_required
def remove_from_search(request, company_id):
    """
    remove a company using the search result from the generic company
    search.
    """
    department = request.committee_department
    company = get_object_or_404(Company,pk=company_id)

    selections = (CompanySelection.
                  objects.filter(department=department,
                                 company=company,
                                 year=get_next_worktraining_year()))
    if len(selections) != 0:
        for selection in selections:
            selection.delete()

        request.session['notice'] = (u'ลบ %s ออกจากรายการสถานประกอบการแล้ว' % 
                                     company.name)
    else:
        request.session['notice'] = (u'คุณไม่ได้เลือก %s ในรายการสถานประกอบการ' % 
                                     company.name)
        
    return redirect(reverse('committee_company_selection_search'))
    
####################################
#  These are for ajax requests
####################################

@committee_required
def list_select(request):
    department = request.committee_department
    if request.method=='POST':
        company_id = (request.POST['company_id'])
        status = (request.POST['status'])
        company = get_object_or_404(Company, pk=company_id)
        if status == "add":
            add_company_to_selection_for_next_year(department,
                                                   company)
        else:
            companyselection = CompanySelection.objects.filter(
                department=department,
                company=company,
                year=get_next_worktraining_year())
            companyselection.delete()
    return HttpResponse("OK")
    
@committee_required
def page_bookmark(request, page_id):
    department = request.committee_department
    DepartmentSelectionInfo.set_bookmarked_page_for(department, int(page_id))
    return redirect('committee_company_list_selection',
                    page_id=page_id)
