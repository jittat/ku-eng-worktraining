# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse

from commons.decorators import committee_required, admin_or_committee_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from regis.models import Company, Department, Student, DepartmentNameInRequestLetter, Receipt, Grade
from commons.utils import extract_notice
from direct_appl.models import DirectApplicationPosition, StudentApplication
from std.models import SelectionRound, SelectionRoundStatus, StudentSelection
from std.models import assign_selections_to_receipts
from commons.models import Log
from commons.email import send_approval_confirmation
from commons.email import send_mail_to_student

#currentpage = 'student_selection'
currentpage = 'department'
sendmail = False

@admin_or_committee_required
def index(request, department_code):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()

    student_count = len(Student.all_registered_for_year(year,
                                                        department=department))

    direct_position_count = DirectApplicationPosition.get_all_for_year(year).count()

    selection_round_status = SelectionRoundStatus.get_status()
    if selection_round_status:
        if selection_round_status.is_open:
            last_round_number = selection_round_status.current_round_number
        else:
            last_round_number = selection_round_status.next_round_number
            if last_round_number == None:
                last_round = SelectionRoundStatus.get_last_round()
                if last_round!=None:
                    last_round_number = last_round.number
    else:
        last_round_number = 0

    rounds = [r for r in SelectionRound.get_all_rounds()
              if r.number <= last_round_number]

    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)

    return render_to_response("committee/student_selection/index.html",
                              { 'department': department,
                                'year': year,
                                'student_count': student_count,
                                'direct_position_count': direct_position_count,
                                'selection_round_status': selection_round_status,
                                'rounds': rounds,
                                'last_round_number': last_round_number,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))

@admin_or_committee_required
def list_direct_appl(request, department_code, is_show_all=False):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)

    direct_positions = DirectApplicationPosition.get_all_for_year(year)
    applications = StudentApplication.all_for_department(department,year)

    apps_for_pos = {}
    for app in applications:
        if app.position.id not in apps_for_pos:
            apps_for_pos[app.position.id] = [app]
        else:
            apps_for_pos[app.position.id].append(app)

    for p in direct_positions:
        if p.id in apps_for_pos:
            p.applications = apps_for_pos[p.id]
        else:
            p.applications = []


    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)

    return render_to_response("committee/student_selection/list_direct_appl.html",
                              { 'department': department,
                                'year': year,
                                'direct_positions': direct_positions,
                                'is_show_all': is_show_all,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))


@admin_or_committee_required
def show_direct_appl(request, department_code, position_id):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    position = get_object_or_404(DirectApplicationPosition, pk=position_id)
    applications = (StudentApplication
                    .all_for_department(department,year)
                    .filter(position=position))

    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)

    return render_to_response("committee/student_selection/show_direct_appl.html",
                              { 'department': department,
                                'year': year,
                                'position': position,
                                'applications': applications,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))


def compute_acceptance_count(receipts):
    for r in receipts:
        c = 0
        for s in r.selections:
            if s.result:
                c += 1
        r.acceptance_count = c

@admin_or_committee_required
def list(request, department_code, round_number):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    
    actual_department = DepartmentNameInRequestLetter.to_requested_department(department)
    if round_number!='0':
        sel_round = SelectionRound.objects.get(year=year, number=round_number)
    else:
        sel_round = None

    receipts = (Receipt
                .all_for_round(sel_round,
                               department=actual_department)
                .select_related().all())

    assign_selections_to_receipts(receipts, round_number, department)
    compute_acceptance_count(receipts)

    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)

    total_available_positions = 0
    total_shown_available_positions = 0
    for r in receipts:
        if r.available:
            total_available_positions += r.num_available_positions()
            if not r.is_hidden:
                total_shown_available_positions += r.num_available_positions()

    # permissions
    is_approval_period = (SelectionRoundStatus
                          .is_approval_period_for_round(sel_round))
    is_own_department = (request.committee_department == department)

    if is_approval_period:
        for r in receipts:
            if len(r.selections) > 0:
                r.rank = 0
            else:
                r.rank = 1
        receipts = sorted(receipts,
                          cmp=lambda x,y: cmp(x.rank, y.rank))

    return render_to_response("committee/student_selection/list.html",
                              { 'department': department,
                                'year': year,
                                'selection_round': sel_round,
                                'receipts': receipts,
                                'total_available_positions':
                                    total_available_positions,
                                'total_shown_available_positions':
                                    total_shown_available_positions,

                                'is_approval_period': is_approval_period,
                                'is_own_department': is_own_department,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))


@admin_or_committee_required
def list_all_receipts(request, department_code):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    
    all_receipts = Receipt.objects.filter(year=year).all()

    company_dict = {}

    for r in all_receipts:
        if r.company_id not in company_dict:
            company_dict[r.company_id] = r.company

    department_dict = dict([(d.id,d) for d in Department.objects.all()])
    companies = company_dict.values()

    for c in companies:
        c.returned_receipts = []

    # assign receipts
    for r in all_receipts:
        try:
            company = company_dict[r.company_id]
            company.returned_receipts.append(r)
        except:
            pass

        r.department_short_name = department_dict[r.department_id].short_name

    is_committee = request.user.userprofile.is_committee_member

    return render_to_response("committee/student_selection/list_all_receipts.html",
                              { 'department': department,
                                'year': year,
                                'companies': companies,

                                'currentpage': currentpage,
                                'is_committee': is_committee },
                              context_instance=RequestContext(request))


def get_all_selections(receipt, year, round_number, department):
    departments = DepartmentNameInRequestLetter.reverse_departments(department)
    
    if len(departments)==1:
        selections = StudentSelection.all_for_receipt(receipt, year,
                                                      round_number,
                                                      department)
    else:
        selections = []
        for d in departments:
            selections += StudentSelection.all_for_receipt(receipt, year,
                                                           round_number,
                                                           d)
        selections.sort(cmp=lambda x,y: cmp(x.selection_at,y.selection_at))
    
    return selections

def get_all_selections_no_receipt(year, round_number, department):
    departments = DepartmentNameInRequestLetter.reverse_departments(department)
    
    if len(departments)==1:
        selections = StudentSelection.all_for_department(year,
                                                      round_number,
                                                      department)
    else:
        selections = []
        for d in departments:
            selections += StudentSelection.all_for_department(year,
                                                           round_number,
                                                           d)
        selections.sort(cmp=lambda x,y: cmp(x.selection_at,y.selection_at))
    
    return selections

    
@admin_or_committee_required
def show(request, department_code, round_number, receipt_id):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    receipt = get_object_or_404(Receipt, pk=receipt_id)

    sel_round = SelectionRound.objects.get(year=year, number=round_number)
    company = receipt.company

    selections = get_all_selections(receipt,
                                    year,
                                    round_number,
                                    department)

    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)

    return render_to_response("committee/student_selection/show.html",
                              { 'department': department,
                                'year': year,
                                'selection_round': sel_round,
                                'selections': selections,
                                'company': company,
                                'receipt': receipt,
                                
                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))


@committee_required
def approve(request, department_code, round_number, receipt_id):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    sel_round = SelectionRound.objects.get(year=year, number=round_number)

    is_approval_period = (SelectionRoundStatus
                          .is_approval_period_for_round(sel_round))

    if (request.committee_department != department) or (not is_approval_period):
        return HttpResponseForbidden()

    receipt = get_object_or_404(Receipt, pk=receipt_id)

    company = receipt.company

    # TODO: have to fix this for next round
    accepting_count = receipt.num_available_positions()

    selections = get_all_selections(receipt,
                                    year,
                                    round_number,
                                    department)
    
    sel_dict = dict([(s.id, s) for s in selections])

    notice = extract_notice(request.session)

    if request.method == 'POST':

        failed = False

        if "ok" in request.POST:
            count = 0
            for sid in sel_dict.keys():
                if ('sel_%d' % sid) in request.POST.keys():
                    sel_dict[sid].result = True
                    count += 1
                elif ('unsel_%d' % sid) in request.POST.keys():
                    sel_dict[sid].result = False
                else:
                    sel_dict[sid].result = None

            if count > accepting_count:
                notice = u'ไม่สามารถจัดเก็บได้ เพราะคุณได้เลือกนิสิตเกินจำนวนที่กำหนดรับ'
                failed = True
            else:
                for s in selections:
                    s.save()
                    Log.create("approve selection - from: %s, student: %s, sel: %d" %
                               (request.META['REMOTE_ADDR'],
                                s.student.student_id,
                                s.id),
                               user=request.user,
                               student=s.student)
                    send_approval_confirmation(s, sel_round)
                request.session['notice'] = u'จัดเก็บการพิจารณาอนุมัติการเลือกสถานที่ฝึกงานที่ %s สำหรับนิสิตจำนวน %d คน' % (company.name, count)
        else:
            request.session['notice'] = u'ยกเลิกการแก้ไขและการพิจารณาอนุมัติการเลือกสถานที่ฝึกงานที่' + company.name

        if not failed:
            return HttpResponseRedirect(
                reverse('committee_student_selection_list',
                        args=[department_code,round_number]))

    accepted_count = len([s for s in selections if s.result==True])
    global sendmail
    if sendmail:
        notice = "ได้ส่ง E-mailให้กับนิสิตทุกคนเรียบร้อยแล้ว"
        sendmail = False
        
    return render_to_response("committee/student_selection/approve.html",
                              { 'department': department,
                                'year': year,
                                'selection_round': sel_round,
                                'selections': selections,
                                'company': company,
                                'receipt': receipt,

                                'accepting_count': accepting_count,
                                'accepted_count': accepted_count,
                                
                                'currentpage': currentpage,
                                'is_committee': True,
                                'notice': notice },
                              context_instance=RequestContext(request))

@committee_required
def view_student_grade(request, department_code, round_number, receipt_id,student_id):
    student = Student.objects.get(student_id=student_id)
    grades = Grade.objects.filter(student=student).order_by('course_number')
    return render_to_response("committee/student_selection/view_grade.html",
                              { 'student': student, 
                                'grades': grades,
                                'department_code' : department_code, 
                                'round_number' : round_number, 
                                'receipt_id' : receipt_id,
                              },
                              context_instance=RequestContext(request))


class EmailForm(forms.Form):
    subject = forms.CharField(label=u'หัวเรื่อง',
                              required=True,
                              widget=forms.TextInput(attrs={'size':'50'}))
    body = forms.CharField(label=u'เนื้อหา',
                           required=True,
                           widget=forms.Textarea)

def send_email(request, department_code, round_number, receipt_id):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    receipt = get_object_or_404(Receipt, pk=receipt_id)
    company = receipt.company

    if request.method =="POST":
        selections = StudentSelection.all_for_receipt(receipt, year,
                                                      round_number,
                                                      department)
        form = EmailForm(request.POST)
        if form.is_valid():
                subject = form.cleaned_data['subject']
                body = form.cleaned_data['body']
                global sendmail
                sendmail = True
                
                for s in selections:
                    #print s.student.get_email()
                    send_mail_to_student(s.student, subject, body)

        return HttpResponseRedirect(reverse('committee_student_selection_approve',args=[department_code,round_number,receipt_id]))
    else:
        form = EmailForm()
        notice = ''
        
        return render_to_response("committee/student_selection/send_email.html",
                                  { 'department': department,
                                    'department_code' : department_code, 
                                    'round_number' : round_number, 
                                    'receipt' : receipt,
                                    'form' : form,
                                    'notice' : notice,
                                    'company' : company,
                                    'currentpage': currentpage,
                                    'is_committee': True,
                                  },
                                  context_instance=RequestContext(request))

@committee_required
def list_no_approve(request,department_code, round_number):
    year = get_next_worktraining_year()
    department = get_object_or_404(Department, code=department_code)
    sel_round = SelectionRound.objects.get(year=year, number=round_number)

    is_approval_period = (SelectionRoundStatus
                          .is_approval_period_for_round(sel_round))

    if (request.committee_department != department) or (not is_approval_period):
        return HttpResponseForbidden()
    
    selections = get_all_selections_no_receipt(
                                    year,
                                    round_number,
                                    department)
    
    return render_to_response("committee/student_selection/list_no_approve.html",
                              { 'department_code' : department_code,
                                'department': department, 
                                'round_number' : round_number, 
                                'selection_round': sel_round,
                                'selections': selections,
                                'currentpage': currentpage,
                                'is_committee': True,
                              },
                              context_instance=RequestContext(request))
    
            
