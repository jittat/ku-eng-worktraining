# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect

from committee.models import ProposedCompany
from commons.decorators import committee_required

class ProposedCompanyForm(ModelForm):
    address = forms.CharField(label=u'ที่อยู่',
                              widget=forms.Textarea(attrs={'rows':2}))
    short_description = forms.CharField(label=u'ลักษณะธุรกิจ',
                                        help_text=u'อธิบายลักษณะธุรกิจคร่าว ๆ '
                                        u'ของสถานประกอบการ เช่น '
                                        u'พัฒนาระบบเครือข่าย '
                                        u'หรือผลิตชิ้นส่วนรถยนต์ เป็นต้น',
                                        widget=forms.Textarea(attrs={'rows':2}))
    class Meta:
        model = ProposedCompany
        exclude = ('department', 
                   'is_approved',
                   'approval_notes')

currentpage = 'committee_company'

@committee_required
def index(request):
    department = request.committee_department

    notice = ''
    if request.method == 'POST':
        form = ProposedCompanyForm(request.POST)
        if form.is_valid():
        
            proposed_company = form.save(commit=False)
            proposed_company.department = department
            proposed_company.is_approved = None
            proposed_company.save()

            notice = u'เสนอชื่อ %s เข้าในรายการสถานประกอบการแล้ว กรุณารอการอนุมัติจากทางหน่วยกิจการนิสิต' % proposed_company.name
            # create a new empty form
            form = ProposedCompanyForm()
    else:
        if 'notice' in request.session:
            notice = request.session['notice']
            del request.session['notice']
        form = ProposedCompanyForm()

    proposed_count = ProposedCompany.objects.filter(department=department).count()

    return render_to_response("committee/company_proposal/index.html",
                              { 'form': form,
                                'proposed_count': proposed_count,
                                'notice': notice,
                                'is_committee': True,
                                'department': department,
                                'currentpage': currentpage },
                              context_instance=RequestContext(request))

@committee_required
def list(request):
    department = request.committee_department

    proposed_companies = ProposedCompany.objects.filter(department=department).all()

    return render_to_response("committee/company_proposal/list.html",
                              { 'proposed_companies': proposed_companies,
                                'is_committee': True,
                                'department': department,
                                'currentpage': currentpage },
                              context_instance=RequestContext(request))


@committee_required
def edit(request, proposed_company_id):
    department = request.committee_department
    proposed_company = get_object_or_404(ProposedCompany,pk=proposed_company_id)

    if request.method == 'POST':
        if 'ok_button' in request.POST:
            form = ProposedCompanyForm(request.POST, instance=proposed_company)
            if form.is_valid():
        
                proposed_company = form.save(commit=False)
                proposed_company.department = department
                proposed_company.is_approved = None
                proposed_company.save()

                request.session['notice'] = u'แก้ไขข้อมูลสถานประกอบการ ' + proposed_company.name + u' แล้ว'
                return redirect('committee_company_proposal')
        elif 'delete_button' in request.POST:
            proposed_company.delete()
            request.session['notice'] = u'ยกเลิกการเสนอสถานประกอบการ ' + proposed_company.name + u' แล้ว'
            return redirect('committee_company_proposal')
        else:
            request.session['notice'] = u'ข้อมูลของสถานประกอบการ ' + proposed_company.name + u' ไม่ถูกแก้ไข'
            return redirect('committee_company_proposal')
    else:
        form = ProposedCompanyForm(instance=proposed_company)

    return render_to_response("committee/company_proposal/edit.html",
                              { 'form': form,
                                'company_name': proposed_company.name,
                                'is_committee': True,
                                'department': department,
                                'currentpage': currentpage },
                              context_instance=RequestContext(request))
