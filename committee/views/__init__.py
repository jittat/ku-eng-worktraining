# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response 
from django.template import RequestContext 
from django.http import HttpResponseForbidden

from commons.decorators import committee_required
from regis.models import Company, Status
from std.models import SelectionRoundStatus, CompanyDirectApplication
from commons.academia import get_next_worktraining_year, get_current_academic_year, get_current_information_year
from committee.student_stat import find_selection_statistics, find_accepted_self_appl_students
from committee.student_stat import get_registered_students
from committee.student_stat import get_assigned_students
from committee.student_stat import get_assigned_students_from_selection
from committee.student_stat import get_all_direct_appl_students
from committee.student_stat import get_direct_appl_waitlist_students
from committee.student_stat import get_students_with_no_selections
from committee.student_stat import get_students_with_companies
from committee.student_stat import get_unassigned_students
currentpage = 'committee'

@committee_required
def index(request):
    department = request.committee_department
    new_company_count = Company.all_new().count()

    sel_status = SelectionRoundStatus.get_status()
    if sel_status and (not sel_status.is_open):
        last_sel_round = SelectionRoundStatus.get_last_round()
    else:
        last_sel_round = None

    if sel_status:
        next_sel_round = sel_status.get_next_round()
    else:
        next_sel_round = None

    year = get_next_worktraining_year()
    current_year = get_current_academic_year()
    current_info_year = get_current_information_year()
    sel_stat = find_selection_statistics(department, current_info_year)

    return render_to_response("committee/index.html",
                              { 'new_company_count': new_company_count,

                                'sel_status': sel_status,
                                'last_sel_round': last_sel_round,
                                'next_sel_round': next_sel_round,

                                'sel_stat': sel_stat,

                                'department': department,
                                'current_year': current_year,
                                'current_info_year': current_info_year,
                                'currentpage':currentpage,
                                'is_committee': True },
                              context_instance=RequestContext(request))


def get_assigned_students_from_direct_appl_wrapper(department, year):
    accepted_self_appl_students = find_accepted_self_appl_students(year, department)
    all_assigned_students = get_assigned_students(department, year)
    assigned_students_from_selection = get_assigned_students_from_selection(department, year)
    sel_assigned_set = set([s.id for s in assigned_students_from_selection])

    additional_assigned_students = [s for s in all_assigned_students
                                    if s.id not in sel_assigned_set]

    return accepted_self_appl_students + additional_assigned_students


def get_registered_students_and_companies(department, year):
    students = get_registered_students(department, year)
    assigned_students = get_students_with_companies(department, year)
    assigned_dict = dict([(s.id, s) for s in assigned_students])

    for s in students:
        if s.id in assigned_dict:
            s.assigned_company = assigned_dict[s.id].assigned_company
        else:
            s.assigned_company = None
    return students
    

def get_direct_appl_waitlist_students_wrapper(department, year):
    students = []
    accepted_students = find_accepted_self_appl_students(year, department)
    accepted_ids = [s.id for s in accepted_students]
    for s in get_registered_students(department, year):
        if ((s.current_status.is_registered_for_self_application()) and
            (s.id not in accepted_ids) and
            (not s.assignment_for_year(year))):
            students.append(s)

    company_direct_applications = (CompanyDirectApplication.
                                   objects.
                                   filter(year=year).
                                   filter(student__department=department).
                                   select_related('student').
                                   all())
    app_by_stdids = {}
    for a in company_direct_applications:
        if not a.is_active():
            continue
        if a.student.id not in app_by_stdids:
            app_by_stdids[a.student.id] = []
        app_by_stdids[a.student.id].append(a)

    for s in students:
        if s.id in app_by_stdids:
            s.applications = app_by_stdids[s.id]
        else:
            s.applications = []
    
    return students

def get_students_with_no_selections_wrapper(department, year):
    all_students = get_registered_students(department, year)
    direct_appl_students = [s for s in get_registered_students(department, year)
                            if s.current_status.is_registered_for_self_application()]
    assigned_students = get_assigned_students(department, year)

    return get_students_with_no_selections(department, year,
                                           all_students,
                                           assigned_students,
                                           direct_appl_students)

def find_registered_stat(department, year, students):
    return { 'regtype_counts': Status.find_regtype_stat(students),
             'total': len(students) }

STAT_TYPES = {
    'all':
        (u"นิสิตทั้งหมดที่ได้ลงทะเบียนขอฝึกงาน",
         get_registered_students_and_companies,
         'committee/stat/registered.html',
         find_registered_stat),
    'assigned':
        (u"นิสิตที่ได้สถานประกอบการแล้ว",
         get_students_with_companies,
         'committee/stat/assigned.html'),
    'unassigned':
        (u"นิสิตที่ยังไม่ได้สถานประกอบการ",
         get_unassigned_students,
         'committee/stat/all.html'),
    'assigned_direct':
        (u"นิสิตที่ได้สถานประกอบการแล้ว (สถานประกอบการคัดเลือกเอง)",
         get_assigned_students_from_direct_appl_wrapper,
         'committee/stat/assigned.html'),
    'assigned_selection':
        (u"นิสิตที่ได้สถานประกอบการแล้ว (คัดเลือกผ่านคณะ)",
         get_assigned_students_from_selection,
         'committee/stat/assigned.html'),
    'wait':
        (u"นิสิตที่รอผลสถานประกอบการที่คัดเลือกเอง",
         get_direct_appl_waitlist_students_wrapper,
         'committee/stat/wait.html'),
    'no_activity':
        (u"นิสิตที่ยังไม่ได้ดำเนินการเลือก",
         get_students_with_no_selections_wrapper,
         'committee/stat/all.html'),
    }

@committee_required
def student_list(request,stat_type):
    if stat_type not in STAT_TYPES:
        return HttpResponseForbidden()

    department = request.committee_department
    year = get_current_information_year()

    title, finder, template_name = STAT_TYPES[stat_type][:3]
    
    students = finder(department, year)

    if len(STAT_TYPES[stat_type])>=4:
        additional_stat = STAT_TYPES[stat_type][3](department,year,students)
    else:
        additional_stat = None
        
    return render_to_response(template_name,
                              { 'stat_type_title': title,
                                'students': students,
                                'additional_stat': additional_stat,
                                
                                'department': department,
                                'currentpage':currentpage,
                                'is_committee': True },
                              context_instance=RequestContext(request))
