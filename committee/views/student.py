# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext, loader, Context
from django.http import HttpResponseRedirect, HttpResponse
from django import forms
from django.core.urlresolvers import reverse
import json

from commons.decorators import committee_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year, get_current_information_year, get_current_year
from commons.utils import UnicodeWriter
from regis.models import Student,Department
from evaluations.models import Evaluation

currentpage = 'student'

GRADING_START_YEAR = 2559

def load_department_evaluations(department, year):
    evaluations = list(
        Evaluation
        .objects
        .filter(assignment__year__exact=year)
        .select_related('student')
        .filter(student__department=department)
        .all())
    evaluations = sorted(evaluations, 
                         cmp=lambda x,y: cmp(x.student.student_id, y.student.student_id))
    return evaluations

@committee_required
def student_approve(request):
    department = request.committee_department
    year = get_current_year()
    if request.GET.get('year','') != '':
        year = int(request.GET.get('year'))
    evaluations = load_department_evaluations(department, year)

    student_year = (GRADING_START_YEAR - 3) % 100

    for ev in evaluations:
        ev_student_year =  int(ev.student.student_id[:2])
        ev.is_score_required = ev_student_year >= student_year

    if year < GRADING_START_YEAR:
        return render_to_response("committee/student/student_approve.html",
                                  { 'department': department,
                                    'currentpage':currentpage,
                                    'evaluations':evaluations,
                                    'is_committee':True},
                                  context_instance=RequestContext(request))
    else:
        return render_to_response("committee/student/student_grades.html",
                                  { 'department': department,
                                    'currentpage':currentpage,
                                    'evaluations':evaluations,
                                    'is_committee':True},
                                  context_instance=RequestContext(request))

@committee_required
def student_export_grades(request):
    department = request.committee_department
    year = get_current_year()
    if request.GET.get('year','') != '':
        year = int(request.GET.get('year'))
    evaluations = load_department_evaluations(department, year)

    student_year = (GRADING_START_YEAR - 3) % 100

    lines = []
    for ev in evaluations:
        ev_student_year =  int(ev.student.student_id[:2])
        is_score_required = ev_student_year >= student_year

        if not is_score_required:
            continue

        if ev.has_report_score():
            lines.append(u'%s,%s\r\n' % (ev.student.student_id, ev.calculate_grade()))

    content = ''.join(lines)
    response = HttpResponse(content_type='text/csv')
    filename = 'grades-%s-%d.csv' % (department.code, year)
    response['Content-Disposition'] = 'attachment; filename=' + filename
    content = content.encode("tis-620")
    response.content = content
    return response


@committee_required
def student_export_eval_scores(request):
    department = request.committee_department
    year = get_current_year()
    if request.GET.get('year','') != '':
        year = int(request.GET.get('year'))
    evaluations = load_department_evaluations(department, year)

    student_year = (GRADING_START_YEAR - 3) % 100

    lines = []
    lines += [
        u',,คะแนนโดยละเอียด',
        u',,(รายการหัวข้อคะแนนประเมินจากบริษัทอยู่ท้ายตาราง)',
        u''
    ]
    lines.append(u'ลำดับที่,รหัสประจำตัวนิสิต,ชื่อ-นามสกุล,ทำงาน,สาย,ขาด,ลากิจ,ลาป่วย,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,คะแนนหน่วยงาน(75),คะแนนวันทำการ(10),คะแนนรวมหน่วยงาน(50),คะแนนรายงาน(50),คะแนนรวม')
    counter = 0
    for ev in evaluations:
        ev_student_year =  int(ev.student.student_id[:2])
        is_score_required = ev_student_year >= student_year

        if not is_score_required:
            continue

        student = ev.student
        counter += 1
        items = [
            counter,
            student.student_id,
            student.short_prefix()+student.full_name(),
        ]
        if ev.num_working_days > 0:
            items += [
                ev.num_working_days,
                ev.num_late_days,
                ev.num_absent_leave_days,
                ev.num_personal_leave_days,
                ev.num_heath_leave_days,
            ]
            if ev.scores != '':
                items += ev.scores.split(',')
            else:
                items += ['']*15
            items += [
                '%0.2f' % (ev.raw_total_score,),
                '%0.2f' % (ev.working_day_score_updated(),),
                '%0.2f' % (ev.company_score_updated(),),
            ]
            if ev.has_report_score():
                items += [
                    '%0.2f' % (ev.report_score,),
                    '%0.2f' % (ev.grading_score(),),
                ]
            else:
                items += ['','']
        else:
            items += ['?'] * 5
            if ev.scores != '':
                items += ev.scores.split(',')
            else:
                items += ['']*15
            items += [
                '%0.2f' % (ev.raw_total_score,),
                '-',
                '-',
            ]
            if ev.has_report_score():
                items += [
                    '0.2f' % (evaluation.report_score,),
                    '',
                ]
            else:
                items += ['','']

        lines.append(u','.join([unicode(i) for i in items]))

    lines.append(u'')
    lines.append(u'')
    lines += [u',,รายละเอียดการประเมินโดยบริษัท']
    for section in Evaluation.get_sections(year):
        for num, desc, desc_eng in section['items']:
            lines.append(u',,%d. %s' % (num,(desc.decode('utf-8'))))
    
    response = HttpResponse(content_type='text/csv')
    filename = 'ev-scores-%s-%d.csv' % (department.code, year)
    response['Content-Disposition'] = 'attachment; filename=' + filename

    writer = UnicodeWriter(response)
    for l in lines:
        writer.writerow(l.split(u','))
    return response


@committee_required
def student_approve_all_pass(request):
    department = request.committee_department
    evaluations = Evaluation.objects.filter(assignment__year__exact=get_current_year()).select_related('student').all()
    for evaluation in evaluations:
        if evaluation.is_department(department):
            if evaluation.is_basic_criteria_passed():
                evaluation.is_passed = True 
            else:
                evaluation.is_passed = False
            evaluation.save()
    
    return HttpResponseRedirect(reverse('committee_student_approve'))


@committee_required
def student_result_list(request, year=None):
    department = request.committee_department
    if year==None:
        year = get_current_year()
    evaluations = list(
        Evaluation
        .objects
        .filter(assignment__year__exact=get_current_year())
        .select_related('student')
        .filter(student__department=department)
        .all())
    evaluations = dict([(e.student.student_id,e)
                        for e in evaluations])

    registered_students = Student.all_registered_for_year(year,department)
    for s in registered_students:
        if s.student_id in evaluations:
            s.evaluation = evaluations[s.student_id]
        else:
            s.evaluation = 0

    return render_to_response("committee/student/result_list.html",
                              { 'department': department,
                                'currentpage':currentpage,
                                'students':registered_students,
                                'year': year,
                                'is_committee':True},
                              context_instance=RequestContext(request))

####################################
#  These are for ajax requests
####################################

@committee_required
def student_approve_select(request):
    if request.method=='POST':
        evaluation_id = (request.POST['evaluation_id'])
        status = (request.POST['status'])
        evaluation = get_object_or_404(Evaluation, pk=evaluation_id)
        if status == "pass":
            evaluation.is_passed = True
        else:
            evaluation.is_passed = False

        evaluation.save()
        return HttpResponse()

    
@committee_required
def student_approve_update_report_score(request):
    if request.method=='POST':
        evaluation_id = request.POST['evaluation_id']
        evaluation = get_object_or_404(Evaluation, pk=evaluation_id)
        raw_score = request.POST['score'].strip()
        if raw_score == '':
            evaluation.report_score = None
        else:
            try:
                evaluation.report_score = float(raw_score)
            except:
                evaluation.report_score = None
        if evaluation.report_score != None:
            evaluation.update_grade()
        else:
            evaluation.grade = ''
        evaluation.save()
        result = {'id': evaluation.id,
                  'score': evaluation.report_score,
                  'grading_score': evaluation.grading_score_display(),
                  'grade': evaluation.grade }
        return HttpResponse(json.dumps(result))
