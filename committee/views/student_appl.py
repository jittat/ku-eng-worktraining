# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse

from datetime import datetime

from commons.decorators import committee_required, admin_or_committee_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from regis.models import Company, Department, Student, DepartmentNameInRequestLetter, Receipt, Grade
from commons.utils import extract_notice
from direct_appl.models import DirectApplicationPosition, StudentApplication
from std.models import SelectionRound, SelectionRoundStatus, StudentSelection, CompanyRequest, CompanyDirectApplication, CompanyDirectApplicationDetail
from std.models import assign_selections_to_receipts
from commons.models import Log
from commons.email import send_approval_confirmation
from commons.email import send_mail_to_student
from commons.email import send_company_request_approval_email
from commons.email import send_company_request_acceptance_confirmation
from commons.email import send_company_request_rejectance_confirmation
from commons.email import send_company_request_result_reset
from commons.email import send_company_request_result_reject
from commons.email import send_company_direct_application_result

from reports import build_student_request_letter_pdf_response

currentpage = 'department'
sendmail = False

@admin_or_committee_required
def list_company_requests(request, department_code, is_active=True):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    if request.GET.get('year',None):
        year = int(request.GET.get('year'))

    all_company_requests = (CompanyRequest
                            .all_for_department(department, 
                                                year,
                                                True)
                            .order_by('company_name'))

    active_company_requests = [r for r in all_company_requests
                               if r.is_approved_by_committee == None]
    inactive_company_requests = [r for r in all_company_requests
                                 if r.is_approved_by_committee != None]
    active_count = len(active_company_requests)
    inactive_count = len(all_company_requests) - len(active_company_requests)

    if is_active:
        company_requests = active_company_requests
    else:
        company_requests = inactive_company_requests

    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)
    return render_to_response("committee/student_appl/list_company_requests.html",
                              { 'department': department,
                                'year': year,
                                'company_requests': company_requests,
                                'is_active': is_active,
                                'active_count': active_count,
                                'inactive_count': inactive_count,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))

@admin_or_committee_required
def show_company_request(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    company_request = get_object_or_404(CompanyRequest,
                                        pk=request_id)
    year = get_next_worktraining_year()
    if request.GET.get('year',None):
        year = int(request.GET.get('year'))
    
    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)
    return render_to_response("committee/student_appl/show_company_request.html",
                              { 'department': department,
                                'year': year,
                                'company_request': company_request,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))


@admin_or_committee_required
def update_company_request(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    company_request = get_object_or_404(CompanyRequest,
                                        pk=request_id)
    
    committee_department = request.user.userprofile.department

    if (department.id != committee_department.id or
        company_request.student.department_id != department.id):
        return HttpResponseForbidden()

    if company_request.is_approved_by_committee != None:
        return HttpResponseForbidden()

    if request.method != 'POST':
        return HttpResponseForbidden()

    if 'cancel' in request.POST:
        request.session['notice'] = u'คุณได้ยกเลิกการแก้ไขแล้ว'
    else:
        company_request.is_approved_by_committee = (request.POST['result'] == 'yes')
        if not company_request.is_approved_by_committee:
            company_request.reject_comments = request.POST['reject_explanation']
            company_request.save_committee_approval_to_students(False)
        else:
            company_request.reject_comments = None
            company_request.save_committee_approval_to_students(True)
        company_request.approved_at = datetime.now()
        company_request.save()

        if company_request.is_approved_by_committee:
            for s in company_request.get_approved_students():
                send_company_request_approval_email(s, company_request, True)
            Log.create("committee approve company request - from: %s, cr: %d" %
                       (request.META['REMOTE_ADDR'],
                        company_request.id),
                       student=company_request.student,
                       user=request.user)
        else:
            for s in company_request.get_approved_students():
                send_company_request_approval_email(s, company_request, False)
            Log.create("committee reject company request - from: %s, cr: %d" %
                       (request.META['REMOTE_ADDR'],
                        company_request.id),
                       student=company_request.student,
                       user=request.user)


        if company_request.is_approved_by_committee:
            request.session['notice'] = u'คุณได้อนุมัติการขอฝึกงานที่' + company_request.company_name + u'แล้ว'
        else:
            request.session['notice'] = u'คุณได้ปฏิเสธการขอฝึกงานที่' + company_request.company_name + u'แล้ว'

    return redirect('committee_student_appl_list_company_requests',department.code)


@admin_or_committee_required
def update_company_request_result(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    company_request = get_object_or_404(CompanyRequest,
                                        pk=request_id)
    
    committee_department = request.user.userprofile.department

    if (department.id != committee_department.id or
        company_request.student.department_id != department.id):
        return HttpResponseForbidden()

    if company_request.accepted_at != None:
        return HttpResponseForbidden()

    if not company_request.is_approved_by_committee:
        return HttpResponseForbidden()

    if request.method=='POST':
        if "ok" not in request.POST:
            return redirect('committee_student_appl_list_company_requests_inactive',department.code)

        accepted_some = False
        request_students = company_request.students.all()
        for s in request_students:
            if s.is_approved:
                student = s.student
                name = 'st-%d' % student.id
                s.is_accepted = name in request.POST
                s.accepted_at = datetime.now()
                s.save()
                    
                if s.is_accepted:
                    send_company_request_acceptance_confirmation(student, company_request)
                    accepted_some = True
                else:
                    send_company_request_rejectance_confirmation(student, company_request)


        company_request.is_accepted = accepted_some
        company_request.accepted_at = datetime.now()
        company_request.save()

        Log.create("committee update company request - from: %s, cr: %d" %
                   (request.META['REMOTE_ADDR'],
                    company_request.id),
                   student=company_request.student,
                   user=request.user)

        return redirect('committee_student_appl_list_company_requests_inactive',department.code)

    is_committee = request.user.userprofile.is_committee_member
    return render_to_response("committee/student_appl/update_company_request_result.html",
                              { 'department': department,
                                'year': year,
                                'company_request': company_request,

                                'currentpage': currentpage,
                                'is_committee': is_committee },
                              context_instance=RequestContext(request))


def has_any_other_active_self_appl(student, year, company_request):
    all_company_request_items = student.request_set.filter(year=year).all()
    if len([r for r in all_company_request_items if r.is_active() and r.company_request != company_request]) != 0:
        return True
    company_direct_applications = CompanyDirectApplication.get_all_for(student, year)
    if len([a for a in company_direct_applications if a.is_active()]) != 0:
        return True
    return False


@admin_or_committee_required
def reset_company_request_result(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    company_request = get_object_or_404(CompanyRequest,
                                        pk=request_id)
    
    committee_department = request.user.userprofile.department

    if (department.id != committee_department.id or
        company_request.student.department_id != department.id):
        return HttpResponseForbidden()

    if company_request.accepted_at == None:
        return HttpResponseForbidden()

    if not company_request.is_approved_by_committee:
        return HttpResponseForbidden()

    if request.method!='POST':
        return redirect('committee_student_appl_list_company_requests_inactive',department.code)

    
    request_students = company_request.students.all()

    has_any_active_appl = False
    error_names = []
    names = []
    for s in request_students:
        if s.is_approved:
            student = s.student
            names.append(student.full_name())
            if has_any_other_active_self_appl(student, year, company_request):
                has_any_active_appl = True
                error_names.append(student.full_name())

    if has_any_active_appl:
        request.session['notice'] = u'เกิดปัญหา: ไม่สามารถยกเลิกการบันทึกผลการตอบรับจากหน่วยงาน ' + company_request.company_name + u' ได้ (นิสิตคือ' + u', '.join(names) + u') เนื่องจากมีนิสิต (' + u', '.join(error_names) + u') ได้มีการเลือกหน่วยงานใหม่แล้ว'
        return redirect('committee_student_appl_list_company_requests_inactive',department.code)


    for s in request_students:
        if s.is_approved:
            student = s.student
            s.is_accepted = None
            s.accepted_at = None
            s.save()
            send_company_request_result_reset(student, company_request)

    company_request.is_accepted = None
    company_request.accepted_at = None
    company_request.save()

    request.session['notice'] = u'ยกเลิกการบันทึกผลการตอบรับจากหน่วยงาน ' + company_request.company_name + u' (นิสิตคือ ' + u', '.join(names) + u') เรียบร้อยแล้ว'
    
    Log.create("committee reset company request - from: %s, cr: %d" %
               (request.META['REMOTE_ADDR'],
                company_request.id),
               student=company_request.student,
               user=request.user)

    return redirect('committee_student_appl_list_company_requests_inactive',department.code)


@admin_or_committee_required
def reject_company_request_result(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    company_request = get_object_or_404(CompanyRequest,
                                        pk=request_id)
    
    committee_department = request.user.userprofile.department

    if (department.id != committee_department.id or
        company_request.student.department_id != department.id):
        return HttpResponseForbidden()

    if company_request.accepted_at == None:
        return HttpResponseForbidden()

    if not company_request.is_approved_by_committee:
        return HttpResponseForbidden()

    if request.method!='POST':
        return redirect('committee_student_appl_list_company_requests_inactive',department.code)
    
    request_students = company_request.students.all()

    names = []
    for s in request_students:
        if s.is_approved and s.is_accepted:
            student = s.student
            s.is_accepted = False
            s.accepted_at = datetime.now()
            s.save()
            send_company_request_result_reset(student, company_request)
            names.append(student.full_name())

    company_request.is_accepted = False
    company_request.accepted_at = datetime.now()
    company_request.save()

    request.session['notice'] = u'เปลี่ยนการบันทึกผลการตอบรับเป็นปฏิเสธทั้งหมดจากหน่วยงาน ' + company_request.company_name + u' (นิสิตคือ ' + u', '.join(names) + u') เรียบร้อยแล้ว'
    
    Log.create("committee reject all company request - from: %s, cr: %d" %
               (request.META['REMOTE_ADDR'],
                company_request.id),
               student=company_request.student,
               user=request.user)

    return redirect('committee_student_appl_list_company_requests_inactive',department.code)


@admin_or_committee_required
def list_company_direct_applications(request, department_code):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()

    direct_applications = (CompanyDirectApplication
                           .all_for_department(department,year)
                           .order_by('company_name'))

    is_committee = request.user.userprofile.is_committee_member

    if is_committee:
        committee_department = request.user.userprofile.department

        if department.id != committee_department.id:
            return HttpResponseForbidden()

    notice = extract_notice(request.session)
    return render_to_response("committee/student_appl/list_company_direct_applications.html",
                              { 'department': department,
                                'year': year,
                                'direct_applications': direct_applications,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))


@admin_or_committee_required
def delete_company_direct_application(request, department_code, application_id):
    department = get_object_or_404(Department, code=department_code)
    application = get_object_or_404(CompanyDirectApplication, pk=application_id)

    if department.id != request.user.userprofile.department_id:
        return HttpResponseForbidden()

    if application.student.department_id != department.id:
        return HttpResponseForbidden()

    application.delete()
    request.session['notice'] = u'คุณได้ยกเลิกการแจ้งการสมัครแล้ว'
    return redirect('committee_student_appl_list_company_direct_applications',
                    department.code)

@admin_or_committee_required
def update_company_direct_application(request, department_code, application_id):
    department = get_object_or_404(Department, code=department_code)
    application = get_object_or_404(CompanyDirectApplication, pk=application_id)
    year = get_next_worktraining_year()

    if department.id != request.user.userprofile.department_id:
        return HttpResponseForbidden()

    if application.student.department_id != department.id:
        return HttpResponseForbidden()

    if (not application.is_accepted) or (not application.is_chosen):
        return HttpResponseForbidden()

    student = application.student
    detail = application.detail

    is_committee = request.user.userprofile.is_committee_member
    notice = extract_notice(request.session)

    if request.method == 'POST':
        if "remove" in request.POST:
            if application.is_confirmed_by_committee == False:
                application.is_chosen = False
                application.save()

                Log.create("committee update company direct application (removed chosen flag) - from: %s, app: %d" %
                           (request.META['REMOTE_ADDR'],
                        application.id),
                           student=application.student,
                           user=request.user)
            return redirect('committee_student_appl_list_company_direct_applications',department.code)

        if "ok" not in request.POST:
            return redirect('committee_student_appl_list_company_direct_applications',department.code)
        
        application.is_confirmed_by_committee = (('result' in request.POST) and 
                                                 (request.POST['result']=='yes'))
        application.confirmed_at = datetime.now()
        application.save()

        send_company_direct_application_result(student, application, application.is_confirmed_by_committee)

        Log.create("committee update company direct application - from: %s, app: %d" %
                   (request.META['REMOTE_ADDR'],
                    application.id),
                   student=application.student,
                   user=request.user)

        return redirect('committee_student_appl_list_company_direct_applications',department.code)
       

    return render_to_response("committee/student_appl/update_company_direct_application.html",
                              { 'department': department,
                                'year': year,
                                'application': application,
                                'detail': detail,
                                'student': student,

                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))

@admin_or_committee_required
def confirm_company_direct_application_result(request, department_code, application_id):
    department = get_object_or_404(Department, code=department_code)
    application = get_object_or_404(CompanyDirectApplication, pk=application_id)
    year = get_next_worktraining_year()

    if department.id != request.user.userprofile.department_id:
        return HttpResponseForbidden()

    if application.student.department_id != department.id:
        return HttpResponseForbidden()

    if (application.is_accepted) and (application.is_chosen):
        return HttpResponseForbidden()

    if request.method == 'POST':
        application.is_confirmed_by_committee = True
        application.confirmed_at = datetime.now()
        application.save()

        Log.create("committee confirm company direct application - from: %s, app: %d" %
                   (request.META['REMOTE_ADDR'],
                    application.id),
                   student=application.student,
                   user=request.user)

        return HttpResponse("ok")
    else:
        return HttpResponseForbidden()
        

@admin_or_committee_required
def print_company_direct_application_letter(request, department_code, application_id):
    department = get_object_or_404(Department, code=department_code)
    app = get_object_or_404(CompanyDirectApplication,
                            pk=application_id)

    if not request.user.userprofile.is_sa_admin:
        if department.id != request.user.userprofile.department_id:
            return HttpResponseForbidden()

        department_id = request.user.userprofile.department_id

        if app.student.department_id != department_id:
            return HttpResponseForbidden()

    year = get_next_worktraining_year()
    students = [app.student]

    if not app.is_letter_downloaded:
        app.is_letter_downloaded = True
        app.letter_downloaded_at = datetime.now()
        app.save()
        
    return build_student_request_letter_pdf_response(None, students,
                                                     company_name=app.company_name,
                                                     year=year,
                                                     signer_name=u'ผู้จัดการแผนกทรัพยากรมนุษย์',
                                                     only_request_letter=True)
