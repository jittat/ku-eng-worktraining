# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from datetime import datetime
import re

from commons.decorators import admin_or_committee_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from commons.utils import extract_notice
from commons.models import Log, UserProfile
from regis.models import Department

currentpage = 'department'

class DepartmentAdminForm(forms.Form):
    username = forms.CharField(max_length=50)
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    #nontri_username = forms.CharField(max_length=50, required=False)

    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.match(r'[a-z]+$', username) or len(username) < 3:
            raise forms.ValidationError(u'รูปแบบชื่อบัญชีผิดพลาด')
        if User.objects.filter(username=username).count() != 0:
            raise forms.ValidationError(u'มีการใช้ชื่อบัญชีนี้แล้ว')
        return username

@admin_or_committee_required
def list(request, department_code='0'):
    if department_code!='0':
        department = get_object_or_404(Department, code=department_code)
        if (not request.user.userprofile.is_sa_admin) and department != request.committee_department:
            return HttpResponseForbidden()
    else:
        department = request.committee_department

    notice = extract_notice(request.session)

    if request.method == 'POST':
        form = DepartmentAdminForm(request.POST)
        if form.is_valid():
            user = User()
            user.username = form.cleaned_data['username']
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.set_unusable_password()
            user.email = user.username + '@ku.ac.th'
            user.is_active = True
            user.save()
            profile = UserProfile()
            profile.user = user
            profile.is_sa_admin = False
            profile.is_committee_member = False
            profile.department = department
            profile.is_department_admin = True
            profile.save()
            request.session['notice'] = 'เพิ่มบัญชีเจ้าหน้าที่ภาควิชาแล้ว'
            return redirect('committee_dept_admin_list',department_code)
        else:
            notice = 'เกิดข้อผิดพลาดในการจัดเก็บ'
    else:
        form = DepartmentAdminForm()

    profiles = UserProfile.objects.filter(department=department,
                                          is_department_admin=True).all()
    admins = [p.user for p in profiles]

    is_committee = request.user.userprofile.can_view_committee_menu()

    return render_to_response("committee/dept_admin/list.html",
                              { 'department': department,

                                'admins': admins,

                                'form': form,
                                
                                'notice': notice,
                                'currentpage': currentpage,
                                'is_committee': is_committee },
                              context_instance=RequestContext(request))


@admin_or_committee_required
def delete(request, department_code, user_id):
    if department_code!='0':
        department = get_object_or_404(Department, code=department_code)
        if (not request.user.userprofile.is_sa_admin) and department != request.committee_department:
            return HttpResponseForbidden()
    else:
        department = request.committee_department

    user = get_object_or_404(User, pk=user_id)
    profile = user.userprofile
    
    if profile.department != department:
        return HttpResponseForbidden()

    profile.delete()
    user.delete()

    return redirect('committee_dept_admin_list',department_code)

    
                                        

