# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.core.urlresolvers import reverse

from datetime import datetime

from commons.decorators import committee_required, admin_or_committee_required, department_admin_required
from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from regis.models import Company, Department, Student, DepartmentNameInRequestLetter, Receipt, Grade
from commons.utils import extract_notice
from std.models import SelectionRound, SelectionRoundStatus, StudentSelection, CompanyRequest, CompanyDirectApplication, CompanyDirectApplicationDetail
from commons.models import Log
from committee.models import CompanyRequestConfigLetter, CompanyDirectApplicationConfigLetter

from reports import build_sending_letter_pdf_response, build_label_pdf_response

currentpage = 'department'

@department_admin_required
def index(request, department_code='0'):
    if department_code!='0':
        department = get_object_or_404(Department, code=department_code)
    else:
        department = request.committee_department
    year = get_next_worktraining_year()

    if 'year' in request.GET:
        year = int(request.GET['year'])
    
    company_direct_applications = (CompanyDirectApplication.
                                   objects.
                                   filter(year=year).
                                   select_related('student').
                                   filter(student__department=department).
                                   all())
    accepted_company_direct_applications = [a for a in
                                            company_direct_applications
                                            if a.is_accepted_and_confirmed()]

    company_requests = (CompanyRequest.
                        objects.
                        filter(year=year).
                        select_related('student').
                        filter(student__department=department).
                        filter(is_accepted=True).
                        all())

    from coop import load_co_op_requests_and_check_printable
    
    co_op_requests = load_co_op_requests_and_check_printable(department, year)
    
    is_committee = request.user.userprofile.can_view_committee_menu()

    if 'saved_address_ids' in request.session:
        saved_address_count = len(request.session['saved_address_ids'])
    else:
        saved_address_count = 0

    return render_to_response("committee/letters/index.html",
                              { 'department': department,
                                'year': year,

                                'company_direct_applications': 
                                accepted_company_direct_applications,
                                'company_requests':
                                company_requests,

                                'co_op_requests': co_op_requests,
                               
                                'saved_address_count':
                                saved_address_count,
 
                                'currentpage': currentpage,
                                'is_committee': is_committee },
                              context_instance=RequestContext(request))

@department_admin_required
def print_sending_letter_for_request_pdf(request, department_code, request_id):
    department = get_object_or_404(Department, code=department_code)
    company_request = get_object_or_404(CompanyRequest,
                                        pk=request_id)
    year = company_request.year
    if not request.user.userprofile.is_sa_admin:
        committee_department = request.user.userprofile.department

        if (department.id != committee_department.id or
            company_request.student.department_id != department.id):
            return HttpResponseForbidden()

    if not company_request.is_accepted:
        return HttpResponseForbidden()

    letter_config = company_request.get_letter_config_or_default(CompanyRequestConfigLetter,
                                                                 save=True)
    
    company_request.is_sending_letter_printed = True
    company_request.sending_letter_printed_at = datetime.now()
    company_request.save()

    students = company_request.get_accepted_students()

    from evaluations.models import EvaluationSecret
    evaluation_secrets = [EvaluationSecret.get_or_create_for_student(student,
                                                                     year,
                                                                     request.user)
                          for student in students]

    return build_sending_letter_pdf_response(students,
                                             letter_config.year,
                                             letter_config.letter_number,
                                             letter_config.letter_date,
                                             letter_config.signer_name,
                                             letter_config.beginning_date,
                                             letter_config.end_date,
                                             letter_config.eval_date,
                                             evaluation_secrets=evaluation_secrets,
                                             company_name=company_request.company_name)


@department_admin_required
def print_sending_letter_for_application_pdf(request, department_code, application_id):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()
    application = get_object_or_404(CompanyDirectApplication,
                                    pk=application_id)
    student = application.student

    if not request.user.userprofile.is_sa_admin:
        committee_department = request.user.userprofile.department
    
        if (department.id != committee_department.id or
            student.department_id != department.id):
            return HttpResponseForbidden()

    if not application.is_accepted_and_confirmed():
        return HttpResponseForbidden()

    letter_config = application.get_letter_config_or_default(CompanyDirectApplicationConfigLetter,
                                                             save=True)

    application.is_sending_letter_printed = True
    application.sending_letter_printed_at = datetime.now()
    application.save()

    students = [student]

    from evaluations.models import EvaluationSecret
    evaluation_secret = EvaluationSecret.get_or_create_for_student(student,
                                                                   year,
                                                                   request.user)

    return build_sending_letter_pdf_response(students,
                                             letter_config.year,
                                             letter_config.letter_number,
                                             letter_config.letter_date,
                                             letter_config.signer_name,
                                             letter_config.beginning_date,
                                             letter_config.end_date,
                                             letter_config.eval_date,
                                             evaluation_secrets=[evaluation_secret],
                                             company_name=application.company_name)

@department_admin_required
def save_label(request, department_code, address_type, id):
    if 'saved_address_ids' not in request.session:
        request.session['saved_address_ids'] = []

    if address_type == 'requests':
        cid = 'r' + str(int(id))
    elif address_type == 'applications':
        cid = 'a' + str(int(id))
    else:
        return HttpResponseForbidden()

    if cid not in request.session['saved_address_ids']:
        request.session['saved_address_ids'].append(cid)
        request.session.modified = True

    return HttpResponse(str(len(request.session['saved_address_ids'])))


@department_admin_required
def remove_label(request, department_code, type_code, id):
    department = get_object_or_404(Department, code=department_code)
    
    if 'saved_address_ids' not in request.session:
        request.session['saved_address_ids'] = []

    if type_code == 'all':
        request.session['saved_address_ids'] = []
        request.session['notice'] = 'ลบข้อมูลที่อยู่ที่เก็บไว้ทั้งหมดแล้ว'
    else:
        if type_code == 'app':
            cid = 'a' + id
        else:
            cid = 'r' + id

        new_ids = [nid for nid in request.session['saved_address_ids']
                   if nid != cid]

        request.session['saved_address_ids'] = new_ids
        request.session['notice'] = 'ลบข้อมูลที่อยู่แล้ว'
        
    request.session.modified = True
    
    return redirect('committee_letters_print_labels',department_code)

def get_float_or_none(st):
    if not st or st == '':
        return None

    try:
        f = float(st)
        return f
    except:
        return None

@department_admin_required
def print_labels(request, department_code):
    department = get_object_or_404(Department, code=department_code)
    year = get_next_worktraining_year()

    if 'saved_address_ids' not in request.session:
        request.session['saved_address_ids'] = []

    company_requests = []
    direct_applications = []

    for id in request.session['saved_address_ids']:
        try:
            oid = id[1:]
            if id.startswith('r'):
                company_requests.append(CompanyRequest.
                                        objects.
                                        get(pk=oid))
            else:
                direct_applications.append(CompanyDirectApplication.
                                           objects.
                                           get(pk=oid))
        except:
            pass

    if request.method == 'POST':
        companies = ([r.get_temp_company() for r in company_requests] + 
                     [a.get_temp_company() for a in direct_applications])

        ecount = 0
        if 'empty_slot_count' in request.POST:
            try:
                ecount = int(request.POST['empty_slot_count'])
            except:
                pass

        companies = ([None] * ecount) + companies

        xoffset = None
        yoffset = None
        rowwidth = None
        if 'xoffset' in request.POST:
            xoffset = get_float_or_none(request.POST['xoffset'])
        if 'yoffset' in request.POST:
            yoffset = get_float_or_none(request.POST['yoffset'])
        if 'rowwidth' in request.POST:
            rowwidth = get_float_or_none(request.POST['rowwidth'])

        return build_label_pdf_response(companies,
                                        'internship_labels.pdf',
                                        print_number=False,
                                        xoffset=xoffset,
                                        yoffset=yoffset,
                                        rowwidth=rowwidth)

    is_committee = request.user.userprofile.can_view_committee_menu()

    notice = extract_notice(request.session)

    return render_to_response("committee/letters/labels.html",
                              { 'department': department,
                                'year': year,

                                'company_direct_applications': 
                                direct_applications,
                                'company_requests':
                                company_requests,
                                'company_request_count':
                                len(company_requests),
 
                                'currentpage': currentpage,
                                'is_committee': is_committee,
                                'notice': notice },
                              context_instance=RequestContext(request))
    
                                        

