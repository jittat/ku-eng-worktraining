from django.contrib import admin

from models import DepartmentSelectionInfo, ProposedCompany, CoOpCommitteeContact

admin.site.register(DepartmentSelectionInfo)
admin.site.register(ProposedCompany)
admin.site.register(CoOpCommitteeContact)

