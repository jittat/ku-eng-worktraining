# -*- coding: utf-8 -*-
from django.db import models
from regis.models import Company, Department, ConfigLetterBase
from commons.academia import get_next_worktraining_year

class CompanySelection(models.Model):
    company = models.ForeignKey(Company, related_name='department_selected')
    department = models.ForeignKey(Department, related_name='selected_companies')
    year = models.IntegerField(null=True,
                               default=None,
                               verbose_name=u'ปีที่เข้ารับการฝึกงาน')

    @staticmethod
    def selected_by_department_for_year(department, year):
        return CompanySelection.objects.filter(department=department,
                                               year=year)
            
    @staticmethod
    def selected_by_department_for_next_year(department):
        year = get_next_worktraining_year()
        return CompanySelection.selected_by_department_for_year(department,
                                                                year)

    @staticmethod
    def get_all_selected_companies_for_year(year):
        selections = CompanySelection.objects.filter(year=year).select_related('company','department')
        company_dict = dict([(s.company_id,s.company) for s in selections])
        companies = [company_dict[i] for i in sorted(company_dict.keys())]
        return companies

class DepartmentSelectionInfo(models.Model):
    """
    stores misc information.  Currently, it is used to remember
    company list page bookmark.
    """

    department = models.OneToOneField(Department, related_name='company_selection_info')
    bookmarked_page = models.IntegerField(null=True, 
                                          blank=True,
                                          default=None)


    @staticmethod
    def set_bookmarked_page_for(department, page_no):
        try:
            sel_info = DepartmentSelectionInfo.objects.get(department=department)
        except DepartmentSelectionInfo.DoesNotExist:
            sel_info = DepartmentSelectionInfo(department=department)

        sel_info.bookmarked_page = page_no
        sel_info.save()

    @staticmethod
    def get_bookmarked_page_for(department):
        try:
            sel_info = DepartmentSelectionInfo.objects.get(department=department)
            return sel_info.bookmarked_page
        except DepartmentSelectionInfo.DoesNotExist:
            return None


class ProposedCompany(models.Model):
    name = models.CharField(max_length=200,
                            verbose_name=u'ชื่อ')
    tel_no = models.CharField(max_length=15,
                              verbose_name=u'โทรศํพท์')
    fax_no = models.CharField(max_length=15,
                              blank=True,
                              verbose_name=u'โทรสาร')
    address = models.TextField(verbose_name=u'ที่อยู่')
    contact_name = models.CharField(max_length=200,
                                    verbose_name=u'ชื่อผู้ประสานงาน',
                                    help_text=u'ชื่อบุคคลที่คณะฯ และนิสิตสามารถติดต่อเกี่ยวกับการฝึกงานนี้ได้')
    signer_name = models.CharField(max_length=200,
                                   blank=True,
                                   default='',
                                   verbose_name=u'ชื่อผู้ลงนาม',
                                   help_text=u'ชื่อหรือตำแหน่งที่คณะฯ จะทำจดหมายไปถึง')
    short_description = models.TextField(verbose_name=u'ลักษณะงานสถานประกอบการ',
                                         help_text=u'อธิบายลักษณะธุรกิจคร่าว ๆ '
                                         u'ของสถานประกอบการ เช่น '
                                         u'พัฒนาระบบเครือข่าย '
                                         u'หรือผลิตชิ้นส่วนรถยนต์ เป็นต้น')

    department = models.ForeignKey(Department,
                                   related_name='proposed_companies')
    
    is_approved = models.NullBooleanField(default=None,
                                          blank=True,
                                          verbose_name=u'ผลการอนุมัติแล้ว')
    approval_notes = models.CharField(max_length=100,
                                      blank=True,
                                      default='',
                                      verbose_name=u'หมายเหตุการอนุมัติ')

    def __unicode__(self):
        return u'%s (proposed by %s)' % (self.name, self.department.name)

    @staticmethod
    def unapproved_companies():
        return ProposedCompany.objects.filter(is_approved=None).all()

    @staticmethod
    def unapproved_company_count():
        return ProposedCompany.unapproved_companies().count()

class CompanyRequestConfigLetter(ConfigLetterBase):
    company_request = models.OneToOneField('std.CompanyRequest', related_name='letter_config')

class CompanyDirectApplicationConfigLetter(ConfigLetterBase):
    application = models.OneToOneField('std.CompanyDirectApplication', related_name='letter_config')


class CoOpCommitteeContact(models.Model):
    department = models.ForeignKey('regis.Department')
    full_name = models.CharField(max_length=100,
                                 verbose_name=u'อาจารย์ผู้ประสานงาน')
    email = models.EmailField(verbose_name=u'อีเมลติดต่อ')
    tel_number = models.CharField(max_length=50,
                                  verbose_name=u'เบอร์โทรศัพท์ติดต่อ')
    full_name_english = models.CharField(max_length=100,
                                         verbose_name=u"ชื่ออาจารย์ภาษาอังกฤษ")
    tel_number_english = models.CharField(max_length=50,
                                          verbose_name=u'เบอร์ติดต่อสำหรับจดหมายภาษาอังกฤษ')

    @staticmethod
    def get_for(department):
        contacts = CoOpCommitteeContact.objects.filter(department=department).all()
        if len(contacts) > 0:
            return contacts[0]
        else:
            return None
    
    def __unicode__(self):
        return self.full_name + ' (' + unicode(self.department) + ')'
