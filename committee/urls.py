from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    '',
    url(r'^$',
        'committee.views.index', 
        name='committee_index'),

    url(r'^students/(.+)/',
        'committee.views.student_list', 
        name='committee_student_list'),

    url(r'^department/(?P<code>\d+)/$',
        'regis.department.views.detail',
        {'is_committee': True}, 
        name='committee_department_detail'),
)

# ------------------------------------
# these are URLs for company selection
urlpatterns += patterns(
    'committee.views.company_selection',

    url(r'^company_selection/$',
        'menu',
        name='committee_company_selection'),

    url(r'^company_selection/show/$',
        'show',
        name='committee_company_selection_show'),
        
    url(r'^company_selection/show/delete/(?P<company_id>\d+)$',
        'show_delete',
        name='committee_company_selection_show_delete'),

    url(r'^company_selection/list/page/(?P<page_id>\d+)/$',
        'list', 
        name='committee_company_list_selection'),

    url(r'^company_selection/list/stats/(\d+)/(\d+)/$',
        'list_stats', 
        name='committee_company_list_from_stats'),

    url(r'^company_selection/list/select/$',
        'list_select', 
        name='committee_company_selected'),

    url(r'^company_selection/list/bookmark/(?P<page_id>\d+)/$',
        'page_bookmark',
        name='committee_company_list_selection_bookmark'),

    url(r'^company_selection/last_year/$',
        'select_using_last_year', 
        name='committee_company_selection_last_year'),
        
    url(r'^company_selection/select_all/$',
        'select_all', 
        name='committee_company_selection_all'),
        
    url(r'^company_selection/copy_other_department/$',
        'select_using_copy_other_department', 
        name='committee_company_selection_copy_other_department'),
        
    url(r'^company_selection/copy_other_department/list/$',
        'select_using_copy_other_department_list', 
        name='committee_company_selection_copy_other_department_list'),
    
    url(r'^company_selection/copy_other_department/submit/$',
        'select_using_copy_other_department_submit', 
        name='committee_company_selection_copy_other_department_submit'),

    url(r'^company_selection/search/add/(\d+)/$',
        'select_from_search',
        name='committee_company_selection_from_search'),
    url(r'^company_selection/search/remove/(\d+)/$',
        'remove_from_search',
        name='committee_company_selection_from_search_remove'),
)

# company search form is from generics
urlpatterns += patterns(
    '',
    url(r'^company/search/$',
        'commons.views.generics.company_search',
        { 'is_committee': True, 'view_only': True },
        name='committee_company_search'),

    url(r'^company_selection/search/$',
        'commons.views.generics.company_search',
        { 'is_committee': True },
        name='committee_company_selection_search'),
)


# ------------------------------------
# these are URLs for company proposal

urlpatterns += patterns(
    'committee.views.company_proposal',
    url(r'^company_proposal/$',
        'index',
        name='committee_company_proposal'),
    url(r'^company_proposal/list/$',
        'list',
        name='committee_company_proposal_list'),
    url(r'^company_proposal/edit/(\d+)/$',
        'edit',
        name='committee_company_proposal_edit'),
)


# ------------------------------------
# these are URLs for student approval

urlpatterns += patterns(
    'committee.views.student',
    url(r'^student_approve/$',
        'student_approve',
        name='committee_student_approve'),
    url(r'^student_approve/select$',
        'student_approve_select', 
        name='committee_student_approve_select'), 
    url(r'^student_approve_all_past$',
        'student_approve_all_pass', 
        name='committee_student_approve_all_pass'),
    url(r'^student_results$',
        'student_result_list', 
        name='committee_student_results'),
    url(r'^student_approve/update_report$',
        'student_approve_update_report_score', 
        name='committee_student_approve_update_report_score'), 
    url(r'^student_approve/export_grades/$',
        'student_export_grades', 
        name='committee_student_approve_export_grades'), 
    url(r'^student_approve/export_eval_scores/$',
        'student_export_eval_scores', 
        name='committee_student_approve_export_eval_scores'), 
)

# ----------------------------------------------
# these are URLs for student company selection

urlpatterns += patterns(
    'committee.views.student_selection',
    url(r'^student_selection/(\d+)/$',
        'index',
        name='committee_student_selection_index'),

    url(r'^student_selection/(\d+)/direct_appl/$',
        'list_direct_appl',
        name='committee_student_selection_direct_appl_list'),
    url(r'^student_selection/(\d+)/direct_appl/all/$',
        'list_direct_appl', {'is_show_all': True},
        name='committee_student_selection_direct_appl_list_all'),
    url(r'^student_selection/(\d+)/direct_appl/(\d+)/$',
        'show_direct_appl',
        name='committee_student_selection_direct_appl_show'),

    url(r'^student_selection/(\d+)/(\d+)/$',
        'list',
        name='committee_student_selection_list'),
    url(r'^student_selection/(\d+)/all/$',
        'list_all_receipts',
        name='committee_student_selection_list_all_receipts'),

    url(r'^student_selection/(\d+)/(\d+)/(\d+)/$',
        'show',
        name='committee_student_selection_show'),

    url(r'^student_selection/approve/(\d+)/(\d+)/(\d+)/$',
        'approve',
        name='committee_student_selection_approve'),
    url(r'^student_selection/approve/(\d+)/(\d+)/(\d+)/(\d+)/$',
        'view_student_grade', 
        name='committee_student_approve_view_grade'),
    url(r'^student_selection/approve/send_mail/(\d+)/(\d+)/(\d+)/$',
        'send_email', 
        name='committee_student_send_email'), 
        
    url(r'^student_selection/list_no_approve/(\d+)/(\d+)/$',
        'list_no_approve', 
        name='committee_student_list_no_approve'),  
)

# ----------------------------------------------
# these are URLs for student self application

urlpatterns += patterns(
    'committee.views.student_appl',

    url(r'^self_appl/(\d+)/requests/$',
        'list_company_requests', 
        name='committee_student_appl_list_company_requests'),  
    url(r'^self_appl/(\d+)/requests/inactive/$$',
        'list_company_requests', {'is_active': False},
        name='committee_student_appl_list_company_requests_inactive'),  
    url(r'^self_appl/(\d+)/requests/(\d+)/$',
        'show_company_request', 
        name='committee_student_appl_show_company_request'),  
    url(r'^self_appl/(\d+)/requests/(\d+)/update/$',
        'update_company_request', 
        name='committee_student_appl_update_company_request'),  
    url(r'^self_appl/(\d+)/requests/(\d+)/result/$',
        'update_company_request_result',
        name='committee_student_appl_company_request_result'),
    url(r'^self_appl/(\d+)/requests/(\d+)/result/reset/$',
        'reset_company_request_result',
        name='committee_student_appl_company_request_reset_result'),
    url(r'^self_appl/(\d+)/requests/(\d+)/result/reject/$',
        'reject_company_request_result',
        name='committee_student_appl_company_request_reject_result'),

    url(r'^self_appl/(\d+)/applications/$',
        'list_company_direct_applications', 
        name='committee_student_appl_list_company_direct_applications'),  
    url(r'^self_appl/(\d+)/applications/(\d+)/delete/$',
        'delete_company_direct_application', 
        name='committee_student_appl_delete_company_direct_application'),  
    url(r'^self_appl/(\d+)/applications/(\d+)/print/$',
        'print_company_direct_application_letter', 
        name='committee_student_appl_print_company_direct_application_letter'),  
    url(r'^self_appl/(\d+)/applications/(\d+)/update/$',
        'update_company_direct_application', 
        name='committee_student_appl_update_company_direct_application'),  

    url(r'^self_appl/(\d+)/applications/(\d+)/confirmresult/$',
        'confirm_company_direct_application_result', 
        name='committee_student_appl_confirm_company_direct_application_result'),  
)

urlpatterns += patterns(
    'committee.views.letters',

    url(r'^self_appl/(\d+)/letters/$',
        'index', 
        name='committee_letters_index'),  
    url(r'^self_appl/letters/$',
        'index', 
        name='committee_letters_index_nodept'),  
    url(r'^self_appl/(\d+)/letters/requests/(\d+)/print/$',
        'print_sending_letter_for_request_pdf', 
        name='committee_letters_print_for_request_pdf'),  
    url(r'^self_appl/(\d+)/letters/applications/(\d+)/print/$',
        'print_sending_letter_for_application_pdf', 
        name='committee_letters_print_for_application_pdf'),  

    url(r'^self_appl/(\d+)/letters/save_label/(.+)/(\d+)/$',
        'save_label',
        name='committee_letters_save_label'),
    url(r'^self_appl/(\d+)/letters/remove_label/(.+)/(\d+)/$',
        'remove_label',
        name='committee_letters_remove_label'),
    url(r'^self_appl/(\d+)/letters/print_labels/$',
        'print_labels',
        name='committee_letters_print_labels'),
)

urlpatterns += patterns(
    'committee.views.dept_admin',

    url(r'^dept_admin/(\d+)/$',
        'list', 
        name='committee_dept_admin_list'),  
    url(r'^dept_admin/(\d+)/delete/(\d+)/$',
        'delete', 
        name='committee_dept_admin_delete'),  
)

urlpatterns += patterns(
    'committee.views.coop',

    url(r'^coop/(\d+)/$',
        'index', 
        name='committee_coop_index'),
    url(r'^coop/(\d+)/requests/(\d+)/$',
        'update_request',
        name='committee_coop_update_request'),
    
    url(r'^coop/(\d+)/requests/(\d+)/result/$',
        'update_request_result',
        name='committee_coop_update_request_result'),

    url(r'^coop/(\d+)/requests/(\d+)/edit/$',
        'edit_request',
        name='committee_coop_edit_request'),

    url(r'^coop/(\d+)/companies/(\d+)/edit/$',
        'edit_coop_company',
        name='committee_coop_edit_company'),

    url(r'^coop/requests/print/(\d+)/(\d+)/([a-z]+)/$',
        'print_request_pdf',
        name='committee_coop_print_request_pdf'),    
    url(r'^coop/letters/print/(\d+)/(\d+)/([a-z]+)/$',
        'print_sending_letter_pdf',
        name='committee_coop_print_sending_letter_pdf'),

    url(r'^coop/(\d+)/report/(\d+)/(\d+)/$',
        'report_requests', 
        name='committee_coop_report_requests'),
)
