# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'DepartmentSelectionInfo'
        db.create_table('committee_departmentselectioninfo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('department', self.gf('django.db.models.fields.related.OneToOneField')(related_name='company_selection_info', unique=True, to=orm['regis.Department'])),
            ('bookmarked_page', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
        ))
        db.send_create_signal('committee', ['DepartmentSelectionInfo'])


    def backwards(self, orm):
        
        # Deleting model 'DepartmentSelectionInfo'
        db.delete_table('committee_departmentselectioninfo')


    models = {
        'committee.companyselection': {
            'Meta': {'object_name': 'CompanySelection'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'department_selected'", 'to': "orm['regis.Company']"}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selected_companies'", 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'committee.departmentselectioninfo': {
            'Meta': {'object_name': 'DepartmentSelectionInfo'},
            'bookmarked_page': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'company_selection_info'", 'unique': 'True', 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['committee']
