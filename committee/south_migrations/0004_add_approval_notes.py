# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'ProposedCompany.approval_notes'
        db.add_column('committee_proposedcompany', 'approval_notes', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True), keep_default=False)

        # Changing field 'ProposedCompany.is_approved'
        db.alter_column('committee_proposedcompany', 'is_approved', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True))


    def backwards(self, orm):
        
        # Deleting field 'ProposedCompany.approval_notes'
        db.delete_column('committee_proposedcompany', 'approval_notes')

        # Changing field 'ProposedCompany.is_approved'
        db.alter_column('committee_proposedcompany', 'is_approved', self.gf('django.db.models.fields.BooleanField')(blank=True))


    models = {
        'committee.companyselection': {
            'Meta': {'object_name': 'CompanySelection'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'department_selected'", 'to': "orm['regis.Company']"}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selected_companies'", 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'committee.departmentselectioninfo': {
            'Meta': {'object_name': 'DepartmentSelectionInfo'},
            'bookmarked_page': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'company_selection_info'", 'unique': 'True', 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'committee.proposedcompany': {
            'Meta': {'object_name': 'ProposedCompany'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'approval_notes': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'proposed_companies'", 'to': "orm['regis.Department']"}),
            'fax_no': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_approved': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['committee']
