# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'CoOpCommitteeContact.full_name_english'
        db.add_column('committee_coopcommitteecontact', 'full_name_english',
                      self.gf('django.db.models.fields.CharField')(default='-', max_length=100),
                      keep_default=False)

        # Adding field 'CoOpCommitteeContact.tel_number_english'
        db.add_column('committee_coopcommitteecontact', 'tel_number_english',
                      self.gf('django.db.models.fields.CharField')(default='-', max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'CoOpCommitteeContact.full_name_english'
        db.delete_column('committee_coopcommitteecontact', 'full_name_english')

        # Deleting field 'CoOpCommitteeContact.tel_number_english'
        db.delete_column('committee_coopcommitteecontact', 'tel_number_english')


    models = {
        'committee.companydirectapplicationconfigletter': {
            'Meta': {'object_name': 'CompanyDirectApplicationConfigLetter'},
            'application': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'letter_config'", 'unique': 'True', 'to': "orm['std.CompanyDirectApplication']"}),
            'beginning_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'end_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'eval_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'letter_date': ('django.db.models.fields.DateField', [], {}),
            'letter_number': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'year': ('django.db.models.fields.IntegerField', [], {'max_length': '5'})
        },
        'committee.companyrequestconfigletter': {
            'Meta': {'object_name': 'CompanyRequestConfigLetter'},
            'beginning_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'company_request': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'letter_config'", 'unique': 'True', 'to': "orm['std.CompanyRequest']"}),
            'end_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'eval_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'letter_date': ('django.db.models.fields.DateField', [], {}),
            'letter_number': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'year': ('django.db.models.fields.IntegerField', [], {'max_length': '5'})
        },
        'committee.companyselection': {
            'Meta': {'object_name': 'CompanySelection'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'department_selected'", 'to': "orm['regis.Company']"}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selected_companies'", 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'committee.coopcommitteecontact': {
            'Meta': {'object_name': 'CoOpCommitteeContact'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'full_name_english': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tel_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tel_number_english': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'committee.departmentselectioninfo': {
            'Meta': {'object_name': 'DepartmentSelectionInfo'},
            'bookmarked_page': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'company_selection_info'", 'unique': 'True', 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'committee.proposedcompany': {
            'Meta': {'object_name': 'ProposedCompany'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'approval_notes': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'proposed_companies'", 'to': "orm['regis.Department']"}),
            'fax_no': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_approved': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_description': ('django.db.models.fields.TextField', [], {}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'regis.assignment': {
            'Meta': {'object_name': 'Assignment'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'assignments'", 'null': 'True', 'to': "orm['regis.Company']"}),
            'company_direct_application': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'assignments'", 'null': 'True', 'to': "orm['std.CompanyDirectApplication']"}),
            'company_request': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'assignments'", 'null': 'True', 'to': "orm['std.CompanyRequest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'receipt': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'assignments'", 'null': 'True', 'to': "orm['regis.Receipt']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'starting_char': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'eng_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'short_name_for_letters': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'})
        },
        'regis.education': {
            'Meta': {'object_name': 'Education'},
            'gpax': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_credit': ('django.db.models.fields.IntegerField', [], {}),
            'upto_semester': ('django.db.models.fields.IntegerField', [], {}),
            'upto_year': ('django.db.models.fields.IntegerField', [], {})
        },
        'regis.receipt': {
            'Meta': {'object_name': 'Receipt'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receipts'", 'to': "orm['regis.Company']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receipts'", 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'num_accepted_students': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'num_rounds_shown': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'total_student': ('django.db.models.fields.IntegerField', [], {}),
            'work_address': ('django.db.models.fields.TextField', [], {}),
            'work_description': ('django.db.models.fields.TextField', [], {}),
            'work_detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receipts'", 'to': "orm['regis.WorkDetail']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'max_length': '5'})
        },
        'regis.student': {
            'Meta': {'object_name': 'Student'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']", 'null': 'True'}),
            'education': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['regis.Education']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'eng_first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'eng_last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'eng_prefix': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_companies': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']", 'through': "orm['regis.Assignment']", 'symmetrical': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'prefix': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'})
        },
        'regis.workdetail': {
            'Meta': {'object_name': 'WorkDetail'},
            'beginning_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workdetails'", 'to': "orm['regis.Company']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dressing_description': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'other_description': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'std.companydirectapplication': {
            'Meta': {'ordering': "['created_at']", 'object_name': 'CompanyDirectApplication'},
            'accepted_at': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'chosen_at': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['regis.Company']", 'null': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'confirmed_at': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_accepted': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'is_chosen': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'is_confirmed_by_committee': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'is_letter_downloaded': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_sending_letter_printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'letter_downloaded_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sending_letter_printed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'company_direct_applications'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'std.companyrequest': {
            'Meta': {'ordering': "['requested_at']", 'object_name': 'CompanyRequest'},
            'accepted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'approved_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'beginning_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']", 'null': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '100'}),
            'contact_person': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'contact_tel_no': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'division_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'has_all_confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_accepted': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'is_approved_by_committee': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'is_default_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_sending_letter_printed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'letter_address': ('django.db.models.fields.TextField', [], {}),
            'reject_comments': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'requested_at': ('django.db.models.fields.DateTimeField', [], {}),
            'sending_letter_printed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"}),
            'work_fax_no': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'work_tel_no': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'working_address': ('django.db.models.fields.TextField', [], {}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['committee']