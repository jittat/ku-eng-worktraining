# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0002_auto_20231108_0451'),
        ('std', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyDirectApplicationConfigLetter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35')),
                ('letter_number', models.CharField(max_length=300, verbose_name='\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22')),
                ('letter_date', models.DateField(verbose_name='\u0e01\u0e33\u0e2b\u0e19\u0e14\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e2d\u0e2d\u0e01\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e23\u0e32\u0e0a\u0e01\u0e32\u0e23')),
                ('signer_name', models.CharField(default=b'', max_length=200)),
                ('beginning_date', models.CharField(default=b'', max_length=100)),
                ('end_date', models.CharField(default=b'', max_length=100)),
                ('eval_date', models.CharField(default=b'', max_length=100)),
                ('application', models.OneToOneField(related_name='letter_config', to='std.CompanyDirectApplication')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CompanyRequestConfigLetter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35')),
                ('letter_number', models.CharField(max_length=300, verbose_name='\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22')),
                ('letter_date', models.DateField(verbose_name='\u0e01\u0e33\u0e2b\u0e19\u0e14\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e2d\u0e2d\u0e01\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e23\u0e32\u0e0a\u0e01\u0e32\u0e23')),
                ('signer_name', models.CharField(default=b'', max_length=200)),
                ('beginning_date', models.CharField(default=b'', max_length=100)),
                ('end_date', models.CharField(default=b'', max_length=100)),
                ('eval_date', models.CharField(default=b'', max_length=100)),
                ('company_request', models.OneToOneField(related_name='letter_config', to='std.CompanyRequest')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CompanySelection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(default=None, null=True, verbose_name='\u0e1b\u0e35\u0e17\u0e35\u0e48\u0e40\u0e02\u0e49\u0e32\u0e23\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
                ('company', models.ForeignKey(related_name='department_selected', to='regis.Company')),
                ('department', models.ForeignKey(related_name='selected_companies', to='regis.Department')),
            ],
        ),
        migrations.CreateModel(
            name='CoOpCommitteeContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=100, verbose_name='\u0e2d\u0e32\u0e08\u0e32\u0e23\u0e22\u0e4c\u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e2a\u0e32\u0e19\u0e07\u0e32\u0e19')),
                ('email', models.EmailField(max_length=254, verbose_name='\u0e2d\u0e35\u0e40\u0e21\u0e25\u0e15\u0e34\u0e14\u0e15\u0e48\u0e2d')),
                ('tel_number', models.CharField(max_length=50, verbose_name='\u0e40\u0e1a\u0e2d\u0e23\u0e4c\u0e42\u0e17\u0e23\u0e28\u0e31\u0e1e\u0e17\u0e4c\u0e15\u0e34\u0e14\u0e15\u0e48\u0e2d')),
                ('full_name_english', models.CharField(max_length=100, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e2d\u0e32\u0e08\u0e32\u0e23\u0e22\u0e4c\u0e20\u0e32\u0e29\u0e32\u0e2d\u0e31\u0e07\u0e01\u0e24\u0e29')),
                ('tel_number_english', models.CharField(max_length=50, verbose_name='\u0e40\u0e1a\u0e2d\u0e23\u0e4c\u0e15\u0e34\u0e14\u0e15\u0e48\u0e2d\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e20\u0e32\u0e29\u0e32\u0e2d\u0e31\u0e07\u0e01\u0e24\u0e29')),
                ('department', models.ForeignKey(to='regis.Department')),
            ],
        ),
        migrations.CreateModel(
            name='DepartmentSelectionInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bookmarked_page', models.IntegerField(default=None, null=True, blank=True)),
                ('department', models.OneToOneField(related_name='company_selection_info', to='regis.Department')),
            ],
        ),
        migrations.CreateModel(
            name='ProposedCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u0e0a\u0e37\u0e48\u0e2d')),
                ('tel_no', models.CharField(max_length=15, verbose_name='\u0e42\u0e17\u0e23\u0e28\u0e4d\u0e1e\u0e17\u0e4c')),
                ('fax_no', models.CharField(max_length=15, verbose_name='\u0e42\u0e17\u0e23\u0e2a\u0e32\u0e23', blank=True)),
                ('address', models.TextField(verbose_name='\u0e17\u0e35\u0e48\u0e2d\u0e22\u0e39\u0e48')),
                ('contact_name', models.CharField(help_text='\u0e0a\u0e37\u0e48\u0e2d\u0e1a\u0e38\u0e04\u0e04\u0e25\u0e17\u0e35\u0e48\u0e04\u0e13\u0e30\u0e2f \u0e41\u0e25\u0e30\u0e19\u0e34\u0e2a\u0e34\u0e15\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e15\u0e34\u0e14\u0e15\u0e48\u0e2d\u0e40\u0e01\u0e35\u0e48\u0e22\u0e27\u0e01\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19\u0e19\u0e35\u0e49\u0e44\u0e14\u0e49', max_length=200, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e2a\u0e32\u0e19\u0e07\u0e32\u0e19')),
                ('signer_name', models.CharField(default=b'', help_text='\u0e0a\u0e37\u0e48\u0e2d\u0e2b\u0e23\u0e37\u0e2d\u0e15\u0e33\u0e41\u0e2b\u0e19\u0e48\u0e07\u0e17\u0e35\u0e48\u0e04\u0e13\u0e30\u0e2f \u0e08\u0e30\u0e17\u0e33\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e44\u0e1b\u0e16\u0e36\u0e07', max_length=200, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e1c\u0e39\u0e49\u0e25\u0e07\u0e19\u0e32\u0e21', blank=True)),
                ('short_description', models.TextField(help_text='\u0e2d\u0e18\u0e34\u0e1a\u0e32\u0e22\u0e25\u0e31\u0e01\u0e29\u0e13\u0e30\u0e18\u0e38\u0e23\u0e01\u0e34\u0e08\u0e04\u0e23\u0e48\u0e32\u0e27 \u0e46 \u0e02\u0e2d\u0e07\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23 \u0e40\u0e0a\u0e48\u0e19 \u0e1e\u0e31\u0e12\u0e19\u0e32\u0e23\u0e30\u0e1a\u0e1a\u0e40\u0e04\u0e23\u0e37\u0e2d\u0e02\u0e48\u0e32\u0e22 \u0e2b\u0e23\u0e37\u0e2d\u0e1c\u0e25\u0e34\u0e15\u0e0a\u0e34\u0e49\u0e19\u0e2a\u0e48\u0e27\u0e19\u0e23\u0e16\u0e22\u0e19\u0e15\u0e4c \u0e40\u0e1b\u0e47\u0e19\u0e15\u0e49\u0e19', verbose_name='\u0e25\u0e31\u0e01\u0e29\u0e13\u0e30\u0e07\u0e32\u0e19\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23')),
                ('is_approved', models.NullBooleanField(default=None, verbose_name='\u0e1c\u0e25\u0e01\u0e32\u0e23\u0e2d\u0e19\u0e38\u0e21\u0e31\u0e15\u0e34\u0e41\u0e25\u0e49\u0e27')),
                ('approval_notes', models.CharField(default=b'', max_length=100, verbose_name='\u0e2b\u0e21\u0e32\u0e22\u0e40\u0e2b\u0e15\u0e38\u0e01\u0e32\u0e23\u0e2d\u0e19\u0e38\u0e21\u0e31\u0e15\u0e34', blank=True)),
                ('department', models.ForeignKey(related_name='proposed_companies', to='regis.Department')),
            ],
        ),
    ]
