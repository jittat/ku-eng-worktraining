# -*- coding: utf-8 -*-

from commons.academia import get_current_academic_year, get_next_worktraining_year, get_recent_worktraining_year
from regis.models import Company, Department, Student, DepartmentNameInRequestLetter, Receipt
from std.models import SelectionRound, SelectionRoundStatus, StudentSelection, CompanyRequest, CompanyDirectApplication
from std.models import assign_selections_to_receipts
from commons.email import send_status_report_after_selection
from commons.email import send_company_request_notification_email
from commons.email import send_company_direct_application_result_notification_email
from django.contrib.auth.models import User

def send_status_report(user):
    status = SelectionRoundStatus.get_status()
    year = get_next_worktraining_year()

    profile = user.userprofile
    department = profile.department
    actual_department = DepartmentNameInRequestLetter.to_requested_department(department)

    if status.is_open:
        # TODO: implement this
        pass
    else:
        sel_round = SelectionRoundStatus.get_last_round()
        if sel_round==None:
            return
        next_round = SelectionRoundStatus.get_next_round()

        receipts = (Receipt
                    .all_for_round(sel_round,
                                   department=actual_department)
                    .select_related().all())

        assign_selections_to_receipts(receipts, sel_round.number, department)
       
        send_status_report_after_selection(user,
                                           department,
                                           sel_round,
                                           next_round,
                                           receipts)

        print 'Sent to ' + user.username


def send_email_all(email_function):
    for user in User.objects.all():
        try:
            profile = user.userprofile
            if not profile:
                continue
        except:
            continue

        if not user.is_active:
            continue

        if not profile.is_committee_member:
            continue

        if not (profile.is_accepting_emails and user.email):
            continue

        print user.username

        email_function(user)


def send_status_report_all():
    send_email_all(send_status_report)


def send_company_request_notification(user):
    profile = user.userprofile
    department = profile.department
    year = get_next_worktraining_year()
    all_company_requests = (CompanyRequest
                            .all_for_department(department, 
                                                year,
                                                True)
                            .order_by('company_name'))

    active_company_requests = [r for r in all_company_requests
                               if r.is_approved_by_committee == None]

    if len(active_company_requests)==0:
        return

    send_company_request_notification_email(user, 
                                            department, 
                                            active_company_requests)

        
def send_company_direct_application_result_notification(user):
    profile = user.userprofile
    department = profile.department
    year = get_next_worktraining_year()
    all_direct_applications = (CompanyDirectApplication
                               .all_for_department(department, 
                                                   year)
                               .order_by('company_name'))

    active_direct_applications = [a for a in all_direct_applications
                                  if a.is_accepted and a.is_chosen and (a.is_confirmed_by_committee == None)]

    if len(active_direct_applications)==0:
        return

    send_company_direct_application_result_notification_email(user, 
                                                              department, 
                                                              active_direct_applications)

        
def send_company_request_notification_all():
    send_email_all(send_company_request_notification)
def send_company_direct_application_result_notification_all():
    send_email_all(send_company_direct_application_result_notification)
