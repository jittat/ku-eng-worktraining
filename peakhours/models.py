from django.db import models

from regis.models import Student
from std.models import SelectionRound

class PeakhourKey(models.Model):
    code = models.CharField(max_length=20)
    student = models.ForeignKey(Student)
    selection_round = models.ForeignKey(SelectionRound)

    def __unicode__(self):
        return self.code

