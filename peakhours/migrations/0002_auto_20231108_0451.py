# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0001_initial'),
        ('std', '0001_initial'),
        ('peakhours', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='peakhourkey',
            name='selection_round',
            field=models.ForeignKey(to='std.SelectionRound'),
        ),
        migrations.AddField(
            model_name='peakhourkey',
            name='student',
            field=models.ForeignKey(to='regis.Student'),
        ),
    ]
