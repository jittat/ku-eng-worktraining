# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date

# ------------- User profile ----------------

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    is_sa_admin = models.BooleanField(default=False,
                                      verbose_name=u'เป็นเจ้าหน้าที่หน่วยกิจการนิสิต')
    is_committee_member = models.BooleanField(default=False,
                                              verbose_name=u'เป็นกรรมการกิจการนิสิต')
    department = models.ForeignKey("regis.Department",
                                   blank=True,
                                   null=True)
    is_accepting_emails = models.BooleanField(default=False,
                                              verbose_name=u'รับเมล์แจ้งข้อมูล')
    nontri_username = models.CharField(max_length=30,
                                       verbose_name=u'บัญชีนนทรีสำหรับยืนยันตัวตน',
                                       blank=True,
                                       null=True)
    is_department_admin = models.BooleanField(default=False,
                                              verbose_name=u'เป็นเจ้าหน้าที่ภาควิชา')

    def __unicode__(self):
        if self.is_sa_admin:
            return u"%s (sa admin)" % self.user.username
        elif self.is_committee_member:
            if self.department:
                return u"%s (%s)" % (self.user.username, self.department.name)
            else:
                return u"%s (committee)" % self.user.username
        else:
            return self.user.username


    def is_admin_or_department_committee(self, code):
        return (self.is_sa_admin or
                (self.is_committee_member and self.department and
                 self.department.code == code))

    def can_view_committee_menu(self):
        return (self.is_committee_member or
                self.is_department_admin)

# ------------- CONFIGURATIONS ----------------

CONFIG_VARIABLES = [
    # put config variables in the form:
    #  (name, title, default_value, value_type, help_text)
    ('default.beginning_date',u'กำหนดเริ่มฝึกงาน','','date',u'กำหนดวันเริ่มฝึกงานทั่วไป ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-03-22)'),
    ('default.end_date',u'กำหนดสิ้นสุดฝึกงาน','','date',u'กำหนดวันที่เลิกฝึกงานทั่วไป ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-05-22)'),
    ('default.eval.beginning_date',u'กำหนดเริ่มฝึกงานตั้งต้นในแบบประเมิน','','date',u'กำหนดวันเริ่มฝึกงานที่จะขึ้นในการบันทึกแบบประเมิน ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-03-22)'),
    ('default.eval.end_date',u'กำหนดสิ้นสุดฝึกงานตั้งต้นในแบบประเมิน','','date',u'กำหนดวันที่เลิกฝึกงานที่จะขึ้นในการบันทึกแบบประเมิน ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-05-22)'),
    ('default.eval_end_date',u'กำหนดสิ้นสุดรับแบบประเมิน','','date',u'กำหนดวันที่ส่งแบบประเมินวันสุดท้าย ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-07-31)'),
    ('default.letter_date',u'กำหนดวันที่ออกจดหมายราชการ สำหรับหนังสือขอความอนุเคราะห์','','date',u'กำหนดวันที่ออกจดหมายราชการ ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-05-22)'),
    ('default.letter_number',u'เลขที่จดหมายราชการ สำหรับหนังสือขอความอนุเคราะห์: ที่','','string',u'กำหนดเลขที่จดหมายราชการ เช่น ศธ. 0513.10801/ว.2047'),
    ('default.letter_beginning_date',u'กำหนดเริ่มฝึกงานในจดหมาย','','date',u'กำหนดวันเริ่มฝึกงานทั่วไป ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-03-22)'),
    ('default.letter_end_date',u'กำหนดสิ้นสุดฝึกงานในจดหมาย','','date',u'กำหนดวันที่เลิกฝึกงานทั่วไป ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-05-22)'),
    ('default.letter_signature',u'ผู้ลงนามจดหมาย','','string',u'ชื่อผู้ลงนามจดหมายราชการ เช่น อ.ชาญเวช ศีลพิพัฒน์'),
    ('default.letter_signature_english',u'ผู้ลงนามจดหมายภาษาอังกฤษ','','string',u'ชื่อผู้ลงนามจดหมายราชการ เช่น Asst. Prof. Weerawoot Kanokbannakorn, D.Eng.'),
    ('default.letter_receipt_deadline',u'กำหนดวันตอบจดหมายขอความอนุเคราะห์','','string',u'กำหนดวันที่ตอบจดหมายขอความอนุเคราะห์ ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-11-30)'),
    ('request.letters_to_all_companies',u'ขอความอนุเคราะห์ทุกสถานประกอบการ','','string',u'ใส่ yes ถ้าต้องการส่งจดหมายขอความอนุเคราะห์ไปยังทุกสถานประกอบการ (โดยที่ภาควิชาไม่ต้องเลือก)'),
    ('email.cc_emails',u'รายการอีเมล์ที่ส่งสำเนาเมื่อมีการส่งอีเมล์กลุ่ม','','string',u'สามารถใส่อีเมล์หลายอันโดยคั่นด้วย "," เช่น fengpac@ku.ac.th,fengchj@ku.ac.th เป็นต้น'),
    ('deadline.applying',u'วันสุดท้ายที่สามารถสมัครขอเข้าฝึกงาน','2013-08-31','date',u'เป็นวันสุดท้ายที่สมัครเข้าฝึกงานในภาคฤดูร้อนปีการศึกษาถัดไป ใส่ในรูปแบบ เช่น 2013-08-31'),
    ('deadline.free_self_appl_date',u'วันสุดท้ายที่สามารถจัดหาหน่วยงานเอง','2013-11-15','date',u'วันสุดท้ายที่นิสิตที่เลือกประเภทหาจัดหาหน่วยงานเอง สามารถเพิ่มข้อมูลได้ หลังจากวันนี้ถ้าต้องการหาเองจะไม่สามารถเปลี่ยนกลับไปเป็นแบบให้คณะจัดหาให้ได้ ใส่ในรูปแบบ 2013-08-31'),
    ('default.sending_letter_date',u'กำหนดวันที่ออกจดหมายราชการ สำหรับหนังสือส่งตัวนิสิต','','date',u'กำหนดวันที่ออกจดหมายราชการ ใส่ในรูปแบบ (ปีคศ-เดือน-วัน) เช่น (2010-05-22)'),
    ('default.sending_letter_number',u'เลขที่จดหมายราชการ สำหรับหนังสือส่งตัวนิสิต: ที่','','string',u'กำหนดเลขที่จดหมายราชการ เช่น ศธ. 0513.10801/ว.2047'),
    ]

CONFIG_VALUE_TYPES = (
    ('date','date'),
    ('datetime','datetime'),
    ('integer','integer'),
    ('string','string')
    )

DATE_STR_FORMAT = '%Y-%m-%d'
DATETIME_STR_FORMAT = '%Y-%m-%d %H:%M'

def convert_config_str_to_date(str):
    dt = datetime.strptime(str,DATE_STR_FORMAT)
    return dt.date()

def convert_date_to_config_str(d):
    return d.strftime(DATE_STR_FORMAT)

def convert_config_str_to_datetime(str):
    return datetime.strptime(str,DATETIME_STR_FORMAT)

def convert_datetime_to_config_str(dt):
    return d.strftime(DATETIME_STR_FORMAT)

class Configuration(models.Model):
    name = models.CharField(max_length=100, unique=True)
    title = models.CharField(max_length=100)
    value = models.CharField(max_length=300, blank=True)
    value_type = models.CharField(max_length=20,
                                  choices=CONFIG_VALUE_TYPES)
    help_text = models.CharField(max_length=500, blank=True)
    rank = models.IntegerField(default=0)

    class Meta:
        ordering = ['rank']

    @staticmethod
    def init_config_variables():
        r = 0
        for (n,title,v,vt,help_text) in CONFIG_VARIABLES:
            try:
                conf = Configuration.objects.get(name=n)
            except Configuration.DoesNotExist:
                conf = None

            r += 1
            if not conf:
                conf = Configuration(name=n,
                                     title=title,
                                     value=v,
                                     value_type=vt,
                                     help_text=help_text,
                                     rank=r)
                conf.save()
            else:
                conf.help_text = help_text
                conf.title = title
                conf.rank = r
                conf.save()

    def get_value(self):
        try:
            if self.value_type=='integer':
                return int(self.value)
            elif self.value_type=='date':
                return convert_config_str_to_date(self.value)
            elif self.value_type=='datetime':
                return convert_config_str_to_datetime(self.value)
            else:
                # does not know how to convert, return as string
                return self.value
        except:
            return None


    def set_value(self,val):
        if self.value_type=='integer':
            self.value = int(val)
        elif self.value_type=='date':
            self.value = convert_date_to_config_str(val)
        elif self.value_type=='datetime':
            self.value = convert_datetime_to_config_str(val)
        else:
            # does not know how to convert, return as string
            self.value = val


    @staticmethod
    def get(name):
        try:
            conf = Configuration.objects.get(name=name)
        except Configuration.DoesNotExist:
            conf = None

        if conf:
            return conf.get_value()
        else:
            return None

class Log(models.Model):
    user = models.CharField(blank=True, max_length=20)
    message = models.CharField(max_length=200)
    student = models.ForeignKey('regis.Student',
                                null=True,
                                blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    @staticmethod
    def create(message, user='', student=None):
        if isinstance(user, User):
            user = user.username
        log = Log(user=user, message=message,
                  student=student)
        log.save()
    
    def __unicode__(self):
        return u'%s: %s' % (self.user, self.message)

