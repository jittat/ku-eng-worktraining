# -*- coding: utf-8 -*-
from django.conf import settings
from mailer import send_mail as mailer_send_mail
from commons.utils import admin_email

def send_mail(to_email, subject, message):
    if settings.EMAIL_SENDER=='':
        sender = admin_email()
    else:
        sender = settings.EMAIL_SENDER

    send_real_email = True

    try:
        if settings.FAKE_SENDING_EMAIL:
            send_real_email = False
    except:
        pass
    
    if send_real_email:
        mailer_send_mail(subject,
                  message,
                  sender,
                  [ to_email ],
                  fail_silently=True)
    else:
        print 'Does not send email'
        print 'To: ', to_email
        print 'Message:'
        try:
            print message
        except:
            print "Sorry.... can't show the message."


def send_mail_to_student(student, subject, message):
    email = student.get_email()

    send_mail(email, subject, message)

def send_register_confirmation(student):
    subject = u'ยืนยันการสมัครเข้ารับการฝึกงาน'
    
    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่าคุณได้สมัครขอเข้ารับการฝึกงานประจำภาคฤดูร้อนปีการศึกษาที่จะถึงแล้ว

-ทีมงานเว็บระบบฝึกงาน""" % (student.full_name()))
    
    send_mail_to_student(student, subject, message)


def send_selection_confirmation(student, selection, round_number):
    subject = u'ยืนยันการเลือกสถานประกอบการ'
    
    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่าในการเลือกสถานประกอบการในรอบที่ %d
คุณได้เลือก: %s
เป็นสถานประกอบการที่ต้องการเข้าพิจารณา

-ทีมงานเว็บระบบฝึกงาน

รหัสตรวจสอบ: %s""" % (
            student.full_name(),
            round_number,
            selection.company.name,
            selection.get_token()))
    
    send_mail_to_student(student, subject, message)


def send_deselection_confirmation(student, round_number):
    subject = u'ยืนยันการยกเลิกการเลือกสถานประกอบการ'
    
    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่าในการเลือกสถานประกอบการในรอบที่ %d
คุณได้ยกเลิกการเลือกสถานประกอบการแล้ว

-ทีมงานเว็บระบบฝึกงาน""" % (
            student.full_name(),
            round_number))
    
    send_mail_to_student(student, subject, message)


def send_status_report_after_selection(user, 
                                       department, 
                                       selection_round, 
                                       next_selection_round,
                                       receipts):
    sel_msg_list = []
    unsel_msg_list = []
    counters = [1,1]
    for r in receipts:
        num_students = len(r.selections)
        if num_students > 0:
            counter = counters[0]
            counters[0] += 1
        else:
            counter = counters[1]
            counters[1] += 1

        msg = (u"%d. %s (%d/%d)" %
               (counter,
                r.company.name,
                r.num_available_positions(),
                num_students))
        if num_students > r.num_available_positions():
            msg += " ***"

        if num_students > 0:
            sel_msg_list.append(msg)
        else:
            unsel_msg_list.append(msg)

    mail_msg = []
    mail_msg.append(u"เรียน กรรมการสาขา" + department.name)
    mail_msg.append(u"ขณะนี้การเลือกในรอบ %d เสร็จสิ้นแล้ว" % (selection_round.number))
    if next_selection_round!=None:
        mail_msg.append(u"รอบต่อไปจะเริ่มในวันที่ %s" % 
                        (next_selection_round.beginning_time.strftime("%d %B")))
    
    mail_msg.append(u"")
    mail_msg.append(u"ข้อมูลการเลือกสถานประกอบการของสาขา" + department.name)
    mail_msg.append(u"(สำหรับสถานประกอบการที่มีการเลือกมากกว่าจำนวนรับจะมีเครื่องหมาย *** ตามหลัง)")
    mail_msg.append(u"สถานประกอบการที่มีนิสิตเลือก")
    mail_msg.append(u"=====================")
    mail_msg.append(u"   ชื่อ    (จำนวนรับ/จำนวนเลือก)")
    if len(sel_msg_list)!=0:
        mail_msg += sel_msg_list
    else:
        mail_msg.append(u"    -- ไม่มี --")
    mail_msg.append(u"")
    mail_msg.append(u"สถานประกอบการที่ยังไม่มีนิสิตเลือก")
    mail_msg.append(u"=========================")
    mail_msg.append(u"   ชื่อ    (จำนวนรับ/จำนวนเลือก)")
    if len(unsel_msg_list)!=0:
        mail_msg += unsel_msg_list
    else:
        mail_msg.append(u"    -- ไม่มี --")
    mail_msg.append(u"")
    mail_msg.append(u"ข้อมูลต่าง ๆ นี้ รวมทั้งรายชื่อนิสิตที่เลือกสถานประกอบการ กรรมการสามารถเข้าพิจารณาได้จากเว็บฝึกงาน http://wt.eng.ku.ac.th")
    mail_msg.append(u"ถ้ามีสถานประกอบการที่นิสิตเลือกมากกว่าจำนวน รบกวนกรรมการช่วยเข้าไปพิจารณาและดำเนินการคัดเลือกนิสิตที่เหมาะสมด้วย")
    mail_msg.append(u"")
    mail_msg.append(u"-เมล์นี้ถูกส่งโดยอัตโนมัติจากระบบฝึกงาน-")

    email = user.email
    send_mail(email, u"[wt] รายงานสถานะเกี่ยวกับการฝึกงาน",
              u"\n".join(mail_msg))


def send_approval_confirmation(selection, selection_round):
    subject = u'ผลการเลือกสถานประกอบการ สำหรับการเลือกรอบที่ %d' % (selection_round.number,)
    
    student = selection.student

    if selection.result:
        result = (u"""คุณได้ผ่านการคัดเลือกให้ไปฝึกงานในสถานประกอบการดังกล่าว
และเนื่องจากคุณได้รับคัดเลือกในรอบนี้ คุณจะไม่มีสิทธิในการเลือกในรอบต่อ ๆ ไป""")
    elif selection.result == False:
        result = (u"""คุณไม่ได้รับคัดเลือกให้ไปฝึกงานในสถานประกอบการดังกล่าว
ตามเกณฑ์การพิจารณาของสาขา
และเนื่องจากคุณไม่ได้รับคัดเลือกในรอบนี้ คุณจะสามารถเลือกสถานประกอบการในรอบถัด ๆ ไปได้""")
    else:
        result = (u"""ทางกรรมการฝึกงานยังไม่ได้ตัดสินใจผลการเลือกของคุณ 
ทั้งนี้อาจมาจากการต้องรอพิจารณาเงื่อนไขต่าง ๆ  
อย่างไรก็ดี คุณควรจะติดตามการติดต่อจากทางกรรมการด้วย""")

    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อแจ้งผลการเลือกสถานประกอบการในรอบที่ %d 
ที่คุณได้เลือก %s เป็นสถานประกอบการที่ต้องการเข้าพิจารณา

ขณะนี้กรรมการได้พิจารณาแล้ว %s

-ทีมงานเว็บระบบฝึกงาน
""" % (
            student.full_name(),
            selection_round.number,
            selection.company.name,
            result))

    
    send_mail_to_student(student, subject, message)


def send_direct_approval_confirmation(direct_application):
    subject = u'ผลการสมัครสถานประกอบการที่คัดเลือกนิสิตเอง'
    
    student = direct_application.student

    if direct_application.is_accepted:
        result = (u"""คุณได้ผ่านการคัดเลือกให้ไปฝึกงานในสถานประกอบการดังกล่าว
และเนื่องจากคุณได้รับคัดเลือกแล้ว คุณจะไม่มีสิทธิในการเลือกและสมัครสถานประกอบอื่น ๆ""")
    elif direct_application.is_accepted == False:
        result = (u"""คุณไม่ได้รับคัดเลือกให้ไปฝึกงานในสถานประกอบการดังกล่าว
ดังนั้นถ้าคุณไม่ได้เลือกสมัครสถานประกอบการที่คัดเลือกนิสิตเองไว้ หรือทราบผลการสมัครหมดแล้ว
คุณจะสามารถเข้าเลือกสถานประกอบการที่ให้คณะคัดเลือกไว้ได้  และถ้ามีการเลือกอยู่ในขณะนี้
คุณก็สามารถเข้าไปเลือกได้ทันที""")
    else:
        result = (u"""เกิดการเปลี่ยนแปลงสถานะของการคัดเลือกฝึกงานในสถานประกอบการดังกล่าว
ขณะนี้ผลการฝึกงานเปลี่ยนเป็น 'รอพิจารณาผล'
ทั้งนี้เนื่องจากอาจเกิดความผิดพลาดในการกรอกข้อมูล หรือมีการเปลี่ยนแปลงผลการคัดเลือก
การที่คุณได้รับเมล์นี้อาจเกิดจากความผิดพลาดของระบบ
กรุณาแจ้งทีมงาน ผ่านทางกรรมการกิจการนิสิต หรือทางหน่วยกิจการนิสิตด้วย จักเป็นพระคุณยิ่ง""")

    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อแจ้งผลการสมัครสถานประกอบการที่ขอคัดเลือกนิสิตเอง
คุณได้สมัคร %s เป็นสถานประกอบการที่ต้องการเข้าพิจารณา

ขณะนี้ทางหน่วยกิจการนิสิตได้รับผลการคัดเลือกจากสถานประกอบการแล้ว  ผลคือ:

%s

-ทีมงานเว็บระบบฝึกงาน
""" % (
            student.full_name(),
            direct_application.position.company.name,
            result))

    
    send_mail_to_student(student, subject, message)


def send_peakhour_key(student, selection_round, key):
    subject = u'รหัสส่วนตัวสำหรับการเลือกในช่วงเร่งด่วน รอบที่ %d' % selection_round.number
    
    message = (u"""เรียนคุณ %s %s

อีเมล์นี้เป็นการแจ้งรหัสส่วนตัวสำหรับการเลือกในช่วงเร่งด่วน 
โดยจะส่งให้กับนิสิตที่ลงทะเบียนฝึกงานทุกคน  
เป็นไปได้ที่คุณจะได้ที่ฝึกงานแล้ว หรือไม่สามารถเลือกในรอบนี้ได้
ในกรณีดังกล่าว คุณไม่ต้องเข้าระบบไปเลือกและไม่ต้องสนใจอีเมล์นี้

ด้านล่างเป็นรหัสส่วนตัวสำหรับการเลือกในรอบที่ %d ในช่วงเร่งด่วน

รหัสส่วนตัว: %s

หมายเหตุ: ในการเลือกเร่งด่วนโดยการคอมเมนท์ในระบบเฟสบุ๊ค จะไม่ใช้เลขประจำตัวนิสิต

ถ้าคุณไม่จำเป็นต้องเลือกในช่วงเร่งด่วน คุณสามารถเข้าใช้ระบบการเลือกได้ตามปกติ

-ทีมงานเว็บระบบฝึกงาน
""" % (
            student.full_name(), student.student_id,
            selection_round.number,
            key.code))

    nontri_email = 'b' + student.student_id[:-1] + '@ku.ac.th'

    send_mail(nontri_email, subject, message)


def send_assignment_cancel_confirmation(assignment):
    subject = u'การไปฝึกงานที่ %s ถูกยกเลิก' % (assignment.company.name)
    
    student = assignment.student

    message = (u"""เรียน %s

จดหมายนี้ส่งมาเพื่อแจ้งว่า จากเดิมที่คุณได้รับคัดเลือกให้ไปฝึกงานที่ 
%s
ขณะนี้ได้ถูกยกเลิกแล้ว กรุณาสอบถามทางหน่วยกิจการนิสิตอีกครั้งสำหรับรายละเอียด

ถ้าคุณอยู่ในเกณฑ์ที่สามารถเลือกสถานประกอบการในรอบถัดไปได้
ระบบจะเปิดให้คุณเลือกสถานประกอบการได้

-ทีมงานเว็บระบบฝึกงาน
""" % (
            student.full_name(),
            assignment.company.name))
    
    send_mail_to_student(student, subject, message)


def send_email_confirmation_email(student,email_confirmation):
    subject = u'กรุณายืนยันอีเมล์สำหรับระบบฝึกงาน'

    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อแจ้งให้คุณยืนยันอีเมล์ %s ว่าเป็นของคุณ

ระบบจะถือว่าในการยืนยันอีเมล์ดังกล่าว คุณมีความประสงค์ขอเข้าฝึกงานในภาคฤดูร้อนหน้า
และคุณได้รับทราบและยินยอมปฏิบัติตามระเบียบการฝึกงานของทางคณะวิศวกรรมศาสตร์ 
มหาวิทยาลัยเกษตรศาสตร์ ทุกประการ

ถ้าคุณรับทราบเงื่อนไขดังกล่าวแล้ว คุณสามารถยืนยันอีเมล์ได้โดยการคลิ๊กที่ลิงก์ด้านล่าง

%s

หรือคัดลอกลิงก์ดังกล่าวไปเปิดในบราวเซอร์

-ทีมงานเว็บระบบฝึกงาน
""" % (
            student.full_name(),
            student.get_email(),
            email_confirmation.get_confirmation_url()))
    
    send_mail_to_student(student, subject, message)


def send_application_confirmation_email(student,regtype_message):
    subject = u'ตอบรับการสมัครเข้าฝึกงานหรือสหกิจศึกษา'

    from django.conf import settings
    import hashlib

    code = hashlib.md5(settings.APPLICATION_CONFIRMATION_SALT + student.student_id).hexdigest()

    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อแจ้งยืนยันว่าคุณได้สมัครขอเข้าฝึกงานหรือสหกิจศึกษาเสร็จสมบูรณ์แล้ว

โดยคุณได้สมัครแบบ %s

ด้านล่างเป็นรหัสยืนยัน คุณสามารถเก็บรหัสนี้ไว้เพื่อยืนยันการสมัคร ในกรณีที่มีปัญหาได้
รหัสยืนยัน: %s

-ทีมงานเว็บระบบฝึกงาน
""" % (
    student.full_name(),
    regtype_message,
    code))
    
    send_mail_to_student(student, subject, message)


def send_company_request_approval_email(student, company_request, is_approved):
    if is_approved:
        subject = u'แจ้งการอนุมัติการขอหนังสือขอความอนุเคราะห์'
        message = (u"""เรียน %s %s

คุณได้ขอหนังสือขอความอนุเคราะห์เพื่อขอเข้าฝึกงานที่ %s (ยื่นโดย %s)

ขณะนี้กรรมการฝึกงานประจำสาขาวิชาได้อนุมัติการขอจดหมายดังกล่าวแล้ว
คุณสามารถเข้าระบบเพื่อดาวน์โหลดหนังสือขอความอนุเคราะห์ แบบตอบรับ 
ฟอร์มแจ้งรายละเอียด และหนังสือชี้แจงการเปลี่ยนแปลงขั้นตอนการฝึกงาน
ได้จากระบบฝึกงานแล้ว

เมื่อคุณได้พิมพ์หนังสือดังกล่าวแล้ว ให้นำไปยื่นให้กับหน่วยงาน (ถ้าในการสมัครมีนิสิต
ร่วมยื่นขอหนังสือพร้อมกันหลายคนให้พิมพ์เพียงชุดเดียว) เมื่อทางหน่วยงานได้กรอกข้อมูลแล้ว
ให้นำหนังสือตอบรับและแบบฟอร์มรายละเอียดในการฝึกงานมายื่นให้กับกรรมการฝึกงาน
เพื่อยืนยันการฝึกงานต่อไป

-ทีมงานเว็บระบบฝึกงาน""" % (student.full_name(), 
                       student.student_id,  
                       company_request.company_name, 
                       company_request.student.full_name()))
    else:
        subject = u'แจ้งการไม่อนุมัติการขอหนังสือขอความอนุเคราะห์'
        message = (u"""เรียน %s %s

คุณได้ขอหนังสือขอความอนุเคราะห์เพื่อขอเข้าฝึกงานที่ %s (ยื่นโดย %s)

กรรมการฝึกงานประจำสาขาวิชาไม่อนุมัติการขอหนังสือขอความอนุเคราะห์ดังกล่าว
เนื่องจาก %s

คุณสามารถยื่นขออนุมัติขอหนังสือขอความอนุเคราะห์สำหรับหน่วยงานอื่น
หรือดำเนินการขอสมัครหน่วยงานที่มีการคัดเลือกนิสิตเองต่อไปได้

-ทีมงานเว็บระบบฝึกงาน""" % (student.full_name(), 
                       student.student_id,  
                       company_request.company_name, 
                       company_request.student.full_name(),
                       company_request.reject_comments))

    
    send_mail_to_student(student, subject, message)


def send_company_request_notification_email(user, 
                                            department, 
                                            active_company_requests):
    mail_msg = []
    mail_msg.append(u"เรียน กรรมการสาขา" + department.name)
    mail_msg.append(u"")
    mail_msg.append(u'มีรายการขอหนังสือขอความอนุเคราะห์ที่คุณยังไม่ได้เข้าไปตอบอนุมัติหรือตอบปฏิเสธ ดังรายการด้านล่างนี้')

    num = 1
    for r in active_company_requests:
        s = r.get_approved_students()
        if len(s)==1:
            mail_msg.append(u'%d. %s เสนอโดย %s' % 
                            (num, r.company_name,
                             s[0].full_name()))
        else:
            mail_msg.append(u'%d. %s เสนอโดย %s (ร่วมกับนิสิตอีก %d คน)' % 
                            (num, r.company_name,
                             s[0].full_name(), len(s)-1))
        num += 1

    mail_msg.append(u"")
    mail_msg.append(u"กรรมการสามารถเข้าพิจารณาได้จากเว็บฝึกงาน http://wt.eng.ku.ac.th")
    mail_msg.append(u"")
    mail_msg.append(u"-เมล์นี้ถูกส่งโดยอัตโนมัติจากระบบฝึกงาน-")

    email = user.email
    send_mail(email, u"[wt] มีรายการขอหนังสือขอความอนุเคราะห์รอการอนุมัติ",
              u"\n".join(mail_msg))


def send_company_direct_application_result_notification_email(user, 
                                                              department, 
                                                              company_direct_applications):
    mail_msg = []
    mail_msg.append(u"เรียน กรรมการสาขา" + department.name)
    mail_msg.append(u"")
    mail_msg.append(u'มีรายการแจ้งผลการสมัครหน่วยงานที่คัดเลือกนิสิตเองที่คุณยังไม่ได้ตอบอนุมัติหรือตอบปฏิเสธ ดังรายการด้านล่างนี้')

    num = 1
    for a in company_direct_applications:
        mail_msg.append(u'%d. %s แจ้งผลโดย %s' % 
                        (num, a.company_name,
                         a.student.full_name()))
        num += 1

    mail_msg.append(u"")
    mail_msg.append(u"กรรมการสามารถเข้าพิจารณาได้จากเว็บฝึกงาน http://wt.eng.ku.ac.th")
    mail_msg.append(u"จากนั้นเลือกดูในส่วน รายการการแจ้งสมัครฝึกงานสำหรับหน่วยงานคัดเลือกนิสิตเอง")
    mail_msg.append(u"")
    mail_msg.append(u"-เมล์นี้ถูกส่งโดยอัตโนมัติจากระบบฝึกงาน-")

    email = user.email
    send_mail(email, u"[wt] มีรายการแจ้งผลการสมัครหน่วยงานที่คัดเลือกนิสิตเอง",
              u"\n".join(mail_msg))


def send_company_request_acceptance_confirmation(student, company_request):
    subject = u'แจ้งการตอบรับจากหน่วยงาน'
    
    message = (u"""เรียน %s

จากที่คุณได้ขอหนังสือขอความอนุเคราะห์ไว้ และได้นำหนังสือตอบรับมายื่นกับกรรมการ

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่ากรรมการได้บันทึกข้อมูลว่าหน่วยงาน:

%s

ได้ตอบรับคุณเข้าฝึกงานแล้ว

คุณจะไม่สามารถเลือกหน่วยงานอื่น ๆ หรือแก้ไขหน่วยงานที่จะไปฝึกงานได้อีก

-ทีมงานเว็บระบบฝึกงาน""" % (
            student.full_name(),
            company_request.company_name))
    
    send_mail_to_student(student, subject, message)


def send_company_request_rejectance_confirmation(student, company_request):
    subject = u'แจ้งการปฏิเสธจากหน่วยงาน'
    
    message = (u"""เรียน %s

จากที่คุณได้ขอหนังสือขอความอนุเคราะห์ไว้ และได้นำหนังสือตอบรับมายื่นกับกรรมการ

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่ากรรมการได้บันทึกข้อมูลว่าหน่วยงาน:

%s

ตอบปฏิเสธการขอเข้าฝึกงานของคุณแล้ว

ถ้าหน่วยงานนี้เป็นหน่วยงานเดียวที่คุณรอผลอยู่ คุณสามารถเข้าไปแจ้งขอหนังสือขอความอนุเคราะห์เพื่อ
นำไปสมัครที่หน่วยงานอื่นได้แล้ว

-ทีมงานเว็บระบบฝึกงาน""" % (
            student.full_name(),
            company_request.company_name))
    
    send_mail_to_student(student, subject, message)


def send_company_request_result_reset(student, company_request):
    subject = u'แจ้งการยกเลิกการบันทึกผลการตอบรับจากหน่วยงาน'
    
    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่ากรรมการได้ยกเลิกการบันทึกผลการตอบรับหรือปฏิเสธจากหน่วยงาน

%s

แล้ว  ทั้งนี้เนื่องจากยังไม่ได้มีการยืนยันผลหรือมีความผิดพลาดในการบันทึกครั้งก่อน

อย่างไรก็ตาม ถ้าคุณได้รับผลการตอบรับหรือปฏิเสธแล้ว และคิดว่าการยกเลิกการบันทึกผลนี้ผิดพลาด
กรุณารีบติดต่อกรรมการฝึกงานประจำสาขาของคุณโดยด่วน

-ทีมงานเว็บระบบฝึกงาน""" % (
            student.full_name(),
            company_request.company_name))
    
    send_mail_to_student(student, subject, message)


def send_company_request_result_reject(student, company_request):
    subject = u'แจ้งการแก้ผลการตอบรับจากหน่วยงานเป็นปฏิเสธ'
    
    message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่ากรรมการได้แก้การบันทึกผลการตอบรับเป็นการปฏิเสธจากหน่วยงาน

%s

แล้ว

อย่างไรก็ตาม ถ้าคุณคิดว่าการยกเลิกการบันทึกผลนี้ผิดพลาด
กรุณารีบติดต่อกรรมการฝึกงานประจำสาขาของคุณโดยด่วน

-ทีมงานเว็บระบบฝึกงาน""" % (
            student.full_name(),
            company_request.company_name))
    
    send_mail_to_student(student, subject, message)


def send_regtype_conversion_decision(student, is_converted):
    if is_converted:
        subject = u'แจ้งการเปลี่ยนประเภทการจัดหาหน่วยงาน'
    
        message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อยืนยันว่าคุณได้เปลี่ยนประเภทการจัดหาหน่วยงานจากประเภทจัดหาหน่วยงานเอง
ไปเป็นประเภทที่ให้คณะจัดหาหน่วยงานให้แล้ว

กรุณารอฟังประกาศเกี่ยวกับการเลือกหน่วยงานจากรายการของคณะต่อไป

-ทีมงานเว็บระบบฝึกงาน""" % (student.full_name(),))
    else:
        subject = u'แจ้งการยืนยันไม่เปลี่ยนประเภทการจัดหาหน่วยงาน'
    
        message = (u"""เรียน %s

จดหมายฉบับนี้ส่งมาเพื่อแจ้งว่าคุณได้ยืนยันที่จะจัดหาหน่วยงานสำหรับการฝึกงานเอง
ในการยืนยันดังกล่าว คุณได้รับทราบว่าคุณจะต้องรับผิดชอบหาหน่วยงานสำหรับการฝึกงานเอง
และถ้าคุณไม่สามารถหาหน่วยงานได้ภายในกำหนด คุณจะต้องไปฝึกงานในปีการศึกษาหน้า

-ทีมงานเว็บระบบฝึกงาน""" % (student.full_name(),))
            
    
    send_mail_to_student(student, subject, message)


def send_company_direct_application_result(student, application, result):
    subject = u'แจ้งผลการยืนยันผลการคัดเลือกหน่วยงานคัดเลือกนิสิตเอง'

    message = (u"""เรียน %s

จากที่คุณได้แจ้งข้อมูลผลการคัดเลือกของหน่วยงาน:

%s

ที่มีการคัดเลือกนิสิตเอง ว่าได้รับนิสิตเข้าฝึกงาน และนิสิตก็ประสงค์เข้าฝึกงานที่หน่วยงานดังกล่าว
พร้อมด้วยหลักฐานผลการคัดเลือก

""" % (student.full_name(),
       application.company_name))

    if result:
        message += (u"""จดหมายฉบับนี้ส่งมาเพื่อแจ้งว่า กรรมการได้ยืนยันผลการคัดเลือกดังกล่าวแล้ว

-ทีมงานเว็บระบบฝึกงาน""")
    else:
        message += (u"""จดหมายฉบับนี้ส่งมาเพื่อแจ้งว่า กรรมการได้ปฏิเสธที่จะยืนยันผลการคัดเลือกดังกล่าว
ให้นิสิตติดต่อกับกรรมการโดยตรง เพื่อตรวจสอบข้อมูลและสอบถามการดำเนินการต่าง ๆ ต่อไป

-ทีมงานเว็บระบบฝึกงาน""")
    
    send_mail_to_student(student, subject, message)


