# -*- coding: utf-8 -*-
import datetime

STUDENT_ACTION_APPLY  = 1
STUDENT_ACTION_ON_JOB = 2
STUDENT_ACTION_DONE   = 3

LAST_EXTRA_ACADEMIC_MONTH = 7    # last month is March
LAST_WORK_TRAINING_MONTH  = 7    # students on work training until end of May
LAST_WORK_TRAINING_INFO_MONTH  = 8    # info for students last up to July

REQUIRED_CREDITS_FOR_APPLICATION = 60
MAX_DIRECT_APPLICATION_WAIT = 2

NUM_ROUNDS_BEFORE_RECEIPT_EXPIRED = 2
#NUM_ROUNDS_BEFORE_RECEIPT_EXPIRED = 100

STARTING_CHARACTERS = u'กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรฤลฦวศษสหฬอฮABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

SELF_APPL_START_YEAR = 2557

def to_BC_year(y):
    return y + 543

def get_current_year():
    date = datetime.date.today()
    return to_BC_year(date.year)

def get_current_academic_year():
    """
    returns the current academic year in B.C.
    """
    date = datetime.date.today()

    if date.year == 2012:
        if date.month <= 4:
            return to_BC_year(date.year - 1)
        else:
            return to_BC_year(date.year)

    if date.month <= LAST_EXTRA_ACADEMIC_MONTH:   # count academic year up to March
        return to_BC_year(date.year - 1)
    else:
        return to_BC_year(date.year)

def get_next_worktraining_year():
    return get_current_academic_year() + 1

def get_recent_worktraining_year():
    return get_current_academic_year()

def get_current_information_year():
    date = datetime.date.today()

    if date.month <= LAST_WORK_TRAINING_INFO_MONTH:   # count academic year up to March
        return to_BC_year(date.year)
    else:
        return to_BC_year(date.year+1)


def get_student_default_action(student):
    y = get_current_academic_year() - student.get_enrollment_year()

    if y<=3:
        return STUDENT_ACTION_APPLY

    if y>4:
        return STUDENT_ACTION_DONE

    date = datetime.date.today()
    
    if date.month <= LAST_WORK_TRAINING_MONTH:
        return STUDENT_ACTION_ON_JOB
    else:
        return STUDENT_ACTION_DONE


def can_student_apply_more_direct_application_with_reason(student, 
                                                          year):

    if year == None:
        year = get_next_worktraining_year()

    if not student.has_registered(year):
        return (False,u'นิสิตไม่ได้สมัครฝึกงานในภาคการศึกษาดังกล่าว')
 
    from direct_appl.models import StudentApplication

    applications = StudentApplication.all_for_student(student, year)

    wait_count = 0
    for app in applications:
        if app.is_accepted:
            return (False,u'นิสิตได้รับการตอบรับจากบางสถานประกอบการแล้ว')
        if app.is_accepted == None:
            wait_count += 1

    if wait_count < MAX_DIRECT_APPLICATION_WAIT:
        return (True,'')
    else:
        return (False,u'นิสิตสมัครฝึกงานโดยตรงครบตามจำนวนแล้ว')

    
def can_student_apply_more_direct_application(student, 
                                              year):
    res = can_student_apply_more_direct_application_with_reason(student, year)
    return res[0]

def can_student_apply_in_selection_round_with_reason(student,
                                                     year=None):
    if year == None:
        year = get_next_worktraining_year()

    if not student.department:
        return (False,u'นิสิตยังไม่ได้เลือกสังกัดสาขาวิชา')
 
    if not student.has_registered(year):
        return (False,u'นิสิตไม่ได้สมัครฝึกงานในภาคการศึกษาดังกล่าว')
 
    if student.assignment_for_year(year):
        return (False,u'นิสิตได้สถานประกอบการแล้ว')

    from direct_appl.models import StudentApplication

    applications = StudentApplication.all_for_student(student, year, False)

    wait_count = 0
    for app in applications:
        if not app.is_rejected():
            return (False,u'นิสิตสมัครฝึกงานกับสถานประกอบการไว้และยังไม่ทราบผล')
    
    return (True,u'')

def can_student_apply_in_selection_round(student, year):
    res = can_student_apply_in_selection_round_with_reason(student, year)
    return res[0]

    
def can_convert_student_reg_type(student, department):
    forbidden_departments = set([u'วิศวกรรมไฟฟ้า',u'วิศวกรรมวัสดุ'])

    return department.name not in forbidden_departments

def get_self_appl_start_year():
    return SELF_APPL_START_YEAR
