from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.http import HttpResponseRedirect
from django.views.decorators.cache import never_cache
from django.contrib.auth.views import login as auth_views_login
from django.contrib.auth import login as login_user, authenticate
from django.contrib.auth.models import User
from django.shortcuts import redirect
from commons.models import Log

from std.models import StudentTokenNonce

def login(request, template_name='registration/login.html', redirect_field_name=REDIRECT_FIELD_NAME):
    if settings.HTTPS_LOGIN and (not request.is_secure()):
        new_url = "https://%s%s" % (request.get_host(), request.get_full_path())
        return HttpResponseRedirect(new_url)
    else:
        if request.method=='GET':
            return auth_views_login(request, template_name, redirect_field_name)
        else:
            result = auth_views_login(request, template_name, redirect_field_name)
            if isinstance(result,HttpResponseRedirect):
                # login successful
                if settings.HTTPS_LOGIN:
                    result['Location'] = 'https://%s%s' % (request.get_host(), result['Location'])
                else:
                    result['Location'] = 'http://%s%s' % (request.get_host(), result['Location'])
                
                user = User.objects.get(username=request.POST['username'])
            return result

login = never_cache(login)

def login_user_with_token(request):
    token = request.GET.get('token','')
    role = request.GET.get('role','')
    
    user = authenticate(token=token)
    if user is not None:
        Log.create("User Token Login - from: %s, user: %s" %
                   (request.META['REMOTE_ADDR'],
                    user.username))
        login_user(request, user)
        if role == 'regis':
            return redirect('regis_index')
        else:
            return redirect('committee_index')

    return redirect('admin_login')


