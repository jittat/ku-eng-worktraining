# _*_ coding: utf-8 _*_
from django.shortcuts import render_to_response
from django.template import RequestContext

from regis.models import Announcement
from commons.academia import STARTING_CHARACTERS
import datetime

NUMBER_OF_TOP_ANNOUNCEMENTS = 10

def index(request):
    last_year = datetime.date.today() - datetime.timedelta(days=400)
    all_announcements = Announcement.objects.filter(created_at__gt=last_year).all()
    announcements = [a for a in all_announcements
                     if not a.is_in_download_section]
    download_announcements = [a for a in all_announcements
                              if a.is_in_download_section][0:NUMBER_OF_TOP_ANNOUNCEMENTS]
    top_announcements = announcements[0:NUMBER_OF_TOP_ANNOUNCEMENTS]
    old_announcements = announcements[NUMBER_OF_TOP_ANNOUNCEMENTS:]
    
    is_student_logged_in = 'std_id' in request.session
    return render_to_response("commons/front.html", 
                              { 'top_announcements': top_announcements,
                                'old_announcements': old_announcements,
                                'download_announcements': download_announcements,
                                'is_student_logged_in': is_student_logged_in,
                                'characters': STARTING_CHARACTERS },
                              context_instance=RequestContext(request))

