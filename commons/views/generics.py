# -*- coding: utf-8 -*-
from django.http import HttpResponse , HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django import forms
from django.template import RequestContext
import json

from regis.models import Company
from regis.decorators import ajax_login_required
from committee.models import CompanySelection
from commons.decorators import admin_or_committee_required
from commons.academia import get_next_worktraining_year
from commons.utils import extract_notice
from std.models import VerifiedDirectApplicationCompany

class CompanyAllSearchForm(forms.Form):
    company_short_name = forms.CharField(label=u'ชื่อย่อ',required=False)
    company_name = forms.CharField(label=u'ชื่อสถานประกอบการ',required=False)

def mark_selected_companies(companies, department):
    cids = set([sel.company_id 
                for sel 
                in CompanySelection.objects.filter(department=department,
                                                   year=get_next_worktraining_year())])
    for c in companies:
        c.selected = c.id in cids

@admin_or_committee_required
def company_search(request, is_committee=False, is_admin=False, view_only=False, includes_self_appl=False):

    if (not is_committee) and (not is_admin):
        # it must be called from one of these
        return HttpResponseForbidden()

    if is_committee:
        if not request.user.userprofile.is_committee_member:
            return HttpResponseForbidden()

        department = request.committee_department
        currentpage = 'company_list_selection'
        can_view = view_only
        can_edit = False
        can_select = not view_only
        can_search_self_appl = False
        include_self_appl_checked = False
    else:
        department = None
        currentpage = 'company'
        can_view = True
        can_edit = True
        can_select = False
        can_search_self_appl = includes_self_appl
        include_self_appl_checked = False

    notice = extract_notice(request.session)
    search_result_notice = ''

    if request.method=='POST':
        form = CompanyAllSearchForm(request.POST)
        include_self_appl_checked = 'includes_self_appl' in request.POST
        if form.is_valid():
            company_short_name = form.cleaned_data['company_short_name']
            company_name = form.cleaned_data['company_name']

            companies = Company.objects.all().order_by('short_name')
            if len(company_short_name) != 0:
                companies = companies.filter(short_name=company_short_name).all()
            if len(company_name) != 0:
                exact_companies =companies.filter(name=company_name).all()
                if exact_companies.count() == 0:
                    companies = companies.filter(name__contains=company_name).all()
                else:
                    companies = exact_companies

            company_count = companies.count()

            if (is_admin) and (company_count == 1):
                # unique result, redirected to company page
                return HttpResponseRedirect(reverse('regis_company_detail',kwargs={'company_id':companies[0].id}))
            elif company_count != 0:
                companies = companies[:100]

                verified_companies = {}
                if is_committee:
                    mark_selected_companies(companies, department)
                    verified_companies = VerifiedDirectApplicationCompany.get_dict_for_years(get_next_worktraining_year())

                return render_to_response('commons/generics/company_search/index.html',
                              { 'form': form,
                                'companies':companies,
                                'verified_companies': verified_companies,
                                'company_count': company_count,
                                'notice': notice,
                                'search_result_notice': search_result_notice,
                                'is_committee': is_committee,
                                'is_admin': is_admin,
                                'can_view': can_view,
                                'can_edit': can_edit,
                                'can_select': can_select,
                                'can_search_self_appl': can_search_self_appl,
                                'include_self_appl_checked': include_self_appl_checked,
                                'department': department,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))
            else:
                search_result_notice = 'ไม่พบข้อมูลสถานประกอบการ'
                form = CompanyAllSearchForm()
    else:
        form = CompanyAllSearchForm()

    return render_to_response('commons/generics/company_search/index.html',
                              { 'form': form,
                                'notice': notice,
                                'search_result_notice': search_result_notice,
                                'is_committee': is_committee,
                                'is_admin': is_admin,
                                'can_view': can_view,
                                'can_edit': can_edit,
                                'can_select': can_select,
                                'can_search_self_appl': can_search_self_appl,
                                'include_self_appl_checked': include_self_appl_checked,
                                'department': department,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))

# THESE ARE AJAX CALLS

@ajax_login_required
@admin_or_committee_required
def search_by_name_ajax(request):
    company_name = request.GET['term']
    companies = Company.all_containing_name(company_name)

    companies = companies[:20]  # take only the first 20

    return HttpResponse(json.dumps([c.name for c in companies]))
