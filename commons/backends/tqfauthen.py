from django.conf import settings
from django.contrib.auth.models import User, check_password
from django.db import models
from commons.models import UserProfile

class WebBackend:
    """Authenticate using Web portal for tqf
    """
    def check_password(self, username, password):
        import requests
        username = username.split('@')[0]
        try:
            result = requests.post('https://tqf.cpe.ku.ac.th/authen/',
                                   {'login': username,
                                    'password': password})
            if result:
                data = result.text.strip()
                return data[:2] == 'OK'
            else:
                return False
        except:
            return False

    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            # Since user has to be added to section before using
            # any functionality, we do not create new IMAP user on
            # the fly
            return None 

        try:
            profile = user.userprofile
            if profile.nontri_username:
                username = profile.nontri_username
        except UserProfile.DoesNotExist:
            pass

        if self.check_password(username, password):
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

