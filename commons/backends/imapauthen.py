#
# This file is slightly modified from
# WebPyMail - IMAP python/django web mail client 
# by Helder Guerreiro <helder@paxjulia.com> 
#
# WebPyMail can be found at: http://code.google.com/p/webpymail
# 

## As the original source is licensed under GNU GPL 3, this file and
## the project should be licensed under the same term.
## See <http://www.gnu.org/licenses/>.

import imaplib

from django.conf import settings
from django.contrib.auth.models import User, check_password
from django.db import models
from commons.models import UserProfile

class ImapBackend:
    """Authenticate using IMAP
    """
    def check_imap_password(self, username, password):
        try:
            host = settings.IMAP_SERVER
            port = settings.IMAP_PORT

            if host==None or host=='':    # IMAP authen disabled
                return None

            M = imaplib.IMAP4_SSL(host, port)  # always use SSL
            M.login(username, password)
            M.logout()
            return True
        except:
            return False

    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            # Since user has to be added to section before using
            # any functionality, we do not create new IMAP user on
            # the fly
            return None 

        try:
            profile = user.userprofile
            if profile.nontri_username:
                username = profile.nontri_username
        except UserProfile.DoesNotExist:
            pass

        if self.check_imap_password(username, password):
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
