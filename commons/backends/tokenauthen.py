from django.conf import settings
from django.contrib.auth.models import User, check_password
from django.db import models
from commons.models import UserProfile
from std.models import StudentTokenNonce

class TokenBackend:
    """Authenticate using token
    """
    def authenticate(self, token=None):
        token_items = token.split('_')
        if len(token_items) != 3:
            return None

        username, nonce, token_hash = token_items

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None 

        if StudentTokenNonce.check_and_save(user.username, nonce):
            if StudentTokenNonce.check_token_hash(user.username,
                                                  nonce,
                                                  token_hash):
                return user

        return None
    
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
