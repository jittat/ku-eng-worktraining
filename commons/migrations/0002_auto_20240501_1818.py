# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('commons', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='configuration',
            options={'ordering': ['rank']},
        ),
        migrations.AddField(
            model_name='configuration',
            name='rank',
            field=models.IntegerField(default=0),
        ),
    ]
