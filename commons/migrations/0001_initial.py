# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0002_auto_20231108_0451'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('value', models.CharField(max_length=300, blank=True)),
                ('value_type', models.CharField(max_length=20, choices=[(b'date', b'date'), (b'datetime', b'datetime'), (b'integer', b'integer'), (b'string', b'string')])),
                ('help_text', models.CharField(max_length=500, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.CharField(max_length=20, blank=True)),
                ('message', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('student', models.ForeignKey(blank=True, to='regis.Student', null=True)),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_sa_admin', models.BooleanField(default=False, verbose_name='\u0e40\u0e1b\u0e47\u0e19\u0e40\u0e08\u0e49\u0e32\u0e2b\u0e19\u0e49\u0e32\u0e17\u0e35\u0e48\u0e2b\u0e19\u0e48\u0e27\u0e22\u0e01\u0e34\u0e08\u0e01\u0e32\u0e23\u0e19\u0e34\u0e2a\u0e34\u0e15')),
                ('is_committee_member', models.BooleanField(default=False, verbose_name='\u0e40\u0e1b\u0e47\u0e19\u0e01\u0e23\u0e23\u0e21\u0e01\u0e32\u0e23\u0e01\u0e34\u0e08\u0e01\u0e32\u0e23\u0e19\u0e34\u0e2a\u0e34\u0e15')),
                ('is_accepting_emails', models.BooleanField(default=False, verbose_name='\u0e23\u0e31\u0e1a\u0e40\u0e21\u0e25\u0e4c\u0e41\u0e08\u0e49\u0e07\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25')),
                ('nontri_username', models.CharField(max_length=30, null=True, verbose_name='\u0e1a\u0e31\u0e0d\u0e0a\u0e35\u0e19\u0e19\u0e17\u0e23\u0e35\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e22\u0e37\u0e19\u0e22\u0e31\u0e19\u0e15\u0e31\u0e27\u0e15\u0e19', blank=True)),
                ('is_department_admin', models.BooleanField(default=False, verbose_name='\u0e40\u0e1b\u0e47\u0e19\u0e40\u0e08\u0e49\u0e32\u0e2b\u0e19\u0e49\u0e32\u0e17\u0e35\u0e48\u0e20\u0e32\u0e04\u0e27\u0e34\u0e0a\u0e32')),
                ('department', models.ForeignKey(blank=True, to='regis.Department', null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
