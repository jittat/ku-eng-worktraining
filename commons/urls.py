from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'commons.views.generics',
    url(r'^search_name_ajax/$',
        'search_by_name_ajax',
        name='company_search_by_name_ajax'),
)

