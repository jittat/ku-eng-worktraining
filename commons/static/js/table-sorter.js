var tableSorter = {
    renumberTable: function(table) {
        var rows = $(table).find('tr').slice(1);
        
        var c = 0;
        jQuery.each(rows, function(i, r) {
            $(r).find('td').first().text(c+1);
            c++;
            $(r).removeClass('odd').removeClass('even');
            if(c % 2 == 0)
                $(r).addClass('even');
            else
                $(r).addClass('odd');
        });
    },

    sortTable: function() {
        var th = $(this).parents('th').first();
        var col = $(th).prevAll('th').length;
        var table = $(this).parents('table').first();
        var rows = $(table).find('tr').slice(1);
        
        data = [];
        jQuery.each(rows, function(i, r) {
            var k = $($(r).find('td')[col]).text();
            data.push({key: k, row: r});
            $(r).detach();
        });
        data.sort(function(a,b) { 
            if(a.key < b.key)
                return -1;
            else if(a.key > b.key)
                return 1;
            else
                return 0;
        });
        jQuery.each(data,function(i, d) {
            table.append(d.row);
        });
        tableSorter.renumberTable(table);  
    }
};


