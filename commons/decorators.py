from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.conf import settings
from commons.models import UserProfile

def profile_committee_check(profile):
    return profile.is_committee_member

def profile_sa_admin_check(profile):
    return profile.is_sa_admin

def profile_admin_or_committee(profile):
    return profile.is_committee_member or profile.is_sa_admin

def profile_admin_or_committee_or_department_admin(profile):
    return (profile.is_committee_member or 
            profile.is_sa_admin or
            profile.is_department_admin)

def profile_committee_or_department_admin(profile):
    return (profile.is_committee_member or 
            profile.is_department_admin)

def user_check_required_impl(profile_check_function):
    """
    Returns a view function that checks if the request user profile
    passes profile_check_function.

    """
    def f(view_function):

        def decorate(request, *args, **kwargs):
            try:
                profile = request.user.userprofile
            except UserProfile.DoesNotExist:
                profile = UserProfile(user=request.user)
                profile.save()

            if (not profile) or (not profile_check_function(profile)):
                return HttpResponseForbidden(
                    render_to_response("commons/forbidden.html",
                                       { 'profile': profile },
                                       context_instance=RequestContext(request))
                    )
            request.profile = profile
            request.committee_department = profile.department
            return view_function(request, *args, **kwargs)

        return decorate

    return f

def committee_required(view_function):
    @login_required
    @user_check_required_impl(profile_committee_or_department_admin)
    def f(request, *args, **kwargs):
        if request.profile.is_department_admin:
            return redirect(settings.DEPARTMENT_ADMIN_INDEX)
        else:
            return view_function(request, *args, **kwargs)

    return f

def sa_admin_required(view_function):
    @login_required
    @user_check_required_impl(profile_sa_admin_check)
    def f(request, *args, **kwargs):
        return view_function(request, *args, **kwargs)

    return f

def admin_or_committee_required(view_function):
    @login_required
    @user_check_required_impl(profile_admin_or_committee_or_department_admin)
    def f(request, *args, **kwargs):
        if request.profile.is_department_admin:
            return redirect(settings.DEPARTMENT_ADMIN_INDEX)
        else:
            return view_function(request, *args, **kwargs)

    return f

def department_admin_required(view_function):
    @login_required
    @user_check_required_impl(profile_admin_or_committee_or_department_admin)
    def f(request, *args, **kwargs):
        return view_function(request, *args, **kwargs)

    return f

