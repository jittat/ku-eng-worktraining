from django.contrib import admin

from models import UserProfile, Log

class LogAdmin(admin.ModelAdmin):
    readonly_fields = ['user', 'message', 'student']
    search_fields = ['message']

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['__unicode__','department','is_accepting_emails']
    list_editable = ['is_accepting_emails']

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Log, LogAdmin)
