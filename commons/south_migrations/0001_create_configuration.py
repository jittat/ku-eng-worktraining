# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Configuration'
        db.create_table('commons_configuration', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
            ('value_type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('help_text', self.gf('django.db.models.fields.CharField')(max_length=500, blank=True)),
        ))
        db.send_create_signal('commons', ['Configuration'])


    def backwards(self, orm):
        
        # Deleting model 'Configuration'
        db.delete_table('commons_configuration')


    models = {
        'commons.configuration': {
            'Meta': {'object_name': 'Configuration'},
            'help_text': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'value_type': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['commons']
