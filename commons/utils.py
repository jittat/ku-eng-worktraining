# -*- coding: utf-8 -*-
from django.conf import settings

def admin_email():
    admin = settings.ADMINS[0]
    return admin[1]

def extract_notice(session):
    if 'notice' in session:
        n = session['notice']
        del session['notice']
        return n
    else:
        return None

def to_thai_date(d):
    if not d:
        return ''
    monthst = { 1: u'มกราคม', 2: u'กุมภาพันธ์', 3: u'มีนาคม', 4: u'เมษายน',
                5: u'พฤษภาคม', 6: u'มิถุนายน', 7: u'กรกฎาคม', 8: u'สิงหาคม',
                9: u'กันยายน', 10: u'ตุลาคม', 11: u'พฤศจิกายน', 12: u'ธันวาคม'}
    return u'%d %s %d' % (d.day, monthst[d.month], d.year + 543)

def to_eng_date(d):
    if not d:
        return ''
    return unicode(d.strftime('%B %-d, %Y'))

def now_within_config_deadline(config_name):
    from datetime import datetime, timedelta, date 
    from commons.models import Configuration, convert_config_str_to_date

    config_date = Configuration.get(config_name)
    if config_date:
        try:
            if (type(config_date) == unicode) or (type(config_date) == str):
                config_date = convert_config_str_to_date(config_date)
            return date.today() < config_date + timedelta(days=1)
        except:
            return True
    else:
        return True

def check_if_can_print_letter(student, status):
    if not settings.LETTER_DOWNLOADABLE:
        return False
    
    if not settings.QUIZ_REQUIRED_BEFORE_LETTER_DOWNLOAD:
        return True
    else:
        if status:
            return status.has_passed_quiz_exam
        else:
            return False

import csv, codecs, cStringIO

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


