from django.template import Library

register = Library()

@register.inclusion_tag('regis/tags/thai_full_date.html')
def thai_full_date(date):
    year = date.year + 543
    return { 'date': date,
             'year': year }

@register.inclusion_tag('regis/tags/thai_short_date.html')
def thai_short_date(date):
    year = (date.year + 543)%2500
    return { 'date': date,
             'year': year }
