from django.template import Library
from django.conf import settings

register = Library()

def quiz_url():
    """
    Returns the string contained in the setting MEDIA_URL.
    """
    return settings.QUIZ_URL

quiz_url = register.simple_tag(quiz_url)
