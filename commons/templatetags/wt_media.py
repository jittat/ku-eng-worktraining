from django.template import Library
from django.conf import settings

register = Library()

def media_url():
    """
    Returns the string contained in the setting MEDIA_URL.
    """
    return settings.MEDIA_URL
media_url = register.simple_tag(media_url)

def admin_media_prefix():
    """
    Returns the string contained in the setting ADMIN_MEDIA_PREFIX.
    """
    return settings.ADMIN_MEDIA_PREFIX
admin_media_prefix = register.simple_tag(admin_media_prefix)
