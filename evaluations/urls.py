from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'evaluations.views',
    
    url(r'^assignment/(\d+)/$', 'show_by_assignment', 
        name='evaluations_show_by_assignment'),
    url(r'^assignment/(\d+)/edit/$', 'edit_by_assignment', 
        name='evaluations_edit_by_assignment'),

    url(r'^search/$', 'search',
        name='evaluations_search'),
    url(r'^assignment/(\d+)/submission/$', 'submission',
        name='evaluations_submission'),
    url(r'^list/$', 'list',
        name='evaluations_list'),

    url(r'^generate/(\d+)/$', 'generate_evaluation_form_for_company',
        name='evaluations_generate'),
    
    url(r'^student/(\d+)/(.+)/$', 'edit_by_company',
        name='evaluations_edit_by_company'),

    url(r'^e/$', 'search_evaluation_form',
        name='evaluations_search_evaluation_form'),
)
