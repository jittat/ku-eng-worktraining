# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Changing field 'Evaluation.assignment'
        db.alter_column('evaluations_evaluation', 'assignment_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['regis.Assignment'], unique=True))

        # Adding unique constraint on 'Evaluation', fields ['assignment']
        db.create_unique('evaluations_evaluation', ['assignment_id'])
    
    
    def backwards(self, orm):
        
        # Changing field 'Evaluation.assignment'
        db.alter_column('evaluations_evaluation', 'assignment_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regis.Assignment']))

        # Removing unique constraint on 'Evaluation', fields ['assignment']
        db.delete_unique('evaluations_evaluation', ['assignment_id'])
    
    
    models = {
        'evaluations.evaluation': {
            'Meta': {'object_name': 'Evaluation'},
            'assignment': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['regis.Assignment']", 'unique': 'True'}),
            'beginning_date': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_absent_leave_days': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'num_heath_leave_days': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'num_late_days': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'num_personal_leave_days': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'num_working_days': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'received_at': ('django.db.models.fields.DateTimeField', [], {}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.assignment': {
            'Meta': {'object_name': 'Assignment'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        },
        'regis.student': {
            'Meta': {'object_name': 'Student'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']", 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_companies': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']", 'through': "orm['regis.Assignment']", 'symmetrical': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'prefix': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '11', 'null': 'True'})
        }
    }
    
    complete_apps = ['evaluations']
