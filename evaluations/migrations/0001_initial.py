# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('received_at', models.DateField(verbose_name='\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a\u0e40\u0e21\u0e37\u0e48\u0e2d')),
                ('number', models.IntegerField(default=0, verbose_name='\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e23\u0e31\u0e1a')),
                ('evaluation_form_received', models.BooleanField(verbose_name='\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a\u0e41\u0e1a\u0e1a\u0e1b\u0e23\u0e30\u0e40\u0e21\u0e34\u0e19\u0e41\u0e25\u0e49\u0e27')),
                ('beginning_date', models.DateField(null=True, verbose_name='\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e40\u0e23\u0e34\u0e48\u0e21\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19', blank=True)),
                ('end_date', models.DateField(null=True, verbose_name='\u0e27\u0e31\u0e19\u0e2a\u0e34\u0e49\u0e19\u0e2a\u0e38\u0e14\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19', blank=True)),
                ('num_working_days', models.IntegerField(null=True, verbose_name='\u0e40\u0e27\u0e25\u0e32\u0e17\u0e33\u0e07\u0e32\u0e19\u0e23\u0e27\u0e21', blank=True)),
                ('num_late_days', models.IntegerField(null=True, verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e21\u0e32\u0e2a\u0e32\u0e22', blank=True)),
                ('num_personal_leave_days', models.IntegerField(null=True, verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e25\u0e32\u0e01\u0e34\u0e08', blank=True)),
                ('num_heath_leave_days', models.IntegerField(null=True, verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e25\u0e32\u0e1b\u0e48\u0e27\u0e22', blank=True)),
                ('num_absent_leave_days', models.IntegerField(null=True, verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e02\u0e32\u0e14\u0e07\u0e32\u0e19', blank=True)),
                ('timesheet_received', models.BooleanField(verbose_name='\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a\u0e43\u0e1a\u0e1a\u0e31\u0e19\u0e17\u0e36\u0e01\u0e40\u0e27\u0e25\u0e32\u0e41\u0e25\u0e49\u0e27')),
                ('is_passed', models.NullBooleanField(verbose_name='\u0e1c\u0e48\u0e32\u0e19\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
                ('public_comments', models.TextField(default='', verbose_name='\u0e2b\u0e21\u0e32\u0e22\u0e40\u0e2b\u0e15\u0e38 (\u0e41\u0e2a\u0e14\u0e07\u0e01\u0e31\u0e1a\u0e19\u0e34\u0e2a\u0e34\u0e15)', blank=True)),
                ('internal_comments', models.TextField(default='', verbose_name='\u0e2b\u0e21\u0e32\u0e22\u0e40\u0e2b\u0e15\u0e38\u0e20\u0e32\u0e22\u0e43\u0e19 (\u0e44\u0e21\u0e48\u0e41\u0e2a\u0e14\u0e07\u0e01\u0e31\u0e1a\u0e19\u0e34\u0e2a\u0e34\u0e15)', blank=True)),
                ('scores', models.CharField(max_length=100, verbose_name='\u0e04\u0e30\u0e41\u0e19\u0e19\u0e01\u0e32\u0e23\u0e1b\u0e23\u0e30\u0e40\u0e21\u0e34\u0e19', blank=True)),
                ('total_score', models.FloatField(verbose_name='\u0e04\u0e30\u0e41\u0e19\u0e19\u0e23\u0e27\u0e21')),
                ('raw_total_score', models.IntegerField(verbose_name='\u0e04\u0e30\u0e41\u0e19\u0e19\u0e14\u0e34\u0e1a\u0e23\u0e27\u0e21')),
                ('report_score', models.FloatField(null=True, verbose_name='\u0e04\u0e30\u0e41\u0e19\u0e19\u0e23\u0e32\u0e22\u0e07\u0e32\u0e19')),
                ('is_report_missing', models.NullBooleanField(verbose_name='\u0e02\u0e32\u0e14\u0e2a\u0e48\u0e07\u0e23\u0e32\u0e22\u0e07\u0e32\u0e19')),
                ('grade', models.CharField(max_length=5, verbose_name='\u0e40\u0e01\u0e23\u0e14', blank=True)),
                ('is_evaluation_reported_by_company', models.BooleanField(default=False)),
                ('is_form_submitted_in_english', models.BooleanField(default=False)),
                ('evaluator_name', models.CharField(max_length=100, verbose_name='\u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e40\u0e21\u0e34\u0e19', blank=True)),
                ('evaluator_position', models.CharField(max_length=80, verbose_name='\u0e15\u0e33\u0e41\u0e2b\u0e19\u0e48\u0e07\u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e40\u0e21\u0e34\u0e19', blank=True)),
                ('evaluator_email', models.CharField(max_length=100, verbose_name='\u0e2d\u0e35\u0e40\u0e21\u0e25\u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e40\u0e21\u0e34\u0e19', blank=True)),
                ('evaluator_comments_json', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='EvaluationSecret',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(default=0)),
                ('secret', models.CharField(max_length=100)),
                ('easy_secret', models.CharField(max_length=10, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.CharField(max_length=50)),
            ],
        ),
    ]
