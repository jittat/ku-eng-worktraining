# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('evaluations', '0001_initial'),
        ('regis', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='evaluationsecret',
            name='assignment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, default=None, to='regis.Assignment', null=True),
        ),
        migrations.AddField(
            model_name='evaluationsecret',
            name='student',
            field=models.ForeignKey(related_name='evaluation_secrets', to='regis.Student'),
        ),
        migrations.AddField(
            model_name='evaluation',
            name='assignment',
            field=models.OneToOneField(to='regis.Assignment'),
        ),
        migrations.AddField(
            model_name='evaluation',
            name='student',
            field=models.ForeignKey(related_name='evaluations', to='regis.Student'),
        ),
    ]
