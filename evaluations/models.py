# -*- coding: utf-8 -*-
from django.db import models
import datetime

EVALUATION_SCORE_IN_GRADES = 45

class Evaluation(models.Model):
    student = models.ForeignKey("regis.Student",related_name='evaluations')
    assignment = models.OneToOneField("regis.Assignment")

    created_at = models.DateTimeField(auto_now_add=True)
    received_at = models.DateField(verbose_name=u"ได้รับเมื่อ")

    number = models.IntegerField(default=0,
                                 verbose_name=u'เลขที่รับ')

    # work statistics
    evaluation_form_received = models.BooleanField(verbose_name=u"ได้รับแบบประเมินแล้ว")

    beginning_date = models.DateField(blank=True,
                                      null=True,
                                      verbose_name=u"วันที่เริ่มฝึกงาน")
    end_date = models.DateField(blank=True,
                                null=True,
                                verbose_name=u"วันสิ้นสุดการฝึกงาน")

    num_working_days = models.IntegerField(blank=True,
                                           null=True,
                                           verbose_name=u"เวลาทำงานรวม")
    num_late_days = models.IntegerField(blank=True,
                                        null=True,
                                        verbose_name=u"จำนวนวันที่มาสาย")
    num_personal_leave_days = models.IntegerField(blank=True,
                                                  null=True,
                                                  verbose_name=u"จำนวนวันที่ลากิจ")
    num_heath_leave_days = models.IntegerField(blank=True,
                                               null=True,
                                               verbose_name=u"จำนวนวันที่ลาป่วย")
    num_absent_leave_days = models.IntegerField(blank=True,
                                                null=True,
                                                verbose_name=u"จำนวนวันที่ขาดงาน")

    timesheet_received = models.BooleanField(verbose_name=u"ได้รับใบบันทึกเวลาแล้ว")

    is_passed = models.NullBooleanField(verbose_name=u'ผ่านการฝึกงาน',blank=True)

    public_comments = models.TextField(blank=True,
                                       default=u'',
                                       verbose_name=u'หมายเหตุ (แสดงกับนิสิต)')
    internal_comments = models.TextField(blank=True,
                                         default=u'',
                                         verbose_name=u'หมายเหตุภายใน (ไม่แสดงกับนิสิต)')

    scores = models.CharField(max_length=100,
                              blank=True,
                              verbose_name=u'คะแนนการประเมิน')
    total_score = models.FloatField(verbose_name=u'คะแนนรวม')
    raw_total_score = models.IntegerField(verbose_name=u'คะแนนดิบรวม')
    report_score = models.FloatField(verbose_name=u'คะแนนรายงาน', null=True)
    is_report_missing = models.NullBooleanField(verbose_name=u'ขาดส่งรายงาน')
    
    grade = models.CharField(max_length=5,
                                   blank=True,
                                   verbose_name=u'เกรด')

    is_evaluation_reported_by_company = models.BooleanField(default=False)
    is_form_submitted_in_english = models.BooleanField(default=False)

    evaluator_name = models.CharField(max_length=100,
                                      blank=True,
                                      verbose_name=u'ผู้ประเมิน')
    evaluator_position = models.CharField(max_length=80,
                                          blank=True,
                                          verbose_name=u'ตำแหน่งผู้ประเมิน')
    evaluator_email = models.CharField(max_length=100,
                                       blank=True,
                                       verbose_name=u'อีเมลผู้ประเมิน')
    evaluator_comments_json = models.TextField(blank=True)
    
    def get_number(self):
        if self.number!=0:
            return str(self.number)
        else:
            return ''

    def clean_num_days(self):
        if not self.evaluation_form_received:
            return

        if not self.num_working_days:
            self.num_working_days = 0

        if not self.num_late_days:
            self.num_late_days = 0

        if not self.num_personal_leave_days:
            self.num_personal_leave_days = 0

        if not self.num_personal_leave_days:
            self.num_personal_leave_days = 0

        if not self.num_heath_leave_days:
            self.num_heath_leave_days = 0

        if not self.num_absent_leave_days:
            self.num_absent_leave_days = 0


    def save(self, *args, **kwargs):
        """
        automatically cleans None values for evaluation data if the
        form is received.
        """
        if self.evaluation_form_received:
            self.clean_num_days()
        super(Evaluation, self).save(*args, **kwargs)

    def is_basic_criteria_passed(self):
        self.clean_num_days()

        if self.is_report_missing:
            return False

        return ((self.num_working_days >= 30) and
                (self.num_late_days <= 5) and
                (self.num_personal_leave_days + self.num_heath_leave_days <= 5) and
                (self.num_absent_leave_days == 0))


    def auto_comments(self):
        if not self.evaluation_form_received:
            return u''

        self.clean_num_days()

        comments = u''
        if self.num_working_days < 30:
            comments = comments + (
                u'<p>จำนวนวันฝึกงานน้อยกว่า 30 วัน (%d วัน)</p>' % self.num_working_days)
        if self.num_late_days > 5:
            comments = comments + (
                u'<p>มาสายมากกว่า 5 วัน (%d วัน)</p>' % self.num_late_days)

        if self.num_personal_leave_days + self.num_heath_leave_days > 5:
            comments = comments + (
                u'<p>ลากิจและลาป่วยมากกว่า 5 วัน (%d วัน)</p>' % 
                (self.num_personal_leave_days + self.num_heath_leave_days))
            
        if self.num_absent_leave_days > 0:
            comments = comments + (
                u'<p>ขาดโดยไม่ลา (%d วัน)</p>' % self.num_absent_leave_days)
        
        return comments
    
    def is_department(self,department):
        return self.student.department_id == department.id

    def set_scores_from_list(self, scores, year):
        item_count = Evaluation.get_evaluation_item_count(year)
        items = scores
        if len(scores) < item_count:
            items += [0] * (item_count - len(scores))
        self.scores = ','.join([str(x) for x in items])
        self.raw_total_score = sum(items)
        self.total_score = float(self.raw_total_score*100) / (5*item_count)

    def get_scores_as_dict(self,year):
        if self.scores == None:
            self.scores = ''
        items = self.scores.split(',')
        sc = {}
        for i in range(Evaluation.get_evaluation_item_count(year)):
            if len(items) > i:
                try:
                    sc[i+1] = int(items[i])
                except:
                    sc[i+1] = 0
            else:
                sc[i+1] = 0
        return sc

    def is_score_incomplete(self,year):
        scores = self.get_scores_as_dict(year)
        for s in scores.values():
            if s != 0:
                return False
        return True
    
    def is_info_completed(self):
        return self.num_working_days >= 0

    def mark_info_incomplete(self):
        self.num_working_days = -1

    def auto_run_number(self):
        this_year = datetime.date(self.received_at.year,1,1)
        today_count = len(Evaluation.objects.filter(received_at__gte=this_year).all())
        self.number = today_count + 1

    def company_evaluation_grading_score(self):
        return self.total_score * float(EVALUATION_SCORE_IN_GRADES) / 100

    def working_day_score(self):
        if self.is_basic_criteria_passed():
            return 5.0
        else:
            return 0
    
    def working_day_score_updated(self):
        return self.working_day_score()*2

    def company_score_updated(self):
        return (self.raw_total_score + self.working_day_score_updated())*50.0/85.0
    
    def has_report_score(self):
        return self.report_score != None

    def grading_score(self):
        #return self.company_evaluation_grading_score() + self.working_day_score() + (self.report_score)
        # HACK for 2567
        
        return (self.company_score_updated()) + (self.report_score)
        
    def grading_score_display(self):
        if self.has_report_score():
            return '%.2f' % (self.grading_score())
        else:
            return ''

    def calculate_grade(self):
        if not self.is_basic_criteria_passed():
            return 'F'
        else:
            if self.report_score == 0:
                return 'F'
            score = self.grading_score()
            criterias = [(80,'A'),
                         (75,'B+'),
                         (70,'B'),
                         (65,'C+'),
                         (60,'C'),
                         (55,'D+'),
                         (50,'D')]
            for cr in criterias:
                if score + 0.00001 >= cr[0]:
                    return cr[1]
            return 'F'

    def update_grade(self):
        self.grade = self.calculate_grade()

    def extract_evaluator_comments(self):
        import json
        
        if self.evaluator_comments_json and self.evaluator_comments_json != '':
            return json.loads(self.evaluator_comments_json)
        else:
            return {}

    def set_evaluator_comments(self, comments):
        import json
        
        self.evaluator_comments_json = json.dumps(comments)
        
    @staticmethod
    def get_evaluation_item_count(year):
        if year >= 2563:
            return 15
        else:
            return 18

    @staticmethod
    def get_sections(year):
        if year >= 2563:
           return [ {'number': 1,
                      'title': 'ผลสำเร็จของงาน',
                      'eng_title': 'Work Achievement',
                      'items': [(1,'ปริมาณงานตามที่ได้รับมอบหมาย',
                                 'Quantity of work'),
                                (2,'คุณภาพงานที่ได้รับมอบหมาย',
                                 'Quality of work')]},
                     {'number': 2,
                      'title': 'ความรู้ความสามารถ',
                      'eng_title': 'Knowledge and Ability',
                      'items': [(3,'ความสามารถในการเรียนรู้และประยุกต์วิชาการ',
                                'Ability to learn and apply knowledge'),
                                (4,'ความรู้ความชำนาญด้านปฏิบัติการ',
                                 'Practical ability'),
                                (5,'วิจารณญาณและการตัดสินใจ',
                                 'Judgement and dicision making'),
                                (6,'การจัดการและวางแผน',
                                 'Organization and planning'),
                                (7,'ทักษะการสื่อสาร',
                                 'Communication skills'),
                                (8,'การพัฒนาด้านภาษาและวัฒนธรรมต่างประเทศ',
                                 'Foreign language and cultural development'),
                                (9,'ความเหมาะสมต่อตำแหน่งงานที่ได้รับมอบหมาย',
                                 'Suitability for job position')]},
                     {'number': 3,
                      'title': 'ความรับผิดชอบต่อหน้าที่',
                      'eng_title': 'Responsibility',
                      'items': [(10,'ความรับผิดชอบและเป็นผู้ที่ไว้วางใจได้',
                                 'Responsibility and dependabilty'),
                                (11,'ความสนใจ อุตสาหะในการทำงาน',
                                 'Interest in work'),
                                (12,'ความสามารถเริ่มต้นทำงานได้ด้วยตนเอง',
                                 'Initiative or self starter'),
                                (13,'การตอบสนองต่อการสั่งการ',
                                 'Response to supervision')]},
                     {'number': 4,
                      'title': 'ลักษณะส่วนบุคคล',
                      'eng_title': 'Personality',
                      'items': [(14,'มนุษยสัมพันธ์',
                                 'Interpersonal skills'),
                                (15,'ความมีระเบียบวินัย ปฏิบัติตามวัฒนธรรมขององค์กร',
                                 'Discipline and adaptability to formal organization'),]}]
        else:
           return [ {'number': 1,
                      'title': 'ผลสำเร็จของงาน',
                      'eng_title': 'Work Achievement',
                      'items': [(1,'ปริมาณงานตามที่ได้รับมอบหมาย',
                                 'Quantity of work'),
                                (2,'คุณภาพงานที่ได้รับมอบหมาย',
                                 'Quality of work')]},
                     {'number': 2,
                      'title': 'ความรู้ความสามารถ',
                      'eng_title': 'Knowledge and Ability',
                      'items': [(3,'มีความรู้ความสามารถทางวิชาการ',
                                 'Academic ability'),
                                (4,'ความสามารถในการเรียนรู้และประยุกต์วิชาการ',
                                'Ability to learn and apply knowledge'),
                                (5,'ความรู้ความชำนาญด้านปฏิบัติการ',
                                 'Practical ability'),
                                (6,'วิจารณญาณและการตัดสินใจ',
                                 'Judgement and dicision making'),
                                (7,'การจัดการและวางแผน',
                                 'Organization and planning'),
                                (8,'ทักษะการสื่อสาร',
                                 'Communication skills'),
                                (9,'การพัฒนาด้านภาษาและวัฒนธรรมต่างประเทศ',
                                 'Foreign language and cultural development'),
                                (10,'ความเหมาะสมต่อตำแหน่งงานที่ได้รับมอบหมาย',
                                 'Suitability for job position')]},
                     {'number': 3,
                      'title': 'ความรับผิดชอบต่อหน้าที่',
                      'eng_title': 'Responsibility',
                      'items': [(11,'ความรับผิดชอบและเป็นผู้ที่ไว้วางใจได้',
                                 'Responsibility and dependabilty'),
                                (12,'ความสนใจ อุตสาหะในการทำงาน',
                                 'Interest in work'),
                                (13,'ความสามารถเริ่มต้นทำงานได้ด้วยตนเอง',
                                 'Initiative or self starter'),
                                (14,'การตอบสนองต่อการสั่งการ',
                                 'Response to supervision')]},
                     {'number': 4,
                      'title': 'ลักษณะส่วนบุคคล',
                      'eng_title': 'Personality',
                      'items': [(15,'บุคลิกภาพและการวางตัว',
                                 'Personality'),
                                (16,'มนุษยสัมพันธ์',
                                 'Interpersonal skills'),
                                (17,'ความมีระเบียบวินัย ปฏิบัติตามวัฒนธรรมขององค์กร',
                                 'Discipline and adaptability to formal organization'),
                                (18,'คุณธรรมและจริยธรรม',
                                 'Ethics and morality')]}]
    
                 
class EvaluationSecret(models.Model):
    student = models.ForeignKey("regis.Student",related_name='evaluation_secrets')
    year = models.IntegerField(default=0)
    assignment = models.ForeignKey("regis.Assignment",
                                   default=None,
                                   null=True,
                                   on_delete=models.SET_NULL)
    secret = models.CharField(max_length=100)
    easy_secret = models.CharField(max_length=10, blank=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)

    @staticmethod
    def create_for(student, assignment=None, user=None, length=10, year=None):
        if year==None:
            year=assignment.year
        secret = EvaluationSecret(student=student,
                                  assignment=assignment,
                                  year=year)
        if user:
            secret.created_by = user.username

        secret.random_secret(length)
        secret.random_easy_secret()
        return secret

    @staticmethod
    def get_for(assignment):
        secrets = EvaluationSecret.objects.filter(assignment=assignment).all()

        if len(secrets)==0:
            return None
        else:
            return secrets[0]

    @staticmethod
    def get_for_student(student, year):
        secrets = EvaluationSecret.objects.filter(student=student,year=year).order_by('id').all()

        if len(secrets)==0:
            return None
        else:
            return secrets[0]

    @staticmethod
    def get_or_create_for(assignment, user=None):
        secret = EvaluationSecret.get_for(assignment)
        if secret:
            return secret
        else:
            secret = EvaluationSecret.create_for(assignment.student, assignment, user=user)
            secret.save()
            return secret

    @staticmethod
    def get_or_create_for_student(student, year, user=None):
        secret = EvaluationSecret.get_for_student(student, year)
        if secret:
            return secret
        else:
            secret = EvaluationSecret.create_for(student, year=year, user=user)
            secret.save()
            return secret

    def random_secret(self, length=8):
        from random import sample
        
        alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        self.secret = ''.join(sample(alpha,length))

    def random_easy_secret(self, length=6):
        from random import sample
        
        alpha = '0123456789'
        self.easy_secret = ''.join(sample(alpha,length))

