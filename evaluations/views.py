# -*- coding: utf-8 -*-
import datetime

from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django import forms
from django.contrib.admin import widgets

from commons.models import Configuration, Log
from commons.utils import extract_notice
from commons.decorators import sa_admin_required
from commons.academia import get_current_academic_year, get_current_year

from regis.models import Assignment, Student
from evaluations.models import Evaluation, EvaluationSecret

@sa_admin_required
def show_by_assignment(request, assignment_id):
    return HttpResponseRedirect(reverse("evaluations_edit_by_assignment",
                                        args=[assignment_id]))

class EvaluationForm(forms.ModelForm):
    public_comments = forms.CharField(required=False)
    internal_comments = forms.CharField(required=False)
    class Meta:
        model = Evaluation
        exclude = (
            'assignment',
            'student',
            'scores',
            'total_score',
            'raw_total_score',
            'report_score',
            'is_evaluation_reported_by_company',
            'evaluator_name',
            'evaluator_position',
        )

def combine_evaluation_sections_with_scores(sections, scores):
    for s in sections:
        new_items = []
        for item in s['items']:
            num = item[0]
            title = item[1]
            eng_title = item[2]
            if num in scores:
                score = scores[num]
            else:
                score = 0
            if score == 0:
                score = ''
            new_items.append((num, title, eng_title, score))
        s['items'] = new_items

def extract_evaluation_scores(post_data, item_count):
    scores = []
    for i in range(item_count):
        k = 'eval_score_' + str(i+1)
        if k in post_data:
            try:
                s = int(post_data[k])
            except:
                s = 0
        else:
            s = 0
        scores.append(s)
    return scores

@sa_admin_required
def edit_by_assignment(request, assignment_id):
    assignment = get_object_or_404(Assignment, pk=assignment_id)

    student = assignment.student
    company = assignment.company
    year = assignment.year
    try:
        evaluation = assignment.evaluation
        org_evaluator_comments_json = evaluation.evaluator_comments_json
    except Evaluation.DoesNotExist:
        evaluation = None
        org_evaluator_comments_json = None
        
    if request.method == 'POST':
        form = EvaluationForm(request.POST, instance=evaluation)
        if form.is_valid():
            evaluation = form.save(commit=False)
            evaluation.assignment = assignment
            evaluation.student = student
            evaluation.is_passed = evaluation.is_basic_criteria_passed()

            evaluation.set_scores_from_list(extract_evaluation_scores(request.POST,
                                                                      Evaluation.get_evaluation_item_count(year)),
                                            year)

            if org_evaluator_comments_json:
                evaluation.evaluator_comments_json = org_evaluator_comments_json

            evaluation.save()

            if company:
                return HttpResponseRedirect(reverse("regis_company_detail",
                                                    args=[company.id]))
            else:
                return HttpResponseRedirect(reverse("student_detail",
                                                    args=[student.student_id]))
    else:
        form = EvaluationForm(instance=evaluation)


    # various_form_defaults
    today_date = str(datetime.date.today())
    yesterday_date = str(datetime.date.today() - datetime.timedelta(1))
    default_beginning_date = str(Configuration.get('default.eval.beginning_date'))
    default_end_date = str(Configuration.get('default.eval.end_date'))

    evaluation_sections = Evaluation.get_sections(year)
    if evaluation:
        combine_evaluation_sections_with_scores(evaluation_sections, evaluation.get_scores_as_dict(year))

    if evaluation:
        evaluation_comments = evaluation.extract_evaluator_comments()
    else:
        evaluation_comments = {}

    return render_to_response("evaluations/edit.html",
                              { "evaluation": evaluation,
                                "evaluation_sections": evaluation_sections,
                                "evaluation_comments": evaluation_comments,
                                "student": student,
                                "assignment": assignment,
                                "company": company,
                                "year": year,
                                "form": form,
                                "today_date": today_date,
                                "yesterday_date": yesterday_date,
                                "default_beginning_date": default_beginning_date,
                                "default_end_date": default_end_date
                                },
                              context_instance=RequestContext(request))

@sa_admin_required
def search(request):
    search_error = None
    student = None
    student_id = ''
    assignment = None
    evaluation = None
    company_name = None
    notice = extract_notice(request.session)
    recent_evaluations = []
    
    year = get_current_year()

    if request.method == 'POST':
        if 'student_id' in request.POST:
            student_id = request.POST['student_id'].strip()
        else:
            student_id = ''
            
        if len(student_id) == 14:
            student_id = student_id[3:13]
        students = Student.objects.filter(student_id=student_id).all()
        if len(students) >= 1:
            student = students[0]
        else:
            search_error = u'ไม่พบข้อมูลนิสิต'

        if not search_error:
            assignment = student.assignment_for_year(year)
            evaluations = Evaluation.objects.filter(assignment=assignment).all()
            if len(evaluations) >= 1:
                evaluation = evaluations[0]
            else:
                evaluation = None
    else:
        today = datetime.date.today()
        recent_evaluations = Evaluation.objects.filter(received_at=today).order_by('-number').all()[:20]
            
    return render_to_response("evaluations/search.html",
                              { 'student_id': student_id,
                                'student': student,
                                'assignment': assignment,
                                'evaluation': evaluation,

                                'recent_evaluations': recent_evaluations,
                                'error': search_error,
                                'notice': notice },
                              context_instance=RequestContext(request))


@sa_admin_required
def submission(request, assignment_id):
    assignment = get_object_or_404(Assignment, pk=assignment_id)

    student = assignment.student
    company = assignment.company
    year = assignment.year
    try:
        evaluation = assignment.evaluation
    except Evaluation.DoesNotExist:
        evaluation = None

    if not evaluation:
        evaluation = Evaluation()
        evaluation.received_at = datetime.date.today()
        evaluation.set_scores_from_list([],year)
        evaluation.mark_info_incomplete()
        evaluation.auto_run_number()
        
    evaluation.assignment = assignment
    evaluation.student = student

    evaluation.evaluation_form_received = 'evaluation_form_received' in request.POST
    evaluation.timesheet_received = 'timesheet_received' in request.POST
    evaluation.save()

    request.session['notice'] = (u'จัดเก็บการได้รับแบบประเมินของ ' +
                                 student.student_id + ' ' +
                                 student.full_name() +
                                 u' เรียบร้อยแล้ว ('
                                 + evaluation.get_number() +
                                 ')')
    
    return HttpResponseRedirect(reverse("evaluations_search"))

@sa_admin_required
def list(request):
    today = datetime.date.today()
    yesterday = str(datetime.date.today() - datetime.timedelta(1))
    evaluations = Evaluation.objects.filter(received_at=today).order_by('number').all()
    recent_evaluations = Evaluation.objects.filter(received_at=yesterday).order_by('number').all()
            
    return render_to_response("evaluations/list.html",
                              { 'evaluations': evaluations,
                                'recent_evaluations': recent_evaluations },
                              context_instance=RequestContext(request))

def find_assignment_by_secret(student, secret):
    for ev_secret in student.evaluation_secrets.all():
        if ev_secret.secret == secret:
            if ev_secret.assignment:
                return ev_secret.assignment
            else:
                year = ev_secret.year
                assignments = student.assignments.filter(year=year)
                if len(assignments) != 0:
                    return assignments[0]
                else:
                    return None
    return None


def find_secret_by_easy_secret(student, secret):
    for ev_secret in student.evaluation_secrets.all():
        if ev_secret.easy_secret == secret:
            return ev_secret
    return None


@sa_admin_required
def generate_evaluation_form_for_company(request, assignment_id):
    assignment = get_object_or_404(Assignment, pk=assignment_id)
    student = assignment.student

    assignment_secret = None
    
    for ev_secret in student.evaluation_secrets.all():
        if ev_secret.assignment_id == assignment.id:
            assignment_secret = ev_secret
            break

    if not assignment_secret:
        assignment_secret = EvaluationSecret.create_for(student,
                                                        assignment,
                                                        request.user)
        assignment_secret.save()

    return HttpResponseRedirect(reverse("evaluations_edit_by_company",
                                        args=[
                                            student.student_id,
                                            assignment_secret.secret,
                                        ]))

COMPANY_COMMENT_FIELDS = [
    'comment_job_description',
    
    'comment_obstacle',
    'comment_usefulness',
    'comment_time',
    'comment_knowledge',
    'comment_additional_training',
    'comment_probation',
    'comment_requirements',
    'comment_extra',

    'comment_eng_suggestions',
    'comment_eng_support_available',
    'comment_eng_support_air_tickets',
    'comment_eng_support_living_expenses',
    'comment_eng_support_amount',
    'comment_eng_support_received',

    'comment_next_year_decision',
    'comment_next_year_acceptance_count',
    'comment_next_year_reject_extra',
    'comment_next_year_other_extra',
]

class CompanyEvaluationForm(forms.ModelForm):
    comment_job_description = forms.CharField(widget=forms.TextInput(attrs={'size': '60'}))
    
    comment_obstacle = forms.ChoiceField(choices=[(1,u'เป็นอุปสรรค'), (0,u'ไม่เป็นอุปสรรค')],
                                         required=False)
    comment_usefulness = forms.ChoiceField(choices=[(1,u'มีประโยชน์'), (0,u'ไม่มีประโยชน์')],
                                           required=False)
    comment_time = forms.ChoiceField(choices=[(1,u'เพียงพอ'), (0,u'ไม่เพียงพอ')],
                                     required=False)
    comment_knowledge = forms.CharField(required=False,
                                        widget=forms.Textarea(attrs={'rows': 3}))
    comment_additional_training = forms.CharField(required=False,
                                                  widget=forms.Textarea(attrs={'rows': 3}))
    comment_probation = forms.CharField(required=False,
                                        widget=forms.Textarea(attrs={'rows': 3}))
    comment_requirements = forms.CharField(required=False,
                                           widget=forms.Textarea(attrs={'rows': 3}))
    comment_extra = forms.CharField(required=False,
                                    widget=forms.Textarea(attrs={'rows': 2}))
    
    comment_eng_suggestions = forms.CharField(required=False,
                                              widget=forms.Textarea(attrs={'rows': 2}))
    
    comment_eng_support_available = forms.ChoiceField(choices=[(1,u'Yes'), (0,u'No')],
                                                      required=False)
    comment_eng_support_air_tickets = forms.BooleanField(required=False)
    comment_eng_support_living_expenses = forms.BooleanField(required=False)
    comment_eng_support_amount = forms.CharField(required=False)
    comment_eng_support_received = forms.ChoiceField(choices=[(1,u'Yes'), (0,u'No')],
                                                     required=False)

    comment_next_year_decision = forms.ChoiceField(choices=[(1,u'รับ'), (2,u'ไม่รับ'), (3,u'อื่น ๆ (โปรดระบุ)')])
    comment_next_year_acceptance_count = forms.CharField(required=False)
    comment_next_year_reject_extra = forms.CharField(required=False,
                                                     widget=forms.Textarea(attrs={'rows': 2}))
    comment_next_year_other_extra = forms.CharField(required=False,
                                                    widget=forms.Textarea(attrs={'rows': 2}))
    

    class Meta:
        model = Evaluation
        exclude = (
            'assignment',
            'student',
            'scores',
            'total_score',
            'raw_total_score',
            'report_score',
            'evaluation_form_received',
            'timesheet_received',
            'is_evaluation_reported_by_company',
            'public_comments',
            'internal_comments',
            'received_at',
            'number',
        )


def edit_by_company_wait(request, student_id, secret):
    year = get_current_year()

    return render_to_response("evaluations/edit_by_company_wait.html",
                              { 'year': year,
                              },
                              context_instance=RequestContext(request))
    

        
def search_evaluation_form(request):
    year = get_current_year()
    eng_year = year - 543

    error = ''

    student_code = ''
    pass_code = ''
    if request.method == 'POST':
        student_code = request.POST.get('student_code','').strip()
        if len(student_code) < 5:
            real_student_code = 'yyy'
        else:
            real_student_code = student_code
        pass_code = request.POST.get('pass_code','').strip()

        students = Student.objects.filter(student_id__endswith=real_student_code).all()

        found = False
        student = None
        secret = None
        for s in students: 
            ev_secret = find_secret_by_easy_secret(s, pass_code)
            if ev_secret:
                student = s
                secret = ev_secret
                found = True
                break
            
        if found:
            return redirect(reverse('evaluations_edit_by_company', args=[student.student_id[-5:], secret.secret]))
        else:
            error = 'not-found'
    
    return render_to_response("evaluations/search_evaluation_form.html",
                              { 'year': year,
                                'eng_year': eng_year,
                                'error': error,

                                'student_code': student_code,
                                'pass_code': pass_code,
                              },
                              context_instance=RequestContext(request))
    
        
def edit_by_company(request, student_id, secret):
    students = Student.objects.filter(student_id__endswith=student_id).all()

    assignment = None
    student = None
    for s in students: 
        assignment = find_assignment_by_secret(s, secret)
        if assignment:
            student = s
            break

    year = get_current_year()

    language = 'thai'
    is_success = None
    
    if not assignment:
        return render_to_response("evaluations/secret_not_found.html",
                                  { 'student': student,
                                    'year': year },
                                  context_instance=RequestContext(request))


    Log.create('Evaluation accessed (from %s)' % (request.META['REMOTE_ADDR'],),
               student=student)
    
    company_name = assignment.get_company_name()
    year = assignment.year
    eng_year = year - 543
    
    try:
        evaluation = assignment.evaluation
        evaluator_comments = evaluation.extract_evaluator_comments()
    except Evaluation.DoesNotExist:
        evaluation = None
        evaluator_comments = {}

    if evaluation and (not evaluation.is_score_incomplete(year)):
        language = 'thai'
        if evaluation.is_form_submitted_in_english:
            language = 'english'
        return render_to_response("evaluations/eval_submitted.html",
                                  { 'student': student,
                                    'company_name': company_name,
                                    'year': year,
                                    'eng_year': eng_year,
                                    'language': language,
                                    'evaluation': evaluation },
                                  context_instance=RequestContext(request))

        
    if request.method == 'POST':
        org_evaluation = evaluation
        form = CompanyEvaluationForm(request.POST, instance=evaluation)
        if form.is_valid():
            evaluation = form.save(commit=False)
            evaluation.assignment = assignment
            evaluation.student = student
            evaluation.is_evaluation_reported_by_company = True
            evaluation.evaluation_form_received = True
            evaluation.received_at = datetime.date.today()

            ev_comments = {}
            for f in COMPANY_COMMENT_FIELDS:
                ev_comments[f] = form.cleaned_data[f]
            evaluation.set_evaluator_comments(ev_comments)

            evaluation.is_form_submitted_in_english = (request.POST.get('lang','') == 'en')
            if evaluation.is_form_submitted_in_english:
                language = 'english'
            
            if not org_evaluation:
                evaluation.timesheet_received = False
            
            evaluation.set_scores_from_list(extract_evaluation_scores(request.POST,
                                                                      Evaluation.get_evaluation_item_count(year)),
                                            year)
            is_success = True
            evaluation.save()

            Log.create('Evaluation saved (from %s)' % (request.META['REMOTE_ADDR'],),
                       student=student)
        else:
            is_success = False
    else:
        comment_initials = {}
        for f in COMPANY_COMMENT_FIELDS:
            if f in evaluator_comments:
                comment_initials[f] = evaluator_comments[f]
        form = CompanyEvaluationForm(initial=comment_initials,
                                     instance=evaluation)
        
    today_date = str(datetime.date.today())
    yesterday_date = str(datetime.date.today() - datetime.timedelta(1))
    default_beginning_date = str(Configuration.get('default.eval.beginning_date'))
    default_end_date = str(Configuration.get('default.eval.end_date'))

    evaluation_sections = Evaluation.get_sections(year)
    if evaluation:
        combine_evaluation_sections_with_scores(evaluation_sections, evaluation.get_scores_as_dict(year))
        if evaluation.is_form_submitted_in_english:
            language = 'english'

    return render_to_response("evaluations/edit_by_company.html",
                              { 'student': student,
                                'year': year,
                                'eng_year': eng_year,
                                'assignment': assignment,
                                'evaluation': evaluation,
                                'evaluation_sections': evaluation_sections,
                                'company_name': company_name,
                                'form': form,
                                "today_date": today_date,
                                "yesterday_date": yesterday_date,
                                "default_beginning_date": default_beginning_date,
                                "default_end_date": default_end_date,

                                'language': language,
                                
                                'is_success': is_success,
                              },
                              context_instance=RequestContext(request))
