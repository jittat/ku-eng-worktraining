# -*- coding: utf-8 -*-
from django.http import HttpResponse , HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django import forms
from django.template import RequestContext
from django.db import IntegrityError

from regis.models import Student, Company,Department,Assignment,Status,Education, Grade
from std.models import CompanyDirectApplication, RequestStudent
from evaluations.models import Evaluation
from commons.academia import get_current_academic_year, get_next_worktraining_year
from commons.decorators import sa_admin_required, admin_or_committee_required
from commons.models import UserProfile, Log
from commons.utils import extract_notice
from commons.email import send_register_confirmation

from stdimport import import_students_from_regis_files

import math
import re
currentpage = 'student'
number_student_per_page = 50.0
MAX_DISPLAY_PER_PAGE = 100

class StudentAllSearchForm(forms.Form):
    student_id = forms.CharField(label=u'รหัสนิสิต',required=False)
    student_name = forms.CharField(label=u'ชื่อ',required=False)
    departments = Department.objects.all()
    DEPARTMENT_CHOICE = [[0,u'ทุกภาควิชา']]
    for department in departments:
        DEPARTMENT_CHOICE.append([department.id,department.name])

    student_department = forms.ChoiceField(choices=DEPARTMENT_CHOICE,label=u'ภาควิชา',required=False)

class StudentAddForm(forms.Form):
    student_id = forms.CharField(label=u'รหัสนิสิต')
    prefix = forms.CharField(label=u'คำนำหน้า')
    first_name = forms.CharField(label=u'ชื่อนิสิต')
    last_name = forms.CharField(label=u'นามสกุลนิสิต')
    tel_no = forms.CharField(label=u'เบอร์โทรศัพท์', required=False)
    email = forms.EmailField(label=u'Email', required=False)
    department = forms.ModelChoiceField(queryset=Department.objects,
                                        empty_label=u"ไม่มีข้อมูล",
                                        required=False)

    def clean_student_id(self):
        sid = self.cleaned_data['student_id']

        import re

        if not re.match(r'(\d+)',sid):
            raise forms.ValidationError(u'รหัสนิสิตไม่ถูกต้อง')

        if (len(sid)!=8) and (len(sid)!=10):
            raise forms.ValidationError(u'ความยาวของรหัสนิสิตไม่ถูกต้อง')

        return sid


class StdEditForm(forms.Form):
    tel_no = forms.CharField(label=u'Tel. :',
                             required=False)
    email = forms.EmailField(label=u'Email :',
                             required=False)
    department = forms.ModelChoiceField(queryset=Department.objects,
                                        empty_label=u"ไม่มีข้อมูล")
    total_credit = forms.IntegerField(label=u'หน่วยกิตรวม')
    gpax = forms.FloatField(label=u'เกรดเฉลี่ย')
    upto_year = forms.IntegerField(label=u'ปีการศึกษาสุดท้ายที่คำนวณ')
    upto_semester = forms.IntegerField(label=u'ภาคการศึกษาสุดท้ายที่คำนวณ')

class StdImportForm(forms.Form):
    std_list_file = forms.FileField(label=u'แฟ้มข้อมูลรายการนิสิต',
                                    help_text=u'แฟ้มข้อมูลที่ save จากระบบลงทะเบียน')

@admin_or_committee_required
def index(request):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department

    notice = extract_notice(request.session)
    search_result_notice = ''
    if request.method=='POST':
        form = StudentAllSearchForm(request.POST)
        if form.is_valid():
            student_name = form.cleaned_data['student_name']
            student_id = form.cleaned_data['student_id']
            student_department = form.cleaned_data['student_department']

            students = Student.all_by_id_name_department(
                student_id=student_id,
                name=student_name,
                department=student_department)

            result_count = students.count()
            if result_count > 300:
                students = students[:300]
                search_result_notice = u'มีนิสิตที่ตรงเงื่อนไขจำนวน %d คน แสดงเฉพาะ 300 รายการแรกเท่านั้น' % (result_count)

            if len(students) != 0:
                year = get_current_academic_year() + 1
                return render_to_response('regis/student/index.html',
                              { 'form': form,
                                'students':students,
                                'notice': notice,
                                'search_result_notice': search_result_notice,
                                'is_committee': is_committee,
                                'department': department,
                                'year':year,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))
            else:
                search_result_notice = 'ไม่พบข้อมูลนิสิตที่ค้นหา คุณสามารถเพิ่มข้อมูลของนิสิตดังกล่าวได้โดยเลือก "เพิ่มรายชื่อนิสิต" จากเมนูข้อมูลนิสิตด้านบน'
                form = StudentAllSearchForm()
    else:
        form = StudentAllSearchForm()

    return render_to_response('regis/student/index.html',
                              { 'form': form,
                                'notice': notice,
                                'search_result_notice': search_result_notice,
                                'is_committee': is_committee,
                                'department': department,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))

def get_student_worktraining_info(student):

    info = {}

    for assignment in student.assignments.all():
        if assignment.year not in info:
            info[assignment.year] = {}
        info[assignment.year]['assignment'] = assignment
        try:
            evaluation = assignment.evaluation
        except Evaluation.DoesNotExist:
            evaluation = None
        info[assignment.year]['evaluation'] = evaluation

    from direct_appl.models import StudentApplication

    for app in StudentApplication.all_for_student(student):
        if app.position.year not in info:
            info[app.position.year] = {}
        if 'direct_apps' not in info[app.position.year]:
            info[app.position.year]['direct_apps'] = []

        info[app.position.year]['direct_apps'].append(app)

    return [(y, info[y]) for y in sorted(info.keys())]


def get_student_self_appl_info(student, year):
    from std.views.company import CompanyDirectApplicationDetailForm

    info = student.get_all_self_appl_info(year)

    from std.views.company import CompanyRequestForm
    
    for ritem in info['company_request_items']:
        ritem.form = CompanyRequestForm.create_from_company_request(ritem.company_request)

    from django.forms import TextInput
        
    for aitem in info['direct_applications']:
        if aitem.is_accepted and aitem.is_chosen:
            aitem.form = CompanyDirectApplicationDetailForm(instance=aitem.detail)
            aitem.form.fields['end_date'].widget = TextInput()
    
    has_active = student.has_active_self_appl(year, info)
    info['has_active'] = has_active
    return info


@admin_or_committee_required
def detail(request, student_id):
    student = get_object_or_404(Student, student_id=student_id)

    worktraining_info = get_student_worktraining_info(student)

    profile = request.user.userprofile
    is_committee = profile.is_committee_member
    department = profile.department

    notice = extract_notice(request.session)

    # TODO: clean up this
    permission = { 'can_change_company': False,
                   'can_edit_evaluation': False,
                   'can_view_grades': False,
                   'can_cancel_registration': False }
    if profile.is_sa_admin:
        permission['can_edit_evaluation'] = True
        permission['can_change_company'] = True
        permission['can_view_grades'] = True
        permission['can_cancel_registration'] = True
    if department == student.department:
        permission['can_view_grades'] = True

    next_worktraining_year = get_next_worktraining_year()
    next_status = student.get_status(next_worktraining_year)

    if next_status and next_status.is_registered_for_self_application():
        self_appl_info = get_student_self_appl_info(student,
                                                    next_worktraining_year)
    else:
        self_appl_info = None
        
    return render_to_response("regis/student/profile.html",
                              {'student': student,
                               'wt_info': worktraining_info,
                               'next_status': next_status,
                               'next_worktraining_year': next_worktraining_year,
                               'self_appl_info': self_appl_info,
                               
                               'permission': permission,
                               'profile': profile,
                               'is_committee': is_committee,
                               'department': department,
                               'notice': notice,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def edit_detail(request,student_id):
    student = get_object_or_404(Student, student_id=student_id)
    noticed = {}
    if request.method=='POST':
        form = StdEditForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['total_credit']<0:
                noticed['credit'] ='หน่วยกิตสะสมต้องมีค่ามากกว่า '
                if form.cleaned_data['gpax']<0 or form.cleaned_data['gpax']>4:
                    noticed['gpax']='เกรดเฉลี่ยต้องเป็นค่าตั้งแต่ 0.00-4.00'
            else:
                if form.cleaned_data['gpax']<0 or form.cleaned_data['gpax']>4:
                    noticed['gpax']='เกรดเฉลี่ยต้องเป็นค่าตั้งแต่ 0.00-4.00'
                else:
                    student.email = form.cleaned_data['email']
                    student.tel_no = form.cleaned_data['tel_no']
                    student.department = form.cleaned_data['department']
                    if student.education_id == None:
                        student.education = Education()
                    student.education.upto_year = form.cleaned_data['upto_year']
                    student.education.upto_semester = form.cleaned_data['upto_semester']
                    student.education.total_credit = form.cleaned_data['total_credit']
                    student.education.gpax = form.cleaned_data['gpax']
                    student.education.save()
                    student.education_id = student.education.id
                    student.save()

                    Log.create('Student detail editted (%s)' % (student.student_id,),
                               request.user,
                               student)
                    request.session['notice'] = u'จัดเก็บการแก้ไขเรียบร้อยแล้ว'
                    return HttpResponseRedirect(reverse('student_detail',args=[student_id]))
    else:
        if student.department:
           department_id = student.department.id
        else:
           department_id = None
        if student.education_id != None:
            form = StdEditForm(initial={'email':student.email,
                                        'tel_no' : student.tel_no,
                                        'department': department_id,
                                        'total_credit':student.education.total_credit,
                                        'gpax':student.education.gpax,
                                        'upto_year':student.education.upto_year,
                                        'upto_semester':student.education.upto_semester
                                        })
        else:
            form = StdEditForm(initial={'email':student.email,
                                        'tel_no' : student.tel_no,
                                        'department': department_id
                                        })

    return render_to_response("regis/student/editprofile.html",
                              { 'student': student,
                                'form': form ,
                                'noticed':noticed,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@admin_or_committee_required
def show_logs(request, student_id):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department

    student = get_object_or_404(Student, student_id=student_id)

    logs = Log.objects.filter(student=student)

    return render_to_response("regis/student/logs.html",
                              {'student': student ,
                               'logs': logs,
                               'is_committee': is_committee,
                               'department': department,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@admin_or_committee_required
def show_grades(request, student_id):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department
    student = get_object_or_404(Student, student_id=student_id)

    profile = request.user.userprofile

    if (not profile.is_sa_admin) and (student.department != department):
        return HttpResponseForbidden()
    grades = Grade.objects.filter(student=student).order_by('course_number')
    return render_to_response("regis/student/grades.html",
                              {'student': student ,
                               'grades': grades,
                               'is_committee': is_committee,
                               'department': department,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))    

@admin_or_committee_required
def add_student(request):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department

    notice = ''
    action_successful = False
    if request.method=='POST':
        form = StudentAddForm(request.POST)
        if form.is_valid():
            student = Student()
            student.student_id = form.cleaned_data['student_id']
            student.prefix = form.cleaned_data['prefix']
            student.first_name = form.cleaned_data['first_name']
            student.last_name = form.cleaned_data['last_name']
            student.tel_no = form.cleaned_data['tel_no']
            student.email = form.cleaned_data['email']
            student.department = form.cleaned_data['department']
            try:
                student.save()

                Log.create('Student created (%s)' % (student.student_id,),
                           request.user,
                           student)

                notice = (
                    u'เพิ่มข้อมูลของ %s %s เข้าสู่ระบบเรียบร้อยแล้ว' %
                    (student.student_id, student.full_name()))
                action_successful = True
                form = StudentAddForm()
            except IntegrityError:
                student = get_object_or_404(Student, student_id=form.cleaned_data['student_id'])
                form = StudentAddForm()
            return render_to_response("regis/student/add_std.html",
                                      {'form': form,
                                       'action_successful': action_successful,
                                       'student':student,
                                       'notice': notice,
                                       'is_committee': is_committee,
                                       'department': department,
                                       'currentpage':currentpage,
                                       },
                                      context_instance=RequestContext(request))
    else:
        if department:
            dept_id = department.id
        else:
            dept_id = None
        form = StudentAddForm(initial={ 'department':dept_id })

    return render_to_response("regis/student/add_std.html",
                              {'form': form,
                               'notice': notice,
                               'is_committee': is_committee,
                               'department': department,
                               'currentpage':currentpage,
                               },
                              context_instance=RequestContext(request))


@sa_admin_required
def import_students(request):
    notice = ''
    if request.method=='POST':
        form = StdImportForm(request.POST,request.FILES)
        if form.is_valid():
            students = import_students_from_regis_files(form.cleaned_data['std_list_file'])
            notice = u'นำเข้าข้อมูลนิสิตทั้งสิ้น %d คน' % len(students)
    else:
        form = StdImportForm()

    return render_to_response('regis/student/import_students.html',
                              {'form': form,
                               'notice': notice},
                              context_instance=RequestContext(request))

class CompanyAllSearchForm(forms.Form):
    company_id = forms.CharField(label=u'ชื่อย่อ',required=False)
    company_name = forms.CharField(label=u'ชื่อสถานประกอบการ',required=False)

@sa_admin_required
def search_change_company(request,student_id,year):
    notice = ''
    student = get_object_or_404(Student, student_id=student_id)
    if request.method=='POST':
        form = CompanyAllSearchForm(request.POST)
        if form.is_valid():
            company_id = form.cleaned_data['company_id']
            company_name = form.cleaned_data['company_name']

            companies = Company.objects.all().order_by('short_name')
            if len(company_id) != 0:
                companies = Company.objects.filter(short_name=company_id).all()
            if len(company_name) != 0:
                companies = Company.objects.filter(name__contains=company_name).all()

            if len(companies) != 0:
                return render_to_response("regis/student/change_company.html",
                                              { 'student': student ,
                                                'companies':companies,
                                                'form': form,
                                                'year':year,
                                                'currentpage':currentpage},
                                              context_instance=RequestContext(request))
            else:
                notice = 'ไม่พบข้อมูลสถานประกอบการ'
                form = CompanyAllSearchForm()
    else:
         form = CompanyAllSearchForm()
    return render_to_response("regis/student/change_company.html",
                                  { 'student': student ,
                                    'year':year,
                                    'form':form,
                                    'currentpage':currentpage},
                                  context_instance=RequestContext(request))


@sa_admin_required
def change_company(request,student_id,year,company_id):
    student = Student.objects.get(student_id = student_id)
    company = Company.objects.get(id = company_id)
    assignment = student.assignments.get(year=year)
    assignment.company_id = company.id
    assignment.save()
    return render_to_response("regis/student/profile.html",
                              { 'student': student ,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

class ReceivedAtForm(forms.Form):
    received_at = forms.DateField(label=u'ได้รับเมื่อวันที่',required=False)
class DepartmentListForm(forms.Form):
    departments = Department.objects.all()
    DEPARTMENT_CHOICE = []
    for department in departments:
        DEPARTMENT_CHOICE.append([department.id,department.name])

    student_department = forms.ChoiceField(choices=DEPARTMENT_CHOICE,label=u'หรือเลือกแสดงตามภาควิชา',required=False)

@sa_admin_required
def list_all_has_evaluation(request):
    notice = ''
    received_at = 'all'
    department = None
    department_id = None
    filter_type = None
    if request.method=='POST':
        if 'received_at' in request.POST:
            form = ReceivedAtForm(request.POST)
            if form.is_valid():
                received_at = form.cleaned_data['received_at']
                evaluations = (Evaluation
                               .objects
                               .filter(assignment__year__exact=get_current_academic_year(),
                                       received_at__exact=received_at)
                               .select_related('student')
                               .order_by('student').all())
                filter_type = received_at
            else:
                evaluations = []
                notice = 'การกรอกวันที่ผิดรูปแบบ'
                form = ReceivedAtForm()

            dept_form = DepartmentListForm()

        else:
            dept_form = DepartmentListForm(request.POST)
            if dept_form.is_valid():
                department_id = int(dept_form.cleaned_data['student_department'])
                department = Department.objects.get(id=department_id)
                filter_type = department_id

                evaluations = (Evaluation
                               .objects
                               .filter(assignment__year__exact=get_current_academic_year())
                               .select_related('student')
                               .all())
                eval_by_dept = []
                for evaluation in evaluations:
                    if evaluation.is_department(department):
                        eval_by_dept.append(evaluation)
                evaluations = sorted(eval_by_dept, cmp=lambda x,y: cmp(x.student.student_id, y.student.student_id))
            form = ReceivedAtForm()

    else:
        evaluations = (Evaluation
                       .objects
                       .filter(assignment__year__exact=get_current_academic_year())
                       .select_related('student')
                       .order_by('-received_at')
                       .all()[:MAX_DISPLAY_PER_PAGE])
        form = ReceivedAtForm()
        dept_form = DepartmentListForm()

    return render_to_response("regis/student/has_evaluation.html",
                              { 'evaluations': evaluations,
                                'currentpage': currentpage,
                                'form': form,
                                'dept_form':dept_form,
                                'received_at': received_at,
                                'department':department,
                                'dep_id': department_id,
                                'filter_type': filter_type},
                              context_instance=RequestContext(request))

@sa_admin_required
def print_list_eval(request,received_at):
    if received_at != 'all':
        evaluations = (Evaluation.
                       objects
                       .filter(assignment__year__exact=get_current_academic_year(),
                               received_at__exact=received_at)
                       .select_related('student')
                       .order_by('student')
                       .all())
    else:
        evaluations = (Evaluation
                       .objects
                       .filter(assignment__year__exact=get_current_academic_year())
                       .select_related('student')
                       .order_by('-received_at')
                       .all())

    return render_to_response('regis/student/list_eval.html' ,
                              {'evaluations': evaluations,
                               'received_at':received_at},
                               context_instance=RequestContext(request))

@sa_admin_required
def print_list_eval_by_department(request,code):
    evaluations = (Evaluation
                   .objects
                   .filter(assignment__year__exact=get_current_academic_year())
                   .select_related('student')
                   .order_by('student')
                   .all())
    eval_by_dept = []
    department = Department.objects.get(code=code)
    for evaluation in evaluations:
        if evaluation.is_department(department):
            eval_by_dept.append(evaluation)
    return render_to_response('regis/student/list_eval.html' ,
                              {'evaluations': eval_by_dept,
                               'department':department},
                               context_instance=RequestContext(request))

@sa_admin_required
def listall_std_registered(request,page_id):
    statuses = Status.objects.filter(year=get_current_academic_year()+1,is_registered=True)

    regtype = request.GET.get('regtype',None)
    regtype_label = ''
    if regtype!=None and int(regtype) in Status.REGISTRATION_TYPE_LABELS:
        statuses = statuses.filter(registration_type=regtype)
        regtype_label = Status.REGISTRATION_TYPE_LABELS[int(regtype)]

    statuses = statuses.select_related('student').order_by('student__student_id').all()

    regis_students = []
    for status in statuses:
        student = status.student
        student.current_status = status
        regis_students.append(student)

    regtype_counts = []
    total_all_regtype = 0
    for rt,rtlabel in Status.REGISTRATION_TYPE_LABELS.items():
        std_count = (Status.objects.
                     filter(year=get_current_academic_year()+1,
                            is_registered=True,registration_type=rt).
                     count())
        regtype_counts.append({ 'id': rt, 
                                'label': rtlabel,
                                'count': std_count })
        total_all_regtype += std_count

    number_student_per_page = 100
    total_std = len(regis_students)
    total_page = (total_std+number_student_per_page-1)/number_student_per_page
    pages = []
    for i in range(1,total_page+1):
        pages.append(i)

    query_offset = (int(page_id)*int(number_student_per_page))-int(number_student_per_page)
    regis_students = regis_students[query_offset:int(page_id)*int(number_student_per_page)]
    return render_to_response('regis/student/std_registered.html' ,
                              {'students': regis_students,
                               'regtype_counts': regtype_counts,
                               'regtype_label': regtype_label,
                               'total_std': total_std,
                               'total_all_regtype': total_all_regtype,
                               'pages': pages,
                               'page_number': int(page_id),
                               'currentpage': currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def export_csv_has_eval(request, filterType):
    response = HttpResponse(content_type='text/csv')
    if filterType == 'None':
        response['Content-Disposition'] = 'attachment; filename=list_std_hasEval.csv'
        evaluations = (Evaluation
                       .objects
                       .filter(assignment__year__exact=get_current_academic_year())
                       .select_related('student')
                       .order_by('-received_at')
                       .all())        
        students = [evaluation.student for evaluation in evaluations]
    elif re.match('\w+-', filterType) == None:
        evaluations = (Evaluation
                       .objects
                       .filter(assignment__year__exact=get_current_academic_year())
                       .select_related('student')
                       .all())
        department = Department.objects.get(id=int(filterType))
        response['Content-Disposition'] = 'attachment; filename=list_std_hasEval_'+department.code+'.csv'
        students = []
        for evaluation in evaluations:
            if evaluation.is_department(department):
                students.append(evaluation.student)
        students = sorted(students, cmp=lambda x,y: cmp(x.student_id, y.student_id))
    else:
       response['Content-Disposition'] = 'attachment; filename=list_std_hasEval_'+filterType+'.csv'
       evaluations = (Evaluation
                      .objects
                      .filter(assignment__year__exact=get_current_academic_year(),
                              received_at__exact=filterType)
                      .select_related('student')
                      .order_by('student')
                      .all())
       students = [evaluation.student for evaluation in evaluations]
    content = ''
    content = u'รหัสนิสิต,ชื่อ,สกุล,สาขาวิชา,วันที่ได้รับแบบประเมิน,เลขรับ,ใบลงเวลา,แบบประเมิน,วันเริ่มผึกงาน,วันจบฝึกงาน,จำนวนวันที่ฝึกงาน,สาย(วัน),ลากิจ(วัน),ลาป่วย(วัน),ขาด(วัน),สถานประกอบการที่ไปฝึกงาน\n'

    department_map = dict([(d.id,d.name) for d in Department.objects.all()])
    department_map[None] = u''

    for student in students:
        content += (str(student.student_id)+','+student.first_name+','+
                    student.last_name + ',' +
                    department_map[student.department_id] + ',' +
                    str(student.get_evaluation().received_at)+','+
                    str(student.get_evaluation().get_number())+','+
                    str(student.get_evaluation().timesheet_received)+','+
                    str(student.get_evaluation().evaluation_form_received)+','+
                    str(student.get_evaluation().beginning_date)+','+
                    str(student.get_evaluation().end_date)+','+
                    str(student.get_evaluation().num_working_days)+','+
                    str(student.get_evaluation().num_late_days)+','+
                    str(student.get_evaluation().num_personal_leave_days)+','+
                    str(student.get_evaluation().num_heath_leave_days)+','+
                    str(student.get_evaluation().num_absent_leave_days)+','+
                    student.intern_company_for_current_year().name
                    +'\n')
    content = '\xef\xbb\xbf' + content.encode("utf-8")
    response.content = content
    return response

@sa_admin_required
def register_internship(request,student_id):
    student = get_object_or_404(Student, student_id=student_id)

    profile = request.user.userprofile
    if student.department:
        if not profile.is_admin_or_department_committee(student.department.code):
            explanation = u'คุณสามารถสมัครการฝึกงานให้นิสิตได้เฉพาะนิสิตในสาขาของคุณเท่านั้น'
            return render_to_response('commons/forbidden.html',
                                      { 'profile': profile,
                                        'explanation': explanation },
                                      context_instance=RequestContext(request))

    if not student.has_registered_for_next_year():
        student.save_registered_status(get_current_academic_year() + 1, Status.FACULTY_SELECTION_TYPE)
        send_register_confirmation(student)

        Log.create('Student internship registered (%s)' %
                   (student.student_id,),
                   request.user,
                   student)

        notice = (u'ได้สมัครนิสิต %s %s เข้ารับการฝึกงานในภาคการศึกษาหน้าแล้ว' %
                  (student_id, student.full_name()))
    else:
        notice = u'นิสิตคนดังกล่าวได้สมัครเข้ารับการฝึกงานไว้แล้ว'

    request.session['notice'] = notice
    return HttpResponseRedirect(reverse('student_detail', kwargs={'student_id': student.student_id}))

@sa_admin_required
def cancel_internship(request,student_id):
    student = get_object_or_404(Student, student_id=student_id)
    next_year = get_current_academic_year() + 1

    if student.has_registered(next_year):
        statuses = student.status.filter(year=next_year).all()
        if len(statuses)!=0:
            statuses.delete()
        
            Log.create('Student internship canceled (%s)' %
                       (student.student_id,),
                       request.user,
                       student)

            notice = (u'ได้ยกเลิกการสมัครเข้าฝึกงานของ %s %s ในภาคการศึกษาหน้าแล้ว' %
                      (student_id, student.full_name()))
        else:
            notice = u'ข้อมูลผิดพลาด นิสิตคนดังกล่าวยังไม่ได้สมัครฝึกงาน'
    else:
        notice = u'ข้อมูลผิดพลาด นิสิตคนดังกล่าวยังไม่ได้สมัครฝึกงาน'

    request.session['notice'] = notice
    return HttpResponseRedirect(reverse('student_detail',args=[student.student_id]))

@sa_admin_required
def change_internship_type(request,student_id,new_type):
    student = get_object_or_404(Student, student_id=student_id)
    next_year = get_current_academic_year() + 1

    if student.has_registered(next_year):
        statuses = student.status.filter(year=next_year).all()
        if len(statuses)!=0:
            status = statuses[0]

            if (int(new_type) != 2) or (not student.has_active_self_appl(next_year)):
                status.registration_type = int(new_type)
                status.save()
            
                Log.create('Change internship type to %s (%s)' %
                           (new_type,student.student_id,),
                           request.user,
                           student)

                notice = (u'ได้เปลี่ยนประเภทการสมัครเข้าฝึกงานของ %s %s ในภาคการศึกษาหน้าแล้ว' %
                          (student_id, student.full_name()))
            else:
                notice = u'ข้อมูลผิดพลาด นิสิตคนดังกล่าวยังมีการเลือกฝึกงานแบบที่ 1 ที่ยังไม่สิ้นสุด'
        else:
            notice = u'ข้อมูลผิดพลาด นิสิตคนดังกล่าวยังไม่ได้สมัครฝึกงาน'
    else:
        notice = u'ข้อมูลผิดพลาด นิสิตคนดังกล่าวยังไม่ได้สมัครฝึกงาน'

    request.session['notice'] = notice
    return HttpResponseRedirect(reverse('student_detail',args=[student.student_id]))

@admin_or_committee_required
def update_company_request(request,student_id,company_request_id):
    from std.models import CompanyRequest
    from std.views.company import CompanyRequestForm
    
    student = get_object_or_404(Student, student_id=student_id)
    company_request = get_object_or_404(CompanyRequest, pk=company_request_id)

    request_set = [r for r in student.request_set.all()
                   if r.company_request_id == company_request.id]
    if len(request_set) == 0:
        return HttpResponseForbidden()

    if 'ok' not in request.POST:
        request.session['notice'] = u'ยกเลิกการแก้ไขแล้ว'
    else:
        form = CompanyRequestForm(request.POST)
        if form.is_valid():
            form.save_to_company_request(company_request)
            company_request.save()
            company_request.delete_letter_config()
            request.session['notice'] = u'จัดเก็บการแก้ไขเรียบร้อยแล้ว'
            Log.create('Student company request editted (%s, %d)' % (student.student_id,company_request.id),
                       request.user,
                       student)
        else:
            request.session['notice'] = u'เกิดข้อผิดพลาด ไม่สามารถจัดเก็บการแก้ไขได้'

    return HttpResponseRedirect(reverse('student_detail',args=[student.student_id]))
    

@admin_or_committee_required
def update_company_direct_application(request,student_id,application_id):
    from std.models import CompanyDirectApplication
    from std.views.company import CompanyDirectApplicationDetailForm
    
    student = get_object_or_404(Student, student_id=student_id)
    company_direct_applications = get_object_or_404(CompanyDirectApplication, pk=application_id)
    app_detail = company_direct_applications.detail

    if 'ok' not in request.POST:
        request.session['notice'] = u'ยกเลิกการแก้ไขแล้ว'
    else:
        form = CompanyDirectApplicationDetailForm(request.POST, instance=app_detail)
        if form.is_valid():
            app_detail = form.save(commit=False)
            app_detail.is_default_schedule = (form.cleaned_data['period_choice'] == 'standard')
            app_detail.save()
            company_direct_applications.delete_letter_config()
            request.session['notice'] = u'จัดเก็บการแก้ไขเรียบร้อยแล้ว'
            Log.create('Student company direct app editted (%s, %d)' % (student.student_id,company_direct_applications.id),
                       request.user,
                       student)
        else:
            request.session['notice'] = u'เกิดข้อผิดพลาด ไม่สามารถจัดเก็บการแก้ไขได้'

    return HttpResponseRedirect(reverse('student_detail',args=[student.student_id]))
    

@sa_admin_required
def list_all_student_no_eval(request,page_id):
    students = Student.objects.all().order_by('student_id').order_by('department')
    assignments_with_evaluation = set([ev.assignment_id for
                                       ev in Evaluation.objects.all()])
    students_with_bad_assignments = set([assignment.student_id for
                                         assignment in
                                         Assignment.objects.filter(year=get_current_academic_year).all()
                                         if assignment.id not in assignments_with_evaluation ])
    lststudent = []
    for student in students:
        if student.id in students_with_bad_assignments:
            lststudent.append(student)

    total_std = len(lststudent)
    total_page = int(math.ceil(total_std/number_student_per_page))
    pages = []
    for i in range(1,total_page+1):
        pages.append(i)

    query_offset = (int(page_id)*int(number_student_per_page))-int(number_student_per_page)

    lststudent = lststudent[query_offset:int(page_id)*int(number_student_per_page)]

    return render_to_response("regis/student/list_all_no_evaluation.html",
                              {
                                'pages':pages,
                                'page_number':int(page_id),
                                'total_std':total_std,
                                'students': lststudent,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def registration_stats(request):
    from committee.student_stat import find_accepted_self_appl_students

    departments = Department.objects.order_by('code').all()
    regtype_count = len(Status.REGISTRATION_TYPE_LABELS)
    year = get_next_worktraining_year()

    regtype_labels = {0: 'no_pref',
                      1: 'own_selection',
                      2: 'faculty_selection'}
    default_stat_keys = ['no_pref', 
                         'own_selection',
                         'faculty_selection',
                         'own_selection_accepted']

    dmaps = dict([(d.id, {'department': d, 
                          'counts':dict([(v,0) for v in default_stat_keys])})
                  for d in departments])

    statuses = (Status
                .objects
                .filter(year=year,is_registered=True)
                .select_related('student')
                .all())

    for s in statuses:
        dept_id = s.student.department_id
        if dept_id in dmaps:
            if s.registration_type in regtype_labels:
                dmaps[dept_id]['counts'][regtype_labels[s.registration_type]] += 1


    accepted_own_selection_students = find_accepted_self_appl_students(year)
    for student in accepted_own_selection_students:
        dept_id = student.department_id
        if dept_id in dmaps:
            dmaps[dept_id]['counts']['own_selection_accepted'] += 1

    for d in departments:
        d.stats = dmaps[d.id]['counts']
        d.stats['own_selection_not_accepted'] = d.stats['own_selection'] - d.stats['own_selection_accepted']

    return render_to_response("regis/student/registration_stats.html",
                              { 'year': year,
                                'departments': departments,                                 
                                'currentpage': currentpage},
                              context_instance=RequestContext(request))


    

