# -*- coding: utf-8 -*-
import codecs
from regis.models import Student

def parse_name(st):
    u"""
    >>> (u'นางสาว',u'สมหญิง',u'สุดเท่') == parse_name(u'นางสาวสมหญิง สุดเท่')
    True
    >>> (u'นาย',u'สมหญิง',u'สุดเท่') == parse_name(u'นายสมหญิง สุดเท่')
    True
    >>> (u'นางสาว',u'สมหญิง',u'สุดเท่ มีต่อ') == parse_name(u'นางสาวสมหญิง สุดเท่ มีต่อ')
    True
    >>> (u'',u'ไม่รู้สมหญิง',u'สุดเท่ มีต่อ') == parse_name(u'ไม่รู้สมหญิง สุดเท่ มีต่อ')
    True
    """
    pos_prefs = [u'นางสาว',u'นาย']
    pref = ''
    for pf in pos_prefs:
        if st.startswith(pf):
            pref = pf
            st = st[len(pf):]
            break
    names = st.split()
    return (pref,names[0],u' '.join(names[1:]))
    

def parse_line(l):
    try:
        items = l.strip().split(',')
        std_id = items[2]
        pref,first,last = parse_name(items[3])
        department = items[4]
        return (std_id,pref,first,last,department)
    except:
        return None

def import_students_from_regis_files(f):
    line_str = f.read().splitlines()
    tisdecoder = codecs.getdecoder('tis-620')
    try:
        lines = [tisdecoder(line)[0] for line in line_str]
    except UnicodeError:
        try: 
            lines = [
                unicode(line, "utf8").lstrip(unicode(codecs.BOM_UTF8,"utf8"))
                for line in line_str
                ]
        except UnicodeDecodeError:
            return []
        
    students = []
    for l in lines:
        sdata = parse_line(l)
        if not sdata:
            continue

        sid,pf,f,l,d = sdata

        if not d.startswith('E'):
            continue

        try:
            s = Student.objects.get(student_id=sid)
        except:
            s = None

        if not s:
            s = Student()
            s.student_id=sid
            s.prefix=pf
            s.first_name=f
            s.last_name=l
            #s.email='b%s@ku.ac.th' % (sid)

            s.save()
            students.append(s)

    return students

if __name__ == "__main__":
    import doctest
    doctest.testmod()
