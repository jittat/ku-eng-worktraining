from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'regis.student.views',

    url(r'^$','index',name='regis_student_index'),
    #url(r'^search_id/$','search_by_id',name='student_search_by_id'),
    #url(r'^search_name/$','search_by_name',name='student_search_by_name'),

    url(r'^std_profile/(?P<student_id>\d+)/$','detail',name='student_detail'),
    url(r'^std_profile/(?P<student_id>\d+)/edit/$','edit_detail',name='student_edit_detail'),
    url(r'^std_profile/(?P<student_id>\d+)/logs/$','show_logs',name='student_logs'),
    url(r'^std_profile/(?P<student_id>\d+)/grades/$','show_grades',name='student_grades'),


    url(r'^search_change_company/(?P<student_id>\d+)/(?P<year>\d+)/$','search_change_company',name='search_change_student_company'),
    url(r'^change_company/(?P<student_id>\d+)/(?P<year>\d+)/(?P<company_id>\d+)/$','change_company',name='change_student_company'),
    
    url(r'^has_evaluation/$','list_all_has_evaluation',name='regis_student_has_all_evaluation'),
    
    url(r'^print_eval/(?P<received_at>[\w,\W]+)/$','print_list_eval',name='regis_student_print_eval'),
    url(r'^print_eval_by_dept/(?P<code>\d+)/$' ,'print_list_eval_by_department',
        name='regis_student_print_eval_by_dept'),
    
    url(r'^has_evaluation/csv/(?P<filterType>[\w,\W]+)/$','export_csv_has_eval',name='regis_student_csv_has_eval'),
    url(r'^std_all_registered/page/(?P<page_id>\d+)$','listall_std_registered',name='listall_student_registered'),
    url(r'^std_all_registered/stats/$','registration_stats',name='listall_registered_stats'),

    url(r'^std_addinternship_status/(?P<student_id>\d+)/$','register_internship',name='add_internship_status'),
    url(r'^std_cancelinternship/(?P<student_id>\d+)/$','cancel_internship',name='cancel_internship'),
    url(r'^std_change_internship_type/(?P<student_id>\d+)/(?P<new_type>\d+)$','change_internship_type',name='change_internship_type'),

    url(r'^companyrequest/(\d+)/(\d+)/$','update_company_request',name='regis_update_company_request'),
    url(r'^directapplication/(\d+)/(\d+)/$','update_company_direct_application',name='regis_update_direct_company_application'),

    url(r'^std_add/$','add_student',name='regis_add_student'),
    url(r'^std_import/$','import_students',name='regis_import_students'),
    url(r'^list_all_student_no_evaluation/page/(?P<page_id>\d+)$','list_all_student_no_eval',name='regis_student_list_all_student_no_eval'),
)
