# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0001_initial'),
        ('std', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='company_direct_application',
            field=models.ForeignKey(related_name='assignments', blank=True, to='std.CompanyDirectApplication', null=True),
        ),
        migrations.AddField(
            model_name='assignment',
            name='company_request',
            field=models.ForeignKey(related_name='assignments', blank=True, to='std.CompanyRequest', null=True),
        ),
        migrations.AddField(
            model_name='assignment',
            name='receipt',
            field=models.ForeignKey(related_name='assignments', blank=True, to='regis.Receipt', null=True),
        ),
        migrations.AddField(
            model_name='assignment',
            name='student',
            field=models.ForeignKey(related_name='assignments', to='regis.Student'),
        ),
    ]
