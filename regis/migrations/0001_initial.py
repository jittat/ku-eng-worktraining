# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Announcement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0e2b\u0e31\u0e27\u0e40\u0e23\u0e37\u0e48\u0e2d\u0e07')),
                ('description', models.TextField(help_text=b'Description of document.', verbose_name='\u0e23\u0e32\u0e22\u0e25\u0e30\u0e40\u0e2d\u0e35\u0e22\u0e14')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0e40\u0e27\u0e25\u0e32\u0e2a\u0e23\u0e49\u0e32\u0e07')),
                ('file', models.FileField(default=None, null=True, upload_to=b'upload', blank=True)),
                ('is_in_download_section', models.BooleanField(default=False, verbose_name='\u0e41\u0e2a\u0e14\u0e07\u0e41\u0e22\u0e01\u0e43\u0e19\u0e2a\u0e48\u0e27\u0e19\u0e14\u0e32\u0e27\u0e19\u0e4c\u0e42\u0e2b\u0e25\u0e14')),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(default=None, null=True, verbose_name='\u0e1b\u0e35\u0e17\u0e35\u0e48\u0e40\u0e02\u0e49\u0e32\u0e23\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u0e1a\u0e23\u0e34\u0e29\u0e31\u0e17')),
                ('short_name', models.CharField(max_length=20, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e22\u0e48\u0e2d')),
                ('starting_char', models.CharField(default=b'', max_length=1)),
                ('short_description', models.TextField(default=b'', verbose_name='\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25\u0e40\u0e1a\u0e37\u0e49\u0e2d\u0e07\u0e15\u0e49\u0e19\u0e40\u0e01\u0e35\u0e48\u0e22\u0e27\u0e01\u0e31\u0e1a\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23', blank=True)),
                ('tel_no', models.CharField(default=None, max_length=100, null=True, verbose_name='\u0e42\u0e17\u0e23\u0e28\u0e4d\u0e1e\u0e17\u0e4c')),
                ('fax_no', models.CharField(default=None, max_length=100, null=True, verbose_name='\u0e42\u0e17\u0e23\u0e2a\u0e32\u0e23')),
                ('address', models.TextField(default=None, null=True, verbose_name='\u0e17\u0e35\u0e48\u0e2d\u0e22\u0e39\u0e48')),
                ('contact_name', models.CharField(default=b'', max_length=200, verbose_name='\u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e2a\u0e32\u0e19\u0e07\u0e32\u0e19', blank=True)),
                ('contact_email', models.EmailField(default=b'', max_length=100, verbose_name='Email \u0e1c\u0e39\u0e49\u0e1b\u0e23\u0e30\u0e2a\u0e32\u0e19\u0e07\u0e32\u0e19', blank=True)),
                ('signer_name', models.CharField(default=b'', max_length=200, verbose_name='\u0e1c\u0e39\u0e49\u0e25\u0e07\u0e19\u0e32\u0e21', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('is_new', models.BooleanField(default=False, verbose_name='\u0e40\u0e1b\u0e47\u0e19\u0e2a\u0e16\u0e32\u0e19\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e01\u0e32\u0e23\u0e17\u0e35\u0e48\u0e40\u0e1e\u0e34\u0e48\u0e21\u0e43\u0e2b\u0e21\u0e48')),
            ],
        ),
        migrations.CreateModel(
            name='CompanyStatusMarker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35')),
                ('number', models.IntegerField(default=0)),
                ('company', models.ForeignKey(related_name='markers', to='regis.Company')),
            ],
        ),
        migrations.CreateModel(
            name='Conditions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('detail', models.TextField(verbose_name='\u0e23\u0e32\u0e22\u0e25\u0e30\u0e40\u0e2d\u0e35\u0e22\u0e14\u0e41\u0e25\u0e30\u0e02\u0e49\u0e2d\u0e15\u0e01\u0e25\u0e07\u0e43\u0e19\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
            ],
        ),
        migrations.CreateModel(
            name='ConfigLetter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35')),
                ('letter_number', models.CharField(max_length=300, verbose_name='\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22')),
                ('letter_date', models.DateField(verbose_name='\u0e01\u0e33\u0e2b\u0e19\u0e14\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e2d\u0e2d\u0e01\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22\u0e23\u0e32\u0e0a\u0e01\u0e32\u0e23')),
                ('signer_name', models.CharField(default=b'', max_length=200)),
                ('beginning_date', models.CharField(default=b'', max_length=100)),
                ('end_date', models.CharField(default=b'', max_length=100)),
                ('eval_date', models.CharField(default=b'', max_length=100)),
                ('company', models.ForeignKey(related_name='letter_config', to='regis.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(default=None, max_length=3, null=True, verbose_name='\u0e23\u0e2b\u0e31\u0e2a\u0e20\u0e32\u0e04\u0e27\u0e34\u0e0a\u0e32')),
                ('name', models.CharField(default=None, max_length=100, null=True, verbose_name='\u0e20\u0e32\u0e04\u0e27\u0e34\u0e0a\u0e32')),
                ('eng_name', models.CharField(default=None, max_length=100, verbose_name='Department name', blank=True)),
                ('short_name', models.CharField(default=None, max_length=10, null=True, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e22\u0e48\u0e2d')),
                ('short_name_for_letters', models.CharField(default=None, max_length=50, null=True, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e2a\u0e31\u0e49\u0e19\u0e2a\u0e33\u0e2b\u0e23\u0e31\u0e1a\u0e08\u0e14\u0e2b\u0e21\u0e32\u0e22')),
                ('is_hidden', models.BooleanField(default=False, verbose_name='\u0e0b\u0e48\u0e2d\u0e19\u0e44\u0e21\u0e48\u0e43\u0e2b\u0e49\u0e19\u0e34\u0e2a\u0e34\u0e15\u0e40\u0e2b\u0e47\u0e19')),
            ],
        ),
        migrations.CreateModel(
            name='DepartmentNameInRequestLetter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_to_be_requested', models.CharField(max_length=100, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e17\u0e35\u0e48\u0e08\u0e30\u0e43\u0e0a\u0e49\u0e2a\u0e48\u0e07\u0e40\u0e2d\u0e01\u0e2a\u0e32\u0e23')),
                ('department', models.ForeignKey(verbose_name='\u0e20\u0e32\u0e04\u0e27\u0e34\u0e0a\u0e32', to='regis.Department')),
            ],
            options={
                'verbose_name': 'Department Name in Official Letters',
                'verbose_name_plural': 'Department Names in Official Letters',
            },
        ),
        migrations.CreateModel(
            name='Education',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('total_credit', models.IntegerField(verbose_name='\u0e2b\u0e19\u0e48\u0e27\u0e22\u0e01\u0e34\u0e15\u0e23\u0e27\u0e21')),
                ('gpax', models.FloatField(verbose_name='\u0e40\u0e01\u0e23\u0e14\u0e40\u0e09\u0e25\u0e35\u0e48\u0e22')),
                ('upto_year', models.IntegerField(verbose_name='\u0e1b\u0e35\u0e01\u0e32\u0e23\u0e28\u0e36\u0e01\u0e29\u0e32\u0e2a\u0e38\u0e14\u0e17\u0e49\u0e32\u0e22\u0e17\u0e35\u0e48\u0e04\u0e33\u0e19\u0e27\u0e13')),
                ('upto_semester', models.IntegerField(verbose_name='\u0e20\u0e32\u0e04\u0e01\u0e32\u0e23\u0e28\u0e36\u0e01\u0e29\u0e32\u0e2a\u0e38\u0e14\u0e17\u0e49\u0e32\u0e22\u0e17\u0e35\u0e48\u0e04\u0e33\u0e19\u0e27\u0e13', choices=[(0, '\u0e20\u0e32\u0e04\u0e24\u0e14\u0e39\u0e23\u0e49\u0e2d\u0e19'), (1, '\u0e20\u0e32\u0e04\u0e15\u0e49\u0e19'), (2, '\u0e20\u0e32\u0e04\u0e1b\u0e25\u0e32\u0e22')])),
            ],
        ),
        migrations.CreateModel(
            name='Grade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course_number', models.CharField(max_length=8, verbose_name='\u0e23\u0e2b\u0e31\u0e2a\u0e27\u0e34\u0e0a\u0e32')),
                ('course_name', models.CharField(max_length=100, null=True, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e27\u0e34\u0e0a\u0e32')),
                ('credit', models.IntegerField(verbose_name='\u0e2b\u0e19\u0e48\u0e27\u0e22\u0e01\u0e34\u0e15')),
                ('grade', models.CharField(max_length=3, verbose_name='\u0e40\u0e01\u0e23\u0e14')),
                ('regis_type', models.CharField(max_length=1, verbose_name='\u0e1b\u0e23\u0e30\u0e40\u0e20\u0e17\u0e25\u0e07\u0e17\u0e30\u0e40\u0e1a\u0e35\u0e22\u0e19')),
            ],
        ),
        migrations.CreateModel(
            name='Receipt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35')),
                ('total_student', models.IntegerField(verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e19\u0e34\u0e2a\u0e34\u0e15')),
                ('work_address', models.TextField(verbose_name='\u0e17\u0e35\u0e48\u0e2d\u0e22\u0e39\u0e48')),
                ('work_description', models.TextField(verbose_name='\u0e25\u0e31\u0e01\u0e29\u0e13\u0e30\u0e07\u0e32\u0e19')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('available', models.BooleanField(default=True)),
                ('is_hidden', models.BooleanField(default=False)),
                ('num_accepted_students', models.IntegerField(default=0, verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e19\u0e34\u0e2a\u0e34\u0e15\u0e17\u0e35\u0e48\u0e2d\u0e19\u0e38\u0e21\u0e31\u0e15\u0e34\u0e41\u0e25\u0e49\u0e27')),
                ('num_rounds_shown', models.IntegerField(default=0, verbose_name='\u0e08\u0e33\u0e19\u0e27\u0e19\u0e04\u0e23\u0e31\u0e49\u0e07\u0e17\u0e35\u0e48\u0e41\u0e2a\u0e14\u0e07\u0e43\u0e2b\u0e49\u0e19\u0e34\u0e2a\u0e34\u0e15\u0e40\u0e25\u0e37\u0e2d\u0e01')),
                ('company', models.ForeignKey(related_name='receipts', to='regis.Company')),
                ('department', models.ForeignKey(related_name='receipts', verbose_name='\u0e20\u0e32\u0e04\u0e27\u0e34\u0e0a\u0e32', to='regis.Department')),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(verbose_name='\u0e1b\u0e35')),
                ('is_registered', models.NullBooleanField(verbose_name='\u0e2a\u0e21\u0e31\u0e04\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19\u0e41\u0e25\u0e49\u0e27')),
                ('attended_orientation', models.NullBooleanField(verbose_name='\u0e40\u0e02\u0e49\u0e32\u0e23\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e1b\u0e10\u0e21\u0e19\u0e34\u0e40\u0e17\u0e28\u0e41\u0e25\u0e49\u0e27')),
                ('sent_request_form', models.NullBooleanField(verbose_name='\u0e2a\u0e48\u0e07\u0e43\u0e1a\u0e04\u0e33\u0e23\u0e49\u0e2d\u0e07\u0e02\u0e2d\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19\u0e41\u0e25\u0e49\u0e27')),
                ('request_form_checkcode', models.CharField(max_length=32, null=True, verbose_name='\u0e23\u0e2b\u0e31\u0e2a\u0e15\u0e23\u0e27\u0e08\u0e2a\u0e2d\u0e1a\u0e43\u0e1a\u0e04\u0e33\u0e23\u0e49\u0e2d\u0e07\u0e02\u0e2d\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
                ('registration_type', models.IntegerField(default=0)),
                ('is_registration_type_confirmed', models.BooleanField(default=False)),
                ('is_converted_from_self_appl', models.NullBooleanField(default=None)),
                ('is_self_appl_confirmed', models.NullBooleanField(default=None)),
                ('converted_at', models.DateTimeField(null=True, blank=True)),
                ('confirmed_at', models.DateTimeField(null=True, blank=True)),
                ('asked_to_confirm_at', models.DateTimeField(null=True, blank=True)),
                ('has_passed_quiz_exam', models.BooleanField(default=False, verbose_name='\u0e1c\u0e48\u0e32\u0e19\u0e01\u0e32\u0e23\u0e17\u0e33\u0e41\u0e1a\u0e1a\u0e17\u0e14\u0e2a\u0e2d\u0e1a')),
                ('best_quiz_score', models.IntegerField(default=-1, verbose_name='\u0e04\u0e30\u0e41\u0e19\u0e19\u0e41\u0e1a\u0e1a\u0e17\u0e14\u0e2a\u0e2d\u0e1a\u0e2a\u0e39\u0e07\u0e2a\u0e38\u0e14')),
                ('last_quiz_submitted_at', models.DateTimeField(null=True, verbose_name='\u0e2a\u0e48\u0e07\u0e04\u0e33\u0e15\u0e2d\u0e1a\u0e41\u0e1a\u0e1a\u0e17\u0e14\u0e2a\u0e2d\u0e1a\u0e25\u0e48\u0e32\u0e2a\u0e38\u0e14', blank=True)),
            ],
            options={
                'ordering': ['-year'],
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student_id', models.CharField(max_length=15, verbose_name='\u0e23\u0e2b\u0e31\u0e2a\u0e19\u0e34\u0e2a\u0e34\u0e15')),
                ('prefix', models.CharField(max_length=10, verbose_name='\u0e04\u0e33\u0e19\u0e33\u0e2b\u0e19\u0e49\u0e32')),
                ('first_name', models.CharField(max_length=100, verbose_name='\u0e0a\u0e37\u0e48\u0e2d\u0e19\u0e34\u0e2a\u0e34\u0e15')),
                ('last_name', models.CharField(max_length=100, verbose_name='\u0e19\u0e32\u0e21\u0e2a\u0e01\u0e38\u0e25\u0e19\u0e34\u0e2a\u0e34\u0e15')),
                ('eng_prefix', models.CharField(max_length=10, verbose_name='prefix', blank=True)),
                ('eng_first_name', models.CharField(max_length=100, verbose_name='First name', blank=True)),
                ('eng_last_name', models.CharField(max_length=100, verbose_name='Last name', blank=True)),
                ('email', models.EmailField(default=None, max_length=100, null=True)),
                ('tel_no', models.CharField(default=None, max_length=50, null=True, verbose_name='\u0e40\u0e1a\u0e2d\u0e23\u0e4c\u0e42\u0e17\u0e23\u0e28\u0e31\u0e1e\u0e17\u0e4c')),
                ('department', models.ForeignKey(verbose_name='\u0e20\u0e32\u0e04\u0e27\u0e34\u0e0a\u0e32', to='regis.Department', null=True)),
                ('education', models.OneToOneField(null=True, blank=True, to='regis.Education')),
                ('intern_companies', models.ManyToManyField(to='regis.Company', through='regis.Assignment')),
            ],
        ),
        migrations.CreateModel(
            name='WorkDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('beginning_date', models.DateField(null=True, verbose_name='\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48\u0e40\u0e23\u0e34\u0e48\u0e21\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19', blank=True)),
                ('end_date', models.DateField(null=True, verbose_name='\u0e27\u0e31\u0e19\u0e2a\u0e34\u0e49\u0e19\u0e2a\u0e38\u0e14\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19', blank=True)),
                ('dressing_description', models.TextField(default=None, null=True, verbose_name='\u0e01\u0e32\u0e23\u0e41\u0e15\u0e48\u0e07\u0e01\u0e32\u0e22\u0e43\u0e19\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19', blank=True)),
                ('other_description', models.TextField(default=None, null=True, verbose_name='\u0e23\u0e32\u0e22\u0e25\u0e30\u0e40\u0e2d\u0e35\u0e22\u0e14\u0e2d\u0e37\u0e48\u0e19 \u0e46 ', blank=True)),
                ('year', models.IntegerField(default=None, null=True, verbose_name='\u0e1b\u0e35\u0e17\u0e35\u0e48\u0e40\u0e02\u0e49\u0e32\u0e23\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e1d\u0e36\u0e01\u0e07\u0e32\u0e19')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(related_name='workdetails', to='regis.Company')),
            ],
        ),
        migrations.AddField(
            model_name='status',
            name='student',
            field=models.ForeignKey(related_name='status', to='regis.Student'),
        ),
        migrations.AddField(
            model_name='receipt',
            name='work_detail',
            field=models.ForeignKey(related_name='receipts', to='regis.WorkDetail'),
        ),
        migrations.AddField(
            model_name='grade',
            name='student',
            field=models.ForeignKey(related_name='grades', to='regis.Student'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='company',
            field=models.ForeignKey(related_name='assignments', blank=True, to='regis.Company', null=True),
        ),
    ]
