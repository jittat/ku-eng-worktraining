from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'regis.views',

    url(r'^$', 'index', name='regis_index'),
    url(r'^company/', include('regis.company.urls')),
    url(r'^student/', include('regis.student.urls')),
    url(r'^department/', include('regis.department.urls')),
    url(r'^config/', include('regis.config.urls')),
    url(r'^mail/', include('regis.mass_mailing.urls')),
)

urlpatterns += patterns(
    'regis.stats.views',
    
    url(r'^stats/details/$', 'assignment_details_by_dept' , name='regis_stat_detail'),
    url(r'^stats/$', 'assignment_stats' , name='regis_stat'),
    url(r'^stats/(\d+)/$', 'assignment_stats', name='regis_stat'),
    url(r'^stats/students/(\d+)/(\w+)$', 'show_std_list' , name='regis_show_std_list'),
    url(r'^stats/students/(\d+)/(\w+)/(\d+)/$', 'show_std_list' , name='regis_show_std_list'),

    url(r'^stats/assignments/companies/$', 'assignment_company_stats', name='regis_stats_assignments_companies'),
    url(r'^stats/assignments/companies/(\d+)/$', 'assignment_company_stats', name='regis_stats_assignments_companies'),

    url(r'^stats/coop/$', 'coop_stats_by_dept', name='regis_stats_coop'),
    url(r'^stats/coop/requests/(\d+)/(\d+)/', 'coop_report', name='regis_stats_coop_report'),
)
