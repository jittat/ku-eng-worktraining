from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'regis.mass_mailing.views',

    url(r'^$','index', name='regis_mail_index'),
)
