# -*- coding: utf-8 -*-
from django import forms
from django.shortcuts import render_to_response
from django.template import RequestContext

from commons.academia import get_next_worktraining_year
from commons.decorators import sa_admin_required
from commons.email import send_mail_to_student, send_mail
from regis.models import Student, Department
from commons.models import Configuration

class EmailForm(forms.Form):
    start_year = 2553
    end_year = get_next_worktraining_year()
    if start_year < end_year - 2:
        start_year = end_year - 2
    YEAR_CHOICE = []
    for year in range(start_year,end_year+1):
        YEAR_CHOICE.append([year,year])
    YEAR_CHOICE.reverse()
    year = forms.ChoiceField(choices=YEAR_CHOICE,
                             label=u'ปีที่เข้าฝึกงาน')
    dept_choices = ([(0,'ทุกสาขาวิชา')] +
                    [(d.id,d.name) for d in
                     Department.objects.order_by('name').all() if not d.is_hidden])
    department = forms.MultipleChoiceField(label=u'สาขาวิชา',
                                           required=True,
                                           choices=dept_choices,
                                           widget=forms.CheckboxSelectMultiple(attrs={'class':'department_choices'}))
    subject = forms.CharField(label=u'หัวเรื่อง',
                              required=True,
                              widget=forms.TextInput(attrs={'size':'50'}))
    body = forms.CharField(label=u'เนื้อหา',
                           required=True,
                           widget=forms.Textarea)

@sa_admin_required
def index(request):
    notice = ''
    if request.method=='POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            body = form.cleaned_data['body']
            department_ids = [int(x) for x in form.cleaned_data['department']]

            year = int(form.cleaned_data['year'])

            if 0 in department_ids:
                department = None
                students = Student.all_registered_for_year(year, department)
            else:
                students = []
                for d in department_ids:
                    department = Department.objects.get(pk=d)
                    students += Student.all_registered_for_year(year, department)

            cc_emails = Configuration.get('email.cc_emails')
            if cc_emails:
                for email in cc_emails.split(','):
                    email = email.strip()
                    if email != '':
                        send_mail(email, subject + u' (สำเนา)', body)

            count = 0
            for s in students:
                #print s.student_id
                send_mail_to_student(s, subject, body)
                count += 1

            notice = u'ส่งเมล์ไปยังนิสิตจำนวน %d คนแล้ว' % count
            form = EmailForm()

    else:
        form = EmailForm()

    return render_to_response("regis/mass_mailing/index.html",
                              { 'form': form,
                                'notice': notice },
                              context_instance=RequestContext(request))
