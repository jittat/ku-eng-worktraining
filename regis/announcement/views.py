# _*_ coding: utf-8 _*_
from django.http import HttpResponse , HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django import forms

from regis.models import Announcement
from django.forms import ModelForm
from django.core.files.storage import default_storage
from django.contrib.auth.decorators import login_required

currentpage = 'web_system'

def announcement_detail(request,announcement_id):
    announcement = get_object_or_404(Announcement, pk=announcement_id)
    return render_to_response("regis/announcement/announcement_detail.html",{'announcement':announcement},
                               context_instance=RequestContext(request))

class AnnouncementForm(ModelForm):
    class Meta:
        model = Announcement
        fields = ('title', 'description','file','is_in_download_section')

@login_required
def announcement_edit(request,announcement_id):
    announcement = get_object_or_404(Announcement, pk=announcement_id)
    oldpath = announcement.file
    if request.method == 'POST':
        form = AnnouncementForm(request.POST, request.FILES, instance=announcement)
        if form.is_valid():
            form.save()
            if request.FILES and oldpath:
                default_storage.delete(oldpath)
            return HttpResponseRedirect(reverse('front_announcement_detail',kwargs={'announcement_id':announcement.id}))
    else:
        form = AnnouncementForm(instance=announcement)

    return render_to_response("regis/announcement/announcement_edit.html",
                              {'form':form ,
                               'announcement':announcement,
                               'currentpage':currentpage },
                               context_instance=RequestContext(request))

@login_required
def list(request):
    announcements = Announcement.objects.all()
    return render_to_response("regis/announcement/index.html",
                              {'announcements': announcements,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@login_required    
def announcement_create(request):
    if request.method == 'POST':
       form = AnnouncementForm(request.POST, request.FILES)
       if form.is_valid():
           announcement = Announcement()
           if request.FILES:
               announcement.file = request.FILES['file']         
           announcement.title = form.cleaned_data['title']
           announcement.description = form.cleaned_data['description']
           announcement.is_in_download_section = form.cleaned_data['is_in_download_section']
           announcement.save()
           return HttpResponseRedirect(reverse('announcement_index'))
    else:
        form = AnnouncementForm()
        
    return render_to_response('regis/announcement/announcement_create.html',
                              { 'form': form ,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@login_required   
def announcement_delete(request,announcement_id):
    announcement = Announcement.objects.get(id = announcement_id)
    announcement.delete()
    return HttpResponseRedirect(reverse('announcement_index')) 
        
    
