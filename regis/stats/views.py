# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext

from regis.models import Student, Assignment, Department, Receipt, Status, Company
from commons.decorators import sa_admin_required
from commons.academia import get_recent_worktraining_year, get_next_worktraining_year
from committee.models import ProposedCompany
from committee.student_stat import find_selection_statistics, get_assigned_students, get_unassigned_students, get_students_with_companies

from std.models import CoOpCompany, CoOpRequest, CoOpStudentDetail

currentpage = 'regis'

def find_department_sel_stat(year=None):
    if not year:
        year = get_next_worktraining_year()
    dept_sel_stat = []
    for d in Department.objects.all():
        dept_sel_stat.append(
            {'department': d,
             'stat': find_selection_statistics(d, year)})
    return dept_sel_stat


@sa_admin_required
def assignment_details_by_dept(request):
    dept_sel_stat = find_department_sel_stat()
    
    return render_to_response("regis/stats/department_details.html",
                              {'dept_sel_stat': dept_sel_stat,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def assignment_stats(request, year=None):
    if not year:
        year = get_next_worktraining_year()
    else:
        year = int(year)

    dept_sel_stat = find_department_sel_stat(year)
    
    for dept in dept_sel_stat:
        dept['stat']['num_no_assigned'] = dept['stat']['num_students']-dept['stat']['num_assigned']
            
    return render_to_response("regis/stats/departments.html",
                              {'dept_sel_stat': dept_sel_stat,
                               'year': year,
                               'prev_year': year-1,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def show_std_list(request,dep_code,role,year=None):
    stdinfo = 1
    if not year:
        year = get_next_worktraining_year()
    else:
        year = int(year)
    dep = Department.objects.filter(code=dep_code)
    department = dep[0]

    with_assigned_company = False
    with_registration_type = False
    
    if role == 'all_std':
        showlist = Student.all_registered_for_year(year,department)
        head = u"รายชื่อของนิสิตทั้งหมดใน ภาค"+department.name
    elif role == "assigned":
        showlist = get_students_with_companies(department,year)
        head = u"รายชื่อของนิสิตที่ได้สถานประกอบการแล้ว ภาค"+department.name
        with_assigned_company = True
        with_registration_type = True
    elif role == "no_assigned":
        showlist = get_unassigned_students(department,year)
        head = u"รายชื่อของนิสิตที่ยังไม่ได้สถานประกอบการ ภาค"+department.name
    elif role =="available_positions":
        receipts = Receipt.objects.filter(department=department,
                                          year=year).all()
        showlist = [r for r in receipts if r.num_available_positions()>0]
        #showlist = [r for r in receipts if r.available]
        head = u"ตำแหน่งที่ว่างของสถานประกอบการ ภาค"+department.name
        stdinfo = 0
        
    return render_to_response("regis/stats/showlist.html",
                              {'showlist': showlist,
                               'head': head,
                               'stdinfo':stdinfo,
                               'year': year,
                               'with_assigned_company': with_assigned_company,
                               'with_registration_type': with_registration_type,
                               'currentpage': currentpage },
                              context_instance=RequestContext(request))


def add_to_stat(student, company, name, name_map, stat):
    department = student.department
    if name in name_map:
        num = name_map[name]
    else:
        num = len(stat)
        name_map[name] = num
        stat.append({'company': company, 'stat':{}})
        
    if department.code not in stat[num]['stat']:
        stat[num]['stat'][department.code] = 1
    else:
        stat[num]['stat'][department.code] += 1


def normalize_company_name(name):
    name = name.strip().lower()
    deleting = [' ',u'็',u'้',u'์',u'จำกัด',u'(มหาชน)']
    for d in deleting:
        name = name.replace(d,'')
    prefixs = [u'บ.', u'บริษัท', u'บจก.', u'บมจ.']
    for p in prefixs:
        if name.startswith(p):
            name = name[len(p):].strip()
    return name
            
@sa_admin_required
def assignment_company_stats(request, year=None):
    if not year:
        year = get_next_worktraining_year() - 1
    else:
        year = int(year)
    
    assignments = Assignment.objects.filter(year=year)

    company_name_map = dict()
    student_company_name_map = dict()

    company_stat = []
    student_company_stat = []
    
    for a in assignments:
        student = a.student
        if a.company != None:
            company_name = a.company.name

            add_to_stat(student, a.company, company_name,
                        company_name_map, company_stat)
            
        else:
            if a.company_request != None:
                company_name = normalize_company_name(a.company_request.company_name)
                company = a.company_request
            else:
                company_name = normalize_company_name(a.company_direct_application.company_name)
                company = a.company_direct_application

            add_to_stat(student, company, company_name,
                        student_company_name_map, student_company_stat)

    departments = Department.objects.order_by('code').all()
    report_company_stat = []
    report_student_company_stat = []

    for r,stat in [(report_company_stat, company_stat),
                   (report_student_company_stat, student_company_stat)]:
        for s in stat:
            rstats = []
            for d in departments:
                if d.code in s['stat']:
                    rstats.append(s['stat'][d.code])
                else:
                    rstats.append(0)
            r.append({'company': s['company'], 'stat': rstats})

    report_student_company_stat = sorted(report_student_company_stat,
                                         key=lambda s: normalize_company_name(s['company'].company_name))
            
    return render_to_response("regis/stats/assignment_companies.html",
                              { 'year': year,
                                'company_stat': report_company_stat,
                                'student_company_stat': report_student_company_stat,
                                'departments': departments,
                                'currentpage': currentpage },
                              context_instance=RequestContext(request))

@sa_admin_required
def coop_stats_by_dept(request):
    departments = Department.objects.order_by('code').all()

    current_year = get_next_worktraining_year()
    year = current_year
    if request.GET.get('year','0')!='0':
        try:
            year = int(request.GET['year'])
        except:
            year = current_year
    
    stats = dict([(d.id,{'department': d,
                         'company_count': 0,
                         'request_count': 0,
                         'approved_request_count': 0,
                         'rejected_request_count': 0}) for d in departments])

    for c in CoOpCompany.objects.filter(year=year):
        stats[c.department_id]['company_count'] += 1

    for r in CoOpRequest.objects.filter(year=year):
        stats[r.department_id]['request_count'] += 1
        if r.is_approved_by_committee:
            stats[r.department_id]['approved_request_count'] += 1
        elif r.is_approved_by_committee != None:
            stats[r.department_id]['rejected_request_count'] += 1

    sorted_stats = [stats[d.id] for d in departments]
    
    return render_to_response("regis/stats/coop.html",
                              {'stats': sorted_stats,
                               'year': year,
                               'current_year': current_year,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

        
@sa_admin_required
def coop_report(request, year, semester):
    internship_year = int(year)
    internship_semester = int(semester)
    y = internship_year
    if internship_semester == 2:
        y +=1

    co_op_requests = (CoOpRequest
                      .objects
                      .filter(year=y,
                              internship_semester=internship_semester,
                              is_accepted=True)
                      .select_related('student','co_op_company')
                      .order_by('student__student_id')
                      .all())

    student_details = { d.student.student_id: d
                        for d in CoOpStudentDetail.objects.select_related('student').filter(year=y).all() }

    for r in co_op_requests:
        s = r.student
        if s.student_id in student_details:
            s.detail = student_details[s.student_id]
        else:
            s.detail = None
    
    return render_to_response("regis/stats/coop_report.html",
                              { 'internship_year': internship_year,
                                'internship_semester': internship_semester,

                                'co_op_requests': co_op_requests,
                                
                                'year': year,
                                'currentpage':currentpage },
                              context_instance=RequestContext(request))

        
            
            
