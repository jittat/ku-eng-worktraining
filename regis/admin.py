from django.contrib import admin

from models import Student, Company, Assignment, Department, Announcement, DepartmentNameInRequestLetter

class AssignmentInline(admin.TabularInline):
    model = Assignment
    extra = 1

class StudentAdmin(admin.ModelAdmin):
    inlines = (AssignmentInline,)

class CompanyAdmin(admin.ModelAdmin):
    inlines = (AssignmentInline,)

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'short_name')
    list_editable = ('short_name',)
admin.site.register(Student, StudentAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(DepartmentNameInRequestLetter)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Announcement)

