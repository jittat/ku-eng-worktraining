# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django import forms
from django.template import RequestContext, loader, Context
import math, csv
from commons.academia import get_current_academic_year, get_next_worktraining_year

from regis.models import Student, Department, Assignment, Status, Receipt
from evaluations.models import Evaluation
from commons.decorators import admin_or_committee_required, sa_admin_required
from commons.academia import get_recent_worktraining_year, get_next_worktraining_year

from committee.student_stat import find_selection_statistics, get_assigned_students, get_unassigned_students, get_students_with_companies

currentpage = 'department'
number_student_per_page = 25.0

@sa_admin_required
def index(request):
    return HttpResponseRedirect(reverse('regis_department_list'))

@sa_admin_required
def list(request):
    departments = Department.objects.all()
    return render_to_response("regis/department/list.html",
                              { 'departments': departments,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))

class filter_internship_year(forms.Form):
    start_year = 2553
    end_year = get_recent_worktraining_year() + 2
    YEAR_CHOICE = []
    for year in range(start_year,end_year):
        YEAR_CHOICE.append([year,year])
    YEAR_CHOICE.reverse()
    year = forms.ChoiceField(choices=YEAR_CHOICE,label=u'ปีการศึกษา')


@admin_or_committee_required
def detail(request, code, is_committee=False):

    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()
    next_year = get_recent_worktraining_year()+1
    department = get_object_or_404(Department,code=code)
    stat = department.assignment_stat()
    return render_to_response("regis/department/detail.html",
                              { 'department': department,
                                'stat': stat,
                                'next_year':next_year,
                                'currentpage':currentpage,
                                'is_committee': is_committee },
                              context_instance=RequestContext(request))

@admin_or_committee_required
def detail_listall_std(request,code,page_id):
    year = get_recent_worktraining_year()+1
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    is_committee = request.user.userprofile.is_committee_member

    department = get_object_or_404(Department,code=code)
    stat = department.assignment_stat()
    students = department.current_students()
    total_std = len(students)
    total_page = int(math.ceil(total_std/number_student_per_page))
    pages = []
    for i in range(1,total_page+1):
        pages.append(i)

    query_offset = (int(page_id)*int(number_student_per_page))-int(number_student_per_page)

    students = students[query_offset:int(page_id)*int(number_student_per_page)]


    return render_to_response("regis/department/detail_listall_std.html",
                              { 'department': department,
                                'students': students,
                                'stat': stat,
                                'pages':pages,
                                'next_year':year,
                                'page_number':int(page_id),
                                'total_std':total_std,
                                'is_committee': is_committee,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))


@admin_or_committee_required
def detail_unassigned_std(request,code,year,page_id):
    next_year = get_recent_worktraining_year()+1
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    if request.method=='POST':
        form = filter_internship_year(request.POST)
        if form.is_valid():
            year = form.cleaned_data['year']
            next_year = year
    else:
        if year and year != '0':
            next_year = year
        form = filter_internship_year(initial={'year':year})
       
    is_committee = request.user.userprofile.is_committee_member
    
    department = get_object_or_404(Department,code=code)
    stat = department.assignment_stat()
    assigned_student_ids = set([s.id for s in
                                get_students_with_companies(department, next_year)])
    unassigned_students = [s for s in department.current_students(next_year)
                           if s.id not in assigned_student_ids]
    total_std = len(unassigned_students)
    total_page = int(math.ceil(total_std/number_student_per_page))
    pages = []
    for i in range(1,total_page+1):
        pages.append(i)

    query_offset = (int(page_id)*int(number_student_per_page))-int(number_student_per_page)

    unassigned_students = unassigned_students[query_offset:int(page_id)*int(number_student_per_page)]

    std_num = query_offset + 1
    for s in unassigned_students:
        s.number = std_num
        std_num += 1
    
    return render_to_response("regis/department/detail_unassigned_std.html",
                              { 'department': department,
                                'stat': stat,
                                'pages':pages,
                                'page_number':int(page_id),
                                'year':year,
                                'next_year':next_year,
                                'form':form,
                                'total_std':total_std,
                                'unassigned_students': unassigned_students ,
                                'is_committee': is_committee,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))


@admin_or_committee_required
def detail_assigned_std(request,code,year,page_id):
    #next_year = get_recent_worktraining_year()+1
    next_year = year
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    form = filter_internship_year(request.POST)
    
    if request.method=='POST':
        if form.is_valid():
            year = form.cleaned_data['year']
            next_year = year
    else:
        form = filter_internship_year(initial={'year': year})
            
       
    is_committee = request.user.userprofile.is_committee_member

    if code!='0':
        department = get_object_or_404(Department,code=code)
        assigned_students = get_students_with_companies(department, next_year)
    else:
        assigned_students = []
        for d in Department.objects.all():
            assigned_students += get_students_with_companies(d, next_year)
        department = d
    total_std = len(assigned_students)
    total_page = int(math.ceil(total_std/number_student_per_page))
    pages = []
    for i in range(1,total_page+1):
        pages.append(i)

    if int(page_id) != 0:
        query_offset = (int(page_id)*int(number_student_per_page))-int(number_student_per_page)

        assigned_students = assigned_students[query_offset:int(page_id)*int(number_student_per_page)]
    

    return render_to_response("regis/department/detail_assigned_std.html",
                              { 'department': department,
                                'pages':pages,
                                'page_number':int(page_id),
                                'year':year,
                                'next_year':next_year,
                                'form':form,
                                'total_std':total_std,
                                'assigned_students': assigned_students,

                                'shows_departments': (code == '0'),
                                
                                'is_committee': is_committee,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))


@admin_or_committee_required
def detail_assigned_std_label_report(request,code,year):
    next_year = year
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    department = get_object_or_404(Department,code=code)
    assigned_students = get_students_with_companies(department, next_year)

    from reports import build_evaluation_label_pdf_response

    return build_evaluation_label_pdf_response(assigned_students,
                                               department,
                                               next_year,
                                               'eval-labels'+ department.code +'.pdf')


@admin_or_committee_required
def detail_all_std_label_report(request,code,year):
    next_year = year
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    department = get_object_or_404(Department,code=code)
    students = department.current_students(next_year)

    from reports import build_evaluation_label_pdf_response

    return build_evaluation_label_pdf_response(students,
                                               department,
                                               next_year,
                                               'eval-labels'+ department.code +'.pdf')


@admin_or_committee_required
def detail_assigned_std_list_report(request,code,year):
    next_year = year
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    department = get_object_or_404(Department,code=code)
    assigned_students = get_students_with_companies(department, next_year)

    from reports import build_assigned_student_list_pdf_response

    return build_assigned_student_list_pdf_response(assigned_students, department)


@admin_or_committee_required
def detail_no_evaluation(request,code,year,page_id):
    next_year = get_recent_worktraining_year()+1
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    is_committee = request.user.userprofile.is_committee_member
    
    if request.method=='POST':
        form = filter_internship_year(request.POST)
        if form.is_valid():
            year = form.cleaned_data['year']
    else:
        form = filter_internship_year(initial={'year':year})
            
    department = get_object_or_404(Department,code=code)
    students = department.current_students(year)
    lststudent = []
    for student in students:
        if not student.has_registered(year):
            continue
        assignment = student.assignment_for_year(year)
        if not assignment:
            continue
        student.assignment = assignment
        evaluations = Evaluation.objects.filter(assignment=assignment).order_by('-received_at')
        if not evaluations:
            student.intern_company = student.intern_company_for_year(year)
            lststudent.append(student)

    total_std = len(lststudent)
    total_page = int(math.ceil(total_std/number_student_per_page))
    pages = []
    for i in range(1,total_page+1):
        pages.append(i)

    query_offset = (int(page_id)*int(number_student_per_page))-int(number_student_per_page)

    lststudent = lststudent[query_offset:int(page_id)*int(number_student_per_page)]


    return render_to_response("regis/department/detail_no_evaluation.html",
                              { 'department': department,
                                'pages':pages,
                                'page_number':int(page_id),
                                'total_std':total_std,
                                'students': lststudent,
                                'year': year,
                                'next_year':next_year,
                                'form':form,
                                'is_committee': is_committee,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))


@admin_or_committee_required
def detail_registered_std(request, code):
    year = get_recent_worktraining_year()+1
    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    is_committee = request.user.userprofile.is_committee_member

    department = get_object_or_404(Department,code=code)
    students = Student.all_registered_for_year(year,department)
    total_std = len(students)

    under_credit_students = [
        s for s in students if not s.is_eligible_to_apply()
        ]

    regtype_counts = Status.find_regtype_stat(students)

    return render_to_response("regis/department/detail_registered_std.html",
                              {'department': department,
                               'currentpage':currentpage,
                               'students':students,
                               'under_credit_students':under_credit_students,

                               'regtype_counts': regtype_counts,

                               'next_year': year,
                                'is_committee': is_committee,
                               'total_std':total_std}, 
                              context_instance=RequestContext(request))


def csv(content,filename):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename='+filename+'.csv'    
    content = '\xef\xbb\xbf' + content.encode("utf-8")
    response.content = content
    return response

@sa_admin_required
def export_csv_all(request):
    content = u'รหัสนิสิต,ชื่อ-สกุล,ภาควิชา,E-mail,เบอร์โทรศัพท์\n'
    departments = Department.objects.all()
    for department in departments:
        students = department.current_students().order_by('-student_id')
        for student in students:
            if student.has_registered_for_next_year():
                content += str(student.student_id)+','+student.prefix+student.first_name+' '+student.last_name+','+department.name+','+str(student.email)+','+str(student.tel_no)+'\n'
                
    return csv(content, 'student_list_all')

@admin_or_committee_required
def export_csv_dep(request, code):

    if not request.user.userprofile.is_admin_or_department_committee(code):
        return HttpResponseForbidden()

    department = get_object_or_404(Department,code=code)
    students = department.current_students()
    content = u'รหัสนิสิต,ชื่อ-สกุล,E-mail,เบอร์โทรศัพท์\n'
    for student in students:
        if student.has_registered_for_next_year():
            content += str(student.student_id)+','+student.prefix+student.first_name+' '+student.last_name+','+str(student.email)+','+str(student.tel_no)+'\n'
            
    return csv(content,'student_list_'+code)

#Function for export csv total_student_requested and total_student_assigned
def get_content(mode,departments,records):
    if records.count() > 0:
        #Initial variable 
        init_company_id = records[0].company.id
        company_name = records[0].company.name
        sum_row = 0
        sum_col = {}
        counter = {}
        #First row (header)
        content = u'สถานประกอบการ,'
        for department in departments:
            content += department.name + ','
            sum_col[department.id] = 0
            counter[department.id] = 0
        content += u'ผลรวม'+'\n'
        #Loop get record row 
        for record in records:
            if record.company.id != init_company_id:
                content += '"'+company_name.replace('"','\"\"')+'",'
                for department in departments:
                    if counter.has_key(department.id):
                        content += str(counter[department.id]) + ','
                        sum_row += counter[department.id]
                    else:
                        content += '0,'
                content += str(sum_row)+'\n' #Show sum in each row          
                #Reset all counter for each company
                sum_row = 0
                counter = {}
                for department in departments:
                    counter[department.id] = 0
                init_company_id = record.company.id
                company_name = record.company.name
                
            
            if mode == 'requested':    
                counter[record.department.id] += record.total_student
                sum_col[record.department.id] += record.total_student
            elif mode == 'assigned':
                if record.student.department:
                    counter[record.student.department.id] += 1
                    sum_col[record.student.department.id] += 1
                else:
                    print 'student id '+str(record.student.student_id)+' has not been assigned department'   
        #Get last record row 
        content += company_name.replace('"','\"\"') + ' '+str(init_company_id)+ ','
        for department in departments:
            if counter.has_key(department.id):
                content += str(counter[department.id]) + ','
                sum_row += counter[department.id]
            else:
                content += '0,'
        content += str(sum_row)+'\n' #Show sum in last row
        #Loop show sum in each column
        content += u'ผลรวม,'
        for department in departments:
            content += str(sum_col[department.id]) + ','
    else:
        content = ''
        
    return content

@sa_admin_required
def export_csv_total_student_requested(request):
    nextyear = get_next_worktraining_year()
    departments = Department.objects.all()
    receipts = Receipt.objects.filter(year=nextyear).select_related('student').order_by('company')

    content = get_content('requested', departments, receipts)   
    #Return csv file
    return csv(content, 'total_requested')

@sa_admin_required
def export_csv_total_student_assigned(request):
    nextyear = get_next_worktraining_year()
    departments = Department.objects.all()
    assignments = Assignment.objects.filter(year=nextyear).select_related('student').order_by('company')
    
    content = get_content('assigned', departments, assignments)
    #Return csv file
    return csv(content, 'total_assigned')
