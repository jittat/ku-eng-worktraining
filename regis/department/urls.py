from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'regis.department.views',

    url(r'^$','index',name='regis_department_index'),
    url(r'^list/$', 'list', name='regis_department_list'),

    url(r'^(?P<code>\d+)/$',
        'detail', 
        name='regis_department_detail'),
    url(r'^(?P<code>\d+)/unassigned/(?P<year>\d+)/page/(?P<page_id>\d+)/$',
        'detail_unassigned_std', 
        name='regis_department_unassigned_std'),
    url(r'^(?P<code>\d+)/assigned/(?P<year>\d+)/page/(?P<page_id>\d+)/$',
        'detail_assigned_std', 
        name='regis_department_assigned_std'),
    url(r'^(?P<code>\d+)/all/page/(?P<page_id>\d+)/$',
        'detail_listall_std', 
        name='regis_department_listall_std'),
    url(r'^(?P<code>\d+)/no_evaluation/(?P<year>\d+)/page/(?P<page_id>\d+)/$',
        'detail_no_evaluation', name='regis_department_no_evaluation'),
    url(r'^(?P<code>\d+)/registered/$',
        'detail_registered_std',
        name='regis_department_registered_std'),

    url(r'^(?P<code>\d+)/assigned/(?P<year>\d+)/reports/list/$',
        'detail_assigned_std_list_report',
        name='regis_department_assigned_std_list_report'),

    url(r'^(?P<code>\d+)/assigned/(?P<year>\d+)/reports/labels/$',
        'detail_assigned_std_label_report',
        name='regis_department_assigned_std_label_report'),

    url(r'^(?P<code>\d+)/all/(?P<year>\d+)/reports/labels/$',
        'detail_all_std_label_report',
        name='regis_department_all_std_label_report'),

    url(r'^export_csv/all$', 'export_csv_all', name='export_csv_all'),
    url(r'^export_csv/(?P<code>\d+)$', 'export_csv_dep', name='export_csv_dep'),
    url(r'^export_total_requested/$', 'export_csv_total_student_requested', name='export_csv_total_student_requested'), 
    url(r'^export_total_assigned/$', 'export_csv_total_student_assigned', name='export_csv_total_student_assigned'), 
)
