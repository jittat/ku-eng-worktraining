from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

from commons.models import Configuration
from commons.decorators import sa_admin_required
from regis.models import Conditions
from django.forms import ModelForm

currentpage = 'web_system'


@sa_admin_required
def index(request):
    configs = Configuration.objects.all()

    return render_to_response('regis/config/index.html',
                              { 'configs': configs ,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def update(request,config_id):
    config = get_object_or_404(Configuration,pk=config_id)
    config.value = request.POST['value']
    v = config.get_value()
    config.set_value(v)
    config.save()

    return HttpResponseRedirect(reverse('regis_config_index'))

class ConditionsForm(ModelForm):
    class Meta:
        model = Conditions
        fields = ['detail']

def terms_edit(request):
    if request.method == 'POST':
        form = ConditionsForm(request.POST)
        if form.is_valid():
            try:
                conditions = Conditions.objects.all()[0]
            except IndexError:
                conditions = Conditions()

            conditions.detail = form.cleaned_data['detail']
            conditions.save()
            return HttpResponseRedirect(reverse('regis_config_index'))

    else:
        try:
            conditions = Conditions.objects.all()[0]
            form = ConditionsForm(instance=conditions)
        except IndexError:
            form = ConditionsForm()

    return render_to_response('regis/config/terms_edit.html', {'form': form}, context_instance=RequestContext(request))


