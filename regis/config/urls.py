from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'regis.config.views',

    url(r'^$', 'index', name='regis_config_index'),
    url(r'^update/(\d+)/$', 'update', name='regis_config_update'),
    url(r'^terms/', 'terms_edit', name='regis_config_terms'),
)
