# -*- encoding: utf-8 -*-

from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding field 'Student.student_year'
        db.add_column('regis_student', 'student_year', orm['regis.student:student_year'])
        
        # Adding field 'Student.student_tel'
        db.add_column('regis_student', 'student_tel', orm['regis.student:student_tel'])
        
        # Changing field 'Student.email'
        # (to signature: django.db.models.fields.EmailField(max_length=100, null=True))
        db.alter_column('regis_student', 'email', orm['regis.student:email'])
        
    
    
    def backwards(self, orm):
        
        # Deleting field 'Student.student_year'
        db.delete_column('regis_student', 'student_year')
        
        # Deleting field 'Student.student_tel'
        db.delete_column('regis_student', 'student_tel')
        
        # Changing field 'Student.email'
        # (to signature: django.db.models.fields.CharField(max_length=100, null=True))
        db.alter_column('regis_student', 'email', orm['regis.student:email'])
        
    
    
    models = {
        'regis.assignment': {
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.company': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'regis.department': {
            'department_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'department_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.student': {
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'student_tel': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '11', 'null': 'True'}),
            'student_year': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'})
        }
    }
    
    complete_apps = ['regis']
