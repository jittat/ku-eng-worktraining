# -*- encoding: utf-8 -*-

from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding field 'Company.collab_name'
        db.add_column('regis_company', 'collab_name', orm['regis.company:collab_name'])
        
        # Adding field 'Company.fax_no'
        db.add_column('regis_company', 'fax_no', orm['regis.company:fax_no'])
        
        # Adding field 'Company.signer_name'
        db.add_column('regis_company', 'signer_name', orm['regis.company:signer_name'])
        
        # Adding field 'Company.tel_no'
        db.add_column('regis_company', 'tel_no', orm['regis.company:tel_no'])
        
        # Adding field 'Company.duration'
        db.add_column('regis_company', 'duration', orm['regis.company:duration'])
        
        # Adding field 'Company.address'
        db.add_column('regis_company', 'address', orm['regis.company:address'])
        
    
    
    def backwards(self, orm):
        
        # Deleting field 'Company.collab_name'
        db.delete_column('regis_company', 'collab_name')
        
        # Deleting field 'Company.fax_no'
        db.delete_column('regis_company', 'fax_no')
        
        # Deleting field 'Company.signer_name'
        db.delete_column('regis_company', 'signer_name')
        
        # Deleting field 'Company.tel_no'
        db.delete_column('regis_company', 'tel_no')
        
        # Deleting field 'Company.duration'
        db.delete_column('regis_company', 'duration')
        
        # Deleting field 'Company.address'
        db.delete_column('regis_company', 'address')
        
    
    
    models = {
        'regis.assignment': {
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.company': {
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'collab_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'department_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'department_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.student': {
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'student_tel': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '11', 'null': 'True'}),
            'student_year': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'})
        }
    }
    
    complete_apps = ['regis']
