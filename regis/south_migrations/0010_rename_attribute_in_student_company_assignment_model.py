# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):

        db.rename_column('regis_department', 'department_name', 'name')
        db.rename_column('regis_assignment', 'assignment_year', 'year')
        db.rename_column('regis_student', 'student_tel', 'tel_no')

    def backwards(self, orm):

        db.rename_column('regis_department', 'name', 'department_name')
        db.rename_column('regis_assignment', 'year', 'assignment_year')
        db.rename_column('regis_student', 'tel_no', 'student_tel')

    models = {
        'regis.assignment': {
            'Meta': {'object_name': 'Assignment'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'collab_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'department_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        },
        'regis.student': {
            'Meta': {'object_name': 'Student'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']", 'through': "orm['regis.Assignment']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'student_year': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '11', 'null': 'True'})
        }
    }

    complete_apps = ['regis']
