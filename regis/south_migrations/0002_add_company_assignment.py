
from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding model 'Assignment'
        db.create_table('regis_assignment', (
            ('id', orm['regis.assignment:id']),
            ('company', orm['regis.assignment:company']),
            ('student', orm['regis.assignment:student']),
        ))
        db.send_create_signal('regis', ['Assignment'])
        
    
    
    def backwards(self, orm):
        
        # Deleting model 'Assignment'
        db.delete_table('regis_assignment')
        
    
    
    models = {
        'regis.assignment': {
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.company': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'regis.student': {
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }
    
    complete_apps = ['regis']
