
from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding model 'Company'
        db.create_table('regis_company', (
            ('id', orm['regis.Company:id']),
            ('short_name', orm['regis.Company:short_name']),
            ('name', orm['regis.Company:name']),
        ))
        db.send_create_signal('regis', ['Company'])
        
        # Adding model 'Student'
        db.create_table('regis_student', (
            ('id', orm['regis.Student:id']),
            ('student_id', orm['regis.Student:student_id']),
            ('first_name', orm['regis.Student:first_name']),
            ('last_name', orm['regis.Student:last_name']),
        ))
        db.send_create_signal('regis', ['Student'])
        
    
    
    def backwards(self, orm):
        
        # Deleting model 'Company'
        db.delete_table('regis_company')
        
        # Deleting model 'Student'
        db.delete_table('regis_student')
        
    
    
    models = {
        'regis.company': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'regis.student': {
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }
    
    complete_apps = ['regis']
