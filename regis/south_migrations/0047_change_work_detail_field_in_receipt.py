# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Receipt.work_detail'
        db.alter_column('regis_receipt', 'work_detail_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['regis.WorkDetail']))


    def backwards(self, orm):
        
        # Changing field 'Receipt.work_detail'
        db.alter_column('regis_receipt', 'work_detail_id', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['regis.WorkDetail']))


    models = {
        'regis.announcement': {
            'Meta': {'object_name': 'Announcement'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'file': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'regis.assignment': {
            'Meta': {'object_name': 'Assignment'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.conditions': {
            'Meta': {'object_name': 'Conditions'},
            'detail': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        },
        'regis.departmentnameinrequestletter': {
            'Meta': {'object_name': 'DepartmentNameInRequestLetter'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_to_be_requested': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'regis.education': {
            'Meta': {'object_name': 'Education'},
            'gpax': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_credit': ('django.db.models.fields.IntegerField', [], {}),
            'upto_semester': ('django.db.models.fields.IntegerField', [], {}),
            'upto_year': ('django.db.models.fields.IntegerField', [], {})
        },
        'regis.receipt': {
            'Meta': {'object_name': 'Receipt'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receipts'", 'to': "orm['regis.Company']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receipts'", 'to': "orm['regis.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_student': ('django.db.models.fields.IntegerField', [], {}),
            'work_address': ('django.db.models.fields.TextField', [], {}),
            'work_description': ('django.db.models.fields.TextField', [], {}),
            'work_detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'receipts'", 'to': "orm['regis.WorkDetail']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'max_length': '5'})
        },
        'regis.status': {
            'Meta': {'object_name': 'Status'},
            'attended_orientation': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_registered': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'request_form_checkcode': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True'}),
            'sent_request_form': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'status'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'max_length': '5'})
        },
        'regis.student': {
            'Meta': {'object_name': 'Student'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']", 'null': 'True'}),
            'education': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['regis.Education']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_companies': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']", 'through': "orm['regis.Assignment']", 'symmetrical': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'prefix': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'})
        },
        'regis.workdetail': {
            'Meta': {'object_name': 'WorkDetail'},
            'beginning_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workdetails'", 'to': "orm['regis.Company']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dressing_description': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'other_description': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        }
    }

    complete_apps = ['regis']
