# -*- encoding: utf-8 -*-

from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding field 'Department.department_id'
        db.add_column('regis_department', 'department_id', orm['regis.department:department_id'])
        
        # Changing field 'Student.department'
        # (to signature: django.db.models.fields.related.ForeignKey(to=orm['regis.Department']))
        db.alter_column('regis_student', 'department_id', orm['regis.student:department'])
        
    
    
    def backwards(self, orm):
        
        # Deleting field 'Department.department_id'
        db.delete_column('regis_department', 'department_id')
        
        # Changing field 'Student.department'
        # (to signature: django.db.models.fields.related.ForeignKey(to=orm['regis.Department'], null=True))
        db.alter_column('regis_student', 'department_id', orm['regis.student:department'])
        
    
    
    models = {
        'regis.assignment': {
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.company': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'regis.department': {
            'department_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'department_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.student': {
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'email': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }
    
    complete_apps = ['regis']
