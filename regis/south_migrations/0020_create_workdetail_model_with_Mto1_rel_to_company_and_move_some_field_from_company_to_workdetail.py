# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'WorkDetail'
        db.create_table('regis_workdetail', (
            ('description', self.gf('django.db.models.fields.TextField')(default=None, null=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(related_name='workdetail', to=orm['regis.Company'])),
            ('signer_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=200, null=True)),
            ('year', self.gf('django.db.models.fields.IntegerField')(default=None, max_length=5, null=True)),
            ('duration', self.gf('django.db.models.fields.CharField')(default=None, max_length=200, null=True)),
            ('contact_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=200, null=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('regis', ['WorkDetail'])

        # Changing field 'Announcement.file'
        db.alter_column('regis_announcement', 'file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True))

        # Deleting field 'Company.signer_name'
        db.delete_column('regis_company', 'signer_name')

        # Deleting field 'Company.duration'
        db.delete_column('regis_company', 'duration')

        # Deleting field 'Company.contact_name'
        db.delete_column('regis_company', 'contact_name')
    
    
    def backwards(self, orm):
        
        # Deleting model 'WorkDetail'
        db.delete_table('regis_workdetail')

        # Changing field 'Announcement.file'
        db.alter_column('regis_announcement', 'file', self.gf('django.db.models.fields.files.FileField')(max_length=100))

        # Adding field 'Company.signer_name'
        db.add_column('regis_company', 'signer_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=200, null=True), keep_default=False)

        # Adding field 'Company.duration'
        db.add_column('regis_company', 'duration', self.gf('django.db.models.fields.CharField')(default=None, max_length=200, null=True), keep_default=False)

        # Adding field 'Company.contact_name'
        db.add_column('regis_company', 'contact_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=200, null=True), keep_default=False)
    
    
    models = {
        'regis.announcement': {
            'Meta': {'object_name': 'Announcement'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'file': ('django.db.models.fields.files.FileField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'regis.assignment': {
            'Meta': {'object_name': 'Assignment'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': "orm['regis.Student']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        },
        'regis.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'Meta': {'object_name': 'Department'},
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'})
        },
        'regis.education': {
            'Meta': {'object_name': 'Education'},
            'gpax': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_credit': ('django.db.models.fields.IntegerField', [], {}),
            'upto_semester': ('django.db.models.fields.IntegerField', [], {}),
            'upto_year': ('django.db.models.fields.IntegerField', [], {})
        },
        'regis.student': {
            'Meta': {'object_name': 'Student'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']", 'null': 'True'}),
            'education': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['regis.Education']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_companies': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']", 'through': "orm['regis.Assignment']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'prefix': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '11', 'null': 'True'})
        },
        'regis.workdetail': {
            'Meta': {'object_name': 'WorkDetail'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workdetail'", 'to': "orm['regis.Company']"}),
            'contact_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'max_length': '5', 'null': 'True'})
        }
    }
    
    complete_apps = ['regis']
