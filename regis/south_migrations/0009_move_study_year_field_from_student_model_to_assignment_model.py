# -*- encoding: utf-8 -*-

from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding field 'Assignment.assignment_year'
        db.add_column('regis_assignment', 'assignment_year', orm['regis.assignment:assignment_year'])
        
        # Deleting field 'Student.study_year'
        db.delete_column('regis_student', 'study_year')
        
    
    
    def backwards(self, orm):
        
        # Deleting field 'Assignment.assignment_year'
        db.delete_column('regis_assignment', 'assignment_year')
        
        # Adding field 'Student.study_year'
        db.add_column('regis_student', 'study_year', orm['regis.student:study_year'])
        
    
    
    models = {
        'regis.assignment': {
            'assignment_year': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '5', 'null': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.company': {
            'address': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True'}),
            'collab_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'fax_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'signer_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True'}),
            'tel_no': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '15', 'null': 'True'})
        },
        'regis.department': {
            'department_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '3', 'null': 'True'}),
            'department_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.student': {
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'student_tel': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '11', 'null': 'True'}),
            'student_year': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'})
        }
    }
    
    complete_apps = ['regis']
