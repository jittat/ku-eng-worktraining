
from south.db import db
from django.db import models
from worktraining.regis.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding model 'Department'
        db.create_table('regis_department', (
            ('id', orm['regis.department:id']),
            ('department_name', orm['regis.department:department_name']),
        ))
        db.send_create_signal('regis', ['Department'])
        
        # Adding field 'Student.department'
        db.add_column('regis_student', 'department', orm['regis.student:department'])
        
    
    
    def backwards(self, orm):
        
        # Deleting model 'Department'
        db.delete_table('regis_department')
        
        # Deleting field 'Student.department'
        db.delete_column('regis_student', 'department_id')
        
    
    
    models = {
        'regis.assignment': {
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Company']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['regis.Student']"})
        },
        'regis.company': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'regis.department': {
            'department_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'regis.student': {
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['regis.Department']", 'null': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intern_company': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['regis.Company']"}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'student_id': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }
    
    complete_apps = ['regis']
