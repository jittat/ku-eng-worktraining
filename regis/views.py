# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext

from regis.models import Student, Assignment, Department, Receipt, Status
from commons.decorators import sa_admin_required
from commons.academia import get_recent_worktraining_year, get_next_worktraining_year
from committee.models import ProposedCompany
from committee.student_stat import find_selection_statistics, get_assigned_students, get_unassigned_students, get_students_with_companies

currentpage = 'regis'

@sa_admin_required
def index(request):
    year = get_next_worktraining_year()

    departments = Department.objects.filter(is_hidden=False).order_by('name').all()
    
    student_stats = [{ 'department': d,
                       'students': d.current_students(),
                       'students_with_companies': get_students_with_companies(d, year) }
                     for d in departments]
    
    faculty_stat = {
        'student_count': Status.objects.filter(year=year,
                                               is_registered=True).count(),
        'assigned_count': sum([len(s['students_with_companies']) for s in student_stats])
    }
    faculty_stat['wait_count'] = faculty_stat['student_count'] - faculty_stat['assigned_count']
    if faculty_stat['student_count'] != 0:
        faculty_stat['assigned_count_percent'] = faculty_stat['assigned_count']*100.0 / faculty_stat['student_count']
        faculty_stat['wait_count_percent'] = faculty_stat['wait_count']*100.0 / faculty_stat['student_count']

    # proposed companies to be approved
    proposed_company_count = ProposedCompany.unapproved_company_count()

    return render_to_response("regis/index.html",            
                              { 'faculty_stat': faculty_stat,
                                'departments': departments,
                                'student_stats': student_stats,
                                'proposed_company_count': proposed_company_count,
                                'currentpage':currentpage },
                              context_instance=RequestContext(request))
    
