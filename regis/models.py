# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from commons.academia import get_current_academic_year, get_next_worktraining_year
from commons import academia 
import re

class Announcement(models.Model):
    title = models.CharField(max_length=255,verbose_name=u'หัวเรื่อง')
    description = models.TextField(help_text='Description of document.',verbose_name=u'รายละเอียด')

    created_at = models.DateTimeField(verbose_name=u'เวลาสร้าง',
                                      auto_now_add=True)
    file = models.FileField(
        upload_to='upload',
        null=True,
        default=None,
        blank=True)

    is_in_download_section = models.BooleanField(verbose_name=u'แสดงแยกในส่วนดาวน์โหลด',
                                                 default=False)

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.title

class Company(models.Model):
    name = models.CharField(max_length=200,
                            verbose_name=u'บริษัท')

    short_name = models.CharField(max_length=20,
                                  verbose_name=u'ชื่อย่อ')

    starting_char = models.CharField(max_length=1,
                                     default='')   # for company listing

    short_description = models.TextField(blank=True,
                                         default='',
                                         verbose_name=u'ข้อมูลเบื้องต้นเกี่ยวกับสถานประกอบการ')

    tel_no = models.CharField(max_length=100,
                              null=True,
                              default=None,
                              verbose_name=u'โทรศํพท์')
    fax_no = models.CharField(max_length=100,
                              null=True,
                              default=None,
                              verbose_name=u'โทรสาร')
    address = models.TextField(null=True,
                               default=None,
                               verbose_name=u'ที่อยู่')
    contact_name = models.CharField(max_length=200,
                                    blank=True,
                                    default='',
                                    verbose_name=u'ผู้ประสานงาน')
    contact_email = models.EmailField(max_length=100,
                                      blank=True,
                                      default='',
                                      verbose_name=u'Email ผู้ประสานงาน')
    signer_name = models.CharField(max_length=200,
                                   blank=True,
                                   default='',
                                   verbose_name=u'ผู้ลงนาม')

    created_at = models.DateTimeField(auto_now_add=True)

    # this field is mainly for sorting newly added companies at the end.
    is_new = models.BooleanField(default=False,
                                 blank=False,
                                 null=False,
                                 verbose_name=u'เป็นสถานประกอบการที่เพิ่มใหม่')

    def __unicode__(self):
        return self.name

    def assignments_for_year(self, year):
        return self.assignments.filter(year=year).select_related('student')

    def intern_students_for_year(self, year):
        assignments = self.assignments_for_year(year)
        students = [a.student for a in assignments]
        return students

    def workdetail_for_year(self,year):
        try:
            return self.workdetails.get(year=year)
        except WorkDetail.DoesNotExist:
            return None

    def one_line_address(self):
        if self.address:
            return self.address.replace('\n',' ').replace('\r',' ')
        else:
            return ''

    def province(self):
        try:
            items = self.one_line_address().split()
            if len(items) <= 1:
                return self.address
            else:
                province = items[-1]
                if re.match(r'^\d+$', province):
                    province = items[-2]
            return province
        except:
            return ''

    def update_starting_char(self):
        c = ''
        if self.name.startswith(u'บริษัท'):
            name = self.name[6:]
        elif self.name.startswith(u'บ.'):
            name = self.name[2:]
        else:
            name = self.name
        for ch in name.upper():
            if ((ch >= u'A' and ch <= u'Z') or
                (ch >= u'0' and ch <= u'9') or
                (ch >= u'ก' and ch <= u'ฮ')):
                c = ch
                break
        self.starting_char = c
        self.save()

    @staticmethod
    def all_containing_name(name):
        return Company.objects.filter(name__contains=name).all()

    @staticmethod
    def all_with_name(name):
        return Company.objects.filter(name=name).all()

    @staticmethod
    def all_new():
        return Company.objects.filter(is_new=True).all()

class WorkDetail(models.Model):
    beginning_date = models.DateField(blank=True,
                                      null=True,
                                      verbose_name=u"วันที่เริ่มฝึกงาน")
    end_date = models.DateField(blank=True,
                                null=True,
                                verbose_name=u"วันสิ้นสุดการฝึกงาน")
    dressing_description = models.TextField(blank=True,null=True,
                               default=None,
                               verbose_name=u'การแต่งกายในการฝึกงาน')
    other_description = models.TextField(blank=True,null=True,
                               default=None,
                               verbose_name=u'รายละเอียดอื่น ๆ ')
    year = models.IntegerField(null=True,
                               default=None,
                               verbose_name=u'ปีที่เข้ารับการฝึกงาน')
    company = models.ForeignKey(Company, related_name='workdetails')
    created_at = models.DateTimeField(auto_now_add=True)


class Department(models.Model):
    code = models.CharField(max_length=3,
                            null=True,
                            default=None,
                            verbose_name=u'รหัสภาควิชา')
    name = models.CharField(max_length=100,
                            null=True,
                            default=None,
                            verbose_name=u'ภาควิชา')
    eng_name = models.CharField(max_length=100,
                                blank=True,
                                default=None,
                                verbose_name=u'Department name')
    short_name = models.CharField(max_length=10,
                                  null=True,
                                  default=None,
                                  verbose_name=u'ชื่อย่อ')
    short_name_for_letters = models.CharField(max_length=50,
                                              null=True,
                                              default=None,
                                              verbose_name=u'ชื่อสั้นสำหรับจดหมาย')
    is_hidden = models.BooleanField(default=False,
                                    verbose_name=u'ซ่อนไม่ให้นิสิตเห็น')

    def current_students(self, year=None):
        if year==None:
            year = get_next_worktraining_year()
        students = self.student_set.filter(status__year__exact=year,
                                           status__is_registered__exact=True).all()
        return students

    def assignments_for_year(self, year):
        return Assignment.objects.filter(year=year,
                                         student__department=self).all()

    def unassigned_students(self,students=None):
        assignments = self.assignments_for_year(get_next_worktraining_year())

        assigned_ids = set([s.student_id for s in assignments])
        if not students:
            students = self.current_students()

        unassigned = []
        for student in students:
            if not student.id in assigned_ids:
                unassigned.append(student);
        return unassigned
    
    def unassigned_students_for_year(self, year):
        assignments = self.assignments_for_year(year)
    
        assigned_ids = set([s.student_id for s in assignments])
        students = self.current_students()
    
        unassigned = []
        for student in students:
            if not student.id in assigned_ids and student.has_registered(year):
                unassigned.append(student);
        return unassigned

    def assigned_students_for_year(self, year, copy_assignment=False):
        assignments = list(self.assignments_for_year(year).select_related('student'))

        if copy_assignment:
            for a in assignments:
                a.student.assignment = a
        students = [a.student for a in assignments]
        return students

    def assignment_stat(self):
        students = self.current_students()
        all = len(students)
        unassigned = len(self.unassigned_students(students))
        assigned = all - unassigned
        return { 'all': all,
                 'assigned': assigned,
                 'unassigned': unassigned }

    def __unicode__(self):
        return self.name


class DepartmentNameInRequestLetter(models.Model):
    """
    keeps a modification of department name in request letter, e.g.,
    "วิศวกรรมโยธา-ทรัพยากรน้ำ" will be requested as "วิศวกรรมทรัพยากรน้ำ."

    This is also used when the companies return the number of
    available positions to map available departments.
    """
    department = models.ForeignKey(Department,
                                   verbose_name=u'ภาควิชา')
    name_to_be_requested = models.CharField(max_length=100,
                                            verbose_name=u'ชื่อที่จะใช้ส่งเอกสาร')
    name_maps = {}
    rev_name_maps = {}

    def __unicode__(self):
        return u'%s as %s' % (self.department.name, self.name_to_be_requested)

    @staticmethod
    def cache_names():
        cls = DepartmentNameInRequestLetter
        if cls.name_maps == {}:
            for dn in DepartmentNameInRequestLetter.objects.all():
                try:
                    real_department = Department.objects.get(name=dn.name_to_be_requested)
                    cls.name_maps[dn.department.name] = { 
                        'name': dn.name_to_be_requested,
                        'department': real_department 
                        }
                    if real_department.id not in cls.rev_name_maps:
                        cls.rev_name_maps[real_department.id] = []
                    cls.rev_name_maps[real_department.id].append(dn.department)
                except:
                    pass


    @staticmethod
    def clean_dupplicate_names(name_list):
        cls = DepartmentNameInRequestLetter
        cls.cache_names()

        names = set()
        for n in name_list:
            if n in cls.name_maps:
                names.add(cls.name_maps[n]['name'])
            else:
                names.add(n)

        return [n for n in names]
        
    @staticmethod
    def to_requested_department(department):
        cls = DepartmentNameInRequestLetter
        cls.cache_names()

        if department.name in cls.name_maps:
            return cls.name_maps[department.name]['department']
        else:
            return department

    @staticmethod
    def reverse_departments(department):
        cls = DepartmentNameInRequestLetter
        cls.cache_names()

        if department.id in cls.rev_name_maps:
            return cls.rev_name_maps[department.id] + [department]
        else:
            return [department]

    class Meta:
        verbose_name = u'Department Name in Official Letters'
        verbose_name_plural = u'Department Names in Official Letters'


class Student(models.Model):
    student_id = models.CharField(max_length=15,
                                  verbose_name=u'รหัสนิสิต')
    prefix = models.CharField(max_length=10,
                              verbose_name=u'คำนำหน้า')
    first_name = models.CharField(max_length=100,
                                  verbose_name=u'ชื่อนิสิต')
    last_name = models.CharField(max_length=100,
                                 verbose_name=u'นามสกุลนิสิต')
    eng_prefix = models.CharField(max_length=10,
                                  verbose_name=u'prefix',
                                  blank=True)
    eng_first_name = models.CharField(max_length=100,
                                      verbose_name=u'First name',
                                      blank=True)
    eng_last_name = models.CharField(max_length=100,
                                     verbose_name=u'Last name',
                                     blank=True)
    email = models.EmailField(max_length=100,
                              null=True,
                              default=None)
    tel_no = models.CharField(max_length=50,
                              null=True,
                              default=None,
                              verbose_name=u'เบอร์โทรศัพท์')
    intern_companies = models.ManyToManyField(Company,through='Assignment')
    department = models.ForeignKey(Department,
                                   null=True,
                                   verbose_name=u'ภาควิชา')

    education = models.OneToOneField("regis.Education",null=True,blank=True)

    #accepted_conditions = models.NullBooleanField(verbose_name=u'ยอมรับเงื่อนไขการฝึกงาน',default=False)

    def received_evaluation(self):
        return len(self.evaluations.all()) != 0

    def full_name(self):
        return u'%s %s' % (self.first_name,
                           self.last_name)

    def eng_full_name(self):
        return u'%s %s %s' % (self.eng_prefix,
                              self.eng_first_name,
                              self.eng_last_name)

    def short_prefix(self):
        if self.prefix == u"นางสาว":
            return u"น.ส."
        else:
            return self.prefix
        
    def get_year(self):
        return get_current_academic_year() -2500 - int(self.student_id[:2]) +1

    def get_enrollment_year(self):
        return 2500 + int(self.student_id[:2])

    def assignment_for_year(self, year):
        assignments = self.assignments.filter(year=year).all()
        # TODO: handle data error when students has more than one company
        if len(assignments)!=0:
            return assignments[0]
        else:
            return None

    def intern_company_for_year(self, year):
        assignment = self.assignment_for_year(year)
        if assignment:
            return assignment.company
        else:
            return None

    def intern_company_for_current_year(self):
        return self.intern_company_for_year(get_current_academic_year())

    def intern_company_for_next_year(self):
        return self.intern_company_for_year(get_next_worktraining_year())

    def has_been_assigned(self, year):
        assignments = self.assignments.filter(year=year)
        return assignments.count() != 0

    def has_been_assigned_for_current_year(self):
        return self.has_been_assigned(get_current_academic_year())

    def has_not_been_assigned(self):
        assignments = self.assignments.all()
        return assignments.count() == 0

    def get_status(self, year):
        statuses = self.status.filter(year=year,is_registered=True)
        if statuses.count() != 0:
            return statuses[0]
        else:
            return None

    def has_registered(self, year):
        return self.get_status(year)!=None

    def has_registered_for_next_year(self):
        return self.has_registered(get_current_academic_year() + 1)
    
    def has_chosen_registration_type(self,year):
        status = self.get_status(year)
        return (status != None) and (status.registration_type != Status.NO_PREF_TYPE)
        
    def has_chosen_registration_type_for_next_year(self):
        return self.has_chosen_registration_type(get_current_academic_year() + 1)

    def registration_type_for_year(self, year):
        statuses = self.status.filter(year=year,is_registered=True)
        if statuses.count() == 0:
            return Status.NO_PREF_TYPE
        else:
            return statuses[0].registration_type

    def registration_type_for_next_year(self):
        return self.registration_type_for_year(get_current_academic_year() + 1)

    def has_sent_request_form(self, year):
        statuses = self.status.filter(year=year,sent_request_form=True)
        return statuses.count() != 0

    def has_sent_request_form_for_next_year(self):
        return self.has_sent_request_form(get_current_academic_year() + 1)

    def has_confirmed_email(self):
        try:
            confirmation = self.email_confirmation
        except ObjectDoesNotExist:
            confirmation = None
        return confirmation and (confirmation.is_confirmed)

    def save_registered_status(self,year,registration_type=None):
        statuses = self.status.filter(year=year)
        if statuses.count() != 0:
            status = statuses[0]
        else:
            status = Status()
            status.student_id = self.id
            status.year = year
        if registration_type != None:
            status.registration_type = registration_type
        status.is_registered = True
        status.save()
        return status
            
    def save_sent_request_form_status(self,year):
        status = self.status.filter(year=year)
        if status.count() != 0:
            status.sent_request_form = True
            status.save()
        else:
            status = Status()
            status.student_id = self.id
            status.year = year
            status.sent_request_form = True
            status.save()
    
    def get_evaluation(self): 
        evaluation = self.evaluations.all()
        return evaluation[0]       

    def has_to_apply(self):
        current_year = get_current_academic_year()
        return not self.has_been_assigned(current_year)

    def current_credits(self):
        if self.education:
            return self.education.total_credit
        else:
            return None

    def is_eligible_to_apply(self):
        """
        checks if a student can apply for internship.
       
        current condition is that the total credit is at least
        academia.REQUIRED_CREDITS_FOR_APPLICATION.
        """
        return self.current_credits() >= academia.REQUIRED_CREDITS_FOR_APPLICATION

    def is_personal_information_complete(self):
        return ((self.email and self.email!='') and
                (self.tel_no and self.tel_no!=''))

    def is_eligible_to_apply_more_direct_application(self, year=None):
        from commons.academia import can_student_apply_more_direct_application
        if year==None:
            year = get_next_worktraining_year()
        return can_student_apply_more_direct_application(self, year)

    def reasons_failed_to_apply_more_direct_application(self, year=None):
        from commons.academia import can_student_apply_more_direct_application_with_reason
        if year==None:
            year = get_next_worktraining_year()
        result = can_student_apply_more_direct_application_with_reason(self, year)
        return result[1]

    def is_eligible_to_apply_in_selection_round(self, year=None):
        from commons.academia import can_student_apply_in_selection_round
        if year==None:
            year = get_next_worktraining_year()
        return can_student_apply_in_selection_round(self, year)

    def reasons_failed_to_apply_in_selection_round(self, year=None):
        from commons.academia import can_student_apply_in_selection_round_with_reason
        if year==None:
            year = get_next_worktraining_year()
        result = can_student_apply_in_selection_round_with_reason(self, year)
        return result[1]

    def get_approved_company_request(self, year):
        all_company_request_items = self.request_set.filter(year=year).all()
        approved_company_request_items = [r for r in all_company_request_items 
                                 if r.is_active() and r.is_approved]
        if len(approved_company_request_items)!=0:
            return approved_company_request_items[0].company_request
        else:
            return None

    def get_chosen_company_direct_application(self, year):
        from std.models import CompanyDirectApplication
        
        company_direct_applications = CompanyDirectApplication.get_all_for(self, year)
        chosen_company_applications = [a for a in company_direct_applications if a.is_chosen]
        if len(chosen_company_applications)!=0:
            return chosen_company_applications[0]
        else:
            return None

    def get_working_self_appl_info(self, year):
        company_request = self.get_approved_company_request(year)
        direct_application = self.get_chosen_company_direct_application(year)
        return { 'company_request': company_request,
                 'direct_application': direct_application }

    def get_all_self_appl_info(self, year):
        from std.models import CompanyDirectApplication

        all_company_request_items = self.request_set.filter(year=year).all()
        all_company_direct_applications = CompanyDirectApplication.get_all_for(self, year)
        return { 'company_request_items': all_company_request_items,
                 'direct_applications': all_company_direct_applications }

    def has_active_self_appl(self, year, info=None):
        if not info:
            info = self.get_all_self_appl_info(year)
        
        has_active = False
        for item in info['company_request_items']:
            if item.is_active():
                has_active = True
        for a in info['direct_applications']:
            if a.is_active():
                has_active = True
        return has_active

    def __unicode__(self):
        return self.full_name()

    def get_email(self):
        if self.email and self.email!='':
            return self.email
        else:
            # return default KU email
            return 'b' + self.student_id[0:-1] + '@ku.ac.th'

    @staticmethod
    def all_by_id_name_department(student_id='',
                                  name='',
                                  department=''):
        students = Student.objects.all().order_by('student_id')
        if len(student_id) > 0:
            students = students.filter(student_id__startswith=student_id).all()
        if len(name) != 0:
            items = name.split()
            if len(items)==1:
                f = items[0]
                students = students.filter(first_name__contains=f).all()
            else:
                f,l = items[0], items[1]
                students = (students
                            .filter(first_name__contains=f)
                            .filter(last_name__contains=l)
                            .all())
        if len(department) > 0 and department != '0':
            students = students.filter(department__exact=department).all()        
        return students

    @staticmethod
    def all_registered_for_year(year, department=None):
        statuses = (Status
                    .objects
                    .filter(year=year)
                    .filter(is_registered=True)
                    .select_related('student')
                    .order_by('student__student_id')
                    .all())
        if department:
            statuses = statuses.filter(student__department=department)
        students = []
        for s in statuses:
            student = s.student
            student.current_status = s
            students.append(student)
        return students

class Assignment(models.Model):
    company = models.ForeignKey(Company,
                                related_name='assignments',
                                null=True,
                                blank=True)
    company_request = models.ForeignKey('std.CompanyRequest',
                                        related_name='assignments',
                                        null=True,
                                        blank=True)
    company_direct_application = models.ForeignKey('std.CompanyDirectApplication',
                                                   related_name='assignments',
                                                   null=True,
                                                   blank=True)

    student = models.ForeignKey(Student, related_name='assignments')
    year = models.IntegerField(null=True,
                               default=None,
                               verbose_name=u'ปีที่เข้ารับการฝึกงาน')
    receipt = models.ForeignKey('regis.Receipt', related_name='assignments',
                                null=True, blank=True)

    def get_receipt(self):
        if self.is_self_application():
            return None
        if self.receipt:
            return self.receipt
        else:
            department = self.student.department
            actual_department = DepartmentNameInRequestLetter.to_requested_department(department)
            receipts = list(Receipt.objects.filter(company=self.company,
                                                   year=self.year,
                                                   department=actual_department))
            if len(receipts)>0:
                return receipts[0]
            else:
                return None

    def is_self_application(self):
        if self.company:
            return False
        else:
            return True

    def get_company_name(self):
        if self.company:
            return self.company.name
        elif self.company_request:
            return self.company_request.company_name
        elif self.company_direct_application:
            return self.company_direct_application.company_name
        else:
            return ''

    def get_address(self):
        try:
            if self.company:
                return self.company.address
            elif self.company_request:
                return self.company_request.working_address
            elif self.company_direct_application:
                if self.company_direct_application.detail.working_address:
                    return self.company_direct_application.detail.working_address
                else:
                    return self.company_direct_application.detail.letter_address
            else:
                return ''
        except:
            return ''

SEMESTER_CHOICES = (
    (0,u'ภาคฤดูร้อน'),
    (1,u'ภาคต้น'),
    (2,u'ภาคปลาย'))

class Education(models.Model):
    total_credit = models.IntegerField(verbose_name=u'หน่วยกิตรวม')
    gpax = models.FloatField(verbose_name=u'เกรดเฉลี่ย')
    upto_year = models.IntegerField(verbose_name=u'ปีการศึกษาสุดท้ายที่คำนวณ')
    upto_semester = models.IntegerField(verbose_name=u'ภาคการศึกษาสุดท้ายที่คำนวณ',
                                        choices=SEMESTER_CHOICES)

class Conditions(models.Model):
    detail = models.TextField(verbose_name=u'รายละเอียดและข้อตกลงในการฝึกงาน')

    @staticmethod
    def get_combined_conditions():
        return '\n'.join([c.detail for c in Conditions.objects.all()])


class Status(models.Model):
    NO_PREF_TYPE = 0
    OWN_SELECTION_TYPE = 1
    FACULTY_SELECTION_TYPE = 2
    CO_OP_SELECTION_TYPE = 3

    REGISTRATION_TYPE_LABELS = {0: 'ไม่ระบุ',
                                1: 'จัดหาหน่วยงานเอง',
                                2: 'ให้คณะจัดหาหน่วยงานให้',
                                3: 'ฝึกงานสหกิจศึกษา'}

    REGISTRATION_TYPE_SHORT_LABELS = {0: 'ไม่ระบุ',
                                      1: 'จัดหาเอง',
                                      2: 'ให้คณะจัดหา',
                                      3: 'สหกิจ'}

    year = models.IntegerField(verbose_name=u'ปี')
    is_registered = models.NullBooleanField(verbose_name=u'สมัครฝึกงานแล้ว',blank=True)
    attended_orientation = models.NullBooleanField(verbose_name=u'เข้ารับการปฐมนิเทศแล้ว',blank=True)
    sent_request_form = models.NullBooleanField(verbose_name=u'ส่งใบคำร้องขอฝึกงานแล้ว',blank=True)
    request_form_checkcode = models.CharField(max_length=32,verbose_name=u'รหัสตรวจสอบใบคำร้องขอฝึกงาน',null=True)
    student = models.ForeignKey(Student,related_name='status')
    registration_type = models.IntegerField(default=NO_PREF_TYPE)

    is_registration_type_confirmed = models.BooleanField(default=False)

    is_converted_from_self_appl = models.NullBooleanField(default=None)
    is_self_appl_confirmed = models.NullBooleanField(default=None)
    converted_at = models.DateTimeField(null=True,blank=True)
    confirmed_at = models.DateTimeField(null=True,blank=True)
    asked_to_confirm_at = models.DateTimeField(null=True,blank=True)

    has_passed_quiz_exam = models.BooleanField(default=False,
                                               verbose_name=u'ผ่านการทำแบบทดสอบ')
    best_quiz_score = models.IntegerField(default=-1,
                                          verbose_name=u'คะแนนแบบทดสอบสูงสุด')
    last_quiz_submitted_at = models.DateTimeField(null=True,
                                                  blank=True,
                                                  verbose_name=u'ส่งคำตอบแบบทดสอบล่าสุด')
    
    class Meta:
        ordering = ['-year']

    def registration_type_display(self):
        if self.registration_type in Status.REGISTRATION_TYPE_LABELS:
            return Status.REGISTRATION_TYPE_LABELS[self.registration_type]
        else:
            return ''

    def registration_type_display_short(self):
        if self.registration_type in Status.REGISTRATION_TYPE_SHORT_LABELS:
            return Status.REGISTRATION_TYPE_SHORT_LABELS[self.registration_type]
        else:
            return ''

    def is_registered_for_self_application(self):
        return self.registration_type == Status.OWN_SELECTION_TYPE

    def is_registered_for_cooperative_education(self):
        return self.registration_type == Status.CO_OP_SELECTION_TYPE

    @staticmethod
    def find_regtype_stat(students):
        regtypes = Status.REGISTRATION_TYPE_LABELS
        rmap = {}
        regtype_counts = []
        for rt, rtlabel in regtypes.items():
            ii = len(regtype_counts)
            regtype_counts.append({ 'label': rtlabel,
                                    'count': 0 })
            rmap[rt] = ii

        for s in students:
            if s.current_status.registration_type in regtypes:
                regtype_counts[rmap[s.current_status.registration_type]]['count'] += 1
        return regtype_counts


class Receipt(models.Model):
    company = models.ForeignKey(Company, related_name='receipts')
    department = models.ForeignKey(Department,related_name='receipts',
                                   verbose_name=u'ภาควิชา')
    work_detail = models.ForeignKey(WorkDetail, 
                                    related_name='receipts')
    year = models.IntegerField(verbose_name=u'ปี')
    total_student =  models.IntegerField(verbose_name=u'จำนวนนิสิต')
    work_address = models.TextField(verbose_name=u'ที่อยู่')
    work_description = models.TextField(verbose_name=u'ลักษณะงาน')
    created_at = models.DateTimeField(auto_now_add=True)
    available = models.BooleanField(default=True)
    is_hidden = models.BooleanField(default=False)

    num_accepted_students = models.IntegerField(verbose_name=u'จำนวนนิสิตที่อนุมัติแล้ว',
                                                default=0)

    num_rounds_shown = models.IntegerField(verbose_name=u'จำนวนครั้งที่แสดงให้นิสิตเลือก',
                                           default=0)

    __receipt_cache = {}
    __cache_expire = {}

    @staticmethod
    def all_for_round(selection_round, department=None, year=None, cache=False):
        from datetime import datetime, timedelta

        if selection_round:
            beginning_time = selection_round.beginning_time
            cutoff_time = datetime(beginning_time.year,
                                   beginning_time.month,
                                   beginning_time.day)
            receipts = Receipt.objects.filter(created_at__lt=cutoff_time,
                                              year=selection_round.year)
        else:
            if year==None:
                year = get_next_worktraining_year()
            receipts = Receipt.objects.filter(year=year)

        if cache and department and selection_round:
            cache_key = (department.id, selection_round.id)
            if cache_key in Receipt.__receipt_cache:
                time_now = datetime.now()
                if time_now <= Receipt.__cache_expire[cache_key]:
                    return Receipt.__receipt_cache[cache_key]


        if department:
            receipts = receipts.filter(department=department)

        if cache and department and selection_round:
            receipts = receipts.select_related()
            cache_key = (department.id, selection_round.id)
            cache_expire_time = datetime.now() + timedelta(minutes=5) 
            Receipt.__cache_expire[cache_key] = cache_expire_time
            Receipt.__receipt_cache[cache_key] = receipts

        return receipts

    @staticmethod
    def auto_hide_expired_receipts():
        year = get_next_worktraining_year()
        from commons.academia import NUM_ROUNDS_BEFORE_RECEIPT_EXPIRED
        for receipt in Receipt.objects.filter(year=year):
            if receipt.num_rounds_shown == NUM_ROUNDS_BEFORE_RECEIPT_EXPIRED:
                if receipt.is_shown_to_students():
                    receipt.is_hidden = True
                    receipt.save()


    def num_available_positions(self):
        return self.total_student - self.num_accepted_students

    def is_empty(self):
        return self.num_accepted_students == 0

    def is_full(self):
        return self.total_student <= self.num_accepted_students

    def is_shown_to_students(self):
        return (self.available and 
                (not self.is_hidden) and
                (self.num_available_positions() > 0))


class Grade(models.Model):
    student = models.ForeignKey(Student, related_name='grades')
    course_number = models.CharField(max_length=8,verbose_name=u'รหัสวิชา',null=False)
    course_name = models.CharField(max_length=100,verbose_name=u'ชื่อวิชา',null=True)
    credit = models.IntegerField(verbose_name=u'หน่วยกิต',null=False)
    grade = models.CharField(max_length=3,verbose_name=u'เกรด',null=False)
    regis_type = models.CharField(max_length=1,verbose_name=u'ประเภทลงทะเบียน',null=False)


class ConfigLetterBase(models.Model):
    year = models.IntegerField(verbose_name=u'ปี')
    letter_number = models.CharField(max_length=300,verbose_name=u'เลขที่จดหมาย')
    letter_date = models.DateField(verbose_name=u'กำหนดวันที่ออกจดหมายราชการ')
    signer_name = models.CharField(max_length=200, default='')
    beginning_date = models.CharField(max_length=100, default='')
    end_date = models.CharField(max_length=100, default='')
    eval_date = models.CharField(max_length=100, default='')

    class Meta:
        abstract = True

class ConfigLetter(ConfigLetterBase):
    company = models.ForeignKey(Company, related_name='letter_config')

class CompanyStatusMarker(models.Model):
    company = models.ForeignKey(Company, related_name='markers')
    year = models.IntegerField(verbose_name=u'ปี')
    number = models.IntegerField(default=0)

    @staticmethod
    def get_marked_ids_as_set(year, number=0):
        return set([m.company_id
                    for m
                    in CompanyStatusMarker.objects.filter(year=year, number=number)])

