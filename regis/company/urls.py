from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    'commons.views.generics',
    url(r'^search/$', 'company_search', 
        { 'is_admin': True,
          'includes_self_appl': True },
        name='regis_company_search'),
)

urlpatterns += patterns(
    'regis.company.views',

    url(r'^$','index', name='regis_company_index'),
    url(r'^add/$','add', name='regis_company_new'),

    url(r'^(?P<company_id>\d+)/$',
        'detail',
        name='regis_company_detail'),
    url(r'^(?P<company_id>\d+)/edit/$',
        'edit',
        name='regis_company_edit'),

    url(r'^(?P<company_id>\d+)/(?P<year>\d+)/edit/$','editWorkDetail',
        name='regis_company_edit_workdetail'),
    url(r'^(?P<company_id>\d+)/(?P<year>\d+)/add/$','addWorkDetail',
        name='regis_company_add_workdetail'),

    # special search
    url(r'^search_by_province/$','search_by_province', name='regis_company_search_by_province'),

    # print letters
    url(r'^print/(?P<company_id>\d+)/(?P<year_no>\d+)$',
        'print_letter',
        name='regis_company_print_letter'),
    url(r'^conf_print_pdf/(?P<company_id>\d+)/(?P<year_no>\d+)$',
        'config_print_letter_pdf',
        name='regis_company_print_letter_pdf'),
    
    url(r'^print_aid_pdf/(?P<year_no>\d+)/(?P<company_id>\d+)$',
        'print_aid_letter_pdf',
        name='regis_company_print_aid_letter_pdf'),
    
    url(r'^print_aid_pdf/(?P<year_no>\d+)$',
        'print_aid_letter_pdf', 
        {'company_id':None},
        name='regis_company_print_all_aid_letter_pdf'),

    # print labels
    url(r'^print_label_aid_pdf/(?P<year_no>\d+)$',
        'print_all_label_aid_letter',
        name='regis_company_print_all_label_aid_letter_pdf'),    
    url(r'^print_one_label_aid_pdf/(\d+)/$',
        'print_one_label_aid_letter',
        name='regis_company_print_one_label_aid_letter_pdf'),    

    # saved labels
    url(r'^saved_labels/list/$',
        'list_saved_labels',
        name='regis_company_list_saved_labels'),
    url(r'^saved_labels/print/$',
        'print_saved_labels',
        name='regis_company_print_saved_labels_pdf'),
    url(r'^saved_labels/add/(\d+)/$',
        'save_label',
        name='regis_company_save_label'),
    url(r'^saved_labels/delete/(\d+)/$',
        'remove_label',
        name='regis_company_remove_label'),
    url(r'^saved_labels/delete/all/$',
        'remove_all_labels',
        name='regis_company_remove_all_labels'),

    # assignments
    url(r'^(?P<year_no>\d+)/(?P<company_id>\d+)/search/students/$',
        'search_student_to_assign',
        name='regis_company_search_addStd'),
    url(r'^(?P<company_id>\d+)/(?P<student_id>\d+)/(?P<year_no>\d+)$',
        'assign_student',
        name='regis_company_addStd'),
    url(r'^assignments/(\d+)/cancel/$',
        'cancel_assignment',
        name="regis_company_cancel_assignment"),

    # approve proposed companies
    url(r'^approve/list/$', 'approve_list',
        name='regis_company_approve_list'),

    url(r'^approve/new/(?P<proposed_company_id>\d+)/$', 'add',
        name='regis_company_approve_new'),

    url(r'^short_name/check/(.*)',
        'check_short_name',
        name='regis_company_check_short_name'),

    url(r'^company_select',
        'list_company_selection',
        name='regis_company_selection'),
        
    url(r'^receipt/(?P<company_id>\d+)/$', 
        'return_receipt', 
        name='regis_company_return_receipt'),
        

    # company selection statistics
    url(r'^selection/stat/$',
        'company_selection_statistic',
        name='regis_company_selection_stat'),
        
    url(r'^selection/stat/(\d+)/$',
        'company_selection_statistic',
        name='regis_company_selection_stat_with_year'),
        
    url(r'^selection/stat/open/(?P<company_id>\d+)/$',
        'company_selection_statistic_modify', {'change': 'open'},
        name='regis_company_selection_stat_open'),

    url(r'^selection/stat/close/(?P<company_id>\d+)/$',
        'company_selection_statistic_modify', {'change': 'close'},
        name='regis_company_selection_stat_close'),

    url(r'^selection/stat/hide/(?P<company_id>\d+)/$',
        'company_selection_statistic_modify', {'change': 'hide'},
        name='regis_company_selection_stat_hide'),

    url(r'^selection/stat/show/(?P<company_id>\d+)/$',
        'company_selection_statistic_modify', {'change': 'show'},
        name='regis_company_selection_stat_show'),

    url(r'^selection/stat/toggle/(?P<company_id>\d+)/(?P<number>\d+)/$',
        'company_selection_statistic_toggle',
        name='regis_company_selection_stat_toggle'),
)
