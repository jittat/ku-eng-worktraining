# -*- coding: utf-8 -*-
import time ,datetime
from datetime import date
import json

from django.http import HttpResponse , HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.forms import ModelForm, Textarea
from django.forms.formsets import formset_factory
from django.forms.models import modelformset_factory
from django import forms
from django.db import IntegrityError
from django.conf import settings

from commons.models import Configuration, Log
from regis.models import Student, Company, Assignment, WorkDetail, Department, DepartmentNameInRequestLetter, Receipt, ConfigLetter, CompanyStatusMarker
from evaluations.models import Evaluation
from committee.models import ProposedCompany, CompanySelection

from commons.academia import get_recent_worktraining_year, get_next_worktraining_year, get_current_information_year

from regis.decorators import ajax_login_required
from commons.decorators import sa_admin_required, admin_or_committee_required

from regis.student.views import StudentAllSearchForm
from std.models import SelectionRoundStatus, CompanyRequest, CompanyDirectApplication
from direct_appl.models import StudentApplication, DirectApplicationPosition

import os
from settings import PROJECT_DIR
from reports import print_letter_pdf, build_request_letter_pdf_response, build_label_pdf_response

currentpage = 'company'

class CompanyForm(forms.Form):
    short_name = forms.CharField(label=u'ชื่อย่อ')
    name = forms.CharField(label=u'ชื่อ',
                           widget=forms.TextInput(attrs={'size':50}))
    tel_no = forms.CharField(label=u'โทรศัพท์',
                             required=False)
    fax_no = forms.CharField(label=u'โทรสาร', required=False)
    address = forms.CharField(label=u'ที่อยู่',
                              widget=forms.Textarea(attrs={'rows':3}))

    contact_name = forms.CharField(label=u'ชื่อผู้ประสานงาน',
                                   required=False)
    email = forms.EmailField(label=u'อีเมล์ผู้ประสานงาน',
                             widget=forms.TextInput(attrs={'size':30}),
                             required=False)
    signer_name = forms.CharField(label=u'ชื่อผู้ลงนาม', required=False)
    short_description = forms.CharField(label=u'ลักษณะธุรกิจ',
                                        help_text=u'อธิบายลักษณะธุรกิจคร่าว ๆ '
                                        u'ของสถานประกอบการ เช่น '
                                        u'พัฒนาระบบเครือข่าย '
                                        u'หรือผลิตชิ้นส่วนรถยนต์ เป็นต้น',
                                        widget=forms.Textarea(attrs={'rows':2}),
                                        required=False)

class ReceiptForm(ModelForm):
    work_address = forms.CharField(label=u'ที่อยู่สถานที่ฝึกงาน',
                                   widget=forms.Textarea(attrs={'rows':3,'cols': 60,'class':'workaddress'}))
    work_description = forms.CharField(label=u'ลักษณะงาน',
                                       widget=forms.Textarea(attrs={'rows':2,'cols': 60}))
    class Meta:
        model = Receipt
        exclude = ('available',
                   'created_at',
                   'work_detail',
                   'year',
                   'company',
                   'num_rounds_shown')

class WorkDetailForm(ModelForm):
    dressing_description = forms.CharField(label=u'รายละเอียดการแต่งกาย',
                                           required=False,
                                           widget=forms.Textarea(attrs={'rows':2,'cols': 70}))
    other_description = forms.CharField(label=u'รายละเอียดอื่น ๆ ',
                                        required=False,
                                        widget=forms.Textarea(attrs={'rows':2,'cols': 70}))
    class Meta:
        model = WorkDetail
        exclude = ('year','company',)

@sa_admin_required        
def return_receipt(request,company_id):
    company = Company.objects.get(id=company_id)
    nextyear = get_next_worktraining_year()

    ReceiptFormSet = modelformset_factory(Receipt,
                                          ReceiptForm,
                                          exclude=('year',
                                                   'company',
                                                   'work_detail',
                                                   'available',
                                                   'num_accepted_students',
                                                   'num_rounds_shown'))

    default_beginning_date = str(Configuration.get('default.letter_beginning_date'))
    default_end_date = str(Configuration.get('default.letter_end_date'))
    
    workdetails = WorkDetail.objects.filter(company=company_id,year=nextyear).all()
    if workdetails:
        workdetail = workdetails[0]
    else:
        workdetail = WorkDetail()
        workdetail.company = company
        workdetail.year = nextyear
    
    if request.method == 'POST':
        receipt_formset = ReceiptFormSet(request.POST, 
                                         queryset=Receipt.objects.filter(company=company_id,
                                                                         year=nextyear),
                                         prefix='receipt')

        workdetail_form = WorkDetailForm(request.POST, 
                                         instance=workdetail)
        if receipt_formset.is_valid() and workdetail_form.is_valid():

            workdetail = workdetail_form.save()

            receipts = receipt_formset.save(commit=False)
            for receipt in receipts:
                actual_department = (
                    DepartmentNameInRequestLetter.to_requested_department(
                        receipt.department))
                receipt.department = actual_department

                receipt.year = nextyear
                receipt.company = company
                receipt.available = True
                receipt.work_detail = workdetail
                receipt.save()
         
            
            if 'okay' in request.POST:
                return HttpResponseRedirect(reverse('regis_company_search'))
            else:
                receipt_formset = ReceiptFormSet(queryset=Receipt.objects.filter(company=company_id,
                                                                                 year=nextyear),
                                                 prefix='receipt')
                workdetail_form = WorkDetailForm(instance=workdetail)
    else:
        
        receipt_formset = ReceiptFormSet(queryset=Receipt.objects.filter(company=company_id,
                                                                         year=nextyear),
                                         prefix='receipt')
        workdetail_form = WorkDetailForm(instance=workdetail)
    
    return render_to_response('regis/company/receipt.html',
                              { 'receipt_formset': receipt_formset,
                                'form':workdetail_form,
                                'company':company,
                                'nextyear':nextyear,
                                'currentpage':currentpage,
                                'default_beginning_date':default_beginning_date,
                                'default_end_date':default_end_date},
                              context_instance=RequestContext(request))


@sa_admin_required
def add(request, proposed_company_id=None):
    if proposed_company_id:
        proposed_company = get_object_or_404(ProposedCompany, pk=proposed_company_id)
    else:
        proposed_company = None

    short_name_duplicate_error = False

    if request.method=='POST':
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = Company()
            company.short_name = form.cleaned_data['short_name']
            company.name = form.cleaned_data['name']
            company.tel_no = form.cleaned_data['tel_no']
            company.fax_no = form.cleaned_data['fax_no']
            company.address = form.cleaned_data['address']
            company.contact_name = form.cleaned_data['contact_name']
            company.contact_email = form.cleaned_data['email']
            company.signer_name = form.cleaned_data['signer_name']
            company.short_description = form.cleaned_data['short_description']

            # new company will appear at the end of the list
            company.is_new = True
            try:
                company.save()
                if proposed_company:
                    proposed_company.is_approved = True
                    proposed_company.save()

                    # auto select it
                    selection = CompanySelection(company=company,
                                                 department=proposed_company.department,
                                                 year=get_next_worktraining_year())
                    selection.save()

                return HttpResponseRedirect(reverse('regis_company_detail',
                                                    args=[company.id]))
            except IntegrityError:
                short_name_duplicate_error = True
    else:
        if proposed_company:
            form = CompanyForm(initial={
                    'name': proposed_company.name,
                    'tel_no': proposed_company.tel_no,
                    'fax_no': proposed_company.fax_no,
                    'address': proposed_company.address,
                    'contact_name': proposed_company.contact_name,
                    'signer_name': proposed_company.signer_name,
                    'short_description': proposed_company.short_description })
        else:
            form = CompanyForm()

    return render_to_response('regis/company/add.html',
                              { 'form': form ,
                                'proposed_company': proposed_company,
                                'short_name_duplicate_error': short_name_duplicate_error,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def edit(request,company_id):
    company = get_object_or_404(Company, id=company_id)
    if request.method=='POST':
        form = CompanyForm(request.POST)
        if form.is_valid():
            company.short_name = form.cleaned_data['short_name']
            company.name = form.cleaned_data['name']
            company.tel_no = form.cleaned_data['tel_no']
            company.fax_no = form.cleaned_data['fax_no']
            company.address = form.cleaned_data['address']
            company.contact_name = form.cleaned_data['contact_name']
            company.contact_email = form.cleaned_data['email']
            company.signer_name = form.cleaned_data['signer_name']
            company.short_description = form.cleaned_data['short_description']
            company.save()
            company_id = company.id
            return HttpResponseRedirect(reverse('regis_company_detail',args=[company_id]))
    else:
        form = CompanyForm(initial={
                'short_name': company.short_name,
                'name': company.name,
                'tel_no': company.tel_no,
                'fax_no': company.fax_no,
                'address': company.address,
                'contact_name': company.contact_name,
                'email': company.contact_email,
                'signer_name': company.signer_name,
                'short_description': company.short_description })

    return render_to_response('regis/company/edit.html',
                              {'form':form,
                               'company_id':company_id,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def addWorkDetail(request,company_id,year):
    company = Company.objects.get(id = company_id)
    if request.method=='POST':
        form = WorkDetailForm(request.POST)
        if form.is_valid():
            workdetail = WorkDetail()
            workdetail.beginning_date = form.cleaned_data['beginning_date']
            workdetail.end_date = form.cleaned_data['end_date']
            workdetail.dressing_description = form.cleaned_data['dressing_description']
            workdetail.other_description = form.cleaned_data['other_description']
            workdetail.year = year
            workdetail.company = company
            workdetail.save()
            return HttpResponseRedirect(reverse('regis_company_detail',args=[company_id]))
    else:
        form = WorkDetailForm()

    return render_to_response('regis/company/addWorkDetail.html',
                              {'form': form ,
                               'year': year,
                               'company_id':company_id,
                               'company_name': company.name,
                               'currentpage':currentpage},
                              context_instance=RequestContext(request))

@sa_admin_required
def editWorkDetail(request,company_id,year):
    company = Company.objects.get(id = company_id)
    try:
        workdetail = company.workdetail_for_year(year)
        if request.method == 'POST':
            form = WorkDetailForm(request.POST, instance=workdetail)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('regis_company_detail',args=[company_id]))
        else:
            form = WorkDetailForm(instance=workdetail)

        return render_to_response('regis/company/editWorkDetail.html',
                                  {'year':year,
                                   'company_id':company_id,
                                   'company_name':company.name,
                                   'form':form,
                                   'currentpage':currentpage},
                                  context_instance=RequestContext(request))
    except IntegrityError:
        return HttpResponseRedirect(reverse('regis_company_add_workdetail',args=[company_id, year]))

class filter_internship_year(forms.Form):
    start_year = 2553
    end_year = get_next_worktraining_year()
    YEAR_CHOICE = []
    for year in range(start_year,end_year+1):
        YEAR_CHOICE.append([year,year])
    YEAR_CHOICE.reverse()
    year = forms.ChoiceField(choices=YEAR_CHOICE,label=u'ปีการศึกษา')

@admin_or_committee_required
def detail(request,company_id):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department
    next_year = get_next_worktraining_year()
    selections = selections = CompanySelection.objects.filter(company=company_id,year=next_year).all()
    if is_letters_to_all_companies() or (len(selections) != 0):
        aid_button_enable = True
    else:
        aid_button_enable = False
    
    company = Company.objects.get(id = company_id)
    notice = ''

    permission = {
        'can_edit_company': not is_committee,
        'can_add_students': True,
        'can_view_evaluation': not is_committee,
        'can_print_intro_letter': not is_committee,
        'can_edit_workdetail': not is_committee,
        'can_cancel_assignments': not is_committee,
        }

    if request.method=='POST':
        form = filter_internship_year(request.POST)
        if form.is_valid():
            year = form.cleaned_data['year']
            assignments = company.assignments_for_year(year)
            workdetail = company.workdetail_for_year(year)
            return render_to_response("regis/company/detail.html",
                              { 'company': company ,
                                'currentpage':currentpage,
                                'year': year,
                                'next_year':next_year,
                                'aid_button_enable':aid_button_enable,
                                'assignments': assignments,
                                'workdetail': workdetail,
                                'form':form,
                                'eval_link_added': True,
                                'is_committee': is_committee,
                                'department': department,
                                'permission': permission },
                              context_instance=RequestContext(request))
    else:
        year = get_current_information_year()
        assignments = company.assignments_for_year(year)
        workdetail = company.workdetail_for_year(year)

        form = filter_internship_year(initial={'year':year})

        return render_to_response("regis/company/detail.html",
                              { 'company': company ,
                                'currentpage':currentpage,
                                'year': year,
                                'next_year':next_year,
                                'aid_button_enable':aid_button_enable,
                                'assignments': assignments,
                                'workdetail': workdetail,
                                'form':form,
                                'eval_link_added': True,
                                'is_committee': is_committee,
                                'department': department,
                                'permission': permission },
                              context_instance=RequestContext(request))

@admin_or_committee_required
def search_student_to_assign(request,company_id,year_no):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department

    company = Company.objects.get(id = company_id)
    notice = ''
    is_show_info = True

    if request.method=='POST':
        form = StudentAllSearchForm(request.POST)
        if form.is_valid():
            student_name = form.cleaned_data['student_name']
            student_id = form.cleaned_data['student_id']
            student_department = form.cleaned_data['student_department']
           
            students = Student.objects.all().order_by('student_id')
            if len(student_name) != 0:
                students = students.filter(first_name__contains=student_name).all()
            if len(student_id) > 0:
                students = students.filter(student_id__startswith=student_id).all()
            if is_committee:
                students = students.filter(department__exact=department).all()
            else:
                if student_department != '0':
                    students = students.filter(department__exact=student_department).all()
            is_show_info = (students.count() <= 50)
            if not is_show_info:
                students = students[:50]
            if len(students) != 0:
                return render_to_response('regis/company/addStd.html',
                              { 'form': form,
                                'year': year_no,
                                'students':students,
                                'is_show_info': is_show_info,
                                'notice': notice ,
                                'company':company ,
                                'is_committee': is_committee,
                                'department': department,
                                'currentpage':currentpage},
                              context_instance=RequestContext(request))
            else:
                notice = 'ไม่พบข้อมูลนิสิต'
                form = StudentAllSearchForm()
    else:
        form = StudentAllSearchForm()

    return render_to_response("regis/company/addStd.html",
                              {'form':form,
                               'is_show_info': is_show_info,
                               'currentpage':currentpage,
                               'notice': notice ,
                               'year':year_no,
                               'is_committee': is_committee,
                               'department': department,
                               'company':company},
                              context_instance=RequestContext(request))

@admin_or_committee_required
def assign_student(request, company_id, student_id, year_no):
    is_committee = request.user.userprofile.is_committee_member
    department = request.user.userprofile.department

    company = Company.objects.get(id = company_id)
    student = Student.objects.get(student_id = student_id)

    if is_committee and student.department != department:
        return HttpResponseForbidden()

    if student.intern_company_for_next_year()==None:
        assignment = Assignment()
        assignment.company_id = company.id
        assignment.student_id = student.id
        assignment.year = year_no
        assignment.save()

        Log.create("assign -from %s student: %s, company: %d" %
                   (request.META['REMOTE_ADDR'],
                    student.student_id,
                    assignment.company_id),
                   user=request.user,
                   student=student)

        return HttpResponseRedirect(reverse("regis_company_detail",args=[company_id]))
    else:
        return render_to_response("regis/company/addError.html",
                                        {'company_id':company_id})
class DepartmentListForm(forms.Form):
    departments = Department.objects.all()
    DEPARTMENT_CHOICE = [[0,u'ทุกภาควิชา']]
    for department in departments:
        DEPARTMENT_CHOICE.append([department.id,department.name])

    student_department = forms.ChoiceField(choices=DEPARTMENT_CHOICE,label=u'สำหรับภาควิชา',required=False)
    
@sa_admin_required
def list_company_selection(request):
    department = None
    companies = None
    if request.method=='POST':
        dept_form = DepartmentListForm(request.POST)
        if dept_form.is_valid():
            dept_id = int(dept_form.cleaned_data['student_department'])
            year = get_next_worktraining_year()
            if dept_id == 0:
                selection = (CompanySelection.objects.filter(year=year).all())
                companies = []
                cid_set = set()
                for c in [sel.company for sel in selection]:
                    if c.id not in cid_set:
                        cid_set.add(c.id)
                        companies.append(c)
            else:
                department = get_object_or_404(Department,id=dept_id)
                selection = (CompanySelection.selected_by_department_for_year(department, year))
                companies = [sel.company for sel in selection]              

    else:
        dept_form = DepartmentListForm()
        
    return render_to_response("regis/company/list_company_selection.html",
                                  {'companies':companies,
                                   'department': department,
                                   'currentpage':currentpage,
                                   'dept_form':dept_form}, 
                                  context_instance=RequestContext(request))

@sa_admin_required
def print_letter(request,company_id,year_no):
    date = datetime.date.today()
    company = get_object_or_404(Company,pk=company_id)
    assignments = company.assignments.filter(year=year_no).select_related('student')
    students = [a.student for a in assignments]
    return render_to_response("regis/company/printletter.html",
                              { 'company': company ,'students':students,'date':date})

class ConfigLetterForm(forms.Form):
    letter_number         = forms.CharField(label=u'เลขที่', widget=forms.TextInput(attrs={'size':60}))
    letter_date           = forms.CharField(label=u'วันที่ออก')
    signer_name           = forms.CharField(label=u'ชื่อผู้รับ', widget=forms.TextInput(attrs={'size':60}))
    letter_beginning_date = forms.CharField(label=u'วันเริ่มต้นฝึกงาน')
    letter_end_date       = forms.CharField(label=u'วันสิ้นสุดฝึกงาน')
    eval_end_date         = forms.CharField(label=u'วันสุดท้ายรับใบประเมิน')

def default_student_letter_data(company, config_letter, work_detail):
    if config_letter:
        letter_number = config_letter.letter_number
        letter_date = config_letter.letter_date
    else:
        letter_number = Configuration.objects.get(name="default.sending_letter_number").value
        letter_date = Configuration.objects.get(name="default.sending_letter_date").value

    if config_letter and config_letter.signer_name!='':
        signer_name = config_letter.signer_name
    else:
        if company:
            signer_name = u'ผู้จัดการแผนกทรัพยากรมนุษย์' + " " + company.name
            if company.signer_name and company.signer_name != '':
                signer_name = company.signer_name + " " + company.name
        else:
            signer_name = u'ผู้จัดการแผนกทรัพยากรมนุษย์'
    
    if config_letter and config_letter.beginning_date!='':
        letter_beginning_date = config_letter.beginning_date
    elif work_detail:
        letter_beginning_date = work_detail.beginning_date
    else:
        letter_beginning_date = Configuration.objects.get(name="default.letter_beginning_date").value

    if config_letter and config_letter.end_date!='':
        letter_end_date = config_letter.end_date
    elif work_detail:
        letter_end_date = work_detail.end_date
    else:
        letter_end_date = Configuration.objects.get(name="default.letter_end_date").value

    if config_letter and config_letter.eval_date:
        eval_end_date = config_letter.eval_date
    else:
        eval_end_date = Configuration.objects.get(name="default.eval_end_date").value
    
    return (letter_number, 
            letter_date,
            signer_name,
            letter_beginning_date,
            letter_end_date,
            eval_end_date)
    

@sa_admin_required
def config_print_letter_pdf(request,company_id,year_no):
    company = Company.objects.get(id=company_id)
    config_letter = ConfigLetter.objects.filter(company=company_id,year=year_no)

    if config_letter:
        config_letter = config_letter[0]

    try:
        work_detail = WorkDetail.objects.get(company=company_id, year=year_no)
    except WorkDetail.DoesNotExist:
        work_detail = None

    (letter_number, 
     letter_date, 
     signer_name, 
     letter_beginning_date, 
     letter_end_date, 
     eval_end_date) = default_student_letter_data(company, config_letter, work_detail)
    
    if request.method=='POST':
        form = ConfigLetterForm(request.POST)
        if form.is_valid():
            letter_number = form.cleaned_data['letter_number']
            letter_date = form.cleaned_data['letter_date']
            signer_name = form.cleaned_data['signer_name']
            letter_beginning_date = form.cleaned_data['letter_beginning_date']
            letter_end_date = form.cleaned_data['letter_end_date']
            eval_end_date = form.cleaned_data['eval_end_date']
            
            try:
                workdetail = WorkDetail.objects.get(company=company_id,year=year_no)
                workdetail.beginning_date = letter_beginning_date
                workdetail.end_date = letter_end_date
                workdetail.save()
            except WorkDetail.DoesNotExist:
                pass
            
            
            if not config_letter:
                config_letter = ConfigLetter()

            config_letter.company_id = company_id
            config_letter.year = year_no
            config_letter.letter_number = letter_number
            config_letter.letter_date = letter_date
            config_letter.signer_name = signer_name
            config_letter.beginning_date = letter_beginning_date
            config_letter.end_date = letter_end_date
            config_letter.eval_date = eval_end_date
            config_letter.save()
            
            return print_letter_pdf(company_id,
                                    year_no,
                                    letter_number,
                                    letter_date,
                                    signer_name,
                                    letter_beginning_date,
                                    letter_end_date,
                                    eval_end_date,
                                    show_signature=False,
                                    show_secret=True)
            
    else:
        form = ConfigLetterForm({'letter_number':letter_number,
                                 'letter_date':letter_date,
                                 'signer_name':signer_name,
                                 'letter_beginning_date':letter_beginning_date,
                                 'letter_end_date':letter_end_date,
                                 'eval_end_date':eval_end_date})
    
    return render_to_response("regis/company/conf_print_pdf.html",
                                  {'form':form,
                                   'company': company,
                                   'company_id':company_id,
                                   'year_no':year_no,
                                   'currentpage':currentpage,}, 
                                  context_instance=RequestContext(request))


@sa_admin_required
def index(request):
    next_year = get_recent_worktraining_year()+1
    return render_to_response("regis/company/index.html",
                              {'currentpage':currentpage,
                               'next_year':next_year,
                               },
                              context_instance=RequestContext(request))

def is_letters_to_all_companies():
    try:
        c = Configuration.objects.get(name="request.letters_to_all_companies")
        if c.value.strip().upper()[0]=='Y':
            return True
        else:
            return False
    except:
        return False


@sa_admin_required
def index(request):
    next_year = get_recent_worktraining_year()+1
    return render_to_response("regis/company/index.html",
                              {'currentpage':currentpage,
                               'next_year':next_year,
                               },
                              context_instance=RequestContext(request))

def is_letters_to_all_companies():
    try:
        c = Configuration.objects.get(name="request.letters_to_all_companies")
        if c.value.strip().upper()[0]=='Y':
            return True
        else:
            return False
    except:
        return False

@sa_admin_required
def print_aid_letter_pdf(request, year_no, company_id=None):
    
    send_to_all = is_letters_to_all_companies()

    if company_id:
        company = get_object_or_404(Company, id=company_id)
        companies = [company]
    else:
        if not send_to_all:
            companies = CompanySelection.get_all_selected_companies_for_year(year_no)
        else:
            companies = Company.objects.order_by('id').all()

    if send_to_all:
        try:
            all_department_names = settings.DEPARTMENT_NAMES
        except:
            all_departments = Department.objects.all()
            all_department_names = (
                DepartmentNameInRequestLetter.clean_dupplicate_names(
                    [d.name for d in all_departments]))
    else:
        all_department_names = None

    return build_request_letter_pdf_response(companies, all_department_names, send_to_all)


@sa_admin_required
def print_all_label_aid_letter(request, year_no):
    if not is_letters_to_all_companies():
        companies = CompanySelection.get_all_selected_companies_for_year(year_no)
    else:
        companies = Company.objects.order_by('id').all()
    return build_label_pdf_response(companies, 
                                    'internship_all_label_aid_letter.pdf',
                                    print_number=True)


@sa_admin_required
def print_one_label_aid_letter(request, company_id):
    company = get_object_or_404(Company, pk=company_id)
    if request.method != 'POST':
        return HttpResponseForbidden()

    empty_slots = 0
    if 'empty_slot_count' in request.POST:
        try:
            empty_slots = int(request.POST['empty_slot_count'])
        except:
            pass
    
    companies = ([None] * empty_slots) + [company]
    return build_label_pdf_response(companies, 
                                    'internship_label_aid_letter.pdf',
                                    print_number=False)

def get_saved_companies(request):
    if 'saved_company_ids' in request.session:
        return Company.objects.filter(id__in=request.session['saved_company_ids']).all()
    else:
        return []

def get_saved_company_count(request):
    if 'saved_company_ids' in request.session:
        return len(request.session['saved_company_ids'])
    else:
        return 0

@sa_admin_required
def save_label(request, company_id):
    if 'saved_company_ids' not in request.session:
        request.session['saved_company_ids'] = []

    cid = int(company_id)
    if cid not in request.session['saved_company_ids']:
        request.session['saved_company_ids'].append(cid)
        request.session.modified = True

    return HttpResponse(str(len(request.session['saved_company_ids'])))

@sa_admin_required
def remove_label(request, company_id):
    if 'saved_company_ids' in request.session:
        cid = int(company_id)
        if cid in request.session['saved_company_ids']:
            request.session['saved_company_ids'].remove(cid)
            request.session.modified = True

    return HttpResponseRedirect(reverse('regis_company_list_saved_labels'))

@sa_admin_required
def remove_all_labels(request):
    if 'saved_company_ids' in request.session:
        request.session['saved_company_ids'] = []

    return HttpResponseRedirect(reverse('regis_company_list_saved_labels'))

@sa_admin_required
def list_saved_labels(request):
    companies = get_saved_companies(request)

    return render_to_response('regis/company/list_saved_labels.html',
                              { 'companies': companies,
                                'currentpage':currentpage }, 
                              context_instance=RequestContext(request))   

@sa_admin_required
def print_saved_labels(request):
    if request.method != 'POST':
        return HttpResponseForbidden()

    empty_slots = 0
    if 'empty_slot_count' in request.POST:
        try:
            empty_slots = int(request.POST['empty_slot_count'])
        except:
            pass
    
    companies = ([None] * empty_slots) + list(get_saved_companies(request))
    return build_label_pdf_response(companies, 
                                    'internship_label_aid_letters.pdf',
                                    print_number=False)

##############################
#
# approve proposed companies
#

@sa_admin_required
def approve_list(request):
    companies = ProposedCompany.unapproved_companies()
    return render_to_response('regis/company/approve_list.html',
                              { 'companies': companies,
                                'currentpage': currentpage},
                              context_instance=RequestContext(request))


@ajax_login_required
@sa_admin_required
def check_short_name(request, short_name):
    companies = Company.objects.filter(short_name__startswith=short_name).all()
    if len(companies)!=0:
        result = {'found': True,
                  'name': companies[0].name,
                  'org_short_name': companies[0].short_name } 
    else:
        result = {'found': False,
                  'name': ''}

    return HttpResponse(json.dumps(result))

##############################
#
# selection statistics
#

def prepare_receipt_for_stat(receipt, department_dict=None):
    if department_dict == None:
        receipt.department_code = receipt.department.code
        receipt.department_short_name = receipt.department.short_name
    else:
        receipt.department_code = department_dict[r.department_id].code
        receipt.department_short_name = department_dict[r.department_id].short_name

def init_company_for_stat(company):
    company.returned_receipts = []
    company.can_send_letter = True
    company.available = True
    company.hidden = False

def prepare_company_for_stat(company):
    company.can_send_letter = False
    for r in company.returned_receipts:
        company.available = company.available and r.available
        # TODO: FIX LOGIC FOR CAN SEND LETTER
        #if not r.is_full():
        #    company.can_send_letter = False
        if r.is_hidden:
            company.hidden = True
        if not r.is_empty():
            company.can_send_letter = True
    # TODO: FIX THIS
    #if (not company.available) or (company.hidden):
    #    company.can_send_letter = True

def assign_makers(companies, year, number=0):
    ids0 = CompanyStatusMarker.get_marked_ids_as_set(year,0)
    ids1 = CompanyStatusMarker.get_marked_ids_as_set(year,1)
    for c in companies:
        c.is_marked0 = (c.id in ids0)
        c.is_marked1 = (c.id in ids1)

@sa_admin_required
def company_selection_statistic(request,year=None):
    if not year:
        year = get_next_worktraining_year()   
        allows_changes = True
    else:
        allows_changes = False

    all_companies = list(Company.objects.all())
    all_receipts = Receipt.objects.filter(year=year).all()

    company_dict = dict([(c.id,c) for c in all_companies])
    department_dict = dict([(d.id,d) for d in Department.objects.all()])
    
    for c in all_companies:
        init_company_for_stat(c)

    # assign receipts
    for r in all_receipts:
        try:
            prepare_receipt_for_stat(r)

            company = company_dict[r.company_id]
            company.returned_receipts.append(r)
        except:
            pass

    for c in all_companies:
        prepare_company_for_stat(c)

    # update sorting order
    for c in all_companies:
        if not c.available:
            c.sorting_value = 3
        elif c.hidden:
            c.sorting_value = 2
        else:
            c.sorting_value = 1
    
    all_companies = sorted(all_companies,
                           cmp=lambda x,y: cmp(x.sorting_value,y.sorting_value))

    companies = [c for c in all_companies if len(c.returned_receipts)!=0]

    assign_makers(companies, year)

    saved_company_count = get_saved_company_count(request)

    return render_to_response('regis/company/selection_stat.html',
                              { 'companies': companies,
                                'saved_company_count': saved_company_count,
                                'currentpage':currentpage, 
                                'allows_changes': allows_changes,
                                'year':year},
                              context_instance=RequestContext(request))

@sa_admin_required    
def company_selection_statistic_modify(request, company_id, change="close"):
    year = get_next_worktraining_year()
    company = get_object_or_404(Company,pk=company_id)
    receipts = Receipt.objects.filter(year=year,company=company).all()
    
    init_company_for_stat(company)
        
    for receipt in receipts:
        prepare_receipt_for_stat(receipt)
        company.returned_receipts.append(receipt)

        if change == "open":
            receipt.available = True
        elif change == "close":
            receipt.available = False
        elif change == "hide":
            receipt.is_hidden = True
        elif change == "show":
            receipt.is_hidden = False
        receipt.save()

    prepare_company_for_stat(company)

    return render_to_response("regis/company/include/company_stat_row.html",
                              { 'company': company,
                                'year': year })

@sa_admin_required
def cancel_assignment(request, assignment_id):
    assignment = get_object_or_404(Assignment,pk=assignment_id)
    if request.method != 'POST':
        return HttpResponseForbidden()

    student = assignment.student
    
    found = False
    for sel in student.selections.all():
        if (sel.company == assignment.company) and (sel.result):
            sel.result = False
            sel.save()

            if not found:
                receipt = Receipt.objects.get(company=sel.company,
                                              year=sel.year,
                                              department=student.department)
                receipt.num_accepted_students -= 1
                if request.POST.get('position_choice','remove')=='remove':
                    receipt.total_student -= 1
                receipt.save()

            found = True

    if not found:
        try:
            position = DirectApplicationPosition.objects.get(
                company=assignment.company,
                year=assignment.year)
        except DirectApplicationPosition.DoesNotExist:
            position = None
        
        if position:
            try:
                app = StudentApplication.objects.get(student=student,
                                                     position=position)
            except StudentApplication.DoesNotExist:
                app = None

            if app:
                app.is_accepted = False
                app.save()

    Log.create("cancel assignment - from %s, student: %s, company: %d" %
               (request.META['REMOTE_ADDR'],
                student.student_id,
                assignment.company_id),
               user=request.user,
               student=student)
    
    assignment.delete()

    from commons.email import send_assignment_cancel_confirmation

    send_assignment_cancel_confirmation(assignment)
    
    return redirect('regis_company_detail',
                    company_id=assignment.company_id)

@sa_admin_required    
def company_selection_statistic_toggle(request, company_id, number):
    year = get_next_worktraining_year()

    try: 
        marker = CompanyStatusMarker.objects.get(year=year, company=company_id, number=number)
        marker.delete()
        return HttpResponse('false')
    except CompanyStatusMarker.DoesNotExist:
        marker = CompanyStatusMarker(year=year, company_id=company_id, number=number)
        marker.save()
        return HttpResponse('true')

class CompanySearchByProvinceForm(forms.Form):
    start_year = 2553
    end_year = get_recent_worktraining_year() + 2
    YEAR_CHOICE = []
    for year in range(start_year,end_year):
        YEAR_CHOICE.append([year,year])
    YEAR_CHOICE.reverse()
    province_name = forms.CharField(label=u'จังหวัด',required=True)
    year = forms.ChoiceField(choices=YEAR_CHOICE, label=u'ปีการศึกษาที่ฝึกงาน')


@admin_or_committee_required
def search_by_province(request):
    companies = []
    company_requests = []
    has_search_result = False
    
    if request.method == 'POST':
        form = CompanySearchByProvinceForm(request.POST)
        if form.is_valid():
            has_search_result = True
            province_name = form.cleaned_data['province_name']
            year = int(form.cleaned_data['year'])
            all_companies = Company.objects.filter(address__contains=province_name).all()
            for company in all_companies:
                assignments = company.assignments_for_year(year)
                if len(assignments) > 0:
                    company.current_assignments = assignments
                    company.company_type = '2'
                    companies.append(company)

            all_company_requests = CompanyRequest.objects.filter(year=year,
                                                                 working_address__contains=province_name).all()
            cr_added = set()
            cr_map = dict([(cr.id,cr) for cr in all_company_requests])
            for assignment in Assignment.objects.filter(year=year, company_request__isnull=False).all():
                cr_id = assignment.company_request.id
                if cr_id in cr_map:
                    if cr_id in cr_added:
                        cr_map[cr_id].current_assignments.append(assignment)
                    else:
                        company_request = cr_map[cr_id]
                        company_requests.append(company_request)
                        company_request.current_assignments = [assignment]
                        cr_added.add(cr_id)
    else:
        form = CompanySearchByProvinceForm()

    return render_to_response('regis/company/search_by_province.html',
                              { 'form': form,
                                'companies': companies,
                                'company_requests': company_requests,
                                'has_search_result': has_search_result,
                                
                                'currentpage': currentpage },
                              context_instance=RequestContext(request))
    
    
    
