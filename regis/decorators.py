from django.http import HttpResponseForbidden

def ajax_login_required(view_function):
    
    def decorate(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseForbidden() 
        return view_function(request, *args, **kwargs)

    return decorate

