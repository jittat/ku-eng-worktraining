import os
import sys

sys.path.append('/home/wt/ENV/wt/lib/python2.7/site-packages')
sys.path.append('/home/wt/lib/python-packages')
sys.path.append('/home/wt/prog')
sys.path.append('/home/wt/prog/worktraining')

os.environ['DJANGO_SETTINGS_MODULE'] = 'worktraining.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

