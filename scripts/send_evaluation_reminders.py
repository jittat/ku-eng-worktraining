# -*- coding: utf-8 -*-
"""
sends reminder emails for students with no evaluation
"""
import sys

if len(sys.argv)!=2:
    print "Usage: python send_evaluation_reminders.py [year]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Student, Assignment
from evaluations.models import Evaluation
from commons.email import send_mail_to_student

year = int(sys.argv[1])
for assignment in Assignment.objects.filter(year=year).all():
    student = assignment.student
    evaluations = Evaluation.objects.filter(assignment=assignment).all()
    if not evaluations and student.has_registered(year):
        print student.department, student
        send_mail_to_student(student,
                             u'การติดตามแบบการประเมินผลฝึกงาน',
                             u'''เรียนคุณ %s

ขณะนี้ในระบบฝึกงาน **ยังไม่ได้รับข้อมูล** แบบการประเมินผลการฝึกงานของคุณ

นิสิตจะต้องติดตามแบบประเมินผลและใบลงเวลาการฝึกงานมาส่งที่หน่วยกิจการนิสิตให้เรียบร้อยก่อน 31 สิงหาคม 2559 
หากพ้นกำหนดนี้จะถือว่าไม่ผ่านการฝึกงาน โดยตรวจสอบรายชื่อได้ที่เว็บไซต์การฝึกงาน www.wt.eng.ku.ac.th 
หรือที่หน่วยกิจการนิสิต

-ทีมงานเว็บฝึกงาน
''' % student)
