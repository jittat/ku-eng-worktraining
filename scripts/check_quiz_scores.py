from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

import requests

from regis.models import Student, Status
from datetime import datetime

FULL_SCORE = 10
WT_YEAR = 2564

LINE_SKIPPED = 4567

def strip_quotes(st):
    if st[0] == '"':
        return st[1:-1]

def load_result_sheets(url):
    result = requests.get(url)
    lines = result.text.split("\n")
    results = []
    counter = 0
    for l in lines[1:]:
        items = l.split(",")
        if len(items) < 5:
            continue

        counter += 1
        if counter <= LINE_SKIPPED:
            continue
        
        timestamp = strip_quotes(items[0])
        email = strip_quotes(items[1])
        score_str = items[2].split()[0][1:]
        try:
            score = int(score_str)
        except:
            score = 0
        student_id = strip_quotes(items[3])

        data = {
            'timestamp': timestamp,
            'email': email,
            'score': score,
            'student_id': student_id.strip(),
        }
        results.append(data)
    return results

def main():
    url = settings.QUIZ_RESULT_URL
    results = load_result_sheets(url)

    best_scores = {}
    last_timestamps = {}
    for r in results:
        std_id = r['student_id']

        if std_id == '':
            continue
        
        if std_id not in best_scores:
            best_scores[std_id] = r['score']
        else:
            if r['score'] > best_scores[std_id]:
                best_scores[std_id] = r['score']
        last_timestamps[std_id] = r['timestamp']

    for student_id in best_scores.keys():
        students = Student.objects.filter(student_id=student_id)
        if len(students)!=1:
            continue

        student = students[0]
        status = student.get_status(WT_YEAR)

        if not status:
            continue

        if status.has_passed_quiz_exam:
            continue

        score = best_scores[student_id]
        
        if score > status.best_quiz_score:
            status.best_quiz_score = score
        if best_scores[student_id] >= FULL_SCORE:
            status.has_passed_quiz_exam = True

        ts = datetime.strptime(last_timestamps[student_id],
                               '%m/%d/%Y %H:%M:%S')

        if (not status.last_quiz_submitted_at) or (status.last_quiz_submitted_at < ts):
            status.last_quiz_submitted_at = ts
            status.save()

if __name__ == '__main__':
    main()
