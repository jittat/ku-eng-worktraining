
def check_memo_position(data,p):
    mm = data[i:i+4]
    if mm=='Memo':
        if data[i+4] == '1':
            if '0' <= data[i+5] <= '9':
                return (int(data[i+4:i+6]),i+6)
            else:
                return (int(data[i+4:i+5]),i+5)
        if '2' <= data[i+4] <= '9':
            return (int(data[i+4:i+5]),i+5)
    return None

def extract_raw(raw):
    for i in range(len(raw)):
        if raw[i:i+3] == '\xff\xff\xff':
            start = i+8
            for c in range(start,len(raw)):
                if raw[c] == '\x00':
                    return raw[start:c]

f = open("../../data/label/label.frp","rb")

data = f.read()
f.close()

length = len(data)

memo_sequences = [6,7,8,9,12,1,2,3,4,5,10,11]
memo_count = len(memo_sequences)
memo_index = 0
old_index = memo_count - 1

chunks = []
old_start = -1

for i in range(length):
    m = check_memo_position(data,i)
    if m!=None:
        if m[0] == memo_sequences[memo_index]:
            # in sequence
            if old_start != -1:
                chunks.append((old_index, data[old_start:i]))

            old_start = m[1]
            old_index = memo_index

            memo_index = (memo_index + 1) % memo_count
        else:
            print 'BAD SEQUENCE'

chunks.append((old_index, data[old_start:i]))
for mid,raw in chunks:
    print mid, extract_raw(raw)

