# -*- coding: utf-8 -*-

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Status, Assignment

def main():
    companies = Company.objects.all()
    for company in companies:
        #assignments = company.assignments.all()
        assignments = company.assignments.filter(year=2561).all()
        if len(assignments) != 0:
            last_year = max([a.year for a in assignments])
            items = [
                company.id,
                company.name,
                len(assignments),
                company.contact_name,
                company.contact_email,
                company.tel_no,
                company.fax_no,
                last_year,
            ]

            print u",".join([u'"' + unicode(i) + u'"' for i in items])

if __name__=='__main__':
    main()



