# -*- coding: utf-8 -*-

"""
fixes Civil Eng and Chem Eng messed up
"""
from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from django.shortcuts import get_object_or_404

from regis.models import Company, Student, Department, Assignment, Education


def fix_department(ce, che):
    print "fixing department numbers"

    ce.code = '299'
    ce.save()
    che.code = '202'
    che.save()
    ce.code = '203'
    ce.save()

ce_dept = get_object_or_404(Department, code='202')
che_dept = get_object_or_404(Department, code='203')

if ce_dept.name != u'วิศวกรรมโยธา':
    print 'Already fixed'
    quit()

if che_dept.name != u'วิศวกรรมเคมี':
    print 'Already fixed'
    quit()

for student in Student.objects.all():
    if student.department == ce_dept:
        print "To CHE", student.full_name()
        student.department = che_dept
        student.save()
        continue

    if student.department == che_dept:
        print "To CE", student.full_name()
        student.department = ce_dept
        student.save()

fix_department(ce_dept, che_dept)



