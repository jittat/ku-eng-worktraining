from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection

for sel in StudentSelection.objects.all():
    sel.created_at = sel.selection_at
    sel.selection_at = sel.modified_at
    sel.save()
