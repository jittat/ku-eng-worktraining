import sys
import os
from datetime import datetime,date

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import SelectionRound
from regis.models import Receipt

today = datetime.today()
sel_rounds = list(SelectionRound.objects.filter(beginning_date__lt=today).all())
for receipt in Receipt.objects.all():
    num = len([r for r in sel_rounds
               if r.beginning_date > receipt.created_at.date()])
    receipt.num_rounds_shown = num
    receipt.save()
    print receipt.id, num
                   


        
