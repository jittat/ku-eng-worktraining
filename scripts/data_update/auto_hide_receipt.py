import sys
import os
from datetime import datetime,date

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import SelectionRound
from regis.models import Receipt

MAX_NUM_SHOWN_BEFORE_HIDDEN = 2

for receipt in Receipt.objects.all():
    if receipt.num_rounds_shown >= MAX_NUM_SHOWN_BEFORE_HIDDEN:
        print "Hide:", receipt.id, receipt.num_rounds_shown
        receipt.is_hidden = True
        receipt.save()



        
