import sys
import os
from datetime import datetime,date

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import SelectionRound, SelectionRoundForReceipt
from regis.models import Receipt
from commons.academia import get_next_worktraining_year

MAX_AUTO_ROUND_NUMBER = 6

year = get_next_worktraining_year()
today = datetime.today()
sel_rounds = list(SelectionRound.objects.filter(beginning_date__lt=today).all())
for receipt in Receipt.objects.filter(year=year):
    rounds = [r for r in sel_rounds
              if r.beginning_date > receipt.created_at.date() and r.number <= MAX_AUTO_ROUND_NUMBER]
    
    for r in rounds:
        (SelectionRoundForReceipt.
         create_one_for_receipt_and_round(receipt,
                                          r.number,
                                          year))

    print receipt.id, len(rounds)

                   


        
