# -*- coding: utf-8 -*-
"""
reads and imports student information from the Registra.
"""
import sys

if len(sys.argv)!=4:
    print "Usage: python import_student.py [student_file_name] [info_year] [info_semester]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Department, Assignment, Education

import codecs
import csv

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.DictReader(utf_8_encoder(unicode_csv_data),
                                dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield dict([(k,unicode(v, 'utf-8')) for k,v in row.items() if v!=None])

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


filename = sys.argv[1]
info_year = int(sys.argv[2])
info_semester = int(sys.argv[3])

f = codecs.open(filename,'r',encoding='utf-8')

dept_not_found = set()

for line in unicode_csv_reader(f):
    student_id = line['STD_ID']

    try:
        student = Student.objects.get(student_id=student_id)
    except Student.DoesNotExist:
        print 'Created:', student_id
        student = Student()
        student.student_id = student_id

    student.prefix = line['STD_TITLE']
    student.first_name = line['STD_NAME']
    student.last_name = line['STD_SURNAME']

    if len(line['STD_TITLE_E'].strip()) <= 10:
        student.eng_prefix = line['STD_TITLE_E'].strip()
    else:
        print line['STD_TITLE_E']
    student.eng_first_name = line['STD_ENAME']
    student.eng_last_name = line['STD_ESURNAME']
    
    department_name = line['MAJOR_NAME']
    if department_name==u'วิศวกรรมการบินและอวกาศ-บริหารธุรกิจ':
        department_name=u'วิศวกรรมการบินและอวกาศ'

    if u'ภาคภาษาอังกฤษ' in department_name:
        department_name = department_name.split()[0]
        
    if u'ภาษาอังกฤษ' in department_name:
        department_name = department_name.split()[0]
        
    try:
        department = Department.objects.get(name=department_name)
    except:
        department = None
        print 'Department Not Found:', department_name

    if not student.department:
        student.department = department
    else:
        if (department) and (student.department_id != department.id):
            print ('Department conflict %s (db:%d, csv:%d)' % 
                   (student.student_id,
                    student.department_id,
                    department.id))

    if student.education!=None:
        edu = student.education
    else:
        edu = Education()
            
    edu.total_credit = int(float(line['ALL_CREDIT']))
    edu.gpax = float(line['ALL_GPA'])
    edu.upto_year = info_year
    edu.upto_semester = info_semester
    edu.save()

    student.education = edu
    student.save()
