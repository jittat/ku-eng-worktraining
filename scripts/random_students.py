from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from random import randint

from regis.models import Student, Department

ALPHA = 'abcdefghijklmnopqrstuvwxyz'

def random_string(l, alpha=ALPHA):
    return ''.join(
        [alpha[randint(0,len(alpha)-1)] 
         for i in range(l)])

def generate_one_random_student(departments):
    student_id = '50' + random_string(6,'0123456789')
    first_name = 'S' + random_string(7)
    last_name = 'T' + random_string(10)
    email = random_string(5) + '@ku.ac.th'
    tel_no = random_string(9,'0123456789')
    department = departments[randint(0,len(departments)-1)]

    student = Student(
        student_id=student_id,
        first_name=first_name,
        last_name=last_name,
        email=email,
        tel_no=tel_no,
        department=department)
    student.save()

def main():
    n = 300

    departments = list(Department.objects.all())

    for i in range(n):
        generate_one_random_student(departments)

if __name__=='__main__':
    main()
