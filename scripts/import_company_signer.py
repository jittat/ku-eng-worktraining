import sys

if len(sys.argv)!=2:
    print "Usage: python import_company_signer.py [company_file_name]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company

import codecs
import csv

filename = sys.argv[1]
f = codecs.open(filename,'r',encoding='utf-8')

position = None

for l in f.readlines():
    items = l.split()
    num = int(items[0])
    data = ' '.join(items[1:])

    if num==0 or num==5:
        position = data
    if num==1 or num==6:
        companies = Company.objects.filter(name=data)
        if len(companies) == 0:
            companies = Company.objects.filter(name__startswith=data)

        if len(companies)!=1:
            print "ERROR", data, " with ", len(companies)
            continue

        company = companies[0]
        company.signer_name = position
        company.save()

