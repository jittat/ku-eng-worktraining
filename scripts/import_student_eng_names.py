# -*- coding: utf-8 -*-
"""
reads and imports student information from the Registra.
"""
import sys

if len(sys.argv)!=2:
    print "Usage: python import_student_eng_names.py [file]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Department, Assignment, Education

import codecs
import csv

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.DictReader(utf_8_encoder(unicode_csv_data),
                                dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield dict([(k,unicode(v, 'utf-8')) for k,v in row.items() if v!=None])

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


filename = sys.argv[1]

f = codecs.open(filename,'r',encoding='utf-8')

dept_not_found = set()

count = 0
for line in unicode_csv_reader(f):
    student_id = line['STD_ID']

    try:
        student = Student.objects.get(student_id=student_id)
    except Student.DoesNotExist:
        print 'NOT FOUND:', student_id
        continue

    student.eng_prefix = line['STD_TITLE_E']
    student.eng_first_name = line['STD_ENAME']
    student.eng_last_name = line['STD_ESURNAME']
    student.save()
    print student.student_id
    count += 1

print count, 'imported'
