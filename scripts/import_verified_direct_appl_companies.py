"""
reads and imports student information from the Registra.
"""
import sys

if len(sys.argv)!=3:
    print "Usage: python import_direct_appl_companies.py [filename] [year]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Department, Assignment, Education
from std.models import VerifiedDirectApplicationCompany

import codecs
import csv

filename = sys.argv[1]
year = int(sys.argv[2])

f = codecs.open(filename,'r',encoding='utf-8')

lines = f.readlines()
for l in lines:
    l = l.strip()
    if l=='':
        continue
    c = Company.objects.filter(name=l).all()
    if len(c)==0:
        print 'Not found:',l
    else:
        cc = c[0]
        if VerifiedDirectApplicationCompany.check(cc,year):
            print '(OK)',l
            continue
        vc = VerifiedDirectApplicationCompany(company=cc,
                                              year=year)
        vc.save()
        print 'OK',l


