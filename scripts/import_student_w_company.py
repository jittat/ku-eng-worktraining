import sys

if len(sys.argv)!=4 or len(sys.argv[2])!=2:
    print "Usage: python import_student.py [student_file_name] [std_year_to_import] [assignment_year]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Department, Assignment

import codecs
import csv

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


filename = sys.argv[1]
year = sys.argv[2]
assignment_year = int(sys.argv[3])

f = codecs.open(filename,'r',encoding='utf-8')

dept_not_found = set()

added_count = 0
updated_count = 0

for line in unicode_csv_reader(f):
    std_id = line[0]
    prefix = line[1]
    name =line[2]
    items = name.split()

    if len(items)==0:
        continue

    first_name = items[0]
    last_name = ' '.join(items[1:])

    if len(line)>3:
        dept_code = line[3]
    else:
        dept_code = ''

    if len(line)>4:
        company_short_name = line[4]
    else:
        company_short_name = ''
    
    if not std_id.startswith(year):
        continue
    
    try:
        dept = Department.objects.get(code=dept_code)
    except:
        dept_not_found.add(dept_code)
        dept = None


    students = Student.objects.filter(student_id=std_id).all()
    if len(students)!=0:
        st = students[0]
        updated_count += 1
    else:
        st = Student(student_id=std_id)
        added_count += 1

    st.prefix = prefix
    st.first_name = first_name
    st.last_name = last_name
    st.department = dept

    st.save()

    if std_id.startswith(year) and company_short_name!='':
        try:
            company = Company.objects.get(short_name=company_short_name)
        except:
            company = None

        if company:
            try:
                assignment = Assignment.objects.get(student=st,
                                                    year=assignment_year)
            except Assignment.DoesNotExist:
                assignment = None

            if not assignment:
                assignment = Assignment(company=company,
                                        student=st,
                                        year=assignment_year)
            else:
                assignment.company = company

            assignment.save()

print 'Total:', added_count, 'added, ', updated_count, 'updated'
print 'Department code not found:'
for d in dept_not_found:
    print d,
