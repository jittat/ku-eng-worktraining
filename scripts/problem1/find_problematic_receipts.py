import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection, SelectionRoundStatus
from regis.models import Receipt, DepartmentNameInRequestLetter
from commons.academia import get_next_worktraining_year

status = SelectionRoundStatus.get_status()

#if status.is_open:
#    quit()

year = get_next_worktraining_year()
sel_round = SelectionRoundStatus.get_last_round()
round_number = 3 #sel_round.number

print 'Update for round', round_number

# update assignment
for sel in StudentSelection.objects.filter(round_number=round_number,
                                           year=year).all():
    if sel.result:
        sel.save_to_assignment()

# update available positions
reciepts = Receipt.objects.filter(year=year).select_related(depth=1).all()
counter = {}
for r in reciepts:
    counter[(r.company_id,r.department_id)] = [0,r,0,[]]

for sel in (StudentSelection.objects.filter(year=year)
            .select_related(depth=1).all()):
    actual_dept = DepartmentNameInRequestLetter.to_requested_department(sel.student.department)
    if sel.result:
        counter[(sel.company_id, actual_dept.id)][0] += 1
        if sel.round_number==2:
            counter[(sel.company_id, actual_dept.id)][2] += 1

    counter[(sel.company_id, actual_dept.id)][3].append(sel)


for k in counter.keys():
    c = counter[k]
    if c[0]!=c[2] or c[0]==c[1].total_student:
        r = c[1]
        
        effected = [s for s in c[3] if s.round_number==3]

        if len(effected)!=0:

            num_left = r.total_student - c[0]
            num_left_shown = r.total_student - c[2]

            print u"%s,%s,%d,%d,%d,%d" % (r.company, r.department, num_left, num_left_shown, c[0], c[2])
            
            for s in effected:
                print "    %s, %s, %s, %s" % (s.student.student_id, s.student, s.student.email, s.modified_at.strftime("%d Nov, %H:%M"))
        


