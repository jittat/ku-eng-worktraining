import sys

if len(sys.argv)!=2:
    print "Usage: python report_sel_for_problem_std.py [raw-problem-filename]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection
from regis.models import Student

import codecs
import csv

filename = sys.argv[1]
fp = codecs.open(filename,'r',encoding='utf-8')

round_number = 3

org_company = ''

for l in fp.readlines():
    if l[0]!=' ':
        items = l.split(',')
        if len(items)>=2:
            org_company = items[0].strip()
            dept = items[1].strip()
    else:
        items = l.split(',')
        if len(items)==4:
            stdid, name, date, time = [s.strip() for s in items]
        elif len(items)==5:
            stdid, name, email, date, time = [s.strip() for s in items]
        else:
            continue

        student = Student.objects.get(student_id=stdid)
        sel = list(StudentSelection.objects.filter(student=student,round_number=round_number).all())
        
        if len(sel)!=0:
            print u"%s,%s,%s,%s,%s,%s,%s" % (stdid, name, dept, org_company, date, time, sel[0].company)
        else:
            print u"%s,%s,%s,%s,%s,%s,***" % (stdid, name, dept, org_company, date, time)



