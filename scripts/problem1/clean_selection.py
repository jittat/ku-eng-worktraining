import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection, SelectionRoundStatus
from regis.models import Receipt, DepartmentNameInRequestLetter, Student
from commons.academia import get_next_worktraining_year

s = """51057263
51059020
51057073
51057669
51056323
51551729
51551919
51051969
51050623
51053049
51051878
50056704
51051852
51058303"""

round_number = 3
for sid in s.split("\n"):
    s = Student.objects.get(student_id=sid)
    sel = StudentSelection.objects.filter(student=s, round_number=round_number)
    print sel


