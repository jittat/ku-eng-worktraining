import sys
import datetime

if len(sys.argv) not in [2, 3]:
    print "Usage: python import_old_evaluations.py [csv] [--yes/--yes-update]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Student, Company, Assignment
from evaluations.models import Evaluation

CURRENT_YEAR = 2553
START_DATE = '23-03-2010'
END_DATE = '23-05-2010'

import codecs
import csv

def str_to_date(s):
    return datetime.datetime.strptime(s,'%d-%m-%Y').date()

START_DATE = str_to_date(START_DATE)
END_DATE = str_to_date(END_DATE)

def unicode_csv_reader(unicode_csv_data, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data))
    for row in csv_reader:
        yield row

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')

def zero_or_num(nstr):
    if nstr!='-':
        try:
            n = int(nstr)
        except:
            n = 0
        return n
    else:
        return 0

def read_input(filename):
    f = codecs.open(filename,'r',encoding='utf-8')
    all_data = []
    for line in unicode_csv_reader(f):
        data = {
            'student_id': line[1],
            'name': line[2],
            'dept': line[3],
            'company': line[4],
            'num_working_days': zero_or_num(line[5]),
            'num_late_days': zero_or_num(line[6]),
            'num_personal_leave_days': zero_or_num(line[7]),
            'num_heath_leave_days': zero_or_num(line[8]),
            'num_absent_leave_days': zero_or_num(line[9]),
            'comments': line[10],
            'date': str_to_date(line[11]),
            }
        all_data.append(data)
    return all_data

def main():
    all_data = read_input(sys.argv[1])
    
    is_save = (len(sys.argv)==3) and (sys.argv[2] in ['--yes','--yes-update'])
    is_update = (len(sys.argv)==3) and (sys.argv[2] == '--yes-update')


    for data in all_data:
        student_id = data['student_id']
        try:
            student = Student.objects.get(student_id=student_id)
        except:
            print 'Student not found:', student_id
            continue

        assignment = student.assignment_for_year(CURRENT_YEAR)
        if not assignment:
            print 'Assignment not found:', student_id
            continue

        try:
            evaluation = Evaluation.objects.get(assignment=assignment)
            print "(updated)",
            if not is_update:
                print "skipped", student_id
                continue
        except:
            evaluation = Evaluation(student=student, assignment=assignment)

        evaluation.received_at = data['date']
        evaluation.evaluation_form_received = True
        
        evaluation.beginning_date = START_DATE
        evaluation.end_date = END_DATE

        evaluation.num_working_days = data['num_working_days']
        evaluation.num_late_days = data['num_late_days']
        evaluation.num_personal_leave_days = data['num_personal_leave_days']
        evaluation.num_heath_leave_days = data['num_heath_leave_days']
        evaluation.num_absent_leave_days = data['num_absent_leave_days']

        evaluation.timesheet_received = True

        evaluation.internal_comments = data['comments']

        if is_save:
            evaluation.save()

        print student_id, ",", data['name'], ",", data['company'], ",", assignment.company.name

if __name__ == '__main__':
    main()
