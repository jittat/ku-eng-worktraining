# -*- coding: utf-8 -*-
from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Department

namemap = {
    '204':'Department of Computer Engineering',
    '203':'Department of Civil Engineering',
    '202':'Department of Chemical Engineering',
    '205':'Department of Electrical Engineering',
    '206':'Department of Industrial engineering',
    '208':'Department of Mechanical Engineering',
    '213':'Department of Materials Engineering',
    '215':'Department of Aerospace Engineering',
    '211':'Department of Electrical Mechanical Manufacturing Engineering',
    '218':'Department of Survey Engineering and Geographic Information',
    '219':'Department of Aerospace Engineering',
    '220':'Department of Aerospace Engineering',
    '221':'Department of Civil-Water Resources Engineering',
    '222':'Department of Aerospace Engineering',
}

departments = Department.objects.all()
for department in departments:
    if department.code in namemap:
        department.eng_name = namemap[department.code].strip()
        print(department.code +" "+department.eng_name)
        department.save()
    
print("import complete!!")
        
