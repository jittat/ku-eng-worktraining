import sys

if len(sys.argv)!=3:
    print "Usage: python assign_committee.py [login] [dept_number]"
    quit()


from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Department
from django.contrib.auth.models import User
from commons.models import UserProfile

username = sys.argv[1]
dept_code = sys.argv[2]

department = Department.objects.get(code=dept_code)
user = User.objects.get(username=username)

try:
    profile = user.userprofile
except UserProfile.DoesNotExist:
    profile = UserProfile(user=user)

profile.department = department
profile.is_committee_member = True

profile.save()

print "Assign", username, "to", department.name
