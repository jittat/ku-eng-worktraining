import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from committee.status_report import send_company_request_notification_all, send_company_direct_application_result_notification_all

send_company_request_notification_all()
send_company_direct_application_result_notification_all()
