import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import SelectionRound, SelectionRoundStatus, SelectionRoundForReceipt, Receipt
from update_assignments import update_assignments

def update_selection_round():
    status = SelectionRoundStatus.get_status()

    if status:
        is_currently_open = status.is_open
        new_status = SelectionRoundStatus.update(datetime.datetime.now(), fake=True)
        if (not is_currently_open) and new_status.is_open:
            # close -> open
            try:
                current_round = SelectionRound.get_round(new_status.current_round_number)
            except:
                current_round = None
            if current_round:
                SelectionRoundForReceipt.update_receipts_round_shown(current_round)

        elif (is_currently_open) and (not new_status.is_open):
            # open -> close
            Receipt.auto_hide_expired_receipts()

    SelectionRoundStatus.update(datetime.datetime.now())


if __name__=='__main__':
    print "=============================="
    print "Updated at:", datetime.datetime.now()
    update_selection_round()
