import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection, SelectionRoundStatus, SelectionRound
from regis.models import Receipt, DepartmentNameInRequestLetter, Assignment
from commons.academia import get_next_worktraining_year


def update_assignments(new_round_for_open_selection=True,
                       update_position_for_this_round=True):
    status = SelectionRoundStatus.get_status()

    if status.is_open:
        return

    year = get_next_worktraining_year()
    sel_round = SelectionRoundStatus.get_last_round()

    if not sel_round:
        return

    round_number = sel_round.number

    print 'Update for round', round_number

    # update accepted assignment
    for sel in StudentSelection.objects.filter(round_number=round_number,
                                               year=year).all():
        if sel.result:
            sel.save_to_assignment()
        else:
            assignments = Assignment.objects.filter(student=sel.student,
                                                    receipt=sel.receipt).all()
            if len(assignments)!=0:
                for a in assignments:
                    print 'reverting ', a.id
                    a.delete()

    # for open selection, create a new one for the next round
    print 'Open selection:'
    for sel in StudentSelection.objects.filter(round_number=round_number,
                                               year=year,
                                               result=None).all():
        if new_round_for_open_selection:
            new_sel = StudentSelection(
                student=sel.student,
                company=sel.company,
                receipt=sel.receipt,
                round_number=round_number+1,
                year=year,
                selection_at=sel.selection_at,
                result=None)
            #print sel.student.student_id
            #print u'%s,%s,%s,%s' % (sel.student, 
            #                        sel.student.department, 
            #                        sel.company, 
            #                        sel.selection_at)
            try:
                new_sel.save()
            except:
                print 'Dupplicate keys'
        else:
            print sel.student.student_id, sel.student.department.code


    # update available positions
    reciepts = Receipt.objects.filter(year=year).select_related('department').all()
    counter = {}
    for r in reciepts:
        counter[(r.id,r.department_id)] = [0,r]

    for sel in (StudentSelection.objects.filter(year=year)
                .select_related('student').all()):
        if sel.result:
            actual_dept = DepartmentNameInRequestLetter.to_requested_department(sel.student.department)

            if ((not update_position_for_this_round) and
                (sel.round_number == round_number)):
                continue
            counter[(sel.receipt_id, actual_dept.id)][0] += 1

    for k in counter.keys():
        c = counter[k]
        c[1].num_accepted_students = c[0]
        c[1].save()


def main():
    now = datetime.datetime.now()
    next_hour = now + datetime.timedelta(hours=1)

    status = SelectionRoundStatus.get_status()

    if status.is_open:
        return

    if status.current_round_number:
        next_round = SelectionRound.get_round(status.current_round_number + 1)
    else:
        next_round = None
        
    if next_round and (next_hour > next_round.beginning_time):
        print 'Update assignment for next round (and shift unconfirmed selections).'
        update_assignments()
    else:
        if next_hour.day != now.day:
            print 'Update assignment for today.'
            update_assignments(new_round_for_open_selection=False,
                               update_position_for_this_round=True)


if __name__=='__main__':
    main()

