import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection, SelectionRoundStatus
from regis.models import Receipt, DepartmentNameInRequestLetter
from commons.academia import get_next_worktraining_year

status = SelectionRoundStatus.get_status()

if status.is_open:
    quit()

year = get_next_worktraining_year()
sel_round = SelectionRoundStatus.get_last_round()
round_number = sel_round.number

# for open selection, create a new one for the next round

print 'Open selection:'
for sel in StudentSelection.objects.filter(round_number=round_number,
                                           year=year,
                                           result=None).all():
    print u'%s,%s,%s,%s' % (sel.student, 
                            sel.student.department, 
                            sel.company, 
                            sel.selection_at)


        


