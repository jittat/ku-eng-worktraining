from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from random import randint

from regis.models import Company

ALPHA = 'abcdefghijklmnopqrstuvwxyz'

def random_string(l, alpha=ALPHA):
    return ''.join(
        [alpha[randint(0,len(alpha)-1)] 
         for i in range(l)])

def generate_one_random_company():
    name = 'C' + random_string(10)
    short_name = random_string(4)

    tel_no = random_string(9,'0123456789')

    first_name = random_string(7)
    last_name = random_string(10)
    signer_name = 'S' + first_name + ' ' + 'S' + last_name

    first_name = random_string(7)
    last_name = random_string(10)
    contact_name = 'C' + first_name + ' ' + 'C' + last_name

    company = Company(
        name=name,
        short_name=short_name,
        tel_no=tel_no,
        signer_name=signer_name,
        contact_name=contact_name,
        address='dep of cpe\nkasetsart univ'
        )
    company.save()

def main():
    n = 100

    for i in range(n):
        generate_one_random_company()

if __name__=='__main__':
    main()
