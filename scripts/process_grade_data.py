"""
calculates student credits and grades using data from the Registra.
"""
import sys
import codecs
import csv

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.DictReader(utf_8_encoder(unicode_csv_data),
                                dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield dict([(k,unicode(v, 'utf-8')) for k,v in row.items()])

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


def dumpheader(f):
    print >> f, "STD_ID,STD_TITLE,STD_TITLE_E,STD_NAME,STD_SURNAME,STD_ENAME,STD_ESURNAME,MAJOR_NAME,ALL_GPA,ALL_CREDIT"

def dumpdata(f, grade_data):
    if grade_data['grade_credit']==0:
        print 'Error', grade_data['id']
        grade_data['gpa'] = 0
    else:
        grade_data['gpa'] = float(grade_data['grade_score'])/grade_data['grade_credit']
    print >> f, (u"%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%d" %
                 tuple([grade_data[k] for k in
                        ['id','title','eng_title',
                         'first_name','last_name',
                         'eng_first_name','eng_last_name',
                         'major','gpa','credit']]))


def grade_to_number(grade):
    numdict = {'A': 4,
               'B+': 3.5,
               'B': 3,
               'C+': 2.5,
               'C': 2,
               'D+': 1.5,
               'D': 1,
               'F': 0 }
    return numdict[grade]

def update_grade_data(grade_data, data):
    grade = data['GRADE']
    credit = int(data['TT_CREDIT'])
    if grade not in ['F','W','S','U','I','N','NP']:
        grade_data['credit'] += credit

    if grade not in ['W','S','U','P','I','N','NP']:
        grade_data['grade_score'] += float(credit) * grade_to_number(grade)
        grade_data['grade_credit'] += credit


def main(grade_filename, output_filename):
    f = codecs.open(grade_filename, 'r', encoding='utf-8')
    fout = codecs.open(output_filename, 'w', encoding='utf-8')
    dumpheader(fout)

    old_id = ''
    grade_data = None

    for data in unicode_csv_reader(f):
        student_id = data['STD_ID']

        #print data

        if student_id != old_id:

            if grade_data != None:
                dumpdata(fout, grade_data)

            grade_data = {
                'id': data['STD_ID'],
                'title': data['STD_TITLE'],
                'first_name': data['STD_NAME'],
                'last_name': data['STD_SURNAME'],
                'eng_title': data['STD_TITLE_E'],
                'eng_first_name': data['STD_ENAME'],
                'eng_last_name': data['STD_ESURNAME'],
                'major': data['MAJOR_NAME'],
                'credit': 0,
                'grade_score': 0.0,
                'grade_credit': 0,
                }
            old_id = student_id

        update_grade_data(grade_data, data)

    if grade_data != None:
        dumpdata(fout, grade_data)

    fout.close()
    f.close()

if __name__ == '__main__':
    if len(sys.argv)!=3:
        print "Usage: python import_student.py [grade_file] [output_file]"
        quit()

    main(sys.argv[1], sys.argv[2])


