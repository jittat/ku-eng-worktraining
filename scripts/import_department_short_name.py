# -*- coding: utf-8 -*-
from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Department

namemap = {u'204': u'คต', 
           u'203': u'ยธ',
           u'202': u'คม',
           u'205': u'ฟฟ',
           u'206': u'อก',
           u'208': u'คก',
           u'209': u'ทน',
           u'210': u'สวล',
           u'213': u'วส',
           u'215': u'กบอ',
           u'211': u'ฟคก',
           u'218': u'สสภ',
           u'219': u'กกบ',
           u'220': u'ทกบ',
           u'221': u'ยธทน',
           u'217': u'ซค'}

departments = Department.objects.all()
for department in departments:
    department.short_name = namemap[department.code]
    print(department.code +" "+department.short_name)
    department.save()
    
print("import complete!!")
        