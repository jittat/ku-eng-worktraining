from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Status, Assignment

import sys

def main():
    year = int(sys.argv[1])
    statuses = Status.objects.filter(year=year,
                                     is_registered=True,
                                     registration_type=Status.OWN_SELECTION_TYPE).select_related('student').all()

    results = []
    
    for status in statuses:
        student = status.student
        info = student.get_working_self_appl_info(year)
        assignment = student.assignment_for_year(year)
        
        if info['company_request']==None and info['direct_application']==None and assignment == None:
            print 'ERROR: None',student.student_id, student, student.assignment_for_year(year)
            continue
        elif info['company_request'] and info['direct_application']:
            print 'ERROR: Too many assignment:',student.student_id, student, student.assignment_for_year(year)
            continue

        
        if info['company_request']==None and info['direct_application']==None:
            if not assignment.company:
                print 'ERROR: None',student.student_id, student, student.assignment_for_year(year)
                continue

            company_name = assignment.company.name
            wt_type = '2'
            data_id = assignment.id
        elif info['company_request']:
            company_name = info['company_request'].company_name
            wt_type = '1.1'
            data_id = info['company_request'].id
        else:
            company_name = info['direct_application'].company_name
            wt_type = '1.2'
            data_id = info['direct_application'].id

        results.append((student.student_id, student, student.email, student.tel_no, student.department, company_name, wt_type, data_id))

    for r in results:
        print u','.join([u'"' + unicode(x) + u'"' for x in r])

if __name__=='__main__':
    main()



