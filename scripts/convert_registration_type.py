# -*- coding: utf-8 -*-
import sys

if len(sys.argv)!=2:
    print "Usage: python convert_registration_type.py [year_for_detail]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from datetime import datetime
from regis.models import Status
from std.models import RequestStudent, CompanyDirectApplication
from commons.email import send_mail_to_student
from commons.academia import can_convert_student_reg_type

def send_notification_email(student):
    subject = u'แจ้งการเปลี่ยนประเภทการสมัครฝึกงาน'
    message = (u"""%s:

จากที่ได้มีการชี้แจงเกี่ยวกับการฝึกงานว่า ในกรณีที่นิสิตที่สมัครเข้าขอฝึกงานในแบบที่ต้องการจัดหาสถานประกอบการเอง
นิสิตจะต้องดำเนินการภายในวันที่ 15 พ.ย. 2556   ถ้านิสิตไม่ได้ดำเนินการใด ๆ คณะฯ จะพิจารณาดำเนินการ
ย้ายประเภทการขอเข้าฝึกงานของนิสิตไปเป็นประเภทที่คณะจัดหาให้

เนื่องจากคุณยังไม่ได้ดำเนินการใด ๆ เกี่ยวกับการจัดหาสถานประกอบการในระบบออนไลน์ และขณะนี้ได้เลยกำหนด
วันที่ 15 พ.ย. 2556 แล้ว  ทางคณะฯ จึงได้เปลี่ยนประเภทการขอเข้าฝึกงานของคุณให้เป็นแบบที่ 2 คือเป็น
แบบที่ให้คณะจัดหาหน่วยงานให้

ถ้าคุณได้ดำเนินการจัดหาหน่วยงาน หรือได้สมัครหน่วยงานไว้แล้ว แต่ไม่ได้แจ้งข้อมูลผ่านระบบออนไลน์
ให้คุณรีบติดต่อหน่วยกิจการนิสิต คณะวิศวกรรมศาสตร์ โดยด่วน

-ทีมงานเว็บระบบฝึกงาน""" % (student.full_name()))
    
    send_mail_to_student(student, subject, message)

def main():
    year = int(sys.argv[1])

    students_with_activities = set()

    for request in RequestStudent.objects.filter(year=year).all():
        students_with_activities.add(request.student_id)

    for app in CompanyDirectApplication.objects.filter(year=year).all():
        students_with_activities.add(app.student_id)

    registrations = Status.objects.filter(year=year,
                                          is_registered=True,
                                          registration_type=Status.OWN_SELECTION_TYPE).all()

    for reg in registrations:
        if reg.student_id not in students_with_activities:
            student = reg.student
            if not can_convert_student_reg_type(student, student.department):
                continue
            print student.student_id, student, student.department
            reg.registration_type = Status.FACULTY_SELECTION_TYPE
            reg.is_converted_from_self_appl = True
            reg.converted_at = datetime.now()
            reg.save()

            send_notification_email(reg.student)

main()
