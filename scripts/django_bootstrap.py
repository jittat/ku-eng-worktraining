import os
import sys

def bootstrap(path=''):
    sys.path.append(os.getcwd())
    sys.path.append(os.path.join(os.getcwd(), '..'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "worktraining.settings")

    import django
    django.setup()

