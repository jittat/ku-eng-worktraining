import sys
import json

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from django.forms.models import model_to_dict
from regis.models import Company, Student, Status, Assignment
from django.core.serializers.json import DjangoJSONEncoder

def main():
    id_filename = sys.argv[1]
    idset = set([l.strip() for l in open(id_filename).readlines()])
    year = 2564

    all_statuses = []
    
    for student in Student.objects.all():
        if student.student_id not in idset:
            continue
        statuses = student.status.filter(year=year).all()
        if len(statuses)==0:
            continue
        print(student.student_id, student, student.department, student.email)
        print(student.request_set.all(), student.company_direct_applications.all())
        sdict = model_to_dict(statuses[0])
        del sdict['student']
        del sdict['id']
        sdict['student'] = {
            'student_id': student.student_id,
            'email': student.email,
            'tel_no': student.tel_no,
            'email_confirmation': model_to_dict(student.email_confirmation),
        }
        all_statuses.append(sdict)
    
    print(json.dumps(all_statuses, indent=1, cls=DjangoJSONEncoder))

if __name__ == '__main__':
    main()

"""
for d in data:
    s = d['student']
    dd = copy.copy(d)
    del dd['student']
    student = Student.objects.get(student_id=s['student_id'])
    #student.email = s['email']
    #student.tel_no = s['tel_no']
    #student.save()
    #st = Status()
    #for f in dd:
    #    setattr(st, f, dd[f])
    #st.student = student
    #st.save()
    confirmation = EmailConfirmation.create_or_update_for(student)
    EmailConfirmation.confirm_using(confirmation.code)
    

    
"""
