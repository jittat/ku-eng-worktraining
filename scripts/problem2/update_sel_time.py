import sys
import os
from datetime import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentSelection

import codecs

file_name = 'selection-time.txt'
round_number = 6

f = codecs.open(file_name,encoding='utf-8')
lines = f.readlines()

for l in lines:
    items = l.strip().split(' ')
    if len(items)!=7:
        print 'Error: ' + l
        continue
    std_id = items[2]
    sel_time_str = items[5] + ' ' + items[6]
    sel_time = datetime.strptime(sel_time_str,"%Y-%m-%d %H:%M:%S")
    
    selections = StudentSelection.objects.filter(student__student_id__exact=std_id,
                                                 round_number=round_number)
    if len(selections)==1:
        selection = selections[0]
        selection.selection_at = sel_time
        selection.save()
        print 'updated: ' + std_id + ' to ' + str(sel_time)
    else:
        print len(selections), std_id

