import json
import re

DEPARTMENTS = [
    "202",
    "203",
    "204",
    "205",
    "206",
    "208",
    "210",
    "211",
    "213",
    "215",
    "217",
    "218",
    "219",
    "220",
    "221",
    ]

START_TIME = u'2012-01-22T11:00:00+0000'
END_TIME = u'2012-01-22T14:01:00+0000'

def parse_message(m):
    res = re.match(r'\s*(\d+)\s+(\d+)\s*',m,re.MULTILINE)
    if res:
        return (res.group(1),res.group(2))
    else:
        return None

def convert(comment):
    r = parse_message(comment['message'])
    if r:
        return {'created_time': comment['created_time'],
                'id': comment['id'],
                'sel_id': r[0],
                'receipt_id': r[1],
                'user_id': comment['from']['id']}
    else:
        return None

def output(fout,comments):
    for c in comments:
        print >> fout, "%s,%s,%s,%s"  % (c['created_time'],
                                         c['id'],
                                         c['sel_id'],
                                         c['receipt_id'])

def convert_file(fname):
    data = json.loads(open(fname).read())
    comments = data['data']
    print fname, ": ", len(comments),'comments'

    fout = open(fname+'.out','w')
    
    cc = {}
    
    for c in comments:
        cc[(c['created_time'],c['id'])] = c

    start_key = (START_TIME,'0')
    end_key = (END_TIME,'0')

    output_comments = []
    sel_ids = set()

    for k in sorted(cc.keys()):
        if k<start_key:
            print "Too early: ", cc[k]['from']['name'], cc[k]['created_time'], cc[k]['message']
        #elif k>=end_key:
        #    print "Too late: ", cc[k]['from']['name'], cc[k]['created_time'], cc[k]['message']
        else:
            c = convert(cc[k])                        
            if c:
                if c['sel_id'] not in sel_ids:
                    output_comments.append(c)
                    sel_ids.add(c['sel_id'])
                else:
                    print 'Dup', c
            

    output(fout,output_comments)

    print '--------------------------------'

    fout.close()

def main():
    for d in DEPARTMENTS:
    #for d in ['211']:
        fname = d + ".html-comments.json"
        convert_file(fname)


if __name__ == '__main__':
    main()
