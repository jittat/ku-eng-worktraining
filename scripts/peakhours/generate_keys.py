import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from random import randint

from regis.models import Student
from std.models import SelectionRound
from peakhours.models import PeakhourKey

try:
    from peakconf import ROUND_NUMBER, WORKTRAINING_YEAR
except:
    print "Config not found.  You need peakconf.py."
    exit()

def random_key():
    return 1000000000 + randint(1,8999999999)


def generate():
    year = WORKTRAINING_YEAR
    round_number = ROUND_NUMBER

    selection_round = SelectionRound.get_round(round_number,year)

    students = Student.all_registered_for_year(year)
    
    all_keys = {}

    for s in students:
        k = random_key()
        while k in all_keys:
            k = random_key()

        all_keys[k] = True

        print "%s,%s" % (s.student_id, k)

        p = PeakhourKey(student=s,
                        selection_round=selection_round,
                        code=k)
        p.save()

if __name__ == '__main__':
    generate()
