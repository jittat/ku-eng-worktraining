# -*- coding: utf-8 -*-
import sys
import os
import datetime
import codecs

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Student, Department
from std.models import SelectionRound
from peakhours.models import PeakhourKey


TEMPLATE = u"""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="th">
<head>
  <title>ชั่วโมงเร่งด่วน | ระบบการฝึกงานภาคฤดูร้อน คณะวิศวกรรมศาสตร์ เกษตรศาสตร์</title>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="/peak/css/base.css" />
  <script type="text/javascript" src="/peak/js/jquery.min.js"></script>
  <meta property="fb:admins" content="645600795"/>
  <meta property="og:url" content="http://wt.eng.ku.ac.th/peak/r%(round_number)d/%(dept_number)s.html"/>
  <meta property="og:title" content="การเลือกในชั่วโมงเร่งด่วน %(dept_name)s รอบที่ %(round_number)d"/>
  <meta property="og:locale" content="th_TH"/>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<h1>การเลือกสถานประกอบการในชั่วโมงเร่งด่วน: สำหรับ%(dept_name)s</h1>
<p>
อย่าลืมอ่านขั้นตอนและกติกาก่อนลงมือเลือก (<a href="/">อ่านก่อน</a>)
</p>

<div class="warnings">
<h2>คำเตือน (<a href="/">อ่านกฎทั้งหมด</a>)</h2>
<ul>
<li>จะพิจารณาคอมเมนท์ที่เฟสบุ๊คแสดงเวลาว่าอยู่ในช่วงของการเลือกเร่งด่วนเท่านั้น</li>
<li>จะไม่พิจารณาคอมเมนท์ของผู้ใช้ที่มีลักษณะดังนี้: (1) คอมเมนท์ก่อนเวลาเลือก  (2) คอมเมนท์หลายครั้ง  (3) คอมเมนท์หลังการหมดเวลาเลือกเป็นเวลานานกว่า 10 นาที</li>
<li>จะไม่พิจารณาคอมเมนท์ที่อยู่ผิดหน้าการเลือกสำหรับสาขาวิชาของนิสิต</li>
</ul>
</div>

<p>
<b>รายการสถานประกอบการสำหรับ%(dept_name)s:</b> <a href="/peak/r%(round_number)d/%(dept_number)s-companies.html">ดูรายการ</a><br/>
<b>ขั้นตอนการเลือก:</b> ให้พิมพ์คอมเมนท์ในช่องด้านล่าง ในรูปแบบ: 
<center>รหัสส่วนตัว รหัสสถานประกอบการ</center>
เช่น 1234567890 12123 ห้ามพิมพ์ข้อความอื่น<br/>
<b>หมายเหตุ:</b> ต้องใช้รหัสสถานประกอบการที่ดูจากหน้ารายการของภาควิชาคุณเท่านั้น
</p>

<p>
ด้านล่างจะเป็นช่องสำหรับพิมพ์คอมเมนท์เพื่อเลือก คุณจะต้องจัดหาบัญชีผู้ใช้ facebook มาเพื่อที่จะคอมเมนท์  ถ้าคุณคอมเมนท์เพื่อเลือกในนี้แล้ว ไม่จำเป็นต้องเข้าไปเลือกในระบบทั่วไปอีก  ข้อมูลการเลือกจะถูกนำเข้าภายในวันที่ %(importing_date)s (ไม่ต้องรีบเข้าระบบไปตรวจสอบในวันนี้)
</p>

<div class="fb-comments" data-href="http://wt.eng.ku.ac.th/peak/r%(round_number)d/%(dept_number)s.html" data-num-posts="20" data-width="500"></div>

</body>
</html>
"""

try:
    from peakconf import ROUND_NUMBER, WORKTRAINING_YEAR, IMPORTING_DATE
except:
    print "Config not found.  You need peakconf.py."
    exit()

def dump(department):
    content = (TEMPLATE %
           {'round_number': ROUND_NUMBER,
            'dept_number': department.code,
            'dept_name': department.name,
            'importing_date': IMPORTING_DATE })
    filename = "r%d/%s.html" % (ROUND_NUMBER, department.code)
    f = codecs.open(filename,"w",encoding="utf8")
    f.write(content)
    f.close()

def main():
    dept_number = sys.argv[1]
    department = Department.objects.get(code=dept_number)
    print department
    dump(department)

if __name__ == '__main__':
    main()
