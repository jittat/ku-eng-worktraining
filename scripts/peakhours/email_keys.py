import sys
import os
import datetime

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from random import randint

from regis.models import Student
from std.models import SelectionRound
from peakhours.models import PeakhourKey
from commons.email import send_peakhour_key

try:
    from peakconf import ROUND_NUMBER, WORKTRAINING_YEAR
except:
    print "Config not found.  You need peakconf.py."
    exit()

def generate():
    year = WORKTRAINING_YEAR
    round_number = ROUND_NUMBER

    selection_round = SelectionRound.get_round(round_number,year)

    students = Student.all_registered_for_year(year)
    
    for s in students:
        k = PeakhourKey.objects.get(student=s,
                                    selection_round=selection_round)
        send_peakhour_key(s,
                          selection_round,
                          k)

if __name__ == '__main__':
    generate()
