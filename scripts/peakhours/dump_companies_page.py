# -*- coding: utf-8 -*-
import sys
import os
import datetime
import codecs

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from django.template import Template, Context

from regis.models import Student, Department, DepartmentNameInRequestLetter, Receipt
from std.models import SelectionRound
from peakhours.models import PeakhourKey


TEMPLATE = u"""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="th">
<head>
  <title>ชั่วโมงเร่งด่วน | ระบบการฝึกงานภาคฤดูร้อน คณะวิศวกรรมศาสตร์ เกษตรศาสตร์</title>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
  <link rel="stylesheet" type="text/css" href="/peak/css/base.css" />
  <script type="text/javascript" src="/peak/js/jquery.min.js"></script>
</head>
<body>
<h1>รายการสถานประกอบการ การเลือกรอบที่ {{ round_number }} สำหรับ{{ dept_name}}</h1>
<p>
<div style="float:right">
[<a href="{{ dept_number }}.html">กลับไปหน้าการเลือกของภาควิชา</a>]
</div>
<b>รหัสสถานประกอบการนี้ ใช้สำหรับการเลือกในรอบที่ {{ round_number }} เท่านั้น</b><br/>
อย่าลืมอ่านขั้นตอนและกติกาก่อนลงมือเลือก (<a href="/">อ่านก่อน</a>)
</p>

<table class="company-list" width="100%">
<tr>
<th>ลำดับ</th>
<th>สถานประกอบการ</th>
<th>จำนวน</th>
<th>รหัสสถานประกอบการ<br/>สำหรับใช้เลือก</th>
</tr>
{% for receipt in receipts %}
<tr class="{% cycle "odd" "even" %}">
<td>{{ forloop.counter }}</td>
<td>
  <b>{{ receipt.company }}</b>  <button class="info-toggle">ดูรายละเอียด</button><br/>

<div class="receipt-info" style="display: none">
รับทั้งหมด {{ receipt.total_student }} คน&nbsp;&nbsp;
อนุมัติรอบก่อนแล้ว {{ receipt.num_accepted_students }} คน&nbsp;&nbsp;
รับรอบนี้ {{ receipt.num_available_positions }} คน<br/>

  <b>สถานที่ฝึกงาน</b><br/>
{{ receipt.work_address }}
<br/>
<b>ลักษณะงาน:</b> {{ receipt.work_description|default:"-" }}
<br/><br/>
<b>ระยะเวลา:</b>
{{ receipt.work_detail.beginning_date|date:"j N" }} -
{{ receipt.work_detail.end_date|date:"j N" }}
<br/>
{% if receipt.work_detail.dressing_description %}
<b>การแต่งกาย:</b> {{ receipt.work_detail.dressing_description }}<br/>
{% endif %}
{% if receipt.work_detail.other_description %}
<b>รายละเอียดอื่น ๆ :</b> {{ receipt.work_detail.other_description }}<br/>
{% endif %}
</div>
</td>
<td style="text-align: center">
  {{ receipt.num_available_positions }}
</td>
<td class="peak-code">{{ receipt.peak_code }}</td>
</tr>
{% endfor %}
</table>

<script type="text/javascript">
$(function(){
  $(".info-toggle").click(function(){
    $(this).parents("td").find(".receipt-info").slideToggle();
    return false;
  });
});
</script>

</body>
</html>
"""

try:
    from peakconf import ROUND_NUMBER, WORKTRAINING_YEAR, IMPORTING_DATE
except:
    print "Config not found.  You need peakconf.py."
    exit()


def dump(department):
    selection_round = SelectionRound.get_round(ROUND_NUMBER, WORKTRAINING_YEAR)
    actual_department = DepartmentNameInRequestLetter.to_requested_department(department)

    all_receipts = (Receipt.all_for_round(selection_round, 
                                          department=actual_department)                            .select_related())

    receipts = [r for r in all_receipts 
                if r.is_shown_to_students()]

    for r in receipts:
        r.peak_code = str(r.id * 10000 + int(department.code)*10 + ROUND_NUMBER)

    template = Template(TEMPLATE)
    content = template.render(
        Context({'round_number': ROUND_NUMBER,
                 'dept_number': department.code,
                 'dept_name': department.name,
                 'receipts': receipts }))
    filename = "r%d/%s-companies.html" % (ROUND_NUMBER, department.code)

    f = codecs.open(filename,"w",encoding="utf8")
    f.write(content)
    f.close()

def main():
    dept_number = sys.argv[1]
    department = Department.objects.get(code=dept_number)
    print department
    dump(department)

if __name__ == '__main__':
    main()
