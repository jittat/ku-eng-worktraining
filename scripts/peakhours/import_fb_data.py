# -*- coding: utf-8 -*-
import sys
import os
import datetime
import codecs

path = os.path.dirname(__file__)
parent_path = os.path.abspath(os.path.join(path, '..'))
sys.path.append(parent_path)

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from django.template import Template, Context

from regis.models import Student, Department, DepartmentNameInRequestLetter, Receipt, Receipt
from std.models import SelectionRound, StudentSelection
from peakhours.models import PeakhourKey
from commons.email import send_selection_confirmation
from commons.models import Log

try:
    from peakconf import ROUND_NUMBER, WORKTRAINING_YEAR
except:
    print "Config not found.  You need peakconf.py."
    exit()

DEPARTMENTS = [
    "202",
    "203",
    "204",
    "205",
    "206",
    "208",
    "210",
    "211",
    "213",
    "215",
    "217",
    "218",
    "219",
    "220",
    "221",
    ]

def get_student_with_sel_code(code, selection_round):
    try:
        k = PeakhourKey.objects.get(code=code, selection_round=selection_round)
    except:
        print 'Std key not found: ', code
        return None
    return k.student

def get_receipt_with_receipt_code(code, selection_round):
    cint = int(code)
    rid = cint / 10000
    round_number = cint % 10
    dept_number = (cint % 10000) / 10
    if selection_round.number != round_number:
        return None

    try:
        receipt = Receipt.objects.get(pk=rid)
    except Receipt.DoesNotExist:
        print 'Rid: ', rid
        return None

    real_dept =  DepartmentNameInRequestLetter.to_requested_department(Department.objects.get(code=dept_number))
    
    if real_dept != receipt.department:
        print 'R dept_id', receipt.department.code
        return None

    return receipt

def parse_time(s):
    utf_time = datetime.datetime.strptime(s,"%Y-%m-%dT%H:%M:%S+0000")
    return utf_time + datetime.timedelta(0,60*60*7)

def validate_and_create_selection(items, selection_round, department):
    raw_selection_time = items[0]
    student = get_student_with_sel_code(items[2], selection_round)

    if not student:
        return None

    if not student.is_eligible_to_apply_in_selection_round():
        print 'Std not allowed: ', student.student_id, items[2], student.reasons_failed_to_apply_in_selection_round()
        return None

    if student.department != department:
        print 'Std department does not match', student.student_id
        return None

    receipt = get_receipt_with_receipt_code(items[3], selection_round)

    if not receipt:
        print 'Receipt not found: ', items[3], ' std: ', student.student_id
        return None

    department = DepartmentNameInRequestLetter.to_requested_department(student.department)
    if receipt.department != department:
        print 'Dep not match: ', items[3], ' std: ', student.student_id
        return None

    if not receipt.available:
        print 'Receipt full: ', items[3], ' std: ', student.student_id, ' Rec: ', receipt.id
        return None

    selection = StudentSelection.get_for_student(student,
                                                 selection_round.number,
                                                 selection_round.year)
    
    if not selection:
        selection = StudentSelection(student=student,
                                     round_number=selection_round.number,
                                     year=selection_round.year)
    else:
        print "Error: old selection exists: ", student.student_id
        return None
        
    selection.selection_at = parse_time(raw_selection_time)
    selection.company = receipt.company

    return selection

def process_comments(fname, department):
    selection_round = SelectionRound.get_round(ROUND_NUMBER, WORKTRAINING_YEAR)
    lines = open(fname).readlines()

    prev_time = ''
    rank = 0
    for l in lines:
        items = l.strip().split(',')
        if len(items)==4:
            raw_selection_time = items[0]

            selection = validate_and_create_selection(items, selection_round, department)

            if not selection:
                continue

            if raw_selection_time==prev_time:
                rank += 1
            else:
                rank = 0

            selection.extra_ranking = rank
            selection.is_imported = True
            selection.save()

            student = selection.student

            Log.create("selection imported - student: %s, company: %d" %
                       (student.student_id,
                        selection.company.id),
                       student=student,
                       user='admin-batch')

            send_selection_confirmation(selection.student, 
                                        selection, 
                                        selection_round.number)

        elif len(items)!=0:
            print 'Format error: ',l


def main():
    for d in DEPARTMENTS:
        fname = d + ".html-comments.json.out"

        department = Department.objects.get(code=d)

        print 'Department: ', d, department

        process_comments(fname,department)

if __name__ == '__main__':
    main()
