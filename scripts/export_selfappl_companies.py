# -*- coding: utf-8 -*-
from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Status, Assignment
from std.models import CompanyRequest, CompanyDirectApplication

def main():
    #assignments = Assignment.objects.all()
    assignments = Assignment.objects.filter(year=2561).all()
    for assignment in assignments:
        if assignment.company_id != None:
            continue

        if assignment.company_request_id != None:
            request = assignment.company_request

            items = [
                request.id,
                '1.1',
                request.company_name,
                request.contact_person,
                request.contact_tel_no,
                request.contact_email,
                assignment.year
            ]
        elif assignment.company_direct_application_id != None:
            application = assignment.company_direct_application
            detail = application.detail
            items = [
                application.id,
                '1.2',
                application.company_name,
                detail.contact_person,
                detail.contact_tel_no,
                detail.contact_email,
                assignment.year
            ]

            
        print u",".join([u'"' + unicode(i) + u'"' for i in items])

if __name__=='__main__':
    main()



