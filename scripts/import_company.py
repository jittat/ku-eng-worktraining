import sys

if len(sys.argv)!=3:
    print "Usage: python import_company.py [company_file_name] [year_for_detail]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, WorkDetail

import codecs
import csv

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


filename = sys.argv[1]
detail_year = int(sys.argv[2])

f = codecs.open(filename,'r',encoding='utf-8')

for line in unicode_csv_reader(f):
    short_name = line[0]
    name = line[1]
    raw_addr = [l for l in [line[2],line[3],line[4]]
                if l != '']
    address = '\n'.join(raw_addr)
    contact = line[5]

    org_companies = Company.objects.filter(short_name=short_name).all()
    if len(org_companies)==0:
        c = Company()
    else:
        c = org_companies[0]

    c.short_name = short_name
    c.name = name
    c.address = address

    c.save()

    org_detail = c.workdetail_for_year(detail_year)
    if not org_detail:
        org_detail = WorkDetail(company=c)

    org_detail.year = detail_year
    org_detail.contact_name = contact
    org_detail.signer_name = contact
    org_detail.save()

    print "Imported:", c.short_name
