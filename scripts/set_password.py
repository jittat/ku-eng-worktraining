import sys

if len(sys.argv)!=3:
    print "Usage: python set_password.py [student_id] [password]"
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from std.models import StudentExtraPassword

student_id = sys.argv[1]
password = sys.argv[2]

StudentExtraPassword.set_password_for(student_id, password)
