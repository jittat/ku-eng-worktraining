import sys
if len(sys.argv)!=2:
    print """usage: python [data_file]"
    Data file should be in the csv format with the following columns:
    """
    quit()

from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Student, Grade

lines = open(sys.argv[1]).readlines()
ostdid = ''
ostudent = None
for l in lines:
    items = l.strip().split(',')
    if items[0].isdigit():
        if len(items)==14:
            student_id = items[0]
            course_number = items[10]
            #course_name = ""
            credit = int(items[12])
            grade = items[13]
            regis_type = items[11]   
            
            if ostdid == student_id:
                student = ostudent
            else:
                students = Student.objects.filter(student_id=student_id)
                if len(students)!=0:
                    student = students[0]
                    ostdid = student_id
                    ostudent = student
                else:
                    student = None
            
            if student:
                grades = Grade()    
                grades.regis_type = regis_type
                grades.course_number = course_number 
                #grades.course_name = course_name
                grades.grade = grade
                grades.credit = credit
                grades.student_id = student.id
                grades.save()
                print student.student_id
                
            else:
                print "filter failed"
                
        else:
            print "itemlen != 5"
