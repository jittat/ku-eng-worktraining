from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company

for c in Company.objects.all():
    c.update_starting_char()
