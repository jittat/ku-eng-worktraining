import sys

if len(sys.argv)!=2:
    print """usage: python import_education_info.py data_file"

Data file should be in the csv format with the following columns:
   student_id,credit,gpax"""
    quit()


from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Student, Education

LAST_ACADEMIC_YEAR = 2552
LAST_SEMESTER = 2

lines = open(sys.argv[1]).readlines()
for l in lines:
    items = l.strip().split(',')
    if len(items)==3:
        student_id = items[0]
        credit = int(items[1])
        gpax = float(items[2])

        students = Student.objects.filter(student_id=student_id)
        if len(students)!=0:
            student = students[0]

            if student.education!=None:
                edu = student.education
            else:
                edu = Education()
            
            edu.total_credit = credit
            edu.gpax = gpax
            edu.upto_year = LAST_ACADEMIC_YEAR
            edu.upto_semester = LAST_SEMESTER
            edu.save()

            student.education = edu
            student.save()

            print student.student_id
