from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from commons.models import Configuration

Configuration.init_config_variables()

