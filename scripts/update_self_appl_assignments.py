from django.conf import settings
from django_bootstrap import bootstrap
bootstrap(__file__)

from regis.models import Company, Student, Status, Assignment

def main():
    year = 2563
    statuses = Status.objects.filter(year=year,
                                     is_registered=True,
                                     registration_type=Status.OWN_SELECTION_TYPE).select_related('student').all()

    for status in statuses:
        student = status.student
        info = student.get_working_self_appl_info(year)
        
        if info['company_request']==None and info['direct_application']==None:
            print 'No assignment:',student.student_id, student, student.assignment_for_year(year)
            continue
        elif info['company_request'] and info['direct_application']:
            print 'Too many assignment:',student.student_id, student, student.assignment_for_year(year)
            continue

        if student.assignment_for_year(year)!=None:
            print 'ALREADY ASSIGNED:', student.student_id, student, student.assignment_for_year(year)
            continue
        assignment = Assignment(student=student,
                                year=year)
        if info['company_request']:
            assignment.company_request = info['company_request']
        else:
            assignment.company_direct_application = info['direct_application']
        assignment.save()
        

if __name__=='__main__':
    main()



